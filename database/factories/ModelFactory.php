<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(PM\Models\User::class, function (Faker\Generator $faker) {
    return [
        'id' => rand(1, 100000),
        'firstname' => 'Duncan  ',
        'lastname' => 'Kinuthia',
        'email' => 'dkinuthia@cytonn.com',
        'phone_number' => '07000104993',
        'department_id' => 2,
        'role_id' => 1,
        'password' => 'password',
        'active' => 1
    ];
});

$factory->define(PM\Models\Department::class, function (Faker\Generator $faker){
    return[

    ];
});

