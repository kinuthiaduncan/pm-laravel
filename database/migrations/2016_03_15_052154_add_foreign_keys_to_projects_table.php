<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->foreign('project_type', 'fk_projects_project_types1')->references('id')->on('project_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('created_by', 'fk_projects_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('project_lead', 'fk_projects_users2')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->dropForeign('fk_projects_project_types1');
			$table->dropForeign('fk_projects_users1');
			$table->dropForeign('fk_projects_users2');
		});
	}

}
