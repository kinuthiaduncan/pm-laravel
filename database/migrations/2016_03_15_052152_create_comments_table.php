<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->longText('name');
			$table->string('image')->nullable();
			$table->string('filename')->nullable();
			$table->string('file')->nullable();
			$table->integer('issue_id')->index('fk_comments_issues1_idx');
			$table->integer('created_by')->index('fk_comments_users1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
