<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIssuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('issues', function(Blueprint $table)
		{
			$table->foreign('priority_id', 'fk_issues_issue_priorities1')->references('id')->on('priorities')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('issue_type', 'fk_issues_issue_types1')->references('id')->on('issue_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('project_id', 'fk_issues_projects1')->references('id')->on('projects')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('status_id', 'fk_issues_status1')->references('id')->on('status')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('created_by', 'fk_issues_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('assigned_to', 'fk_issues_users2')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('issues', function(Blueprint $table)
		{
			$table->dropForeign('fk_issues_issue_priorities1');
			$table->dropForeign('fk_issues_issue_types1');
			$table->dropForeign('fk_issues_projects1');
			$table->dropForeign('fk_issues_status1');
			$table->dropForeign('fk_issues_users1');
			$table->dropForeign('fk_issues_users2');
		});
	}

}
