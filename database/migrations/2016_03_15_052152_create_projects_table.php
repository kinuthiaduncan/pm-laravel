<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->integer('project_type')->nullable()->index('fk_projects_project_types1_idx');
			$table->string('project_key', 45)->unique('project_key_UNIQUE');
			$table->string('project_description');
			$table->string('project_url', 45)->unique('project_url_UNIQUE');
			$table->integer('project_lead')->index('fk_projects_users2_idx');
			$table->integer('created_by')->index('fk_projects_users1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
