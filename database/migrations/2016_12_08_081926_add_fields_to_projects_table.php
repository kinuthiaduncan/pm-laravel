<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('status')->nullable();
            $table->string('priority')->nullable();
            $table->text('resources_required')->nullable();
            $table->text('resources_assigned')->nullable();
            $table->foreign('category_id', 'fk_projects_categories')
                ->references('id')
                ->on('project_categories')
                ->onUpdate('cascade')
                ->onDelete('NO ACTION');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('category_id');
            $table->dropColumn('status');
            $table->dropColumn('priority');
            $table->dropColumn('resources_required');
            $table->dropColumn('resources_assigned');
            $table->dropForeign('fk_projects_categories');
        });
    }
}
