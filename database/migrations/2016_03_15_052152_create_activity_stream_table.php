<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityStreamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_stream', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('activity_description');
			$table->string('path');
			$table->integer('project_id')->index('fk_activity_stream_projects1_idx');
			$table->integer('activity_by')->index('fk_activity_stream_users1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_stream');
	}

}
