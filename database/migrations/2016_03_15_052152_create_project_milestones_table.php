<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectMilestonesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_milestones', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id')->index('fk_project_milestones_projects1_idx');
			$table->string('title');
			$table->string('description');
			$table->date('due_date');
			$table->integer('created_by')->index('fk_project_milestones_users1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_milestones');
	}

}
