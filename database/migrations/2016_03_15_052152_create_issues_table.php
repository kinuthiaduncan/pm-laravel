<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIssuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('issues', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id')->index('fk_issues_projects1_idx');
			$table->string('title');
			$table->longText('description');
			$table->string('image')->nullable();
			$table->integer('status_id')->index('fk_issues_status1_idx');
			$table->integer('parent_id')->nullable();
			$table->integer('assigned_to')->index('fk_issues_users2_idx');
			$table->integer('issue_type')->index('fk_issues_issue_types1_idx');
			$table->integer('priority_id')->index('fk_issues_issue_priorities1_idx');
			$table->integer('percentage_done');
			$table->date('date_due');
			$table->date('status_change_date');
			$table->integer('created_by')->index('fk_issues_users1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('issues');
	}

}
