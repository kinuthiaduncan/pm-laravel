<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponentIssuesRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issues', function (Blueprint $table)
        {
            $table->integer('component_id')->nullable();
//            $table->foreign('component_id', 'fk_issues_components')
//                ->references('id')
//                ->on('project_components')
//                ->onUpdate('NO ACTION')
//                ->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issues', function (Blueprint $table)
        {
            $table->dropForeign('fk_issues_components');
            $table->dropColumn('component_id');
        });
    }
}
