<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_components', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('project_id');
            $table->string('component_name');
            $table->timestamps();

            $table->foreign('project_id', 'fk_project_components_projects_idx')->references('id')->on('projects')->onUpdate('NO ACTION')->onDelete('NO ACTION');

        });
    }

//->index('fk_project_components_projects_idx')
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_components');
    }
}
