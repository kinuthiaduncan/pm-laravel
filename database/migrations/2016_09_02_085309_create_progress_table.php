<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_progress', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('added_by');
            $table->text('content');
            $table->timestamps();

            $table->foreign('project_id', 'fk_project_progress_projects_idx')
                ->references('id')
                ->on('projects')
                ->onUpdate('NO ACTION')
                ->onDelete('NO ACTION');
            $table->foreign('added_by', 'fk_project_progress_users_idx')
                ->references('id')
                ->on('users')
                ->onUpdate('NO ACTION')
                ->onDelete('NO ACTION');
        });
    }

//->index('fk_project_progress_projects_idx')
//->index('fk_project_progress_users_idx')

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_progress');
    }
}
