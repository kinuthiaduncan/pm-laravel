<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectMilestonesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_milestones', function(Blueprint $table)
		{
			$table->foreign('project_id', 'fk_project_milestones_projects1')->references('id')->on('projects')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('created_by', 'fk_project_milestones_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_milestones', function(Blueprint $table)
		{
			$table->dropForeign('fk_project_milestones_projects1');
			$table->dropForeign('fk_project_milestones_users1');
		});
	}

}
