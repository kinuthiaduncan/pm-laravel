<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserCategoryRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_categories', function (Blueprint $table) {
            $table->integer('created_by')->nullable();
            $table->integer('edited_by')->nullable();
            $table->foreign('created_by', 'fk_project_categories_users')
                ->references('id')
                ->on('users')
                ->onUpdate('NO ACTION')
                ->onDelete('NO ACTION');
            $table->foreign('edited_by', 'fk_project_categories_users1')
                ->references('id')
                ->on('users')
                ->onUpdate('NO ACTION')
                ->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_categories', function (Blueprint $table) {
            $table->dropColumn('created_by');
            $table->dropColumn('edited_by');
            $table->dropForeign('fk_project_categories_users');
            $table->dropForeign('fk_project_categories_users1');
        });
    }
}
