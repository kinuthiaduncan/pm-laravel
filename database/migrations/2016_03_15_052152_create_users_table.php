<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('firstname', 100);
			$table->string('lastname', 100);
			$table->string('email', 100);
			$table->integer('role_id')->nullable()->index('fk_users_access_level1_idx');
			$table->integer('group_id')->nullable()->index('fk_users_groups1_idx');
			$table->string('google_id')->nullable();
			$table->string('avatar')->nullable();
			$table->string('remember_token')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
