<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityStreamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_stream', function(Blueprint $table)
		{
			$table->foreign('project_id', 'fk_activity_stream_projects1')->references('id')->on('projects')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('activity_by', 'fk_activity_stream_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_stream', function(Blueprint $table)
		{
			$table->dropForeign('fk_activity_stream_projects1');
			$table->dropForeign('fk_activity_stream_users1');
		});
	}

}
