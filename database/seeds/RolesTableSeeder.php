<?php

use PM\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'role' => 'Super Administrator'
        ]);

        Role::create([
            'role' => 'Administrator'
        ]);

        Role::create([
            'role' => 'Staff'
        ]);

        Role::create([
            'role' => 'Restricted'
        ]);
    }
}