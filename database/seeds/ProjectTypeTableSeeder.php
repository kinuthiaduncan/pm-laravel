<?php

    use PM\Models\ProjectType;
    use Illuminate\Database\Seeder;

    class ProjectTypeTableSeeder extends Seeder {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            ProjectType::create([
                'name' => 'General'
            ]);

            ProjectType::create([
                'name' => 'Software'
            ]);

        }
    }