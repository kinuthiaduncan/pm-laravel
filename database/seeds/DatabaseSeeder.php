<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

    class DatabaseSeeder extends Seeder
    {
        protected $tables = [
            'roles',
            'groups',
            'priorities',
            'issue_types',
            'project_types',
            'status'
        ];

        protected $seeders = [
            'RolesTableSeeder',
            'GroupsTableSeeder',
            'PriorityTableSeeder',
            'IssueTypeTableSeeder',
            'ProjectTypeTableSeeder',
            'StatusTableSeeder'
        ];

        /**
         * Run the database seeds.
         * @return void
         */
        public function run()
        {
            Model::unguard();

           // $this->cleanDatabase();

            foreach($this->seeders as $seeder)
            {
                $this->call($seeder);
            }

            Model::reguard();
        }

        private function cleanDatabase()
        {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');

            foreach($this->tables as $table)
            {
                DB::table($table)->truncate();
            }

            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
    }
