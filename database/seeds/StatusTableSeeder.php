<?php

    use PM\Models\Status;
    use Illuminate\Database\Seeder;

    class StatusTableSeeder extends Seeder {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            Status::create([
                'name'=>'To Do'
            ]);

            Status::create([
                'name'=>'In Progress'
            ]);

            Status::create([
                'name'=>'In Review'
            ]);

            Status::create([
                'name'=>'Done'
            ]);

            Status::create([
                'name'=>'Released'
            ]);

            Status::create([
                'name'=>'Cancelled'
            ]);
        }
    }