<?php
    use Illuminate\Database\Seeder;
    use PM\Models\Priority;

    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */
    class PriorityTableSeeder extends Seeder {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            Priority::create([
                'name'=>'Highest'
            ]);

            Priority::create([
                'name'=>'High'
            ]);

            Priority::create([
                'name'=>'Medium'
            ]);

            Priority::create([
                'name'=>'Low'
            ]);

            Priority::create([
                'name'=>'Lowest'
            ]);
        }
    }