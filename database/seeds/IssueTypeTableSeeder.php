<?php

    use PM\Models\IssueType;
    use Illuminate\Database\Seeder;

    class IssueTypeTableSeeder extends Seeder {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            IssueType::create([
                'name' => 'Bug'
            ]);

            IssueType::create([
                'name' => 'Task'
            ]);

            IssueType::create([
                'name' => 'Improvement'
            ]);

            IssueType::create([
                'name' => 'New Feature'
            ]);

            IssueType::create([
                'name' => 'Epic'
            ]);
        }
    }