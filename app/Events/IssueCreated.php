<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use PM\Models\Issue;
use PM\Models\User;

class IssueCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Issue
     */
    public $issue;
    /**
     * @var User
     */
    public $creator;

    /**
     * Create a new event instance.
     *
     * @param Issue $issue
     * @param User $creator
     */
    public function __construct(Issue $issue, User $creator)
    {
        $this->issue = $issue;
        $this->creator = $creator;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
