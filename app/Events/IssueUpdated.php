<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use PM\Models\Issue;
use PM\Models\User;

class IssueUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Issue
     */
    public $issue;
    /**
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param Issue $issue
     * @param User $user
     */
    public function __construct(Issue $issue, User $user)
    {
        $this->issue = $issue;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
