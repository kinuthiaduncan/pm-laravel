<?php

namespace App\Exceptions;

use Exception;
use Rollbar;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        $this->rollbar($e);

        return parent::report($e);
    }

    public function rollbar(Exception $e)
    {
        $user = ['id'=>null, 'email'=>'email@guest.net'];

        try{
            $user = \Auth::user();
            $user = ['id'=>$user->id, 'username'=>$user->username, 'email'=>$user->email];
        }catch (\Exception $exception){}

        $config = array(
            // required
            'access_token' => getenv('ROLLBAR_ACCESS_TOKEN'),
            // optional - environment name. any string will do.
            'environment' => env('APP_ENV'),
            'person'=>$user,
            // optional - path to directory your code is in. used for linking stack traces.
            'root' => base_path()
        );

        Rollbar::init($config);

        if(env('APP_ENV') == 'production')
        {
            Rollbar::report_exception($e);
        }

        if(env('APP_ENV') == 'staging')
        {
            Rollbar::report_exception($e);
        }

    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }
        if($e instanceof \Illuminate\Session\TokenMismatchException)
        {
            return redirect()
                ->back()
                ->withInput($request->except('_token'))
                ->withErrors('Sorry, your request could not be handled. Please try again.');
        }

        if($e instanceof NotFoundHttpException)
        {
            return response()->view('layouts.partials.404', [], 404);
        }

        return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('/');
    }
}
