<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class IssueTimeCreated extends Notification
{
    use Queueable;

    private $issue;
    private $schedule;

    /**
     * Create a new notification instance.
     *
     * @param $issue
     * @param $schedule
     */
    public function __construct($schedule,$issue)
    {
        $this->issue = $issue;
        $this->schedule = $schedule;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Issue Schedule Added on '.$this->issue->title.' by '.Auth::user()->firstname.' '.Auth::user()->lastname)
            ->view(
                'emails.issues.created_schedule', ['issue' => $this->issue,'schedule'=>$this->schedule]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'subject'=>'New Issue Schedule Created',
            'title' => $this->issue->title,
            'url' => '/issue/'.$this->issue->id.'/track',
        ];
    }
}
