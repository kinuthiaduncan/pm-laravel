<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class ProjectPhaseRejected extends Notification implements ShouldQueue
{
    use Queueable;
    private $phase;

    /**
     * Create a new notification instance.
     *
     * @param $phase
     */
    public function __construct($phase)
    {
        $this->phase = $phase;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = Auth::user();
        return (new MailMessage)
            ->subject('Project Phase Rejected by '.$user->preferred_name)
            ->view(
                'emails.projects.phase_rejected', ['data' => $this->phase]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'Project Phase Approved',
            'url' => '/project/'.$this->phase->project->id.'/phases/',
        ];
    }
}
