<?php
    /**
     * Date: 25/04/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Notifications;


    use Illuminate\Bus\Queueable;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use Illuminate\Notifications\Messages\MailMessage;
    use Illuminate\Notifications\Notification;
    use Illuminate\Support\Facades\Auth;

    class TaskCommentCreated extends Notification implements ShouldQueue
    {
        use Queueable;
        /**
         * @var
         */
        private $task;

        /**
         * @var
         */
        private $assigned_users;
        /**
         * @var
         */
        private $comment;

        /**
         * Create a new notification instance.
         *
         * @param $comment
         * @param $task
         * @param $assigned_users
         */
        public function __construct($comment, $task, $assigned_users)
        {
            $this->task = $task;
            $this->assigned_users = $assigned_users;
            $this->comment = $comment;
        }

        /**
         * Get the notification's delivery channels.
         *
         * @param  mixed $notifiable
         * @return array
         */
        public function via($notifiable)
        {
            return ['database', 'mail'];
        }

        /**
         * Get the mail representation of the notification.
         *
         * @param  mixed $notifiable
         * @return \Illuminate\Notifications\Messages\MailMessage
         */
        public function toMail($notifiable)
        {
            $user = Auth::user();
            return (new MailMessage)
                ->subject('New Task Comment Created by '.$user->preferred_name)
                ->view(
                'emails.tasks.created_comment', ['data' => $this->task, 'comment' => $this->comment, 'assigned_users' => $this->assigned_users]
            );
        }

        /**
         * Get the array representation of the notification.
         *
         * @param  mixed $notifiable
         * @return array
         */
        public function toArray($notifiable)
        {
            return [
                'title' => 'New Comment'. ' '. $this->task->title,
                'url' => '/tasks/my-tasks/' . $this->task->id,
            ];
        }
    }