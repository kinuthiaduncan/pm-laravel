<?php
    /**
     * Date: 28/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Notifications;

    use Illuminate\Bus\Queueable;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use Illuminate\Notifications\Messages\MailMessage;
    use Illuminate\Notifications\Notification;
    use Illuminate\Support\Facades\Auth;

    class TaskCompleted extends Notification implements ShouldQueue
    {
        use Queueable;
        /**
         * @var
         */
        private $task;
        /**
         * @var
         */
        private $assigned_users;

        /**
         * Create a new notification instance.
         *
         * @param $task
         * @param $assigned_users
         */
        public function __construct($task, $assigned_users)
        {
            $this->task = $task;
            $this->assigned_users = $assigned_users;
        }

        /**
         * Get the notification's delivery channels.
         *
         * @param  mixed $notifiable
         * @return array
         */
        public function via($notifiable)
        {
            return ['database', 'mail'];
        }

        /**
         * Get the mail representation of the notification.
         *
         * @param  mixed $notifiable
         * @return \Illuminate\Notifications\Messages\MailMessage
         */
        public function toMail($notifiable)
        {
            $user = Auth::user();
            return (new MailMessage)
                ->subject('Task Completed by '.$user->preferred_name)
                ->view(
                'emails.tasks.task_completed', ['data' => $this->task, 'assigned_users' => $this->assigned_users]
            );
        }

        /**
         * Get the array representation of the notification.
         *
         * @param  mixed $notifiable
         * @return array
         */
        public function toArray($notifiable)
        {
            return [
                'subject'=>'Task Completed',
                'title' => $this->task->title,
                'url' => '/tasks/my-tasks/' . $this->task->id,
            ];
        }

    }