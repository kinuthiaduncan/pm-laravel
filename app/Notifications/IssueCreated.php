<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class IssueCreated extends Notification implements ShouldQueue
{
    use Queueable;

    private $issue;

    /**
     * Create a new notification instance.
     *
     * @param $issue
     */
    public function __construct($issue)
    {
        //
        $this->issue = $issue;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = Auth::user();
        return (new MailMessage)
            ->subject('New Issue Created by '.$user->preferred_name)
            ->view(
            'emails.issue', ['data' => $this->issue]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'subject'=>'New Issue Created',
            'title' => $this->issue->title,
            'url' => '/issue/' . $this->issue->id,
        ];
    }
}
