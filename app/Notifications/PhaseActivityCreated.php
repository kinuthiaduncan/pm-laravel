<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class PhaseActivityCreated extends Notification implements ShouldQueue
{
    use Queueable;

    private $activity;
    private $projectPhase;

    /**
     * Create a new notification instance.
     *
     * @param $activity
     * @param $projectPhase
     */
    public function __construct($activity,$projectPhase)
    {
        $this->activity = $activity;
        $this->projectPhase = $projectPhase;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = Auth::user();
        return (new MailMessage)
            ->subject('New Project Phase Activity Created by '.$user->preferred_name)
            ->view(
                'emails.projects.activity_created', ['data' => $this->activity,'phase'=>$this->projectPhase]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title'=>'Project Phase Activity Created',
            'url'=>'project/'.$this->projectPhase->project_id.'/phases/' . $this->projectPhase->id,
        ];
    }
}
