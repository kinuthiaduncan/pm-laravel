<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class ProjectPhaseUpdated extends Notification implements ShouldQueue
{
    use Queueable;

    private $phase;
    private $project;

    /**
     * Create a new notification instance.
     *
     * @param $phase
     * @param $project
     */
    public function __construct($phase,$project)
    {
        $this->phase = $phase;
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = Auth::user();
        return (new MailMessage)
                    ->subject('Project Phase Updated by ' .$user->preferred_name)
                    ->view(
                        'emails.projects.phase_updated', ['data'=>$this->phase]
                    );

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title'=>'Project Phase Updated on '.$this->project->name,
            'url'=>'/project/'.$this->project->id.'/phases/' . $this->phase->id
        ];
    }
}
