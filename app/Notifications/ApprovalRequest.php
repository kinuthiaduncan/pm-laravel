<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;
use PM\Models\TaskComment;

class ApprovalRequest extends Notification implements ShouldQueue
{
    use Queueable;
    private $comment;

    /**
     * Create a new notification instance.
     *
     * @param $comment_id
     */
    public function __construct($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = Auth::user();
        return (new MailMessage)
            ->subject('Task Comment Approval Requested by '.$user->preferred_name)
            ->view(
                'emails.tasks.approval_request', ['data' => $this->comment]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            'subject'=>'New Task Comment Approval Request',
            'title' => $this->comment->task->title,
            'url' => '/tasks/my-tasks/'.$this->comment->task->id,
        ];
    }
}
