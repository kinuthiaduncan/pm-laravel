<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class TaskTrackingCreated extends Notification implements ShouldQueue
{
    use Queueable;

    private $task;

    private $tracking;


    /**
     * TaskTrackingCreated constructor.
     * @param $task
     * @param $tracking
     */
    public function __construct($task, $tracking)
    {
        $this->task = $task;
        $this->tracking = $tracking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = Auth::user();
        return (new MailMessage)
                    ->subject('New Task Schedule Created by '.$user->preferred_name)
            ->view(
                'emails.tasks.created_task_tracking', ['data' => $this->tracking,'task'=>$this->task]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'subject'=>'New Task Schedule Created',
            'title' => $this->tracking->task->title,

            'url' => '/tasks/task/'. $this->task->id.'/track',
        ];
    }
}
