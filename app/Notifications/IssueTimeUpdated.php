<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class IssueTimeUpdated extends Notification
{
    use Queueable;
    private $schedule;
    private $data;


    /**
     * Create a new notification instance.
     *
     * @param $schedule
     * @param $data
     */
    public function __construct($schedule, $data)
    {
        $this->schedule = $schedule;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Issue Schedule on '.$this->data->title.' Updated by '.Auth::user()->firstname.' '.Auth::user()->lastname)
            ->view(
                'emails.issues.updated_schedule', ['data' => $this->data,'schedule'=>$this->schedule]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'subject'=>'Issue Schedule Updated',
            'title' => $this->data->title,
            'url' => '/issue/'.$this->data->id.'/track',
        ];
    }
}
