<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskCreatedFromEmail extends Notification implements ShouldQueue
{
    use Queueable;
    private $task;
    private $assigned;
    private $assignor;

    /**
     * Create a new notification instance.
     *
     * @param $task
     * @param $assigned
     * @param $assignor
     */
    public function __construct($task,$assigned,$assignor)
    {
        $this->task = $task;
        $this->assigned = $assigned;
        $this->assignor = $assignor;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('New Task Created by '.$this->assignor->preferred_name)
            ->view(
                'emails.tasks.created_task', ['data' => $this->task, 'assigned_users' => $this->assigned]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'subject'=>'New Task Created',
            'title' => $this->task->title,
            'url' => '/tasks/my-tasks/' . $this->task->id,
        ];
    }
}
