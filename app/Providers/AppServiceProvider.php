<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use PM\Models\IssueTracking;
use PM\Models\TaskTracking;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('end_time_validator', function($attribute, $value, $parameters, $validator) {
            $start_time = Carbon::parse(array_get($validator->getData(), $parameters[0]))->toTimeString();

            if($start_time < $value){
                return true;
            }
            return false;
        });

        Validator::extend('task_time_validator', function($attribute, $value, $parameters, $validator) {
            $date = Carbon::parse(array_get($validator->getData(), $parameters[0]))->toDateString();
            if($date != null && $value == null){
                return false;
            }
            return true;
        });

        Validator::extend('start_time_validator', function($attribute, $value, $parameters, $validator) {
            $date = Carbon::parse(array_get($validator->getData(), $parameters[0]))->toDateString();

            $end_time = Carbon::parse(array_get($validator->getData(), $parameters[1]))->toTimeString();
            $start_time = Carbon::parse($value)->toTimeString();
            $record = TaskTracking::where('date',$date)->whereBetween('start_time',[$start_time,$end_time])
                ->get();

            if(count($record) > 0){
                return false;
            }
            else{
                return true;
            }

        });

        Validator::extend('issue_time_validator', function($attribute, $value, $parameters, $validator) {
            $date = Carbon::parse(array_get($validator->getData(), $parameters[0]))->toDateString();
            $end_time = Carbon::parse(array_get($validator->getData(), $parameters[1]))->toTimeString();
            $start_time = Carbon::parse($value)->toTimeString();

            $record = IssueTracking::where('date',$date)->whereBetween('start_time',[$start_time,$end_time])->get();
           
            if(count($record) > 0){
                return false;
            }
            else{
                return true;
            }

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
