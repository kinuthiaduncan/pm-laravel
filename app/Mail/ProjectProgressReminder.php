<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProjectProgressReminder extends Mailable
{
    use Queueable, SerializesModels;

    private $this_week;

    public function __construct($this_week)
    {
        $this->this_week = $this_week;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.project_progress_remind',['this_week'=>$this->this_week]);
    }
}
