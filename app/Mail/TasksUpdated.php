<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use PM\Models\Task;

class TasksUpdated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $task;

    public function __construct(Task $task)
    {
        //
        $this -> task = $task;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user= Auth::user();
        return $this->view('emails.tasks.updated_task')
            ->with(['data' => $this->task])
            ->from('support@cytonn.com', 'Cytonn Project Management')
            ->subject($this->task->title . ' Task Updated by '.$user->preferred_name);
    }
}
