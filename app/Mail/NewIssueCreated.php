<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use PM\Models\Issue;

class NewIssueCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $issue;
    public function __construct(Issue $issue)
    {
        //
        $this->issue=$issue;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = Auth::user();
        return $this->view('emails.issue')
            ->with(['data' => $this->issue])
            ->from('support@cytonn.com', 'Cytonn Project Management')
            ->subject('[' . $this->issue->projects->project_key . '-' . $this->issue->id . '] '
                . $this->issue->title . ' Issue Created by '.$user->preferred_name);
    }
}
