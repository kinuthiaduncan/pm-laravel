<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use PM\Models\DailyReport;

class DailyReportCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $data;

    public function __construct($data)
    {
        //
        $this -> data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = Auth::user();
        return $this->view('emails.tasks.created_daily_report')
            ->with(['data' => $this->data])
            ->from('support@cytonn.com', 'Cytonn Project Management')
            ->subject('New Daily Report Created by '.$user->preferred_name);
    }

}
