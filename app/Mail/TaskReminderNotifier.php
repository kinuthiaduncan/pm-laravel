<?php

    namespace App\Mail;

    use Illuminate\Bus\Queueable;
    use Illuminate\Mail\Mailable;
    use Illuminate\Queue\SerializesModels;
    use PM\Models\Task;

    class TaskReminderNotifier extends Mailable
    {
        use Queueable, SerializesModels;

        /**
         * Create a new message instance.
         *
         * @return void
         */
        private $task;
        /**
         * @var
         */
        private $reminder;

        /**
         * TaskReminderNotifier constructor.
         * @param Task $task
         * @param $reminder
         */
        public function __construct(Task $task, $reminder)
        {
            //
            $this->task = $task;
            $this->reminder = $reminder;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            return $this->view('emails.tasks.reminder')
                ->with(['data' => $this->task, 'reminder'=>$this->reminder])
                ->from('support@cytonn.com', 'Cytonn Project Management')
                ->subject($this->task->title . ' Task Reminder');
        }
    }
