<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class TaskProgressReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $progress;

    public function __construct($progress)
    {
        //
        $this->progress = $progress;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = Auth::user();
        return $this->view('emails.tasks.progress_report')
            ->with(['data' => $this->progress])
            ->from('support@cytonn.com', 'Cytonn Project Management')
            ->subject('New Task Progress Report Created for task'.$this->progress->task->name.'by '.$user->preferred_name);
    }
}
