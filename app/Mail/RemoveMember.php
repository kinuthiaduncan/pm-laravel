<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use PM\Models\Project;
use PM\Presenters\UserPresenter;

class RemoveMember extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $project;
    public function __construct(Project $project)
    {
        //
        $this -> project = $project;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = Auth::user();
        $this->project['added_by'] = UserPresenter::presentFullNames(Auth::user()->id);

        return $this->view('emails.removemember')
            ->with(['member_firstname'=>$this->project->member_firstname, 'added_by'=>$this->project->added_by, 'data'=>$this->project])
            ->from('support@cytonn.com', 'Cytonn Project Management')
            ->subject('You have been removed from ' . $this->project->name . ' Project by '.$user->preferred_name);
    }
}
