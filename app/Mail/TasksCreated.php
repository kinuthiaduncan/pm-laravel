<?php

    namespace App\Mail;

    use Illuminate\Bus\Queueable;
    use Illuminate\Mail\Mailable;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Support\Facades\Auth;
    use PM\Models\Task;

    class TasksCreated extends Mailable
    {
        use Queueable, SerializesModels;

        /**
         * Create a new message instance.
         *
         * @return void
         */

        private $task;
        /**
         * @var
         */
        private $assigned_users;

        /**
         * TasksCreated constructor.
         * @param Task $task
         * @param $assigned_users
         * @internal param User $user
         */
        public function __construct(Task $task, $assigned_users)
        {
            $this->task = $task;
            $this->assigned_users = $assigned_users;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            $user = Auth::user();
            return $this->view('emails.tasks.created_task')
                ->with(['data' => $this->task, 'assigned_users' => $this->assigned_users])
                ->from('support@cytonn.com', 'Cytonn Project Management')
                ->subject($this->task->title . ' Task Created by '.$user->preferred_name);
        }
    }
