<?php

    namespace App\Mail;

    use Illuminate\Bus\Queueable;
    use Illuminate\Mail\Mailable;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Support\Facades\Auth;
    use PM\Models\ProjectComponent;
    use PM\Models\User;

    class ComponentCreationNotifier extends Mailable
    {
        use Queueable, SerializesModels;
        /**
         * @var ProjectComponent
         */
        private $projectComponent;

        /**
         * Create a new message instance.
         *
         * @param ProjectComponent $projectComponent
         */
        public function __construct(ProjectComponent $projectComponent)
        {
            $this->projectComponent = $projectComponent;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            $user = User::find(Auth::id());

            $data = $this->projectComponent;

            $data['component_creator']=$user->preferred_name;

            return $this->view('emails.component')
                ->with(['data' => $data, 'component_name' => $data['component_name'], 'project_name' => $data['project_name'], 'project_id' => $data['project_id'], 'description' => $data['description'], 'componentLead' => $data['componentLead']])
                ->from('support@cytonn.com', 'Cytonn Project Management')
                ->subject('A New Project Component Created by '. $data['component_creator']);
        }
    }
