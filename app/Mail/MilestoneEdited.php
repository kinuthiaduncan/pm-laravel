<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use PM\Models\Milestone;
use PM\Models\Project;

class MilestoneEdited extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $project;


    public function __construct(Milestone $milestone)
    {
        $this->project= $milestone;


    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        $user = Auth::id();
        return $this->view('emails.milestone')
            ->with(['date'=> $this->project->due_date, 'description'=> $this->project->milestone_description,
                'firstname'=> $this->project->member_firstname, 'data'=>$this->project,
                'title'=> $this->project->milestone_title])

            ->from('support@cytonn.com', 'Cytonn Project Management')
            ->subject('[ ' . $this->project->projects->project_key . '-M' .$this->project->id . ' ] '
                . $this->project->title . ' Milestone Edited by '.$user->preferred_name);
    }
}
