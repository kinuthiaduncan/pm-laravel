<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PM\Models\Issue;

class IssueEdited extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $issue;

    public function __construct(Issue $issue)
    {
        //
        $this->issue = $issue;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = Auth::id();
        return $this->view('emails.issuemembers')
            ->with(['data' => $this->issue])
            ->from('support@cytonn.com', 'Cytonn Project Management')
            ->subject('Issue Edited by '.$user->preferred_name);
    }
}
