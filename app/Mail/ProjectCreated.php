<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use PM\Models\Project;


class ProjectCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $project;

    public function __construct(Project $project)
    {
        //
        $this -> project = $project;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = Auth::user();
        return $this->view('emails.project')
            ->with(['data' => $this->project])
            ->from('support@cytonn.com', 'Cytonn Project Management')
            ->subject($this->project->name . ' Project Created by '.$user->preferred_name);
    }
}
