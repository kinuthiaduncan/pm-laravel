<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use PM\Models\ReportItem;
use PM\Departments\DepartmentsRepository;

class ItemsController extends Controller
{

    private $departmentsRepository;

    /**
     * ItemsController constructor.
     * @param DepartmentsRepository $departmentsRepository
     */
    public function __construct(DepartmentsRepository $departmentsRepository)
    {
        $this->departmentsRepository = $departmentsRepository;
    }

    /**
     * Display report Items
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dailyItems = ReportItem::paginate(10);

        return view('reports.items.index', ['dailyItems' => $dailyItems]);
    }

    /**
     * Create a new report item
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $dailyReports = ReportItem::paginate(10);
        $departments = $this->departmentsRepository->getDepartments();

        return view('reports.items.create', ['departments' => $departments, 'dailyReports' => $dailyReports]);
    }

    /**
     * Store a report Item
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        ReportItem::create([
            'item' => $request->input('item'),
            'department_id' => $request->input('department_id'),
            'created_by' => Auth::user()->id]);

        return redirect()->route('report_items.index');
    }

    /**
     * Edit daily report item
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $departments = $this->departmentsRepository->getDepartments();
        $item = ReportItem::findorFail($id);

        return view('reports.items.edit', ['departments' => $departments, 'item' => $item]);
    }

    /**
     * Update daily report item
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param Request $request
     */
    public function update($id)
    {
        ReportItem::where('id', $id)->update(
            ['item' => Input::get('item'),
                'department_id' => Input::get('department_id')]);

        return redirect()->route('report_items.index');
    }
}