<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PM\Models\ReportItem;
use PM\Models\ReportSubItem;

class SubItemsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('report_item_id');
        ReportSubItem::create([
            'name'=>$request->input('name'),
            'description'=>$request->input('description'),
            'report_item_id'=>$id,
        ]);
        return redirect('sub-items/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item = ReportItem::where('id',$id)->first();
        $subItems = ReportSubItem::where('report_item_id',$id)->get();
        return view('reports.items.show',['subItems'=>$subItems,'item'=>$item]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $subItem = ReportSubItem::findorFail($id)->forceDelete();
        return redirect('report-items');
    }
}
