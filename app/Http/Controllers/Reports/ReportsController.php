<?php

    namespace App\Http\Controllers\Reports;

    use App\Http\Controllers\Controller;
    use App\Mail\DailyReportCreated;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\Redirect;
    use Laracasts\Flash\Flash;
    use PDF;
    use PM\Departments\DepartmentsRepository;
    use PM\Models\DailyReport;
    use PM\Models\Department;
    use PM\Models\Issue;
    use PM\Models\ReportItem;
    use PM\Models\Task;
    use PM\Models\TaskTracking;
    use PM\ReportItems\ReportItemsRepository;

    class ReportsController extends Controller
    {
        /**
         * @return \Illuminate\Http\Response
         */
        private $departmentsRepository;
        private $reportItemsRepository;

        /**
         * ReportsController constructor.
         * @param DepartmentsRepository $departmentsRepository
         * @param ReportItemsRepository $reportItemsRepository
         */
        public function __construct(DepartmentsRepository $departmentsRepository, ReportItemsRepository $reportItemsRepository)
        {
            $this->departmentsRepository = $departmentsRepository;
            $this->reportItemsRepository = $reportItemsRepository;
        }

        /**
         * Reports index
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $departments = $this->departmentsRepository->getDepartments();

            return view('reports.index', ['departments' => $departments]);
        }

        /**
         * Display create report page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function create()
        {
            $departments = $this->departmentsRepository->getDepartments();

            return view('reports.reports.create', ['departments' => $departments]);
        }

        /**
         * Store daily report
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function store(Request $request)
        {
            $this->validate($request, [

                'item_id' => 'required',
                'description' => 'required',
                'department_id' => 'required',
                'date' => 'required',
            ]);

            $user = Auth::user()->id;
            DailyReport::create([
                'item_id' => $request->input('item_id'),
                'subitem_id' => $request->input('subItem'),
                'description' => $request->input('description'),
                'department_id' => $request->input('department_id'),
                'created_by' => $user,
                'date' => $request->input('date'),

            ]);
            Flash::success('Report Item Saved');

            return Redirect::back();
        }

        /**
         * Shows more details on a report
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function showReport($id, $date)
        {
            $department = Department::where('id', $id)->first();

            $report = DailyReport::where('department_id', $id)->where('date', $date)->get();

            return view('reports.reports.details', ['report' => $report, 'department' => $department, 'date' => $date]);
        }


        /**
         * @param $id
         * @return Redirect
         */
        public function delete($id)
        {
            DailyReport::findorFail($id)->forceDelete();

            return redirect('daily-reports');
        }

        /**
         * Get items for a department
         * @param $id
         */
        public function getItems()
        {
            $id = Input::get('department_id');
            $items = ReportItem::where('department_id', $id)->get();

            return Response::json($items);
        }

        /**
         * Export personal daily issues and task summary
         * @return mixed
         */
        public function PersonalDailySummary()
        {
            $user = Auth::user();
            $user->name = $user->preferred_name;
            $today = Carbon::today()->toDateString();
            $tomorrow = Carbon::today()->addDays(1)->toDateString();

            if ($user->department_id == 2) {

                $data = Issue::where('created_at','>=',$today)->where('created_at','<=',$tomorrow)
                    ->where('assigned_to', $user->id)
                    ->get();

                $review = Issue::where('status_change_date',Carbon::today()->format('Y-m-d'))
                    ->where('assigned_to', $user->id)->where('status_id',3)->get();

                $done = Issue::where('status_change_date',Carbon::today()->format('Y-m-d'))
                    ->where('assigned_to', $user->id)->where('status_id',4)->get();
//
                $pdf = PDF::loadView('issues.exports.dailyIssuesReport', compact('data', $data, 'user', $user,'review',$review,'done',$done));

                return $pdf->download($user->name.' Daily Issue Report.pdf');
            }
            else
                {
                    $data = Task::where('created_at','>=',$today)->where('created_at','<=',$tomorrow)
                        ->whereHas('assignedUsers', function ($assigned) use ($user){
                            $user = Auth::user();
                            $assigned->where('user_id',$user->id);
                        })
                        ->get();

                    $logged = TaskTracking::where('created_at','>=',$today)->where('created_at','<=',$tomorrow)
                        ->where('created_by',$user->id)->get();

                $pdf = PDF::loadView('tasks.exports.dailyTaskReport', compact('data', $data, 'user', $user,'logged',$logged, 'user',$user));

                return $pdf->download($user->name.' Daily Task Report.pdf');
            }
        }

        /**
         * Export personal weekly task and issues summary
         * @return mixed
         */
        public function PersonalWeeklySummary()
        {
            $user = Auth::user();
            $user->name = $user->preferred_name;
            $weekStart = Carbon::now()->startOfWeek();
            $weekEnd = Carbon::now()->endOfWeek();

            if ($user->department_id == 2)
            {
                $data = Issue::where('created_at','>=',$weekStart)->where('created_at','<=',$weekEnd)
                    ->where('assigned_to', $user->id)
                    ->get();
                $review = Issue::where('status_change_date',Carbon::today()->format('Y-m-d'))
                    ->where('assigned_to', $user->id)->where('status_id',3)->get();

                $done = Issue::where('status_change_date',Carbon::today()->format('Y-m-d'))
                    ->where('assigned_to', $user->id)->where('status_id',4)->get();


                $pdf = PDF::loadView('issues.exports.weeklyIssuesReport', compact('data', $data, 'user', $user,'review',$review,'done',$done));

                return $pdf->download($user->name.' Weekly Issues Report.pdf');

            }
            else
                {
                    $startDate = Carbon::today()->startOfWeek()->toDateString();
                    $endDate = Carbon::today()->endOfWeek()->toDateString();

                    $data = Task::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)
                        ->whereHas('assignedUsers', function ($assigned) use ($user){
                            $user = Auth::user();
                            $assigned->where('user_id',$user->id);
                        })
                        ->get();

                    $logged = TaskTracking::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)
                        ->where('created_by',$user->id)->groupBy('task_id')->get();

                    $totalHours = $logged->map(function($totalHour) {
                        $diff = (Carbon::parse($totalHour->start_time)->diffInMinutes(Carbon::parse($totalHour->end_time)))/60;
                        return $diff;
                    })->sum();

                $pdf = PDF::loadView('tasks.exports.weeklyTaskReport', compact('data', $data, 'user', $user,'logged',$logged,'user',$user,'totalHours',$totalHours,2));

                return $pdf->download($user->name.' Weekly Task Report.pdf');
            }
        }

        /**
         * Export filtered tasks
         * @param Request $request
         * @return mixed
         */
        public function exportTasks(Request $request)
        {
            $data = Task::OfTaskAccess($request->task_access)->OfTaskTitle($request->title)
                ->OfTaskStatus($request->status)
                ->OfTaskDepartment($request->department_id)
                ->OfTaskAssignedTo($request->assigned_to)
                ->get();

            view()->share('data', $data);
            $pdf = PDF::loadView('tasks.exports.tasksFilterReport');

            return $pdf->download('Team_Shared_Task_Filter_Report.pdf');
        }

        /**
         * Export personal tasks
         * @param Request $request
         * @return mixed
         */
        public function exportPersonalTasks(Request $request)
        {
            $data = Task::whereHas('assignedUsers', function ($assigned){
                $user = Auth::user();
                $assigned->where('user_id',$user->id);
            })
            ->OfTaskAccess($request->task_access)
                ->OfTaskTitle($request->title)
                ->OfTaskStatus($request->status_id)
                ->get();

            view()->share('data', $data);
            $pdf = PDF::loadView('tasks.exports.tasksFilterReport');

            return $pdf->download('Individual_Task_Filter_Report.pdf');
        }

        /**
         * Tasks Reports for the head of departments
         * @return mixed
         */
        public function departmentHeadReport()
        {
            $data = Task::where('department_id', Auth::user()->department_id)->get();

            view()->share('data', $data);
            $pdf = PDF::loadView('tasks.exports.departmentReport');

            return $pdf->download('Department Tasks.pdf');
        }

        /**
         * Daily reports PDF exports
         * @param $id
         * @param $date
         * @return mixed
         */
        public function exportDailyReport($id, $date)
        {
            $department = Department::where('id', $id)->first();

            $data = DailyReport::where('department_id', $id)->where('date', $date)->get();

            view()->share('data', $data);
            $pdf = PDF::loadView('reports.exports.dailyReport');

            return $pdf->download('Daily Report.pdf');
        }

        /**
         * Send daily report
         * @param $id
         * @param $date
         */
        public function sendDailyReport($id, $date)
        {
            $department = Department::where('id', $id)->first();
            $data = DailyReport::where('department_id', $id)->where('date', $date)->get();

            //send mails to departments
            $department_id = $department->id;

            $department_email = Department::where('id', $department_id)->pluck('department_email');

            Mail::to($department_email)
                ->cc('bda@cytonn.com')
                ->queue(new DailyReportCreated($data));
            Flash::success('Email sent successfully');

            return Redirect::back();
        }

    }
