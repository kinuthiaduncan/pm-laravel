<?php
    /**
     * Cytonn Technologies
     * @author: Phillis Kiragu <pkiragu@cytonn.com>
     */

    namespace App\Http\Controllers\Users;


    use App\Http\Controllers\Controller;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Http\Request;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Response;
    use League\Fractal\Manager;
    use PM\Api\Transformers\UserTransformer;
    use PM\Authentication\AuthorizeAPI;
    use PM\Models\Department;
    use PM\Models\Follow;
    use PM\Models\User;
    use PM\Users\UserRepository;

    class UserController extends Controller
    {
        protected $userRepository;
        /**
         * @var AuthorizeAPI
         */
        private $authorizeAPI;

        /**
         * UserController constructor.
         * @param UserRepository $userRepository
         * @param AuthorizeAPI $authorizeAPI
         */
        public function __construct(UserRepository $userRepository, AuthorizeAPI $authorizeAPI)
        {
            $this->userRepository = $userRepository;
            $this->authorizeAPI = $authorizeAPI;
        }

        /*
         * get all user
         */
        public function getAllUsers($query = '')
        {
            $users = $this->userRepository->allUsers($query);

            return Response::json($users);
        }

        public function index()
        {
            $departments = Department::all()->map(function ($dept) {
                return ['id' => $dept->id, 'name' => $dept->name];
            });

            return view('users.index', ['departments' => $departments]);
        }
    }