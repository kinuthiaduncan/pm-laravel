<?php
    /**
     * Date: 28/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Users;


    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Laracasts\Flash\Flash;
    use PM\Users\FollowsRepository;

    class FollowsController extends Controller
    {
        /**
         * @var FollowsRepository
         */
        private $followsRepository;

        /**
         * FollowsController constructor.
         * @param FollowsRepository $followsRepository
         */
        public function __construct(FollowsRepository $followsRepository)
        {
            $this->followsRepository = $followsRepository;
        }

        /**
         * Follow a user
         * @param $followed_id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function followUser($followed_id)
        {
            $followed = $this->followsRepository->followUser($followed_id);

            return $followed;
        }

        /**
         * Un-follow a user
         * @param $followed_id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function unFollowUser($followed_id)
        {
            $un_followed = $this->followsRepository->unFollowUser($followed_id);

            return $un_followed;
        }

        /**
         * Follow a task
         * @param $task_id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function followTask($task_id)
        {
            $this->followsRepository->followTask($task_id);

            Flash::success('You have followed this task.');

            return redirect()->back();
        }

        /**
         * Un-follow a task
         * @param $task_id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function unFollowTask($task_id)
        {
            $this->followsRepository->unFollowTask($task_id);

            Flash::warning('You have un-followed this task.');

            return redirect()->back();
        }

        /**
         * Get all the followed tasks by the logged in user
         * @param Request $request
         * @return
         */
        public function followedTasks(Request $request)
        {
            $followed_tasks = $this->followsRepository->getAllFollowedTasksByAuthUser($request);

            return $followed_tasks;
        }
    }