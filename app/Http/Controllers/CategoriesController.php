<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 06/12/2016
 * Time: 09:33
 */

namespace App\Http\Controllers;


use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use PM\Models\ActivityStream;
use PM\Models\Project;
use PM\Models\ProjectCategory;
use PM\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CategoriesController extends Controller
{
    /**
     * ADD Project mailer
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer=$mailer;
    }

    /**
     * Categories Index Page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = auth()->user();
        $categories = ProjectCategory::paginate(10);
        return view('projects.categories.index', ['title'=>'Project Categories','user'=>$user,'categories'=>$categories]);

    }

    /**
     * Categories Create Page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('projects.categories.create', ['title'=>'Project Categories']);

    }

    /**
     * Save a new project category
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $input=array_except(request()->all(), ['_token']);

        $user = Auth::user();

        $rules=['slug'=>'required', 'name'=>'required','description'=>'required'];

        $validator=\Validator::make(array('slug'=>$input['name'], 'name'=>$input['name'], 'description'=>$input['description']), $rules);

        if ($validator->passes())
        {

            $input['created_by']=$user->id;
            ProjectCategory::create($input);

            Flash::message('Project category successfully created');

            return redirect()->to(route('get_categories_index'));

        } else
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
    }

    public function edit($id)
    {
        $category = ProjectCategory::find($id);

        return view('projects.categories.edit', ['title'=>'Project Categories', 'category'=>$category]);
    }

    /**
     * Edit category
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $input=array_except(request()->all(), ['_token']);

        $rules=['slug'=>'required', 'name'=>'required','description'=>'required'];

        $validator=\Validator::make(array('slug'=>$input['name'], 'name'=>$input['name'], 'description'=>$input['description']), $rules);

        if ($validator->passes())
        {
            $category = ProjectCategory::find($input['id']);

            $user = Auth::user();

            $input['edited_by']=$user->id;

            $category->update($input);

            Flash::message('Project category successfully edited');

            return redirect()->to(route('get_categories_index'));

        } else
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
    }

    /**
     * Show individual category
     * Allow user to add projects
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $category = ProjectCategory::find($id);

        $listed = Project::where('project_access', 'public')
            ->where('category_id', $category->id)
            ->get();

        $non_listed = Project::where('project_access', 'public')
            ->whereNull('category_id')
            ->get();
        return view('projects.categories.show', ['title'=>$category->slug, 'category'=>$category, 'listed'=>$listed, 'non_listed'=>$non_listed]);

    }

    /**
     * Add project(s) to a specified category
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAddProjects()
    {
        $input = array_except(request()->all(), '_token');

        if (empty($input['project_id']))
        {
            Flash::error('No items selected.');

            return redirect()->back();
        }

        foreach ($input['project_id'] as $p){

            $project = Project::find($p);
            $project->category_id = $input['category_id'];

            $project->save();
        }

        Flash::message('Selected Projects successfully added to category.');

        return redirect()->back();
    }

    /**
     * Remove a project from the specified category
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRemoveProject()
    {
        $input = array_except(request()->all(), '_token');

        Flash::message('Selected Project successfully removed from the category.');

        $project = Project::find($input['category_id']);
        $project->category_id = null;
        $project->update();

        return redirect()->back();
    }
}