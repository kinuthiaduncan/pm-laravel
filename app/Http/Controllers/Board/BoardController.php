<?php namespace App\Http\Controllers\Board;


use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use League\Fractal\Manager;
use PM\Api\Transformers\IssueTransformer;
use PM\Issues\IssueTrackingRepository;
use PM\Models\Issue;
use PM\Models\IssueTracking;
use PM\Models\Project;
use PM\Models\Status;
use Response;

/**
 * Date: 25/02/2017
 * Time: 12:28
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: project-management
 * Cytonn Technologies
 */
class BoardController extends Controller
{
    private $perPage = 10;
    private $issueTrackingRepository;

    function __construct(IssueTrackingRepository $issueTrackingRepository)
    {
        $this->issueTrackingRepository = $issueTrackingRepository;
    }

    /**
     * Display board
     * @param Request $request
     * @param $projectId
     * @return mixed
     */
    public function index(Request $request, $projectId)
    {
        $statuses = Status::where('show_in_board', true)->orderBy('weight', 'ASC')->get();

        $issueQuery = Issue::with('statuses')
                ->where('project_id', $projectId)
                ->search($request->get('search'))
                ->orderBy('priority_id', 'ASC')
                ->latest('updated_at');

        if ($assigned = $request->get('assigned')) $issueQuery->where('assigned_to', $assigned);
        if ($component = $request->get('component')) $issueQuery->where('component_id', $component);

        $parsedStartDate = null;
        $parsedEndDate = null;

        if (!is_null($request->get('start_date'))) {
            $startDate = Carbon::parse($request->get('start_date'));
            $parsedStartDate = $startDate->toDateTimeString();
        }

        if (!is_null($request->get('end_date'))) {
            $endDate = Carbon::parse($request->get('end_date'));
            $parsedEndDate = $endDate->toDateTimeString();
        }

        if ($start_date = $parsedStartDate && $end_date = $parsedEndDate)
            $issueQuery->where('created_at', '>=', $startDate)
                ->where('created_at', '<=', $endDate);

        $coll = new Collection();

        foreach ($statuses as $status)
        {
            $q = clone $issueQuery;
            $issues = $q->where('status_id', $status->id)
                ->paginate($request->get('limit'));

            $cnt = clone $issueQuery;

            $transformed = new \League\Fractal\Resource\Collection($issues, new IssueTransformer());

            $issues = (new Manager())->createData($transformed)->toArray();

            $coll->push((object) [
                'status' => $status->name,
                'id' => $status->id,
                'issues' => $issues,
                'count' => $cnt->where('status_id', $status->id)->count()
            ]);
        }

        return response()->json($coll->all());
    }

    /**
     * Update issue status
     * @param $issue_id
     * @param $status_id
     * @return mixed
     */
    public function update($issue_id, $status_id)
    {
        $issue = Issue::findOrFail($issue_id);
        $issue->status_id = $status_id;
        $dateToday = Carbon::now();
        $issue->status_change_date = $dateToday->toDateString();

        if($issue->status_id == 4) {
            $schedules = count(IssueTracking::where('issue_id',$issue->id)->get());
            if($schedules == 0) {
                return response()->json($schedules);
            }
            $issue->save();
        }

        $issue->save();

        return response()->json('updated', 202);
    }

    /**
     * Export filtered issues to PDF
     * @param Request $request
     * @param $projectId
     * @return mixed
     */
    public function export(Request $request, $projectId)
    {
        $statuses = Status::where('show_in_board', true)->orderBy('weight', 'ASC')->get();
        $project = Project::findOrFail($projectId);
        $issueQuery = Issue::with('statuses')
            ->where('project_id', $projectId)
            ->search($request->get('search'))
            ->orderBy('priority_id', 'ASC')
            ->latest('updated_at');

        if ($assigned = $request->get('assigned')) $issueQuery->where('assigned_to', $assigned);
        if ($component = $request->get('component')) $issueQuery->where('component_id', $component);

        $parsedStartDate = null;
        $parsedEndDate = null;

        if (!is_null($request->get('start_date'))) {
            $startDate = Carbon::parse($request->get('start_date'));
            $parsedStartDate = $startDate->toDateTimeString();
        }

        if (!is_null($request->get('end_date'))) {
            $endDate = Carbon::parse($request->get('end_date'));
            $parsedEndDate = $endDate->toDateTimeString();
        }

        if ($start_date = $parsedStartDate && $end_date = $parsedEndDate)
            $issueQuery->where('created_at', '>=', $startDate)
                ->where('created_at', '<=', $endDate);

        $coll = new Collection();

        foreach ($statuses as $status) {
            $q = clone $issueQuery;
            $issues = $q->where('status_id', $status->id)->get();

            $cnt = clone $issueQuery;

            $transformed = new \League\Fractal\Resource\Collection($issues, new IssueTransformer());

            $issues = (new Manager())->createData($transformed)->toArray();

            $coll->push((object) [
                'status' => $status->name,
                'id' => $status->id,
                'issues' => $issues,
                'count' => $cnt->where('status_id', $status->id)->count()
            ]);
        }

        view()->share('data', $coll);

        $pdf = PDF::loadView('issues.exports.issueFilterReport');
        $headers = [
            "Content-Type: application/pdf",
            "Connection: close"
        ];
        return $pdf->download($project->name.'_Issue_Filter_Report.pdf', $headers);
    }

    /**
     * Save a schedule created from board
     * @param Request $request
     * @return mixed
     */
    public function saveSchedule(Request $request)
    {
        $this->validate($request,[
            'date'=>'required',
            'start_time'=>'required',
            'end_time'=>'required|end_time_validator:start_time'
        ]);

        $request['date'] = Carbon::parse($request->date)->toDateString();
        $request['start_time'] = Carbon::parse($request->start_time)->toTimeString();
        $request['end_time'] = Carbon::parse($request->end_time)->toTimeString();
        $issue = Issue::findOrFail($request->issue_id);
        $issue->status_id = 4;
        $issue->percentage_done = 100;
        $dateToday = Carbon::now();
        $issue->status_change_date = $dateToday->toDateString();
        $issue->update([$issue]);
        $this->issueTrackingRepository->save($request, $request->issue_id);
        return response()->json('updated', 200);
    }
}