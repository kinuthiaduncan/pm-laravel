<?php
    /**
     * Date: 17/12/16
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers;


    use Carbon\Carbon;
    use Maatwebsite\Excel\Facades\Excel;
    use PM\Models\ActivityStream;
    use PM\Models\Comment;
    use PM\Models\Issue;
    use PM\Models\IssueType;
    use PM\Models\Milestone;
    use PM\Models\Priority;
    use PM\Models\Project;
    use PM\Models\ProjectComponent;
    use PM\Models\ProjectProgress;
    use PM\Models\ProjectType;
    use PM\Models\ProjectUser;
    use PM\Models\Role;
    use PM\Models\Status;
    use PM\Models\User;

    class ExportController extends Controller
    {
        public function tableProjectsExport()
        {
            $projects = Project::all();

            $project_s = $projects->map(function ($project) {
                return [
                    "id" => $project->id,
                    "name" => $project->name,
                    "project_type" => $project->project_type,
                    "project_access" => $project->project_access,
                    "project_key" => $project->project_key,
                    "project_description" => $project->project_description,
                    "project_url" => $project->project_url,
                    "project_lead" => $project->project_lead,
                    "created_by" => $project->created_by,
                    "created_at" => $project->created_at,
                    "updated_at" => $project->updated_at
                ];
            });

            Excel::create('projects', function ($excel) use ($project_s) {
                $excel->sheet('Sheet 1', function ($sheet) use ($project_s) {
                    $sheet->fromArray($project_s);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableUsersExport()
        {
            $users = User::all();

            $users = $users->map(function ($user) {
                return [
                    "id" => $user->id,
                    "firstname" => $user->firstname,
                    "lastname" => $user->lastname,
                    'preferred_name'=>$user->preferred_name,
                    "email" => $user->email,
                    "role_id" => $user->role_id,
                    "google_id" => $user->google_id,
                    "avatar" => $user->avatar,
                    "remember_token" => $user->remember_token,
                    "deleted_at" => $user->deleted_at,
                    "created_at" => $user->created_at,
                    "updated_at" => $user->updated_at
                ];
            });

            Excel::create('users', function ($excel) use ($users) {
                $excel->sheet('Sheet 1', function ($sheet) use ($users) {
                    $sheet->fromArray($users);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableStatusExport()
        {
            $statuses = Status::all();

            $statuses = $statuses->map(function ($status) {
                return [
                    "id" => $status->id,
                    "name" => $status->name,
                    "created_at" => $status->created_at,
                    "updated_at" => $status->updated_at,
                    "slug" => $status->slug,
                    "resolution" => $status->resolution
                ];
            });

            Excel::create('status', function ($excel) use ($statuses) {
                $excel->sheet('Sheet 1', function ($sheet) use ($statuses) {
                    $sheet->fromArray($statuses);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableRolesExport()
        {
            $roles = Role::all();

            $roles = $roles->map(function ($role) {
                return [
                    "id" => $role->id,
                    "role" => $role->role,
                    "created_at" => $role->created_at,
                    "updated_at" => $role->updated_at
                ];
            });

            Excel::create('roles', function ($excel) use ($roles) {
                $excel->sheet('Sheet 1', function ($sheet) use ($roles) {
                    $sheet->fromArray($roles);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableProjectUsersExport()
        {
            $project_users = ProjectUser::all();

            $project_users = $project_users->map(function ($project_user) {
                return [
                    "id" => $project_user->id,
                    "user_id" => $project_user->user_id,
                    "project_id" => $project_user->project_id,
                    "created_at" => $project_user->created_at,
                    "updated_at" => $project_user->updated_at
                ];
            });

            Excel::create('project_users', function ($excel) use ($project_users) {
                $excel->sheet('Sheet 1', function ($sheet) use ($project_users) {
                    $sheet->fromArray($project_users);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableProjectTypesExport()
        {
            $project_types = ProjectType::all();

            $project_types = $project_types->map(function ($project_type) {
                return [
                    "id" => $project_type->id,
                    "name" => $project_type->name,
                    "created_at" => $project_type->created_at,
                    "updated_at" => $project_type->updated_at
                ];
            });

            Excel::create('project_types', function ($excel) use ($project_types) {
                $excel->sheet('Sheet 1', function ($sheet) use ($project_types) {
                    $sheet->fromArray($project_types);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableProjectProgressExport()
        {
            $project_progresses = ProjectProgress::all();

            $project_progresses = $project_progresses->map(function ($project_progress) {
                return [
                    "id" => $project_progress->id,
                    "project_id" => $project_progress->project_id,
                    "added_by" => $project_progress->added_by,
                    "content" => $project_progress->content,
                    "created_at" => $project_progress->created_at,
                    "updated_at" => $project_progress->updated_at
                ];
            });

            Excel::create('project_progress', function ($excel) use ($project_progresses) {
                $excel->sheet('Sheet 1', function ($sheet) use ($project_progresses) {
                    $sheet->fromArray($project_progresses);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableProjectComponentsExport()
        {
            $project_components = ProjectComponent::all();

            $project_components = $project_components->map(function ($project_component) {
                return [
                    "id" => $project_component->id,
                    "project_id" => $project_component->project_id,
                    "component_name" => $project_component->component_name,
                    "created_at" => $project_component->created_at,
                    "updated_at" => $project_component->updated_at,
                    "description" => $project_component->description
                ];
            });

            Excel::create('project_progress', function ($excel) use ($project_components) {
                $excel->sheet('Sheet 1', function ($sheet) use ($project_components) {
                    $sheet->fromArray($project_components);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableProjectMilestonesExport()
        {
            $project_milestones = Milestone::all();

            $project_milestones = $project_milestones->map(function ($project_milestone) {
                return [
                    "id" => $project_milestone->id,
                    "project_id" => $project_milestone->project_id,
                    "title" => $project_milestone->title,
                    "description" => $project_milestone->description,
                    "due_date" => $project_milestone->due_date,
                    "created_by" => $project_milestone->created_by,
                    "created_at" => $project_milestone->created_at,
                    "updated_at" => $project_milestone->updated_at,
                ];
            });

            Excel::create('project_progress', function ($excel) use ($project_milestones) {
                $excel->sheet('Sheet 1', function ($sheet) use ($project_milestones) {
                    $sheet->fromArray($project_milestones);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tablePrioritiesExport()
        {
            $priorities = Priority::all();

            $priorities = $priorities->map(function ($priority) {
                return [
                    "id" => $priority->id,
                    "name" => $priority->name,
                    "created_at" => $priority->created_at,
                    "updated_at" => $priority->updated_at
                ];
            });

            Excel::create('priorities', function ($excel) use ($priorities) {
                $excel->sheet('Sheet 1', function ($sheet) use ($priorities) {
                    $sheet->fromArray($priorities);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableIssuesExport()
        {
            $issues = Issue::all();

            $issues = $issues->map(function ($issue) {
                if ($issue->percentage_done == 0) {
                    $percentage = '0';
                }else{
                    $percentage = $issue->percentage_done;
                }

                if ($issue->status_change_date == NUll) {
                    $status_change_date = Carbon::tomorrow('Europe/London');
                }else{
                    $status_change_date = $issue->status_change_date;
                }

                if ($issue->completed_at == NULL) {
                    $completed_at = Carbon::tomorrow('Europe/London');
                }else{
                    $completed_at = $issue->completed_at;
                }

                return [
                    "id" => $issue->id,
                    "project_id" => $issue->project_id,
                    "component_id" => $issue->component_id,
                    "title" => $issue->title,
                    "description" => $issue->description,
                    "image" => $issue->image,
                    "status_id" => $issue->status_id,
                    "parent_id" => $issue->parent_id,
                    "assigned_to" => $issue->assigned_to,
                    "issue_type" => $issue->issue_type,
                    "priority_id" => $issue->priority_id,
                    "percentage_done" => $percentage,
                    "date_due" => $issue->date_due,
                    "status_change_date" => $status_change_date,
                    "created_by" => $issue->created_by,
                    "created_at" => $issue->created_at,
                    "updated_at" => $issue->updated_at,
                    "completed_at" => $completed_at
                ];
            });

            Excel::create('issues', function ($excel) use ($issues) {
                $excel->sheet('Sheet 1', function ($sheet) use ($issues) {
                    $sheet->fromArray($issues);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableIssueTypesExport()
        {
            $issue_types = IssueType::all();

            $issue_types = $issue_types->map(function ($issue_type) {
                return [
                    "id" => $issue_type->id,
                    "name" => $issue_type->name,
                    "created_at" => $issue_type->created_at,
                    "updated_at" => $issue_type->updated_at
                ];
            });

            Excel::create('issue_types', function ($excel) use ($issue_types) {
                $excel->sheet('Sheet 1', function ($sheet) use ($issue_types) {
                    $sheet->fromArray($issue_types);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableCommentsExport()
        {
            $comments = Comment::all();

            $comments = $comments->map(function ($comment) {
                return [
                    "id" => $comment->id,
                    "name" => $comment->name,
                    "image" => $comment->image,
                    "issue_id" => $comment->issue_id,
                    "created_by" => $comment->created_by,
                    "created_at" => $comment->created_at,
                    "updated_at" => $comment->updated_at
                ];
            });

            Excel::create('comments', function ($excel) use ($comments) {
                $excel->sheet('Sheet 1', function ($sheet) use ($comments) {
                    $sheet->fromArray($comments);
                });
            })->export('csv');

            return redirect()->back();
        }

        public function tableActivityStreamExport()
        {
            $activity_streams = ActivityStream::all();

            $activity_streams = $activity_streams->map(function ($activity_stream) {
                return [
                    "id" => $activity_stream->id,
                    "activity_description" => $activity_stream->activity_description,
                    "path" => $activity_stream->path,
                    "project_id" => $activity_stream->project_id,
                    "activity_by" => $activity_stream->activity_by,
                    "created_at" => $activity_stream->created_at,
                    "updated_at" => $activity_stream->updated_at
                ];
            });

            Excel::create('category_projects', function ($excel) use ($activity_streams) {
                $excel->sheet('Sheet 1', function ($sheet) use ($activity_streams) {
                    $sheet->fromArray($activity_streams);
                });
            })->export('csv');

            return redirect()->back();
        }
    }