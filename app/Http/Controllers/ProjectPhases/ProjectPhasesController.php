<?php

namespace App\Http\Controllers\ProjectPhases;

use App\Notifications\ProjectPhaseApproved;
use App\Notifications\ProjectPhaseRejected;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Maatwebsite\Excel\Facades\Excel;
use PM\Models\Issue;
use PM\Models\Project;
use PM\Models\ProjectPhase;
use PM\Models\ProjectPhaseActivity;
use PM\Models\ProjectPhaseUpdates;
use PM\Presenters\ProjectPhasePresenter;
use PM\Presenters\UserPresenter;
use PM\Projects\ProjectPhasesRepository;
use PDF;

class ProjectPhasesController extends Controller
{
    private $projectPhasesRepository;

    /**
     * ProjectPhasesController constructor.
     * @param ProjectPhasesRepository $projectPhasesRepository
     */
    function __construct(ProjectPhasesRepository $projectPhasesRepository)
    {
        $this->projectPhasesRepository = $projectPhasesRepository;
    }

    /**
     * Display a listing of the resource.
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $phases = ProjectPhase::where('project_id',$id)->orderBy('start_date','DESC')->paginate(10);
        $project  = Project::findOrFail($id);
        return view('projects.phases.index',['phases'=>$phases,'project'=>$project]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $project  = Project::findOrFail($id);
        return view('projects.phases.create',['project'=>$project]);
    }


    /**
     * Store project phase
     * @param Request $request
     * @param $id
     * @return Redirect
     */
    public function store(Request $request, $id)
    {
        $this->validate($request,[
           'title'=>'required|min:4',
            'start_date'=>'required',
            'end_date'=>'required',
        ]);
        $this->projectPhasesRepository->save($request, $id);
        Flash::success('Approval Request Successful');
        return redirect('/project/'.$id.'/phases');
    }

    /**
     * Display a specified project phase.
     *
     * @param $project
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($project,$id)
    {
        $project  = Project::findOrFail($project);
        $phase = ProjectPhase::findOrFail($id);
        $activities = Issue::where('project_phase',$id)->paginate(10);
        return view('projects.phases.show',['project'=>$project,'phase'=>$phase,'activities'=>$activities]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $project
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($project,$id)
    {
        $project  = Project::findOrFail($project);
        $phase = ProjectPhase::findOrFail($id);
        return view('projects.phases.edit',['project'=>$project,'phase'=>$phase]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $project
     * @param  int $id
     * @return redirect
     */
    public function update(Request $request,$project, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'start_date'=>'required',
            'end_date'=>'required'
        ]);
        $this->projectPhasesRepository->update($request,$project,$id);
        Flash::success('Update Approval Request Successful');
        return redirect('/project/'.$project.'/phases');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        ProjectPhase::findOrFail($id)->forceDelete();
        Flash::success('Project Phase Deleted Successfully');
        return Redirect::back();
    }

    /**
     * Export work plan as PDF
     * @param $id
     * @return mixed
     */
    public function exportWorkPlanPDF($id)
    {
        $phase = ProjectPhase::findOrFail($id);
        $activities = Issue::where('project_phase',$id)->get();

        $data = [
            'phase'=>$phase,
            'activities'=>$activities
        ];
        view()->share('data', $data);
        $pdf = PDF::loadView('projects.exports.work_plan');

        return $pdf->download($phase->project->name.'_'.$phase->name.'_Work_Plan.pdf');
    }

    /**
     * Export work plan to Excel
     * @param $id
     */
    public function exportWorkPlanExcel($id)
    {
        $project_phase = ProjectPhase::where('id',$id)->first();
        $phase = ProjectPhase::where('id',$id)->get()->map(function($data) {
            return [
                'project Name' => $data->project->name,
                'Work Plan' => $data->name,
                'Start Date' => $data->start_date,
                'Due Date' => $data->end_date,
                'Percentage Done' => ProjectPhasePresenter::phasePercentDone($data->id).'%'
            ];
        })->toArray();

        $activities = Issue::where('project_phase',$id)->get()->map(function($data){
            return [
                'Issue Title'=>$data->title,
                'Due Date'=>$data->due_date,
                'Days Left'=>ProjectPhasePresenter::timeLeft($data->due_date).' days',
                'Percentage Complete'=>$data->percentage_done.'%',
                'Assigned To'=>UserPresenter::presentFullNames($data->assigned_to)
            ];
        })->toArray();
        Excel::create($project_phase->project->name.'_'.$project_phase->name.'_Work_Plan',function($excel) use ($phase,$activities){
            $excel->sheet('Work Plan', function($sheet) use ($phase) {
                $sheet->fromArray($phase);
            });
            $excel->sheet('Work Plan Issues', function($sheet) use ($activities) {
                $sheet->fromArray($activities);
            });
        })->download('xlsx');
    }

    /**
     * @param $id
     * @param $status
     * Approve project work plan
     */
    public function approveNew($id,$status)
    {
        $phase = ProjectPhase::findOrFail($id);
        $phase->update(['approval_status'=>$status]);
        //notify creator
        $phase->creator->notify(new ProjectPhaseApproved($phase));
    }

    /**
     * Reject New Phase
     * @param $id
     */
    public function rejectNew($id)
    {
        $phase = ProjectPhase::findOrFail($id);
        $phase->creator->notify(new ProjectPhaseRejected($phase));
        $phase->forceDelete();
    }

    /**
     * Approve Phase Update
     * @param $id
     */
    public function approveUpdated($id)
    {
        $phase = ProjectPhaseUpdates::findOrFail($id);
        ProjectPhase::where('id',$phase->project_phase)->update([
            'name'=>$phase->name,
            'start_date'=>$phase->start_date,
            'end_date'=>$phase->end_date,
            'decisions'=>$phase->decisions,
        ]);
        //notify creator
        $phase->creator->notify(new ProjectPhaseApproved($phase));
        $phase->forceDelete();
    }

    /**
     * Reject phase update
     * @param $id
     */
    public function rejectUpdated($id)
    {
        $phase = ProjectPhaseUpdates::findOrFail($id);
        $phase->creator->notify(new ProjectPhaseRejected($phase));
        $phase->forceDelete();
    }

    public function exportSummary(Request $request)
    {
       $summaries  = $this->projectPhasesRepository->summary($request);
       $data = [
           'summaries'=>$summaries,
           'start'=>$request->start_date,
           'end'=>$request->end_date
       ];
        $pdf = PDF::loadView('projects.exports.phase_summary', $data);
        return $pdf->download($request->start_date.'_'.$request->start_date.'Project_Work_Plan_Summaries.pdf');

    }
}
