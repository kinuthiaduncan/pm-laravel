<?php

namespace App\Http\Controllers\ProjectPhases;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use PM\Models\ProjectPhase;
use PM\Models\ProjectPhaseActivity;
use PM\Projects\ProjectPhasesRepository;

class PhaseActivitiesController extends Controller
{
    private $projectPhaseRepository;

    /**
     * PhaseActivitiesController constructor.
     * @param ProjectPhasesRepository $projectPhasesRepository
     */
    function __construct(ProjectPhasesRepository $projectPhasesRepository)
    {
        $this->projectPhaseRepository = $projectPhasesRepository;
    }

    /**
     * Show the form for creating a new resource.
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $phase = ProjectPhase::findOrFail($id);
        return view('projects.phases.activities.create',['phase'=>$phase]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return redirect
     */
    public function store(Request $request, $id)
    {
        $this->validate($request,[
           'name'=>'required|min:5',
            'start_date'=>'required',
            'end_date'=>'required',
            'percent_complete'=>'required|min:0|max:100'
        ]);
        $phase = ProjectPhase::findOrFail($id);
        //Validate that activity is not scheduled after the work plan due date
        if($request->end_date <= $phase->end_date) {
            //save work plan activity
            $this->projectPhaseRepository->saveActivity($request, $id);
            Flash::success('Project Phase Activity Created Successfully');
            return redirect('/project/' . $phase->project_id . '/phases/' . $id);
        }
        else{
            Flash::warning('You cannot add an activity to a project work plan after its due date');
            return redirect()->back()->withInput($request->all());
        }
    }

    /**
     * Show the form for editing an activity
     * @param $phase
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($phase,$id)
    {
        $activity = ProjectPhaseActivity::findOrFail($id);
        $phase = ProjectPhase::findOrFail($phase);
        return view('projects.phases.activities.edit',['activity'=>$activity,'phase'=>$phase]);
    }

    /**
     * Update the specified activity
     * @param  \Illuminate\Http\Request $request
     * @param $phase
     * @param  int $id
     * @return redirect
     */
    public function update(Request $request,$phase,$id)
    {
        $this->validate($request,[
            'name'=>'required|min:5',
            'start_date'=>'required',
            'end_date'=>'required',
            'percent_complete'=>'required|min:0|max:100'
        ]);
        $this->projectPhaseRepository->updateActivity($request,$phase,$id);
        $project_phase = ProjectPhase::findOrFail($phase);
        Flash::success('Project Phase Activity Updated Successfully');
        return redirect('/project/'.$project_phase->project_id.'/phases/'.$phase);
    }

    /**
     * Delete an activity
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $activity = ProjectPhaseActivity::findOrFail($id)->forceDelete();
        Flash::success('Project Phase Activity Deleted Successfully');
        return Redirect::back();
    }
}
