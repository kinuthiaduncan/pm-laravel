<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace App\Http\Controllers;

    use App\Mail\MilestoneCreation;
    use App\Mail\MilestoneEdited;
    use Illuminate\Support\Facades\Mail;
    use PM\Library\Mailer;
    use PM\Models\ActivityStream;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Validator;
    use Laracasts\Flash\Flash;
    use PM\Models\Milestone;
    use PM\Models\Project;
    use PM\Models\ProjectUser;
    use PM\Projects\ProjectRepository;
    use PM\Users\UserRepository;

    class MilestoneController extends Controller{

        /**
         * @param Mailer $mailer
         */
        public function __construct(UserRepository $userRepository, Mailer $mailer, ProjectRepository $projectRepository)
        {
            $this->mailer=$mailer;
            $this->userRepository=$userRepository;
            $this->projectRepository=$projectRepository;
        }

        /**
         * Function to create a project milestone
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function createMilestone($id)
        {
            $project=Project::find($id);

            return view('milestones.create', ['project'=>$project, 'title'=>'Project: ' . $project->name]);
        }

        /**
         * Function to save a milestone
         * @return $this|\Illuminate\Http\RedirectResponse
         */
        public function milestoneStore()
        {
            $input=Input::all();

            $rules=['title'=>'required', 'description'=>'required', 'due_date'=>'required|date'];

            $validator=Validator::make(array('title'=>$input['title'], 'description'=>$input['description'], 'due_date'=>$input['due_date']), $rules);

            if ($validator->passes())
            {
                $input['created_by']=Auth::user()->id;
                $milestone=Milestone::create($input);
                Flash::success('Milestone created successfully');

                //Log this activity
                $created_milestone=Milestone::where('title', $input['title'])->where('description', $input['description'])->where('project_id', $input['project_id'])->first();
                $log['path']='/project/' . $created_milestone->project_id;
                $log['activity_by']=Auth::user()->id;
                $log['activity_description']=$created_milestone->title . ' milestone created';
                $log['project_id']=$created_milestone->project_id;
                ActivityStream::create($log);

                //send mail
                $project=Project::find($input['project_id']);
                $project->milestone_title=$created_milestone->title;
                $project->milestone_description=$created_milestone->description;
                $project->milestone_date=$created_milestone->due_date;
                $project->milestone_id=$created_milestone->id;
                $project_members=ProjectUser::where('project_id', $input['project_id'])->get();
                foreach ($project_members as $project_member)
                {
                    $project->member_firstname=$project_member->members->preferred_name;
                    $project->member_email=$project_member->members->email;

//                    $this->mailer->milestoneCreationNotifier($project);

                    Mail::to( $project->member_email)
                        ->queue(new MilestoneCreation($milestone));
                }

                //Add the project creator and the project lead as project members
                $creator['user_id']=Auth::user()->id;
                $creator['project_id']=$input['project_id'];
                $members=ProjectUser::where('user_id', $creator['user_id'])->where('project_id', $creator['project_id'])->get();
                if (count($members) == 0)
                {
                    ProjectUser::create($creator);
                }

                return redirect()->to('/project/' . $input['project_id']);
            } else
            {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        /**
         * Function to display milestone edit form
         * @param $project_id
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function editMilestone($project_id, $id)
        {
            $project=Project::find($project_id);
            $milestone=Milestone::find($id);

            return view('milestones.edit', ['project'=>$project, 'milestone'=>$milestone, 'title'=>'Project: ' . $project->name]);
        }

        /**
         * Function to update a milestone
         * @return $this|\Illuminate\Http\RedirectResponse
         */
        public function milestoneUpdate()
        {
            $input=Input::all();

            $rules=['title'=>'required', 'description'=>'required', 'due_date'=>'required|date'];

            $validator=Validator::make(array('title'=>$input['title'], 'description'=>$input['description'], 'due_date'=>$input['due_date']), $rules);

            if ($validator->passes())
            {
                $input['created_by']=Auth::user()->id;
                $milestone=Milestone::find($input['milestone_id']);
                $original_milestone=$milestone->title;
                $milestone->update($input);
                Flash::success('Milestone edited successfully');

                //Log this activity
                $created_milestone=Milestone::where('title', $input['title'])->where('description', $input['description'])->where('project_id', $input['project_id'])->first();
                $log['path']='/project/' . $created_milestone->project_id;
                $log['activity_by']=Auth::user()->id;
                $log['activity_description']=$original_milestone . ' milestone edited';
                $log['project_id']=$created_milestone->project_id;
                ActivityStream::create($log);

                //send mail
                $project=Project::find($input['project_id']);
                $project->milestone_title=$created_milestone->title;
                $project->milestone_description=$created_milestone->description;
                $project->milestone_date=$created_milestone->due_date;
                $project->milestone_id=$created_milestone->id;
                $project_members=ProjectUser::where('project_id', $input['project_id'])->get();
                foreach ($project_members as $project_member)
                {
                    $project->member_firstname=$project_member->members->preferred_name;
                    $project->member_email=$project_member->members->email;

                    Mail::to( $project->member_email)
                        ->queue(new MilestoneEdited($milestone));
                }

                return redirect()->to('/project/' . $input['project_id']);
            } else
            {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }
    }