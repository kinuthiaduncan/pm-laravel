<?php
    /**
     * Created by PhpStorm.
     * User: daniel
     * Date: 11/25/16
     * Time: 11:00 AM
     */

    namespace App\Http\Controllers;

    use App\Mail\ComponentCreationNotifier;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\Response;
    use Illuminate\Support\Facades\Validator;
    use Laracasts\Flash\Flash;
    use PM\ComponentUsers\ComponentUserRepository;
    use PM\Library\Mailer;
    use PM\Models\ActivityStream;
    use PM\Models\Project;
    use PM\Models\ProjectComponent;
    use PM\Models\ProjectUser;


    class ComponentsController extends Controller
    {
        /**
         * ADD Project mailer
         *
         * @param Mailer $mailer
         */
        protected $componentUserRepository;

        public function __construct(Mailer $mailer,
                                    ComponentUserRepository $componentUserRepository)
        {
            $this->mailer = $mailer;
            $this->componentUserRepository = $componentUserRepository;
        }

        /*
         * delete a component user
         */
        public function deleteComponentUser($componentId, $componentUser)
        {
            $deleted = $this->componentUserRepository->delete($componentId, $componentUser);

            return Response::json($deleted);
        }

        /**
         * Save a new project component
         *
         * @return $this|\Illuminate\Http\RedirectResponse
         */
        public function store()
        {
            $input = array_except(request()->all(), ['_token']);

            $userId = Auth::user()->id;

            $rules = ['component_name' => 'required', 'description' => 'required', 'component_lead' => 'required'];

            $validator = Validator::make(array('component_name' => $input['component_name'], 'description' => $input['description'], 'component_lead' => $input['component_lead']), $rules);

            if ($validator->passes()) {
                $project = Project::find($input['project_id']);

                $input['created_by'] = $userId;

                if (isset($input['component_id'])) {
                    $projectComponent = ProjectComponent::find($input['component_id']);

                    $projectComponent->component_name = $input['component_name'];

                    $projectComponent->description = $input['description'];

                    $projectComponent->component_lead = $input['component_lead'];

                    $projectComponent->update();

                    if ($input['component_users'] != '') {
                        $this->componentUserRepository->save($input['component_users'], $projectComponent);
                    }

                    Flash::message('Project component successfully updated');
                } else {
                    $projectComponent = ProjectComponent::create($input);

                    if ($input['component_users'] != '') {
                        $this->componentUserRepository->save($input['component_users'], $projectComponent);
                    }

                    Flash::message('Project component successfully created');
                }

                //Log this activity if Project is Public
                if ($project->project_access == 'public') {
                    $log['path'] = '/project/' . $input['project_id'];
                    $log['activity_by'] = $userId;
                    $log['activity_description'] = strip_tags($input['component_name']) . ' project component created';
                    $log['project_id'] = $input['project_id'];
                    ActivityStream::create($log);
                }

                //send mail to project users
                $project_members = ProjectUser::where('project_id', $input['project_id'])->get();
                foreach ($project_members as $project_member) {
                    $projectComponent['member_firstname'] = $project_member->members->preferred_name;
                    $projectComponent['member_email'] = $project_member->members->email;
                    Mail::to($project_member->members->email)->queue(new ComponentCreationNotifier($projectComponent));
                }

                return redirect()->to('/project/' . $input['project_id']);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }
    }