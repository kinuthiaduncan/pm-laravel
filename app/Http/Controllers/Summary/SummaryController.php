<?php
    /**
     * Date: 28/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Summary;


    use App\Http\Controllers\Controller;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use PDF;
    use PM\Models\Follow;
    use PM\Models\Issue;
    use PM\Models\Task;
    use PM\Models\TaskTracking;
    use PM\Models\User;
    use PM\Users\UserRepository;

    class SummaryController extends Controller
    {
        /**
         * @var UserRepository
         */
        private $userRepository;

        /**
         * SummaryController constructor.
         * @param UserRepository $userRepository
         */
        public function __construct(UserRepository $userRepository)
        {
            $this->userRepository = $userRepository;
        }

        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $users = $this->userRepository->allUsersSelectId();

            return view('summary.index', ['users' => $users]);
        }

        /**
         * Fetch the summary
         * @param Request $request
         * @return array
         */
        public function getSummary(Request $request)
        {
            $id = $request->get('selected_user');

            if (!is_null($id)) {
                $user = User::where('id', $id)->first();
                $user->department = $user->department->name;
                $departmentID = User::where('id',$id)->pluck('department_id');
                $users = User::where('department_id',$departmentID)->pluck('id');
            } else {
                $user = User::where('id', Auth::id())->first();
                $user->department = $user->department->name;
                $departmentID = User::where('id',$user->id)->pluck('department_id');
                $users = User::where('department_id',$departmentID)->pluck('id');
            }
            //follow statistics
            $following = count(Follow::where('follower_id', $user->id)->get());
            $followers = count(Follow::where('followed_id', $user->id)->get());

            //Tasks statistics
            $userTasks = Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);             })
                ->orWhere('created_by', $user->id)
                ->latest()->limit(3)
                ->get();

            $team_total_tasks = count(Task::where('department_id',$user->department_id)->get());

            $team_total_tasks = count(Task::whereHas('assignedUsers',function($assignedUser) use ($users){
                $assignedUser->whereIn('user_id',$users);
            })->get());

            $team_completed_tasks = count(Task::where('status_id', '4')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->orWhere('status_id', '5')->get());

            $team_ongoing_tasks = count(Task::where('status_id', '2')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->get());

            $team_pending_tasks = count(Task::where('status_id', '1')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->orWhere('status_id', '5')->get());

            $team_overdue_tasks = count(Task::where('status_id', '<=', '2')->where('due_date', '<', Carbon::today())
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->get());

            $team_review_tasks = count(Task::where('status_id', '3')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->orWhere('status_id', '5')->get());

            $review_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 3)->get());

            $total_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->get());

            $pending_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 1)->get());

            $completed_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 4)->orWhere('status_id', '5')->get());

            $ongoing_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 2)->get());

            $overdue_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', '<=', '2')->where('due_date', '<', Carbon::today())->get());

            //Issue statistics
            $userIssues = Issue::where('assigned_to', $user->id)->orWhere('created_by', $user->id)->latest()->limit(3)->get();
            $team_total_issues = count(Issue::all());
            $team_completed_issues = count(Issue::where('status_id', '4')->orWhere('status_id', '5')->get());
            $team_ongoing_issues = count(Issue::where('status_id', '2')->get());
            $team_pending_issues = count(Issue::where('status_id', '1')->orWhere('status_id', '5')->get());
            $team_overdue_issues = count(Issue::where('status_id', '<', '2')->where('date_due', '<', Carbon::today())->get());
            $team_review_issues = count(Issue::where('status_id', '3')->orWhere('status_id', '5')->get());
            $review_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 3)->get());
            $total_issues = count(Issue::where('assigned_to', $user->id)->get());
            $pending_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 1)->get());
            $completed_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 4)->orWhere('status_id', '5')->get());
            $ongoing_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 2)->get());
            $overdue_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', '<', '2')->where('date_due', '<', Carbon::today())->get());


            $data = ['user' => $user,
                'team_total_issues' => $team_total_issues,
                'team_completed_issues' => $team_completed_issues,
                'team_ongoing_issues' => $team_ongoing_issues,
                'team_review_issues' => $team_review_issues,
                'team_pending_issues' => $team_pending_issues,
                'team_overdue_issues' => $team_overdue_issues,
                'team_total_tasks' => $team_total_tasks,
                'team_completed_tasks' => $team_completed_tasks,
                'team_ongoing_tasks' => $team_ongoing_tasks,
                'team_review_tasks' => $team_review_tasks,
                'team_pending_tasks' => $team_pending_tasks,
                'team_overdue_tasks' => $team_overdue_tasks,
                'total_issues' => $total_issues,
                'completed_issues' => $completed_issues,
                'ongoing_issues' => $ongoing_issues,
                'review_issues' => $review_issues,
                'pending_issues' => $pending_issues,
                'overdue_issues' => $overdue_issues,
                'total_tasks' => $total_tasks,
                'completed_tasks' => $completed_tasks,
                'ongoing_tasks' => $ongoing_tasks,
                'review_tasks' => $review_tasks,
                'pending_tasks' => $pending_tasks,
                'overdue_tasks' => $overdue_tasks,
                'following' => $following,
                'followers' => $followers,
                'user_issues' => $userIssues,
                'user_tasks' => $userTasks,
            ];

            return $data;
        }

        public function exportSummary($id = null)
        {
            $user = User::where('id', $id)->first();
            $user->department = $user->department->name;
            $departmentID = User::where('id',$id)->pluck('department_id');
            $users = User::where('department_id',$departmentID)->pluck('id');
            //follow statistics
            $following = count(Follow::where('follower_id', $user->id)->get());
            $followers = count(Follow::where('followed_id', $user->id)->get());

            //Tasks statistics
            $userTasks = Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->orWhere('created_by', $user->id)->latest()->limit(3)->get();

            $team_total_tasks = count(Task::whereHas('assignedUsers',function($assignedUser) use ($users){
                $assignedUser->whereIn('user_id',$users);
            })->get());

            $team_completed_tasks = count(Task::where('status_id', '4')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->orWhere('status_id', '5')->get());

            $team_ongoing_tasks = count(Task::where('status_id', '2')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->get());

            $team_pending_tasks = count(Task::where('status_id', '1')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->orWhere('status_id', '5')->get());

            $team_overdue_tasks = count(Task::where('status_id', '<=', '2')->where('due_date', '<', Carbon::today())
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->get());

            $team_review_tasks = count(Task::where('status_id', '3')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->orWhere('status_id', '5')->get());
            $review_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 3)->get());

            $total_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->get());

            $pending_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 1)->get());

            $completed_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 4)->orWhere('status_id', '5')->get());

            $ongoing_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 2)->get());

            $overdue_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', '<=', '2')->where('due_date', '<', Carbon::today())->get());

            //Issue statistics
            $userIssues = Issue::where('assigned_to', $user->id)->orWhere('created_by', $user->id)->latest()->limit(3)->get();
            $team_total_issues = count(Issue::all());
            $team_completed_issues = count(Issue::where('status_id', '4')->orWhere('status_id', '5')->get());
            $team_ongoing_issues = count(Issue::where('status_id', '2')->get());
            $team_pending_issues = count(Issue::where('status_id', '1')->orWhere('status_id', '5')->get());
            $team_overdue_issues = count(Issue::where('status_id', '<', '2')->where('date_due', '<', Carbon::today())->get());
            $team_review_issues = count(Issue::where('status_id', '3')->orWhere('status_id', '5')->get());
            $review_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 3)->get());
            $total_issues = count(Issue::where('assigned_to', $user->id)->get());
            $pending_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 1)->get());
            $completed_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 4)->orWhere('status_id', '5')->get());
            $ongoing_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 2)->get());
            $overdue_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', '<', '2')->where('date_due', '<', Carbon::today())->get());


            $data = ['user' => $user,
                'team_total_issues' => $team_total_issues,
                'team_completed_issues' => $team_completed_issues,
                'team_ongoing_issues' => $team_ongoing_issues,
                'team_review_issues' => $team_review_issues,
                'team_pending_issues' => $team_pending_issues,
                'team_overdue_issues' => $team_overdue_issues,
                'team_total_tasks' => $team_total_tasks,
                'team_completed_tasks' => $team_completed_tasks,
                'team_ongoing_tasks' => $team_ongoing_tasks,
                'team_review_tasks' => $team_review_tasks,
                'team_pending_tasks' => $team_pending_tasks,
                'team_overdue_tasks' => $team_overdue_tasks,
                'total_issues' => $total_issues,
                'completed_issues' => $completed_issues,
                'ongoing_issues' => $ongoing_issues,
                'review_issues' => $review_issues,
                'pending_issues' => $pending_issues,
                'overdue_issues' => $overdue_issues,
                'total_tasks' => $total_tasks,
                'completed_tasks' => $completed_tasks,
                'ongoing_tasks' => $ongoing_tasks,
                'review_tasks' => $review_tasks,
                'pending_tasks' => $pending_tasks,
                'overdue_tasks' => $overdue_tasks,
                'following' => $following,
                'followers' => $followers,
                'user_issues' => $userIssues,
                'user_tasks' => $userTasks,
            ];

//         dd($data['user']->id);
            view()->share('data', $data);
            $pdf = PDF::loadView('users.exports.userSummaryReport');

            return $pdf->download('User Summary.pdf');
        }

        /**
         * Display user profile
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function userSummary($id)
        {
            $user = User::where('id', $id)->first();
            $user->department = $user->department->name;
            $user->name = $user->preferred_name;
            $users = $this->userRepository->allUsersSelectId();

            return view('users.user_profile', ['user' => $user, 'id' => $id, 'users' => $users,]);
        }

        /**
         * Fetch user profile data
         * @param $id
         * @return array
         */
        public function getUserSummary($id)
        {
            $user = User::where('id', $id)->first();
            $user->department = $user->department->name;
            $user->name = $user->preferred_name;

            $departmentID = User::where('id',$id)->pluck('department_id');
            $users = User::where('department_id',$departmentID)->pluck('id');

            //Follow statistics
            $following = count(Follow::where('follower_id', $user->id)->get());
            $followers = count(Follow::where('followed_id', $user->id)->get());

            //Tasks statistics
            $userTasks = Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', '<', '2')->orWhere('created_by', $user->id)->latest()->limit(3)->get();

            $team_total_tasks = count(Task::whereHas('assignedUsers',function($assignedUser) use ($users){
                $assignedUser->whereIn('user_id',$users);
            })->get());

            $team_completed_tasks = count(Task::where('status_id', '4')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                $assignedUser->whereIn('user_id',$users);
            })->orWhere('status_id', '5')->get());

            $team_ongoing_tasks = count(Task::where('status_id', '2')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                $assignedUser->whereIn('user_id',$users);
            })->get());

            $team_pending_tasks = count(Task::where('status_id', '1')
                    ->whereHas('assignedUsers',function($assignedUser) use ($users){
                        $assignedUser->whereIn('user_id',$users);
                    })->orWhere('status_id', '5')->get());

            $team_overdue_tasks = count(Task::where('status_id', '<=', '2')->where('due_date', '<', Carbon::today())
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->get());

            $team_review_tasks = count(Task::where('status_id', '3')
                ->whereHas('assignedUsers',function($assignedUser) use ($users){
                    $assignedUser->whereIn('user_id',$users);
                })->orWhere('status_id', '5')->get());

            $review_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', '<', '2')->where('status_id', 3)->get());

            $total_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->get());

            $pending_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 1)->get());

            $completed_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 4)->orWhere('status_id', '5')->get());

            $ongoing_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', 2)->get());

            $overdue_tasks = count(Task::whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->where('status_id', '<=', '2')->where('due_date', '<', Carbon::today())->get());

            //Issue statistics
            $userIssues = Issue::where('assigned_to', $user->id)->orWhere('created_by', $user->id)->latest()->limit(3)->get();
            $team_total_issues = count(Issue::all());
            $team_completed_issues = count(Issue::where('status_id', '4')->orWhere('status_id', '5')->get());
            $team_ongoing_issues = count(Issue::where('status_id', '2')->get());
            $team_pending_issues = count(Issue::where('status_id', '1')->orWhere('status_id', '5')->get());
            $team_overdue_issues = count(Issue::where('status_id', '<', '2')->where('date_due', '<', Carbon::today())->get());
            $team_review_issues = count(Issue::where('status_id', '3')->orWhere('status_id', '5')->get());
            $review_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 3)->get());
            $total_issues = count(Issue::where('assigned_to', $user->id)->get());
            $pending_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 1)->get());
            $completed_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 4)->orWhere('status_id', '5')->get());
            $ongoing_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', 2)->get());
            $overdue_issues = count(Issue::where('assigned_to', $user->id)->where('status_id', '<', '2')->where('date_due', '<', Carbon::today())->get());

            $data = ['user' => $user,
                'team_total_issues' => $team_total_issues,
                'team_completed_issues' => $team_completed_issues,
                'team_ongoing_issues' => $team_ongoing_issues,
                'team_review_issues' => $team_review_issues,
                'team_pending_issues' => $team_pending_issues,
                'team_overdue_issues' => $team_overdue_issues,
                'team_total_tasks' => $team_total_tasks,
                'team_completed_tasks' => $team_completed_tasks,
                'team_ongoing_tasks' => $team_ongoing_tasks,
                'team_review_tasks' => $team_review_tasks,
                'team_pending_tasks' => $team_pending_tasks,
                'team_overdue_tasks' => $team_overdue_tasks,
                'total_issues' => $total_issues,
                'completed_issues' => $completed_issues,
                'ongoing_issues' => $ongoing_issues,
                'review_issues' => $review_issues,
                'pending_issues' => $pending_issues,
                'overdue_issues' => $overdue_issues,
                'total_tasks' => $total_tasks,
                'completed_tasks' => $completed_tasks,
                'ongoing_tasks' => $ongoing_tasks,
                'review_tasks' => $review_tasks,
                'pending_tasks' => $pending_tasks,
                'overdue_tasks' => $overdue_tasks,
                'following' => $following,
                'followers' => $followers,
                'user_issues' => $userIssues,
                'user_tasks' => $userTasks];

            return $data;
        }

        /**
         * Export users daily summary
         * @param $id
         * @return mixed
         */
        public function userDailySummary($id){
            $user = User::where('id', $id)->first();
            $user->name = $user->preferred_name;
            $today = Carbon::today()->toDateString();
            $tomorrow = Carbon::today()->addDays(1)->toDateString();

            if($user->department_id == 2)
            {
                $data = Issue::where('created_at','>=',$today)->where('created_at','<=',$tomorrow)
                    ->where('assigned_to', $id)
                    ->get();

                $review = Issue::where('status_change_date',Carbon::today()->format('Y-m-d'))
                    ->where('assigned_to', $id)->where('status_id',3)->get();

                $done = Issue::where('status_change_date',Carbon::today()->format('Y-m-d'))
                    ->where('assigned_to', $id)->where('status_id',4)->get();
//
                $pdf = PDF::loadView('issues.exports.dailyIssuesReport',compact('data',$data,'user',$user,'review',$review,'done',$done));
                return $pdf->download($user->name.' Daily Issue Report.pdf');
            }
            else
                {

                $data = Task::where('created_at','>=',$today)->where('created_at','<=',$tomorrow)
                    ->whereHas('assignedUsers', function ($assigned) use ($user){
                        $user = Auth::user();
                        $assigned->where('user_id',$user->id);
                    })
                    ->get();

                    $logged = TaskTracking::where('created_at','>=',$today)->where('created_at','<',$tomorrow)
                    ->where('created_by',$user->id)->get();

                $pdf = PDF::loadView('tasks.exports.dailyTaskReport',compact('data',$data,'logged',$logged, 'user',$user));
                return $pdf->download($user->name.' Daily Task Report.pdf');
            }

        }

        /**
         * Export user weekly summary
         * @param $id
         * @return mixed
         */
        public function userWeeklySummary($id)
        {
            $user = User::where('id', $id)->first();
            $user->name = $user->preferred_name;
            $weekStart = Carbon::now()->startOfWeek();
            $weekEnd = Carbon::now()->endOfWeek();

            if($user->department_id == 2)
            {
                $data = Issue::where('created_at','>=',$weekStart)->where('created_at','<=',$weekEnd)
                    ->where('assigned_to', $id)
                    ->get();

                $review = Issue::where('status_change_date',Carbon::today()->format('Y-m-d'))
                    ->where('assigned_to', $id)->where('status_id',3)->get();

                $done = Issue::where('status_change_date',Carbon::today()->format('Y-m-d'))
                    ->where('assigned_to', $id)->where('status_id',4)->get();


                $pdf = PDF::loadView('issues.exports.weeklyIssuesReport',compact('data',$data, 'user',$user,'review',$review,'done',$done));

                return $pdf->download($user->name.' Weekly Issues Report.pdf');
            }
            else{

                $startDate = Carbon::today()->startOfWeek()->toDateString();
                $endDate = Carbon::today()->endOfWeek()->toDateString();

                $data = Task::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)
                    ->whereHas('assignedUsers', function ($assigned) use ($user){
                        $user = Auth::user();
                        $assigned->where('user_id',$user->id);
                    })
                    ->get();

                $logged = TaskTracking::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)
                    ->where('created_by',$user->id)->groupBy('task_id')->get();

                $totalHours = $logged->map(function($totalHour) {
                    $diff = (Carbon::parse($totalHour->start_time)->diffInMinutes(Carbon::parse($totalHour->end_time)))/60;
                    return $diff;
                })->sum();


                $pdf = PDF::loadView('tasks.exports.weeklyTaskReport',compact('data',$data,'logged',$logged,'user',$user,'totalHours',$totalHours));

                return $pdf->download($user->name.' Weekly Task Report.pdf');
            }
        }
    }