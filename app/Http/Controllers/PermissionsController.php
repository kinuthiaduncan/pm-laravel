<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use PM\Models\Permissions;
    use PM\Models\User;
    use PM\Models\UserPermissions;
    use PM\Users\UserRepository;

    class PermissionsController extends Controller
    {
        private $userRepository;

        public function __construct(UserRepository $userRepository)
        {
            $this->userRepository = $userRepository;
        }


        /**
         * View a users permissions
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function show($id)
        {
            $all_permissions = $roles = $this->userRepository->getPermissionsSelect();
            $user = User::where('id', $id)->first();
            $permissions = UserPermissions::where('user_id', $id)->paginate(5);

            return view('authorization.permissions.view', ['user' => $user, 'permissions' => $permissions, 'all_permissions' => $all_permissions]);
        }

        /**
         * Assign permissions to a user
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function assign(Request $request)
        {
            $id = $request->input('id');

            UserPermissions::create(['user_id' => $id,
                'permission_id' => $request->input('permission_id')]);

            return redirect('permissions/'. $id);
        }


        /**
         * Delete a users permission
         * @param $id
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function deleteUserPermission($id)
        {
            UserPermissions::findorFail($id)->forceDelete();

            return redirect('authorization');
        }

    }
