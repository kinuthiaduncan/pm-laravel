<?php

namespace App\Http\Controllers\Tasks;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Laracasts\Flash\Flash;
use PM\Models\IssueTracking;
use PM\Models\Task;
use PM\Models\TaskTracking;
use PM\Models\User;
use PM\Tasks\TasksRepository;
use PM\Tasks\TaskTrackingRepository;
use PM\Users\UserRepository;

class TaskTimelineController extends Controller
{
    private $tasksRepository;
    private $taskTrackingRepository;
    private $userRepository;

    /**
     * TaskTimelineController constructor.
     * @param TasksRepository $tasksRepository
     * @param TaskTrackingRepository $taskTrackingRepository
     * @param UserRepository $userRepository
     */
    function __construct(TasksRepository $tasksRepository, TaskTrackingRepository $taskTrackingRepository, UserRepository $userRepository)
    {
        $this->tasksRepository = $tasksRepository;
        $this->taskTrackingRepository = $taskTrackingRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $users = $this->userRepository->allUsersSelect();
        $events = $this->taskTrackingRepository->fetchAllSchedules($user);

        return view('tasks.timeline.index',['events'=>$events,'users'=>$users]);
    }

    /**
     * Specific user events
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $user = User::findOrFail($request['user_id']);
        $users = $this->userRepository->allUsersSelect();
        $events = $this->taskTrackingRepository->fetchAllSchedules($user);

        return view('tasks.timeline.index',['events'=>$events,'users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::id();
        $tasks = $this->tasksRepository->getTasks($user);

        return view('tasks.timeline.create',['tasks'=>$tasks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'task_id'=>'required',
            'date'=>'required',
            'start_time'=>'required|start_time_validator:date,end_time',
            'end_time'=>'required|end_time_validator:start_time'
        ]);

        $task_id = $request->input('task_id');
        $task = Task::findOrFail($task_id);
        if ( $task->assignedUsers->pluck('user_id')->contains(Auth::id()) || Auth::id() == $task->created_by) {
            $this->taskTrackingRepository->save($request, $task_id);

            Flash::success('The task schedule has been created successfully');
            return redirect('/tasks/timelines');
        }
        else
        {
            Flash::warning('You are not allowed to add a Time Schedule');
            return Redirect::back();
        }
    }
}
