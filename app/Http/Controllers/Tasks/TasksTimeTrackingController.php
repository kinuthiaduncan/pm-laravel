<?php

namespace App\Http\Controllers\Tasks;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use PM\Models\Task;
use PM\Models\TaskTracking;
use PM\Tasks\TaskTrackingRepository;


class TasksTimeTrackingController extends Controller
{
    private $taskTrackingRepository;

    /**
     * TasksTimeTrackingController constructor.
     * @param TaskTrackingRepository $taskTrackingRepository
     */
    function __construct(TaskTrackingRepository $taskTrackingRepository)
    {
        $this->taskTrackingRepository = $taskTrackingRepository;
    }

    /**
     * @param $task_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($task_id)
    {
       $tracks = TaskTracking::where('task_id',$task_id)->latest()->paginate(10);
       $task = Task::findOrFail($task_id);
       $today = Carbon::today()->toDateString();

       return view('tasks.tracking.index',['tracks'=>$tracks,'task'=>$task,'today'=>$today]);
    }

    /**
     * Show task time create form
     * @param $task_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($task_id)
    {
        return view('tasks.tracking.create',['task_id'=>$task_id]);
    }


    /**
     * Store time
     * @param Request $request
     * @param $task_id
     * @return Redirect
     */
    public function store(Request $request, $task_id)
    {

        $task = Task::findOrFail($task_id);

        if ( $task->assignedUsers->pluck('user_id')->contains(Auth::id()) || Auth::id() == $task->created_by) {
        $this->validate($request,[
            'date'=>'required',
            'start_time'=>'required|start_time_validator:date,end_time',
            'end_time'=>'required|end_time_validator:start_time'
        ]);

        $this->taskTrackingRepository->save($request,$task_id);

        Flash::success('The task schedule has been created successfully');
            return redirect('/tasks/task/'.$task_id.'/track');
        }
        else {
            Flash::warning('You are not allowed to add a Time Schedule');
            return Redirect::back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *  @param  int  $track
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$track)
    {
        $task = Task::where('id',$id)->first();
        $schedule = TaskTracking::where('id',$track)->first();

        return view('tasks.tracking.edit',['schedule'=>$schedule, 'task'=>$task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return Redirect
     */
    public function update(Request $request, $id,$task)
    {
        $data = $request->except('_token','_method');
        TaskTracking::where('id',$id)->update($data);

        Flash::success('Updated Successfully');

        return redirect('/tasks/task/'.$task.'/track');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *@param  int $task
     * @return Redirect
     */
    public function delete($task,$id)
    {
        TaskTracking::findOrFail($id)->forceDelete();

        Flash::success('Schedule deleted successfully');
        return redirect('/tasks/task/'.$task.'/track');
    }
}
