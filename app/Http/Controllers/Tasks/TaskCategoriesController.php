<?php
    /**
     * Date: 20/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Tasks;


    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Redirect;
    use Laracasts\Flash\Flash;
    use PM\Departments\DepartmentsRepository;
    use PM\Models\TaskCategory;
    use PM\Tasks\TaskCategoriesRepository;

    class TaskCategoriesController extends Controller
    {
        /**
         * @var TaskCategoriesRepository
         */
        private $taskCategoriesRepository;
        private $departmentsRepository;

        /**
         * TaskCategoriesController constructor.
         * @param TaskCategoriesRepository $taskCategoriesRepository
         * @param DepartmentsRepository $departmentsRepository
         */
        public function __construct(TaskCategoriesRepository $taskCategoriesRepository, DepartmentsRepository $departmentsRepository)
        {
            $this->taskCategoriesRepository = $taskCategoriesRepository;
            $this->departmentsRepository = $departmentsRepository;
        }

        /**
         * Display all categories
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $categories = TaskCategory::paginate(10);
            $departments = $this->departmentsRepository->getDepartments();
            return view('tasks.categories.index', ['categories' => $categories,'departments'=>$departments]);
        }

        /**
         * Display create category page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function create()
        {
            return view('tasks.categories.create');
        }

        /**
         * store a category
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function store()
        {
            $request = request()->all();

            $this->taskCategoriesRepository->save($request);

            Flash::success('Category Created Successfully');

            return redirect()->route('task_category.index');
        }

        /**
         * Display edit category page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function edit($id)
        {
            $category = TaskCategory::findOrFail($id);

            return view('tasks.categories.edit', ['category'=>$category]);
        }

        /**
         * update a category
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function update($id)
        {
            $request = request()->all();

            $this->taskCategoriesRepository->edit($request, $id);

            Flash::success('Category Updated Successfully');

            return redirect()->route('task_category.index');
        }

        public function delete($id)
        {
            TaskCategory::findOrFail($id)->forceDelete();
            Flash::success('Task Category successfully deleted');
            return Redirect::back();
        }

    }