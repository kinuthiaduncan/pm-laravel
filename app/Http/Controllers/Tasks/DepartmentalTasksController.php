<?php
    /**
     * Date: 01/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Tasks;


    use App\Http\Controllers\Controller;
    use PM\Departments\DepartmentsRepository;
    use PM\Issues\IssueRepository;
    use PM\Tasks\TasksRepository;
    use PM\Users\UserRepository;

    class DepartmentalTasksController extends Controller
    {
        /**
         * @var TasksRepository
         */
        private $tasksRepository;
        /**
         * @var UserRepository
         */
        private $userRepository;
        /**
         * @var IssueRepository
         */
        private $issueRepository;
        /**
         * @var DepartmentsRepository
         */
        private $departmentsRepository;

        /**
         * DepartmentalTasksController constructor.
         * @param TasksRepository $tasksRepository
         * @param UserRepository $userRepository
         * @param IssueRepository $issueRepository
         * @param DepartmentsRepository $departmentsRepository
         */
        public function __construct(TasksRepository $tasksRepository, UserRepository $userRepository, IssueRepository $issueRepository, DepartmentsRepository $departmentsRepository)
        {
            $this->tasksRepository = $tasksRepository;
            $this->userRepository = $userRepository;
            $this->issueRepository = $issueRepository;
            $this->departmentsRepository = $departmentsRepository;
        }

        /**
         * Display all public tasks
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $status = $this->issueRepository->getStatusTypesSelect();
            $users = $this->userRepository->allUsersSelect();
            $departments = $this->departmentsRepository->getDepartments();

            return view('tasks.departments.index', ['status'=>$status, 'departments'=>$departments, 'users'=>$users]);
        }
    }