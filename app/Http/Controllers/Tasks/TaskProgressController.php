<?php
    /**
     * Date: 28/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Tasks;


    use App\Http\Controllers\Controller;
    use App\Mail\TaskProgressReport;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Mail;
    use Laracasts\Flash\Flash;
    use PM\Models\Task;
    use PM\Models\TaskProgress;
    use PM\Models\TaskUserGroup;

    class TaskProgressController extends Controller
    {
        /**
         * Display the progress reports filed
         * @param $task_id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         *
         */
        public function index($task_id)
        {
            $progress = TaskProgress::where('task_id', $task_id)->latest()->paginate(10);

            $task = Task::findOrFail($task_id);

            return view('tasks.progress.index', ['task' => $task, 'progress' => $progress]);
        }

        /**
         * Store progress report
         * @param $task_id
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function store($task_id, Request $request)
        {
            $task = Task::findOrFail($task_id);
            if ( $task->assignedUsers->pluck('user_id')->contains(Auth::id()) || Auth::id() == $task->created_by) {
                $this->validate($request, [
                    'title' => 'required|max:255',
                    'description' => 'nullable',
                    'percentage_done' => 'nullable|numeric|max:100|min:0',
                ]);

                $request = $request->all();

                $request['created_by'] = Auth::id();
                $request['task_id'] = $task_id;

                $progress = TaskProgress::create($request);
                $assignor = $task->creator;

                $assignor->notify(new \App\Notifications\TaskProgressReport($progress));

                $user_groups = TaskUserGroup::where('task_id', $task->id)->where('user_id', '!=', $task->assigned_to)->get();
                $user_group_emails = $user_groups->map(function ($user_group) {
                    return $user_group->users->email;
                })->toArray();

                Mail::to($user_group_emails)
                    ->queue(new TaskProgressReport($progress));

                return redirect()->back();
            } else {
                Flash::warning('You are not allowed to add a progress report');

                return redirect()->back();
            }
        }

        /**
         * Update progress report
         * @param Request $request
         * @param $task_id
         * @param $id
         * @return \Illuminate\Http\RedirectResponse
         */
        public function update(Request $request,$task_id)
        {
            $task = Task::findOrFail($task_id);

            if (Auth::id() == $task->assigned_to || Auth::id() == $task->created_by) {
                $this->validate($request, [
                    'title' => 'required|max:255',
                    'description' => 'nullable'
                ]);

                $task_progress = TaskProgress::findOrFail($request->id);

                $request = $request->all();

                $task_progress->update($request);

                return redirect()->back();
            } else {
                Flash::warning('You are not allowed to add a progress report');

                return redirect()->back();
            }
        }
    }