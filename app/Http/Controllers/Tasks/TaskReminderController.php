<?php
    /**
     * Date: 01/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Tasks;


    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use PM\Library\Mailer;
    use PM\Models\Task;
    use PM\Models\TaskReminder;

    class TaskReminderController extends Controller
    {
        /**
         * @var Mailer
         */
        private $mailer;

        /**
         * TaskReminderController constructor.
         * @param Mailer $mailer
         */
        public function __construct(Mailer $mailer)
        {
            $this->mailer = $mailer;
        }

        /**
         * Display all reminders
         * @param $task_id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index($task_id)
        {
            $task_reminders = TaskReminder::where('task_id', $task_id)->latest()->paginate(21);

            return view('tasks.reminders.index', ['task_id' => $task_id, 'task_reminders' => $task_reminders]);
        }

        /**
         * Store a reminder
         * @param $task_id
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function store($task_id, Request $request)
        {
            $this->validate($request, [
                'content' => 'required',
                'time' => 'date|required'
            ]);

            $request = $request->all();

            $request['created_by'] = Auth::id();
            $request['task_id'] = $task_id;

            TaskReminder::create($request);

            return redirect()->back();
        }

        /**
         * update a reminder
         * @param $task_id
         * @param $id
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function update($task_id, $id, Request $request)
        {
            $this->validate($request, [
                'content' => 'required',
                'time' => 'date|required'
            ]);

            $reminder = TaskReminder::findOrFail($id);

            $request = $request->all();

            $reminder->update($request);

            return redirect()->back();

        }
    }