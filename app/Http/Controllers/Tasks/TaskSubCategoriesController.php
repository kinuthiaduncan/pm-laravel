<?php

namespace App\Http\Controllers\Tasks;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use PM\Models\TaskCategory;
use PM\Models\TaskSubCategories;

class TaskSubCategoriesController extends Controller
{

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id)
    {
        $category = TaskCategory::where('id',$id)->first();
        $subcategories = TaskSubCategories::where('task_category_id',$id)->get();

        return view('tasks.sub_categories.index',['category'=>$category,'subcategories'=>$subcategories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $category = TaskCategory::where('id',$id)->first();
        return view('tasks.sub_categories.create',['category'=>$category]);

    }


    public function store(Request $request, $id)
    {
        TaskSubCategories::create([
           'task_category_id'=>$id,
            'subcategory'=>$request->input('subcategory'),
            'description'=>$request->input('description'),
            'created_by'=>Auth::user()->id
        ]);

        return redirect('tasks/tasks_categories/'.$id.'/subcategories');
    }

    /**
     * edit subcategory
     * @param $category
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($category, $id)
    {
        $subcategory = TaskSubCategories::where('id',$id)->first();
        return view('tasks.sub_categories.edit',['subcategory'=> $subcategory,'category'=>$category]);
    }

    /**
     * Update sub category
     * @param Request $request
     * @param $category
     * @param $id
     * @return Redirect
     */
    public function update(Request $request, $category, $id)
    {
        $input = $request->except('_token','_method');
        TaskSubCategories::findOrFail($id)->update($input);

        Flash::success('Successfully Edited Sub Category');
        return redirect('/tasks/tasks_categories/'.$category.'/subcategories');
    }

    /**
     * Delete subcategory
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        TaskSubCategories::findOrFail($id)->forceDelete();
       return Redirect::back();
    }
}
