<?php
    /**
     * Date: 03/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Tasks;


    use App\Http\Controllers\Controller;
    use App\Notifications\ApprovalRequest;
    use App\Notifications\TaskCommentCreated;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Storage;
    use PM\Models\Follow;
    use PM\Models\Task;
    use PM\Models\TaskComment;
    use PM\Models\TaskCommentApprover;
    use PM\Models\TaskFile;
    use PM\Models\TaskUserGroup;
    use PM\Models\User;
    use Webpatser\Uuid\Uuid;

    class TaskCommentsController extends Controller
    {
        public function store(Request $request, $task_id)
        {

            $this->validate($request, [
                'comment' => 'required',
                'files.*' => 'nullable|max:5000',
            ]);

            $files = $request->file('files');

            $request = $request->all();

            $request['task_id'] = $task_id;
            $request['created_by'] = Auth::id();

            $comment = TaskComment::create($request);

            if (!is_null($files)) {
                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();
                    $file_name = Uuid::generate();
                    $saved_file = Storage::put('public/task_files/' . $file_name, $file);

                    $file_input['name'] = $filename;
                    $file_input['url'] = Storage::url($saved_file);
                    $file_input['created_by'] = Auth::id();
                    $file_input['task_id'] = $task_id;
                    $file_input['task_comment_id'] = $comment->id;

                    TaskFile::create($file_input);
                }
            }

            // save approvers
            $this->saveApprovers($request, $comment);

            flash('Comment Saved', 'success');

            // Notifications
            $task = Task::findOrFail($task_id);

            $assigned_users = $task->assignedUsers()->get();

            $assigned_users->map(function ($assignee) use ($comment, $task, $assigned_users) {
                $user = User::findOrFail($assignee->user_id);

                $user->notify(new TaskCommentCreated($comment, $task, $assigned_users));

                //send a notification to the users following the task
                Follow::where('followed_id', $task->id)->get()
                    ->map(function ($follower) use ($comment, $task, $assigned_users) {
                        $id = $follower->follower_id;
                        $user = User::findOrFail($id);
                        $user->notify(new TaskCommentCreated($comment, $task, $assigned_users));
                    });

                //send notification to task group
                TaskUserGroup::where('task_id', $task->id)->where('user_id', '!=', $assignee->user_id)->get()
                    ->map(function ($user_group) use ($comment, $task, $assigned_users) {
                        $id = $user_group->users->id;
                        $user = User::findOrFail($id);
                        $user->notify(new TaskCommentCreated($comment, $task, $assigned_users));
                    });
            });

            $creator = User::findOrFail($task->created_by);

            $creator->notify(new TaskCommentCreated($comment, $task, $assigned_users));

            return redirect()->back();
        }

        /**
         * Save approvers
         * @param $request
         * @param $comment
         */
        private function saveApprovers($request, $comment)
        {
            if (array_key_exists('user_group', $request)) {
                $user_group = $request['user_group'];

                foreach ($user_group as $user) {
                    $user_array[] = $user;

                    $user_group_input['user_id'] = $user;
                    $user_group_input['task_id'] = $comment->task_id;
                    $user_group_input['task_comment_id'] = $comment->id;

                    $present_user = TaskUserGroup::where('task_id', $comment->task_id)
                        ->where('user_id', $user)
                        ->first();

                    if (empty($present_user)) {
                        TaskUserGroup::create([
                            'user_id'=>$user_group_input['user_id'],
                            'task_id'=>$user_group_input['task_id']
                        ]);
                    }
                    TaskCommentApprover::create([
                        'user_id'=>$user_group_input['user_id'],
                        'comment_id'=> $user_group_input['task_comment_id']
                    ]);
                    $approver = User::findOrFail($user_group_input['user_id']);
                    $approver->notify(new ApprovalRequest($comment));
//                    else {
//                        $present_user->update(['task_comment_id' => $comment->id]);
//                    }
                }
            }
        }
    }