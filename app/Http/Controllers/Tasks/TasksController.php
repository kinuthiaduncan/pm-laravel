<?php
    /**
     * Date: 20/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Tasks;


    use App\Http\Controllers\Controller;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Redirect;
    use Laracasts\Flash\Flash;
    use phpDocumentor\Reflection\DocBlock\Tags\Return_;
    use PM\Departments\DepartmentsRepository;
    use PM\Issues\IssueRepository;
    use PM\Models\Follow;
    use PM\Models\Task;
    use PM\Models\TaskFile;
    use PM\Models\TaskGroup;
    use PM\Models\TaskProgress;
    use PM\Models\TaskSubCategories;
    use PM\Models\TaskUserGroup;
    use PM\Models\User;
    use PM\Tasks\TaskCategoriesRepository;
    use PM\Tasks\TasksRepository;
    use PM\Users\UserRepository;

    class TasksController extends Controller
    {
        /**
         * @var TasksRepository
         */
        private $tasksRepository;
        /**
         * @var UserRepository
         */
        private $userRepository;
        /**
         * @var IssueRepository
         */
        private $issueRepository;
        /**
         * @var TaskCategoriesRepository
         */
        private $taskCategoriesRepository;
        /**
         * @var DepartmentsRepository
         */
        private $departmentsRepository;

        /**
         * TasksController constructor.
         * @param TasksRepository $tasksRepository
         * @param DepartmentsRepository $departmentsRepository
         * @param TaskCategoriesRepository $taskCategoriesRepository
         * @param UserRepository $userRepository
         * @param IssueRepository $issueRepository
         */
        public function __construct(TasksRepository $tasksRepository, DepartmentsRepository $departmentsRepository, TaskCategoriesRepository $taskCategoriesRepository, UserRepository $userRepository, IssueRepository $issueRepository)
        {
            $this->tasksRepository = $tasksRepository;
            $this->userRepository = $userRepository;
            $this->issueRepository = $issueRepository;
            $this->taskCategoriesRepository = $taskCategoriesRepository;
            $this->departmentsRepository = $departmentsRepository;
        }

        /**
         * Display tasks
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $status = $this->issueRepository->getStatusTypesSelect();
            $users = $this->userRepository->allUsersSelect();
            $departments = $this->departmentsRepository->getDepartments();
            $today = (Carbon::today()->toDateString());

            return view('tasks.index', ['today'=>$today,'status' => $status, 'departments' => $departments, 'users' => $users]);
        }

        /**
         * Display Create Task Form
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function create()
        {
            $task_categories = $this->taskCategoriesRepository->getTaskCategories();
            $priorities = $this->issueRepository->getPrioritiesSelect();
            $users = $this->userRepository->allUsersSelect();
            $task_groups = TaskGroup::where('created_by', Auth::id())
                ->get()
                ->mapWithKeys(function ($task_group) {
                    return [$task_group['name'] => $task_group['name']];
                })->toArray();

            $assign_to = $task_groups + $users;

            return view('tasks.create', ['task_categories' => $task_categories, 'priorities' => $priorities, 'assign_to' => $assign_to]);
        }


        /**
         * Store a task
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         *
         */
        public function store(Request $request)
        {
            $this->validate($request, [
                'title' => 'required|max:255',
                'description' => 'nullable',
                'due_date' => 'required',
                'assign_date'=>'required',
                'assign_to'=>'required',
                'file.*' => 'nullable|max:5000',
            ]);

            $files = $request->file;

            $this->tasksRepository->save($request, $files);

            return response()->json('created', 200);
        }

        /**
         * Display details of a single task
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function show($id)
        {
            $task = Task::findOrFail($id);

            $task->files = TaskFile::where('task_id', $id)->get();

            $progress = TaskProgress::where('task_id', $id)
                ->latest()
                ->first();

            $task->percentage_done = is_null($progress) ? NUll : $progress->percentage_done;

            $users = $this->userRepository->allUsersSelect();
            $status = $this->issueRepository->getStatusTypesSelect();

            $task_members = TaskUserGroup::where('task_id', $id)->get()
                ->map(function ($member) {
                    return $member->users->preferred_name;
                });

            $assigned_users = $task->assignedUsers()->get()
                ->map(function ($assignee) {
                    return $assignee->user->preferred_name;
                });

            $task->assigned_users = $task->assignedUsers()->get()
                ->map(function ($assignee) {
                    return $assignee->user_id;
                })->toArray();

            $task->edit = in_array(Auth::id(), $task->assigned_users) || Auth::id() == $task->created_by;

            if (Follow::where('follower_id', Auth::id())->where('task_id', $task->id)->first()) {
                $followed = true;
            } else {
                $followed = false;
            };

            return view('tasks.show', ['task' => $task, 'assigned_users' => $assigned_users, 'users' => $users, 'task_members' => $task_members, 'status' => $status, 'followed' => $followed]);
        }

        /**
         * Display edit form of a single task
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function edit($id)
        {
            $task = Task::findOrFail($id);

            $task->assigned_users = $task->assignedUsers()->get()
                ->map(function ($assignee) {
                    return $assignee->user->id;
                })->toArray();

            $task_categories = $this->taskCategoriesRepository->getTaskCategories();
            $priorities = $this->issueRepository->getPrioritiesSelect();
            $users = $this->userRepository->allUsersSelect();
            $status = $this->issueRepository->getStatusEdit($id);

            $task_groups_users_ids = TaskUserGroup::where('task_id', $id)->get()
                ->map(function ($task_group) {
                    return $task_group->user_id;
                })->toArray();

            $task_groups_users_array = User::findMany($task_groups_users_ids)
                ->map(function ($task_groups_user) {
                    return $task_groups_user->id;
                })->toArray();

            $task_groups = TaskGroup::where('created_by', Auth::id())
                ->get()
                ->mapWithKeys(function ($task_group) {
                    return [$task_group['name'] => $task_group['name']];
                })->toArray();

            $assign_to = $task_groups + $users;

            return view('tasks.edit', ['assign_to' => $assign_to, 'task_groups_users_array' => $task_groups_users_array, 'task' => $task, 'task_categories' => $task_categories, 'priorities' => $priorities, 'users' => $users, 'status' => $status]);
        }

        /**
         * edit a task
         * @param Request $request
         * @param $id
         * @return \Illuminate\Http\RedirectResponse
         */
        public function update(Request $request, $id)
        {
            $this->validate($request, [
                'title' => 'required|max:50',
                'description' => 'nullable',
                'due_date' => 'required',
                'files.*' => 'nullable|max:5000',
            ]);
            $files = request()->file('files');

            $this->tasksRepository->edit($request, $files, $id);

            return response()->json('created', 200);
        }

        /**
         * Mark task as done
         * @param $id
         * @param $status
         * @return mixed
         */
        public function markTaskDone($id,$status)
        {
            $date = Carbon::now()->toDateString();
            Task::findOrFail($id)->update(['status_id'=>$status,'status_change_date'=>$date]);
        }
    }