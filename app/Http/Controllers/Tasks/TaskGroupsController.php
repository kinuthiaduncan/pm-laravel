<?php
    /**
     * Date: 18/04/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Tasks;


    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use PM\Models\TaskGroup;
    use PM\Models\TaskGroupMember;
    use PM\Models\User;

    class TaskGroupsController extends Controller
    {
        /**
         * Display all task groups
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $my_task_groups = TaskGroup::where('created_by', Auth::id())->paginate(10);

            return view('tasks.task_groups.index', ['my_task_groups' => $my_task_groups]);
        }

        /**
         * Display create form for task groups
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function create()
        {
            $users = User::where('active', 1)->get()
                ->mapWithKeys(function ($user) {
                    return [$user['id'] => $user['preferred_name']];
                })->toArray();

            return view('tasks.task_groups.create', ['users' => $users]);
        }

        /**
         * Store task groups
         * @param Request $request
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function store(Request $request)
        {
            $request['created_by'] = Auth::id();

            $task_group_id = TaskGroup::create($request->all());

            // Save Task Group Members
            foreach ($request->get('users') as $user_id) {

                $user_input['user_id'] = $user_id;
                $user_input['task_group_id'] = $task_group_id->id;

                $present_user = TaskGroupMember::where('user_id', $user_id)
                    ->where('task_group_id', $task_group_id->id)
                    ->get()->toArray();

                if (empty($present_user)) {
                    TaskGroupMember::create($user_input);
                }
            }

            return redirect()->route('task_group.index');
        }

        /**
         * Display a task group
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function show($id)
        {
            $task_group = TaskGroup::findOrFail($id);

            $task_group_members = TaskGroupMember::where('task_group_id', $id)
                ->get()
                ->map(function ($user) {
                    $member = User::findOrFail($user->user_id);
                    return $member->preferred_name;
                });

            return view('tasks.task_groups.show', ['task_group' => $task_group, 'task_group_members' => $task_group_members]);
        }

        /**
         * Display create form for task groups
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function edit($id)
        {
            $task_group = TaskGroup::findOrFail($id);

            $users = User::where('active', 1)->get()
                ->mapWithKeys(function ($user) {
                    return [$user['id'] => $user['preferred_name']];
                })->toArray();

            $task_group_members = TaskGroupMember::where('task_group_id', $id)
                ->pluck('user_id')->toArray();

            return view('tasks.task_groups.edit', ['users' => $users,'task_group' => $task_group, 'task_group_members' => $task_group_members]);
        }

        /**
         * Store task groups
         * @param Request $request
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function update(Request $request, $id)
        {
            $task_group = TaskGroup::findOrFail($id);

            $task_group->update($request->all());

            // Save Task Group Members
            foreach ($request->get('users') as $user_id) {

                $user_input['user_id'] = $user_id;
                $user_input['task_group_id'] = $id;

                $present_user = TaskGroupMember::where('user_id', $user_id)
                    ->where('task_group_id', $id)
                    ->get()->toArray();

                if (empty($present_user)) {
                    TaskGroupMember::create($user_input);
                }
            }

            return redirect()->route('task_group.show', [$id]);
        }


        /**
         * Delete a task group
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function destroy($id)
        {
            $task_group = TaskGroup::findOrFail($id);

            $task_group->delete();

            return view('tasks.task_groups.index');
        }
    }