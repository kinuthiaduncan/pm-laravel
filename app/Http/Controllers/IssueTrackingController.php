<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use PM\Issues\IssueTrackingRepository;
use PM\Models\Issue;
use PM\Models\IssueTracking;

class IssueTrackingController extends Controller
{
    private $issueTrackingRepository;

    /**
     * IssueTrackingController constructor.
     * @param IssueTrackingController|IssueTrackingRepository $issueTrackingRepository
     */
    function __construct(IssueTrackingRepository $issueTrackingRepository)
    {
        $this->issueTrackingRepository = $issueTrackingRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $today = Carbon::today()->toDateString();
        $issue = Issue::findOrFail($id);
        $schedule = IssueTracking::where('issue_id',$id)->paginate(10);
        return view('issues.tracking.index',['issue'=>$issue,'schedule'=>$schedule, 'today'=>$today]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $issue = Issue::findOrFail($id);
        return view('issues.tracking.create',['issue'=>$issue]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return redirect
     */
    public function store(Request $request,$id)
    {
        $this->validate($request,[
            'date'=>'required',
            'start_time'=>'required|issue_time_validator:date,end_time|start_time_validator:date,end_time',
            'end_time'=>'required|end_time_validator:start_time'
        ]);

        $this->issueTrackingRepository->save($request, $id);
        Flash::success('Issue schedule successfully added');
        return redirect('/issue/'.$id.'/track');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $issue
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit($issue,$id)
    {
        $schedule = IssueTracking::findOrFail($id);
        return view('issues.tracking.edit',['schedule'=>$schedule,'issue'=>$issue]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $issue
     * @param  int $id
     * @return redirect
     */
    public function update(Request $request, $issue,$id)
    {
        $this->validate($request,[
            'description'=>'required|min:5',
            'date'=>'required',
            'start_time'=>'required',
            'end_time'=>'required|end_time_validator:start_time'
        ]);
        $data = Issue::findOrFail($issue);
        $this->issueTrackingRepository->update($request,$data,$id);
        Flash::success('Issue schedule successfully updated');
        return redirect('/issue/'.$issue.'/track');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        IssueTracking::findOrFail($id)->forceDelete();

        Flash::success('Issue Schedule Deleted Successfully');
        return Redirect::back();
    }
}
