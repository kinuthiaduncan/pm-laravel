<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use PM\Models\File;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FileController
 * @package App\Http\Controllers
 */
class FileController extends Controller
{
    /**
     * Stream a file from the filesystem
     * @param $path
     * @param null $title
     * @return \Illuminate\Http\Response ;
     */
    protected function streamFile($path, $title = null)
    {
        $filesystem = app('pm.storage');

        try {
            $stream = $filesystem->readStream($path);
        } catch (FileNotFoundException $e) {
            \Flash::error('File not found');

            return redirect()->back();
        }

        $fname = $title ? $title : basename($path);

        return response()->stream(function () use ($stream) {
            fpassthru($stream);
        }, Response::HTTP_OK, [
            "Content-Type" => $filesystem->getMimetype($path),
            "Content-Length" => $filesystem->getSize($path),
            "Content-disposition" => "inline; filename=\"" .$fname . "\"",
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file = File::findOrFail($id);

        return $this->streamFile($file->path());
    }
}
