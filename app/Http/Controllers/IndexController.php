<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace App\Http\Controllers;

    use Illuminate\Support\Facades\Auth;
    use Laracasts\Flash\Flash;
    use PM\Models\Issue;
    use PM\Models\Project;
    use PM\Models\ProjectUser;
    use PM\Models\Task;
    use PM\Models\User;


    /**
     * Class IndexController
     * @package App\Http\Controllers
     */
    class IndexController extends Controller
    {


        /**
         * Function to load the login page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            return view('index.index');
        }

        /**
         * Function to load the dashboard
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function dashboard()
        {
            $users = count(User::all());
            $user = Auth::user();
            $projects = count(Project::all());
            $issues = count(Issue::all());
            $tasks = count(Task::all());
            $userIssues = Issue::where('assigned_to', $user)->orWhere('created_by', $user)->get();
            $userTasks = Task::where('assigned_to', $user)->orWhere('created_by', $user)->get();
            $tasks_todo = count(Task::where('status_id', '1')->get());
            $tasks_progress = count(Task::where('status_id', '2')->get());
            $tasks_review = count(Task::where('status_id', '3')->get());
            $tasks_done = count(Task::where('status_id', '4')->get());
            $tasks_released = count(Task::where('status_id', '5')->get());
            $issues_todo = count(Issue::where('status_id', '1')->get());
            $issues_progress = count(Issue::where('status_id', '2')->get());
            $issues_review = count(Issue::where('status_id', '3')->get());
            $issues_done = count(Issue::where('status_id', '4')->get());
            $issues_released = count(Issue::where('status_id', '5')->get());
            $following = 0;
            $pending_tasks = count(Task::where('assigned_to', Auth::id())->where('status_id', 1)->get());
            $assigned_tasks = count(Task::where('assigned_to', Auth::id())->get());
            $user_projects = count(ProjectUser::where('user_id', Auth::id())->get());

            return view('index.dashboard', ['user' => $user, 'issues_released' => $issues_released,
                'issues_done' => $issues_done, 'issues_review' => $issues_review,
                'issues_progress' => $issues_progress, 'issues_todo' => $issues_todo,
                'issues' => $issues, 'projects' => $projects, 'users' => $users,
                'title' => 'System Dashboard',
                'following' => $following, 'pending_tasks' => $pending_tasks,
                'assigned_tasks' => $assigned_tasks, 'user_projects' => $user_projects,
                'userIssues' => $userIssues, 'userTasks' => $userTasks, 'tasks_released' => $tasks_released,
                'tasks_done' => $tasks_done, 'tasks_review' => $tasks_review,
                'tasks_progress' => $tasks_progress, 'tasks_todo' => $tasks_todo,
                'tasks' => $tasks,
            ]);
        }

        /**
         * Function to load the boards page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function boards()
        {
            return view('index.boards', ['title' => 'Kanban Board']);
        }

        /**
         * Display notifications
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function notifications()
        {
            $user = User::findOrFail(Auth::id());

            $notifications = $user->notifications()->latest()->paginate(10);

            foreach ($user->unreadNotifications as $notification) {
                $notification->markAsRead();
            }

            return view('index.notifications', ['notifications' => $notifications]);
        }

        /**
         * Clear Notifications
         */
        public function deleteNotifications()
        {
            $user = User::findOrFail(Auth::id());
            $user->notifications()->delete();

            Flash::warning('Notifications Deleted!');

            return redirect()->back();
        }
    }