<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PM\Models\Task;
use PM\Models\User;

class DashboardController extends Controller
{
    /**
     * Dashboard index
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::id();
        $today = Carbon::today()->toDateString();

        //assigned tasks
        $assigned = Task::whereHas('status', function ($status) {
            $status->where('resolution', '!=', true);
        })->whereHas('assignedUsers', function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->orderBy('created_at','DESC')->take(5)->get();

        //past due tasks
        $pastDue = Task::whereHas('status', function ($status) {
            $status->where('resolution', '!=', true);
        })->where('due_date','<',$today)->whereHas('assignedUsers', function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->orderBy('created_at','DESC')->take(5)->get();

        //individual tasks
        $individual = Task::whereHas('assignedUsers', function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->orderBy('created_at','DESC')->take(11)->get();

        //Team tasks
        $department = Auth::user()->department()->first();
        $users = User::where('department_id',$department->id)->pluck('id');

        $teamTasks = Task::whereHas('status', function ($status) {
            $status->where('resolution', '!=', true);
            })->whereHas('assignedUsers', function ($assignedUser) use ($users){
                $assignedUser->whereIn('user_id', $users);
            })->orderBy('created_at','DESC')->take(11)->get();

        return view('dashboard.index', [
            'user'=>$user,
            'assigned'=>$assigned,
            'pastDue'=>$pastDue,
            'individual'=>$individual,
            'teamTasks'=>$teamTasks
        ]);
    }

    /**
     * View all completed tasks
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function completedTasks()
    {
        $user = Auth::id();
       $title = 'Completed Tasks';
        $tasks = Task::where('status_id',4)->whereHas('assignedUsers',function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->paginate(10);

        return view('dashboard.partials.tasks',['tasks'=>$tasks,'title'=>$title]);
    }

    /**
     * View all ongoing tasks
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ongoingTasks()
    {
        $user = Auth::id();
        $title = 'Ongoing Tasks';
        $tasks = Task::where('status_id',2)->whereHas('assignedUsers',function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->paginate(10);
        return view('dashboard.partials.tasks',['tasks'=>$tasks,'title'=>$title]);
    }

    /**
     * View all pending tasks
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pendingTasks()
    {
        $user = Auth::id();
        $title = 'Pending Tasks';
        $tasks = Task::where('status_id',1)->whereHas('assignedUsers',function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->paginate(10);
        return view('dashboard.partials.tasks',['tasks'=>$tasks,'title'=>$title]);
    }

    /**
     * All user assigned tasks
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assignedTasks()
    {
        $user = Auth::id();
        $title = 'Assigned Tasks';
        $tasks = Task::whereHas('status', function ($status) {
            $status->where('resolution', '!=', true);
        })->whereHas('assignedUsers', function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->orderBy('created_at','DESC')->paginate(10);

        return view('dashboard.partials.tasks',['tasks'=>$tasks,'title'=>$title]);
    }

    public function pastDueTasks()
    {
        $user = Auth::id();
        $title = 'Past Due Tasks';
        $today = Carbon::today()->toDateString();
        
        $tasks = Task::whereHas('status', function ($status) {
            $status->where('resolution', '!=', true);
        })->where('due_date','<',$today)->whereHas('assignedUsers', function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->orderBy('created_at','DESC')->paginate(10);

        return view('dashboard.partials.tasks',['tasks'=>$tasks,'title'=>$title]);
    }
}
