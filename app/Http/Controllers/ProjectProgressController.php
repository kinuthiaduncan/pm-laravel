<?php
/**
 * Date: 02/09/2016
 * Time: 11:41 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: ct-project-management
 * Cytonn Technologies
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use PM\Models\Project;
use PM\Models\ProjectProgress;

class ProjectProgressController extends Controller
{
    public function index($projectId)
    {
        $project = Project::findOrFail($projectId);

        $progress = $project->progress()->latest()->paginate(10);

        return view('projects.progress.index', ['title'=>'Progress on '.$project->name, 'project'=>$project, 'progress'=>$progress]);
    }

    /**
     * @param Request $request
     * @param $projectId
     * @param null $entryId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $projectId, $entryId = null)
    {
        $this->validate($request, [
            'description'=>'required|min:20'
        ]);

        $project = Project::findOrFail($projectId);

        if(Auth::user()->id != $project->project_lead)
        {
            Flash::warning('Only the project lead can update progress on a project');

            return redirect()->back()->withInput();
        }

        $progress = ProjectProgress::findOrNew($entryId);
        $progress->title = $request->get('title');
        $progress->description = $request->get('description');
        $progress->project_id = $projectId;
        $progress->added_by = Auth::id();

        $progress->save();

        Flash::message('The progress has been saved');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $projectId
     * @param null $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $projectId, $id)
    {
        $this->validate($request, [
            'description'=>'required|min:20'
        ]);

        $project = Project::findOrFail($projectId);

        if(Auth::id() != $project->project_lead)
        {
            Flash::warning('Only the project lead can update progress on a project');

            return redirect()->back()->withInput();
        }

        $progress = ProjectProgress::findOrFail($id);

        $progress->update($request->all());

        Flash::message('The progress has been updated');

        return redirect()->back();
    }
}