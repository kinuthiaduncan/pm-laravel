<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace App\Http\Controllers;

    use App\Mail\AddMember;
    use App\Mail\ProjectCreated;
    use App\Mail\RemoveMember;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\Redirect;
    use Illuminate\Support\Facades\Validator;
    use Laracasts\Flash\Flash;
    use PM\ActivityLogs\ActivityLogRepository;
    use PM\Issues\IssueRepository;
    use PM\Library\Mailer;
    use PM\Models\CategoryProject;
    use PM\Models\Issue;
    use PM\Models\Milestone;
    use PM\Models\Project;
    use PM\Models\ProjectCategory;
    use PM\Models\ProjectComponent;
    use PM\Models\ProjectType;
    use PM\Models\ProjectUser;
    use PM\Models\User;
    use PM\Presenters\UserPresenter;
    use PM\Projects\ProjectRepository;
    use PM\Users\UserRepository;

    /**
     * Class ProjectController
     * @package App\Http\Controllers
     */
    class ProjectController extends Controller
    {
        /**
         * @var IssueRepository
         */
        private $issueRepository;
        private $userRepository;
        private $projectRepository;
        private $activityLogRepository;
        /**
         * @param UserRepository $userRepository
         * @param Mailer $mailer
         * @param ProjectRepository $projectRepository
         * @param ActivityLogRepository $activityLogRepository
         * @param IssueRepository $issueRepository
         */
        public function __construct(UserRepository $userRepository, Mailer $mailer, ProjectRepository $projectRepository, ActivityLogRepository $activityLogRepository, IssueRepository $issueRepository)
        {
            $this->mailer = $mailer;
            $this->userRepository = $userRepository;
            $this->projectRepository = $projectRepository;
            $this->activityLogRepository = $activityLogRepository;
            $this->issueRepository = $issueRepository;
        }

        /**
         * Function to display all projects
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $projects = Project::orderBy('active', 'ASC')->get();
            $projectSelect = $this->projectRepository->getProjectsSelect();
            $project_types = ProjectType::all();

            $categories = ProjectCategory::all();

            $users = $this->userRepository->allUsersSelect();

            return view('projects.index', ['projects' => $projects, 'projectSelect'=>$projectSelect, 'title' => 'All Projects', 'categories' => $categories, 'project_types' => $project_types, 'users' => $users]);
        }
        /**
         * Function to display all private projects for authorized personnel only
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function myPrivateProjects()
        {

            $user = Auth::user();

            $projects = Project::where('project_access', 'private')
                ->whereHas('projectUsers', function ($q) use ($user) {
                    $q->where('user_id', $user->id);
                })
                ->get();

            //Redirect if no projects
            if ($projects->count() <= 0) {

                Flash::error('You have no private projects');

                return redirect()->back();

            }

            return view('projects.myPrivateProjects', ['projects' => $projects, 'title' => 'My Projects']);
        }


        /**
         * Function to display project creation page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function create()
        {
            $project_types = $this->projectRepository->projectsTypesSelect();
            $users = $this->userRepository->allUsersSelect();
            $categories = $this->projectRepository->categoriesSelect();

            return view('projects.create', ['project_types' => $project_types, 'users' => $users, 'title' => 'Create Project', 'categories' => $categories]);
        }


        /**
         * Function to create a project
         * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function store()
        {
            $input = Input::all();

            $rules = ['name' => 'required|unique:projects', 'project_type' => 'required', 'project_access' => 'required', 'project_description' => 'required', 'project_url' => 'required|unique:projects', 'project_lead' => 'required', 'status' => 'required', 'priority' => 'required', 'resources_required' => 'required', 'resources_assigned' => 'required'];

            $validator = Validator::make(array(
                'name' => $input['name'],
                'project_type' => $input['project_type'],
                'project_access' => $input['project_access'],
                'project_description' => $input['project_description'],
                'project_url' => $input['project_url'],
                'project_lead' => $input['project_lead'],
                'status' => $input['status'],
                'priority' => $input['priority'],
                'resources_required' => $input['resources_required'],
                'resources_assigned' => $input['resources_assigned']),
                $rules
            );

            if ($validator->passes()) {
                $input['project_key'] = $this->projectRepository->generateProjectKey($input['name']);

                $input['created_by'] = Auth::user()->id;
                $created_project = Project::create($input);

                //Log this activity if project access is public

                $access = $input['project_access'];

                if ($access == 'public') {
                    $created_project = $this->activityLogRepository->logProjectCreation($created_project);
                }

                //Add the project creator and the project lead as project members for both private and public projects

                $creator['user_id'] = Auth::user()->id;
                $creator['project_id'] = $created_project->id;
                ProjectUser::create($creator);

                if (Auth::user()->id != $created_project->project_lead) {
                    $lead['user_id'] = $created_project->project_lead;
                    $lead['project_id'] = $created_project->id;
                    ProjectUser::create($lead);
                }

                //send mail
//                $this->mailer->projectCreationNotifier($created_project);

                $projectLead = User::findOrFail($input['project_lead'])->email;

                Mail::to($projectLead)
                    ->queue(new ProjectCreated($created_project));

                Flash::success('Project created successfully');

                return redirect()->route('projects_index');

            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        /**
         * Function to display an individual project
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function showProject($id)
        {
            $current_user = Auth::user()->id;
            $project = Project::where('id', $id)->first();
            $issues = Issue::where('project_id', $id)->get();
            $milestones = Milestone::where('project_id', $id)->get();
            $project_components = ProjectComponent::where('project_id', $id)->get();
            $project_members = $this->projectRepository->getProjectMembers($id);

            return view('projects.project', [
                'current_user' => $current_user,
                'project_members' => $project_members,
                'title' => 'Project: ' . $project->name,
                'project' => $project,
                'issues' => $issues,
                'milestones' => $milestones,
                'project_components' => $project_components
            ]);
        }

        /**
         * Function to display project to be edited
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function edit($id)
        {
            $project = Project::find($id);

            $categories = $this->projectRepository->categoriesSelect();

            $project_types = $this->projectRepository->projectsTypesSelect();

            $users = $this->userRepository->allUsersSelect();

            return view('projects.edit', ['title' => 'Project: ' . $project->name, 'project' => $project, 'project_types' => $project_types, 'users' => $users, 'categories' => $categories]);
        }

        /**
         * Function to update a project
         * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function update()
        {
            $input = Input::all();

            $rules = ['name' => 'required', 'project_type' => 'required', 'project_description' => 'required', 'project_url' => 'required', 'project_lead' => 'required', 'status' => 'required', 'priority' => 'required', 'resources_required' => 'required', 'resources_assigned' => 'required'];

            $validator = Validator::make(array(
                'name' => $input['name'],
                'project_type' => $input['project_type'],
                'project_access' => $input['project_access'],
                'project_description' => $input['project_description'],
                'project_url' => $input['project_url'],
                'project_lead' => $input['project_lead'],
                'status' => $input['status'],
                'priority' => $input['priority'],
                'resources_required' => $input['resources_required'],
                'resources_assigned' => $input['resources_assigned']),
                $rules
            );

            if ($validator->passes()) {
                $input['created_by'] = Auth::user()->id;
                $project = Project::find($input['project_id']);
                $original_project_name = $project->name;
                $project->update($input);
                $created_project = Project::where('name', $input['name'])->where('project_url', $input['project_url'])->first();

                //Log this activity if project is public
                if ($project->project_access == 'public') {
                    $created_project = $this->activityLogRepository->logProjectEditing($input['name'], $input['project_url'], $original_project_name);
                }
                //Add the project editor and the project lead as project members
                $editor_watch = ProjectUser::where('project_id', $created_project->id)->where('user_id', Auth::user()->id)->first();
                $project_lead = ProjectUser::where('project_id', $created_project->id)->where('user_id', $created_project->project_lead)->first();
                if (!$editor_watch) {
                    $creator['user_id'] = Auth::user()->id;
                    $creator['project_id'] = $created_project->id;
                    ProjectUser::create($creator);
                }

                if (Auth::user()->id != $created_project->project_lead) {
                    if (!$project_lead) {
                        $lead['user_id'] = $created_project->project_lead;
                        $lead['project_id'] = $created_project->id;
                        ProjectUser::create($lead);
                    }
                }

                Flash::success('Project edited successfully');

                return redirect()->to('/projects');
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        public function projectBoard($id, $componentId = null)
        {
            $project = Project::find($id);
            $projects_components = ProjectComponent::where('project_id', $id)->get();

            $issues_array = $this->issueRepository->getParentIssues($id);
            $issue_types_array = $this->issueRepository->getIssueTypeSelect();
            $priorities_array = $this->issueRepository->getPrioritiesSelect();
            $users_array = $this->userRepository->allUsersSelect();
            $issue_components = ProjectComponent::get();

            $user_list = User::whereHas('issues', function ($issues) use ($id) {
                $issues->where('project_id', $id);
            })
                ->get()
                ->map(function ($user) {
                    return (object) ['name' => $user->present()->fullName, 'id' => $user->id];
                });

            $component_list = [null => 'Project component'] +
                ProjectComponent::whereHas('issues', function ($issues) use ($id) {
                    $issues->where('project_id', $id);
                })->pluck('component_name', 'id')
                    ->all();

            $projects_components_list = ProjectComponent::where('project_id', $id)->get()
                ->map(function ($component) {
                    return (object) ['name' => $component->component_name, 'id' => $component->id];
                });

            return view('projects.board', [
                'title' => 'Project: ' . $project->name, 'project' => $project, 'lead' => $project->project_lead, 'priorities' => $priorities_array,
                'users' => $users_array, 'issues' => $issues_array, 'issue_type' => $issue_types_array, 'project_id' => $id, 'user_list' => $user_list,
                'project_components' => $projects_components, 'projects_components_list' => $projects_components_list, 'component_list' => $component_list,
                'search_componentId' => $componentId
            ]);
        }

        /**
         * Function to display users that can be added to a project
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function addMembers($id)
        {
            $project = Project::find($id);
            $existing_members = ProjectUser::where('project_id', $id)->pluck('user_id')->toArray();
            $system_users = User::where('active', 1)->pluck('id')->toArray();
            $none_project_members = array_diff($system_users, $existing_members);

            $none_members = User::where('active', 1)->whereIn('id', $none_project_members)->get()
                ->mapWithKeys (function ($user){
                    return [$user['id'] => $user['preferred_name']];
                })->toArray();

            return view('projects.members', ['project_id' => $project->id, 'title' => 'Project: ' . $project->name, 'none_members' => $none_members]);
        }

        /**
         * Function to add a member to a project
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function storeMembers(Request $request)
        {
            if (!empty($request->get('users'))) {
                $project = Project::find($request->get('project_id'));

                foreach ($request->get('users') as $user_id) {
                    $data['project_id'] = $request->get('project_id');
                    $data['user_id'] = $user_id;
                    ProjectUser::create($data);

                    $user = User::find($user_id);
                    $project['member_email'] = $user->email;
                    $project['member_firstname'] = $user->preferred_name;

                    //send mail
                    Mail::to($project['member_email'])
                        ->queue(new AddMember($project));
                }

                Flash::success('Project members added');

                return redirect()->to('/project/' . $request->get('project_id'));
            } else {
                Flash::success('No user was selected hence no member added');

                return redirect()->to('/project/' . $request->get('project_id'));
            }
        }

        /**
         * Function to remove a member from a project
         * @param $project_id
         * @param $user_id
         * @return \Illuminate\Http\RedirectResponse
         */
        public function removeMember($project_id, $user_id)
        {
            $project = Project::findOrFail($project_id);

            if (Auth::id() == $project->project_lead){
                $user = ProjectUser::where('project_id', $project_id)->where('user_id', $user_id)->first();
                $user->delete();

                $project = Project::find($project_id);
                $user = User::find($user_id);
                $project['member_email'] = $user->email;
                $project['member_firstname'] = $user->preferred_name;

                //send mail
                Mail::to($project['member_email'])
                    ->queue(new RemoveMember($project));

                Flash::success('Project member removed successfully');
            } else {
                Flash::success('You are not allowed to perform this action');
            }

            return redirect()->to('/project/' . $project_id);
        }

        /**
         * Watch a Project
         *
         * @param $id
         * @return \Illuminate\Http\RedirectResponse
         */
        public function watchProject($id)
        {
            $project = Project::find($id);
            $watcher['user_id'] = Auth::user()->id;
            $watcher['project_id'] = $project->id;
            $members = ProjectUser::where('user_id', $watcher['user_id'])->where('project_id', $watcher['project_id'])->get();
            if (count($members) == 0) {
                ProjectUser::create($watcher);
            } else {
                Flash::message('You are already watching this project');

                return redirect()->to('/project/' . $project->id);
            }

            Flash::success('You are now watching this project');

            return redirect()->to('/project/' . $project->id);
        }

        /**
         * Download Project Summaries as specified
         *
         * @return \Illuminate\Http\RedirectResponse
         */
        public function downloadSummaries()
        {
            $input = array_except(Input::all(), '_token');

            if (empty($input) || (!isset($input['all_categories']) && empty($input['category_id'])) || (!isset($input['all_types']) && empty($input['type_id']))) {

                Flash::error('Please select either categories and types for successful export');

                return redirect()->back();
            }

            $this->projectRepository->exportSummaries($input);

            Flash::success('Project Summaries Successfully Downloaded');

            return redirect()->back();
        }

        /**
         * Activate an inactive project
         * @param $id
         * @return mixed
         */
        public function activateProject($id)
        {
            Project::where('id',$id)->update(['active'=> 1]);
            Flash::success('Project activated successfully');
            return Redirect::back();
        }

        /**
         * Deactivate an active project
         * @param $id
         * @return mixed
         */
        public function deactivateProject($id)
        {
            Project::where('id',$id)->update(['active'=> 0]);
            Flash::success('Project deactivated successfully');
            return Redirect::back();
        }

    }