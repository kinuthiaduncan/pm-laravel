<?php
/**
 * Cytonn Technologies
 * @author: Edwin Mukiri <emukiri@cytonn.com>
 */

namespace App\Http\Controllers;


use App\Mail\CommentCreation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;
use PM\Library\Mailer;
use PM\Models\ActivityStream;
use PM\Models\Comment;
use PM\Models\Issue;
use PM\Models\ProjectUser;
use PM\Models\User;
use Illuminate\Support\Facades\File;
use Webpatser\Uuid\Uuid;

/**
 * Class CommentController
 * @package App\Http\Controllers
 */
class CommentController extends Controller
{

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * function to display comment creation page
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id)
    {
        $issue = Issue::find($id);

        return view('comments.create', ['title' => 'Issues: ' . $issue->title, 'issue_id' => $id]);
    }

    /**
     * Function to save a new comment
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $rules = ['name' => 'required'];

        $validator = Validator::make(array('name' => $request->input(['name'])), $rules);

        if ($validator->passes()) {

            $image = $request->file('image');
            if ($image) {
                $filename = $image->getClientOriginalName();
                $file_name = Uuid::generate();
                $savedFile = Storage::put('public/comment_files/' . $file_name, $image);
                $input['filename'] = $filename;
                $input['image'] = Storage::url($savedFile);
            }

            $file = $request->file('file');

            if ($file) {
                $filename = $file->getClientOriginalName();
                $file_name = Uuid::generate();
                $savedFile = Storage::put('public/comment_files/' . $file_name, $file);
                $input['filename'] = $filename;
                $input['file'] = Storage::url($savedFile);
            }



            $input['created_by'] = Auth::user()->id;
            $comment = Comment::create($input);
            Flash::message('Comment created successfully');

            //Log this activity
            $issue = Issue::find($input['issue_id']);
            $log['path'] = '/issue/' . $input['issue_id'];
            $log['activity_by'] = Auth::user()->id;
            $log['activity_description'] = strip_tags($input['name']) . ' comment created';
            $log['project_id'] = $issue->project_id;
            ActivityStream::create($log);

            //Add the project creator and the project lead as project members
            $creator['user_id'] = Auth::user()->id;
            $creator['project_id'] = $issue->project_id;
            $members = ProjectUser::where('user_id', $creator['user_id'])->where('project_id', $creator['project_id'])->get();
            if (count($members) == 0) {
                ProjectUser::create($creator);
            }

            //send mail
            $issue['comment_name'] = $input['name'];
            $user = User::find(Auth::user()->id);
            $issue['comment_creator'] = $user->preferred_name;
            $project_members = ProjectUser::where('project_id', $issue['project_id'])->get();
            foreach ($project_members as $project_member) {
                $issue['member_firstname'] = $project_member->members->preferred_name;
                $issue['member_email'] = $project_member->members->email;

                Mail::to($issue['member_email'])
                    ->queue(new CommentCreation($comment));
            }

            return redirect()->to('/issue/' . $input['issue_id']);
        } else {
            return redirect()->back()->withInput()->withErrors($validator);
        }
    }

    /**
     * Function to edit a comment
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $comment = Comment::find($id);
        $issue = Issue::find($comment->issue_id);

        return view('comments.edit', ['title' => 'Issue: ' . $issue->title, 'comment' => $comment]);
    }

    /**
     * Function to update a comment
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $input = Input::all();

        $rules = ['name' => 'required'];

        $validator = Validator::make(array('name' => $input['name']), $rules);

        if ($validator->passes()) {
            $image = Input::file('image');
            if ($image) {
                $filename = $image->getClientOriginalName();
                Storage::disk('uploads')->put($filename, File::get($image));
                $input['image'] = '/uploads/' . $filename;
            }

            $input['created_by'] = Auth::user()->id;
            $comment = Comment::find($input['comment_id']);
            $original_comment = $comment->name;
            $comment->update($input);
            Flash::message('Comment edited successfully');

            //Log this activity
            $issue = Issue::find($input['issue_id']);
            $log['path'] = '/issue/' . $input['issue_id'];
            $log['activity_by'] = Auth::user()->id;
            $log['activity_description'] = strip_tags($original_comment) . ' comment edited';
            $log['project_id'] = $issue->project_id;
            ActivityStream::create($log);

            return redirect()->to('/issue/' . $input['issue_id']);
        } else {
            return redirect()->back()->withInput()->withErrors($validator);
        }
    }
}