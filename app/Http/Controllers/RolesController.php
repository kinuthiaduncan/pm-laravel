<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use PM\Models\Roles;
    use PM\Models\User;
    use PM\Users\UserRepository;
    use PDF;

    class RolesController extends Controller
    {
        private $userRepository;

        public function __construct(UserRepository $userRepository)
        {
            $this->userRepository = $userRepository;
        }

        /**
         * View a users roles
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function show($id)
        {
            $user = User::where('id', $id)->first();
            $roles = $this->userRepository->getRolesSelect();

            return view('authorization.roles.view', ['user' => $user, 'roles' => $roles]);
        }

        /**
         * Update a users role
         * @param Request $request
         * @param $user
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirect
         */
        public function update(Request $request, $user)
        {
            $id = $request->input('role_id');
            User::findOrFail($user)->update(['role_id' => $id]);

            return redirect('/users');
        }

        public function delete($id)
        {
            Roles::findorFail($id)->forceDelete();

            return redirect('authorization');
        }

        /**
         *Export roles
         */
        public function exportRoles()
        {
            $users = User::where('active',1)->orderBy('role_id','ASC')->get();
            $supervisors = User::where('active',1)->pluck('supervisor_id')->toArray();
            $pdf = PDF::loadView('users.exports.user_roles', compact('users','supervisors'));
            return $pdf->download('PM_System_User_Roles.pdf');
        }
    }
