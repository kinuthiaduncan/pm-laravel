<?php
namespace App\Http\Controllers\Api;

use PM\Tasks\TaskCategoryFilter;

class TaskCategoriesController
{
    public function getCategories()
    {

        $input = request()->get('input');
        $pagination = request()->get('pagination');

        $data = TaskCategoryFilter::filter($input, $pagination);

        return $data;
    }
}