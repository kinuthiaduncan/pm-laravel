<?php
    /**
     * Date: 23/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Api;


    use App\Http\Controllers\Controller;
    use App\Notifications\TaskCommentApproved;
    use GuzzleHttp\Psr7\Request;
    use Illuminate\Support\Facades\Input;
    use PDF;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Support\Facades\Auth;
    use Laracasts\Presenter\PresentableTrait;
    use PM\Issues\IssueRepository;
    use PM\Models\Task;
    use PM\Models\TaskComment;
    use PM\Models\TaskCommentApprover;
    use PM\Models\TaskFile;
    use PM\Models\TaskGroup;
    use PM\Models\TaskSubCategories;
    use PM\Models\TaskUserGroup;
    use PM\Models\User;
    use PM\Tasks\PublicTasksFilter;
    use PM\Tasks\TaskCategoriesRepository;
    use PM\Tasks\TasksFilter;
    use PM\Tasks\TasksRepository;
    use PM\Users\UserRepository;

    class TaskController extends Controller
    {
        use PresentableTrait;

        private $taskCategoryRepository;
        private $userRepository;
        private $issueRepository;

        function __construct(TaskCategoriesRepository $taskCategoryRepository,UserRepository $userRepository, IssueRepository $issueRepository)
        {
            $this->taskCategoryRepository = $taskCategoryRepository;
            $this->issueRepository = $issueRepository;
            $this->userRepository = $userRepository;
        }

        /**
         * Display individual tasks
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function getTasks()
        {
            $input = request()->get('input');
            $pagination = request()->get('pagination');

            return TasksFilter::filter($input, $pagination);
        }

        public function getSubCategories()
        {
            $category = Input::get('task_category_id');
            $subCategories = $this->taskCategoryRepository->getTaskSubCategories($category);
            return response()->json($subCategories);
        }

        /**
         * Display all public tasks
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function getPublicTasks()
        {

            $input = request()->get('input');
            $pagination = request()->get('pagination');

            $data = PublicTasksFilter::filter($input, $pagination);

            return $data;
        }

        /**
         * Display all comments for a task
         * @param $id
         * @return LengthAwarePaginator
         */
        public function getComments($id)
        {
            $pagination = request()->get('pagination');

            $task_comments = TaskComment::where('task_id', $id)->latest()->get()

            ->map(function ($task_comment) {
                $task_comment->comment_approvers = TaskCommentApprover::where('comment_id',$task_comment->id)->where('user_id',Auth::id())
                    ->pluck('status');
                $task_comment->notApproved = User::whereHas('taskCommentApprover',function($approver) use ($task_comment){
                    $approver->where('comment_id',$task_comment->id)->where('status',0);
                })->get();
                $task_comment->approved = User::whereHas('taskCommentApprover',function($approver) use ($task_comment){
                    $approver->where('comment_id',$task_comment->id)->where('status',1);
                })->get();
                $task_comment->file = isset(TaskFile::where('task_comment_id', $task_comment->id)->first()->url) ? TaskFile::where('task_comment_id', $task_comment->id)->first()->url : null;
                $task_comment->auth_user = Auth::id();
                $task_comment->comment_user = $task_comment->user;
                $task_comment->time = $task_comment->created_at->diffForHumans();
                $task_comment->date = $task_comment->created_at->toDayDateTimeString();

                return $task_comment;
            });

            $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
            $perPage = $pagination['per_page'];

            $offset = ($page * $perPage) - $perPage;
            $data = $task_comments->slice($offset, $perPage, true);

            $task_comments = new LengthAwarePaginator($data, $task_comments->count(), $perPage, $page);
            $task_comments->setPath('/api/get/task/' .request()->get('id') . '/comments');

            return $task_comments;
        }

        /**
         * Approve a comment
         * @param $id
         */
        public function approveComments($id)
        {
            $data = TaskComment::where('id',$id)->first();

            $comment_owner = User::whereHas('taskComments', function($comment) use ($id){
                $comment->where('id',$id);
            })->first();
            $user = Auth::user();

            \Mail::send('emails.tasks.approved_comment',['data'=>$data], function ($message) use ($comment_owner,$user)
            {
                $message->from(['support@cytonn.com'=>'CT Project Management']);
                $message->to([$comment_owner->email]);
                $message->subject('Comment approved by ' .$user->preferred_name);
            });

            TaskCommentApprover::where('comment_id',$id)->where('user_id',Auth::id())->update(['status'=>1]);
        }

        public function getTaskDetails($id)
        {
            $task = Task::findOrFail($id);
            $task->status_id = strval($task->status_id);
            $task->task_access = strval($task->task_access);
            $task->interval = strval($task->interval);
            $task->task_category_id = strval($task->task_category_id);
            $assign_to = $task->assignedUsers()->pluck('user_id')->toArray();
            $task->assign_to = array_map('strval',$assign_to);
            $email_group = $task->emailGroups()->pluck('email_group_id')->toArray();
            $task->email_group = array_map('strval',$email_group);
            $user_group = TaskUserGroup::where('task_id', $id)->pluck('user_id')->toArray();
            $task->user_group = array_map('strval',$user_group);
            return $task;
        }

        public function getTaskFields()
        {
            $status = $this->issueRepository->getStatusTypesSelect();
            $users = $this->userRepository->allUsersSelect();
            $email_groups = $this->userRepository->emailGroupsSelect();
            $task_categories = $this->taskCategoryRepository->getTaskCategories();
            $priorities = $this->issueRepository->getPrioritiesSelect();
            $task_groups = TaskGroup::where('created_by', Auth::id())
                ->get()
                ->mapWithKeys(function ($task_group) {
                    return [$task_group['name'] => $task_group['name']];
                })->toArray();
            $assign_to = $task_groups + $users;
            $taskFields = [
                'status'=>$status,
                'users'=>$users,
                'task_categories'=>$task_categories,
                'priorities'=>$priorities,
                'assign_to'=>$assign_to,
                'email_groups'=>$email_groups
            ];
            return $taskFields;
        }
    }
