<?php
    /**
     * Date: 21/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Api;


    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Support\Facades\Auth;
    use PM\Models\Follow;
    use PM\Models\User;

    class UserController extends Controller
    {
        /**
         * Return all users
         * @return mixed
         */
        public function getUsers()
        {
            $users = User::where('active', 1)->get()
                ->map(function ($user) {
                    return ['id'=> $user['id'],'name' => $user['preferred_name']];
                })->toArray();

            return $users;
        }

        /**¬
         * @return mixed
         */
        public function allUsersSelect()
        {
            $users = User::where('active', 1)->get()
                ->map(function ($user) {
                    return ['id'=> $user['id'],'text' => $user['preferred_name']];
                })->toArray();

            return $users;
        }

        /**
         * Fetch all users
         * @param Request $request
         * @return LengthAwarePaginator
         */
        public function getAllUsers(Request $request)
        {
            $pagination = $request->get('pagination');

            $userQuery = User::with('department')
                ->where('active', 1)
                ->search($request->get('search'));

            if ($selected_department = $request->get('selected_department')) $userQuery->where('department_id', $selected_department);

            $users = $userQuery->get()->map(function ($user){
                if( $followed = Follow::where('follower_id', Auth::id())->where('followed_id', $user->id)->first())
                {
                    $user->followed = true;
                };
                return $user;
            });

            $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
            $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

            $offset = ($page * $perPage) - $perPage;
            $data = $users->slice($offset, $perPage, true);
            $users = new LengthAwarePaginator($data, $users->count(), $perPage, $page);
            $users->setPath('/api/get/projects');

            return $users;
        }
    }