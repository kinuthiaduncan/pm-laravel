<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace App\Http\Controllers\Api;


    use Carbon\Carbon;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Support\Facades\Auth;
    use Maatwebsite\Excel\Facades\Excel;
    use PM\Api\Transformers\IssueTransformer;
    use PM\Models\Comment;
    use PM\Models\Issue;
    use League\Fractal\Resource\Collection;
    use PM\Models\Status;
    use Illuminate\Support\Collection as LaravelCollection;
    use PM\Models\User;

    /**
     * Class IssueController
     * @package App\Http\Controllers\Api
     */
    class IssueController extends ApiController {

        /**
         * @return string
         */
        public function index()
        {
            return $this->processTable(new Issue(),  new IssueTransformer());
        }

        /**
         * @return string
         */

        public function myIssues()
        {
            $filter = function($issue)
            {
                return $issue->where('assigned_to', Auth::user()->id)->orWhere('created_by', Auth::user()->id);
            };
            return $this->processTable(new Issue(), new IssueTransformer(), $filter);
        }

        /**
         * @return string
         */
        public function myOpenIssues()
        {
            $filter = function($issue)
            {
                return $issue->where('assigned_to', Auth::user()->id)->where('status_id', '<', 3);
            };

            return $this->processTable(new Issue(), new IssueTransformer(), $filter);
        }


        /**
         * Return the issues belonging to a project
         * @param $projectId
         * @param null status
         * @return string
         */
        public function projectIssues($projectId, $status = null)
        {
            $filter = function ($issue) use ($projectId, $status)
            {
                if($status)
                {
                    $issue =  $issue->where('project_id', (int)$projectId)->where('status_id', (int)$status);
                }


                if($project_component = $this->searchParam('project_component'))
                {
                    $issue = $issue->where('component_id', $project_component);
                }

                if($user_id = $this->searchParam('assigned_to'))
                {
                    $issue = $issue->where('assigned_to', $user_id);
                }

                return $issue->orderBy('priority_id')->orderBy('updated_at')->latest();
            };

            return $this->processTable(new Issue(), new IssueTransformer(), $filter);
        }


        /**
         * @param $issueId
         * @param $section
         * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
         */
        public function updateMoved($issueId, $section)
        {
            $issue=Issue::findOrFail($issueId);

            $targetId=Status::where('slug', $section)->first()->id;

            $issue->status_id=$targetId;
            $issue->status_change_date=Carbon::now();
            $issue->save();

            //TODO raise an event to send notifications
            
            return response("", 201);
        }

        /**
         * @return \Illuminate\Http\RedirectResponse
         */
        public function exportIssuesToExcel()
        {
            $issues=Issue::where('updated_at', '>=', '2016-06-01 00:00:00')->where('updated_at', '<=', '2016-06-25 00:00:00')->whereIn('status_id', [3, 4])->orderBy('project_id')->orderBy('status_id')->get();
            //$issues=Issue::where('project_id', 7)->where('status_id',1)->get();
            Excel::create('PM Issues', function ($excel) use ($issues)
            {
                $excel->sheet('PM Issues', function ($sheet) use ($issues)
                {
                    $sheet->loadView('issues.exports.issues_export', ['issues'=>$issues]);
                });
            })->export('xls');

            return redirect()->back();
        }

        public function getComments($id)
        {
            $pagination = request()->get('pagination');
            $issue_comments = Comment::where('issue_id', $id)->latest()->get()
            ->map(function ($issue_comment) {
                $issue_comment->comment_user = $issue_comment->creator;
                $issue_comment->time = $issue_comment->created_at->diffForHumans();
                $issue_comment->date = $issue_comment->created_at->toDayDateTimeString();
            return $issue_comment;
        });

            $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
            $perPage = $pagination['per_page'];

            $offset = ($page * $perPage) - $perPage;
            $data = $issue_comments->slice($offset, $perPage, true);

            $issue_comments = new LengthAwarePaginator($data, $issue_comments->count(), $perPage, $page);
            $issue_comments->setPath('/api/get/issue/' .request()->get('id') . '/comments');

            return $issue_comments;
        }


    }