<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace App\Http\Controllers\Api;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use PM\Api\Transformers\ProjectTransformer;
    use PM\Models\Project;
    use PM\Models\ProjectPhase;
    use PM\Projects\IndividualProjectsFilter;
    use PM\Projects\PhasesFilter;
    use PM\Projects\PrivateProjectsFilter;
    use PM\Projects\ProjectsFilter;

    /**
     * Class ProjectController
     * @package App\Http\Controllers\Api
     */
    class ProjectController extends ApiController
    {

        /**
         * Function to get all projects
         * @return string
         */
        public function index()
        {
            return $this->processTable(new Project(), new ProjectTransformer());
        }

        /**
         * Function to get projects of the logged in users
         * @return string
         */
        public function myProjects()
        {
            $user = Auth::user();

            return $this->processTable(new Project(), new ProjectTransformer(), function ($project) use ($user) {
                return $project->whereHas('projectUsers', function ($q) use ($user) {
                    $q->where('user_id', $user->id);
                });
            });
        }

        /**
         * Function to all projects with filters
         * @return string
         */
        public function getProjects()
        {
            $input = request()->get('input');
            $pagination = request()->get('pagination');

            return ProjectsFilter::filter($input, $pagination);
        }

        public function getIndividualProjects()
        {
            $input = request()->get('input');
            $pagination = request()->get('pagination');

            return IndividualProjectsFilter::filter($input, $pagination);
        }
        public function getPrivateProjects()
        {
            $input = request()->get('input');
            $pagination = request()->get('pagination');

            return PrivateProjectsFilter::filter($input, $pagination);
        }

        /**
         * @return \Illuminate\Pagination\LengthAwarePaginator
         */
        public function getNewApprovals()
        {
            $pagination = request()->get('pagination');
            return PhasesFilter::newApprovals($pagination);
        }

        /**
         * Get phase updates awaiting approval
         * @return \Illuminate\Pagination\LengthAwarePaginator
         */
        public function getUpdatedApprovals()
        {
            $pagination = request()->get('pagination');
            return PhasesFilter::updatedApprovals($pagination);
        }
    }