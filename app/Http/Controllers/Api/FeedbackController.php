<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Notifications\FeedbackCreated;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use PM\Feedback\FeedbackRepository;
use PM\Models\File;
use PM\Models\FileType;
use PM\Models\Project;
use PM\Models\User;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Webpatser\Uuid\Uuid;

class FeedbackController extends Controller
{
    private $feedbackRepository;

    function __construct(FeedbackRepository $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }

    public function postFeedback(Request $request)
    {
        $input = $request->all();

        $feedback = $this->feedbackRepository->saveExternalFeedback($input);
        $project = Project::findOrFail($input['project_id']);
        $lead = User::findOrFail($project->project_lead);
        $lead->notify(new FeedbackCreated($feedback));

        if($request->file('file'))
        {
            $uploaded = $request->file('file');
            $file = File::upload($uploaded, $uploaded->getClientOriginalExtension(), $uploaded->getClientOriginalName(), 'feedback_file');
            $feedback->file()->associate($file);
            $feedback->save();
        }

        return response('', SymfonyResponse::HTTP_CREATED);
    }
}