<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 06/12/2016
 * Time: 10:01
 */

namespace App\Http\Controllers\Api;


use PM\Api\Transformers\CategoryTransformer;
use PM\Models\ProjectCategory;

class CategoriesController  extends ApiController
{

    /**
     * Function to get all projects
     * @return string
     */
    public function index()
    {
        return $this->processTable(new ProjectCategory(), new CategoryTransformer());
    }
}