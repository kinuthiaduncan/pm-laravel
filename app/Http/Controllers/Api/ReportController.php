<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Laracasts\Presenter\PresentableTrait;
use PM\Models\DailyReport;
use PM\Models\ReportSubItem;
use PM\Reports\ReportsFilter;
use PM\ReportItems\ReportItemsRepository;

class ReportController extends Controller
{
    use PresentableTrait;

    private $reportItemsRepository;

    /**
     * ReportController constructor.
     * @param ReportItemsRepository $reportItemsRepository
     */
    function __construct(ReportItemsRepository $reportItemsRepository)
    {
        $this->reportItemsRepository = $reportItemsRepository;
    }

    /**
     * Get reports with filter
     * @return LengthAwarePaginator
     */
    public function getReports()
    {

        $input = request()->get('input');
        $pagination = request()->get('pagination');

        $data = ReportsFilter::filter($input, $pagination);

        return $data;
    }

    /**
     * Get subitems for report create dropdown
     * @return mixed
     */
    public function getSubItems()
    {
        $input = Input::get('item');
        $subItems = $this->reportItemsRepository->getSubItems($input);
        return response()->json($subItems);
    }


    /**
     * Get items for report creation dropdown
     * @return mixed
     */
    public function getItems()
    {
        $input = Input::get('department_id');
        $items = $this->reportItemsRepository->getItems($input);
        return response()->json($items);
    }

    /**
     * Get daily reports for the daily reports page
     * @return mixed
     */
    public function getDailyReports()
    {
        $today = Carbon::today()->toDateString();
        $input = Input::get('department_id');
        $reports = DailyReport::where('department_id', $input)->where('date', $today)->get()
            ->map(function ($item) {
                return [
                    'item' => $item->item->item,
                    'subitem' => !is_null($item->subitem) ? $item->subitem->name : 'N/A',
                    'date' => $item->date,
                    'description' => strip_tags($item->description),
                ];
            })->toArray();

        return response()->json($reports);
    }
}
