<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 11/27/16
 * Time: 1:38 PM
 */

namespace App\Http\Controllers\Api;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use PM\Api\Transformers\CommentTransformer;
use PM\Models\Comment;

class CommentController extends ApiController
{

    /**
     * @return string
     */
    public function index()
    {
        return $this->processTable(new Comment(),  new CommentTransformer());
    }

    /*
     * List all comments for an issue
     */
    public function getIssueComments($id)
    {

        $comments = Comment::where('issue_id', $id)->orderBy('created_at','DESC')->paginate();

        $resource = new Collection($comments, new CommentTransformer());

        return (new Manager())->createData($resource)->toJson();
    }

}