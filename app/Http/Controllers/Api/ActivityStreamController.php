<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace App\Http\Controllers\Api;


    use Illuminate\Http\Request;
    use Illuminate\Pagination\LengthAwarePaginator;
    use PM\Models\ActivityStream;

    class ActivityStreamController extends ApiController
    {
        /**
         * Fetch activity stream
         * @param Request $request
         * @return LengthAwarePaginator|\Illuminate\Support\Collection
         */
        public function index(Request $request)
        {
            $pagination = $request->get('pagination');

            $activity_stream = ActivityStream::latest()->get()->map(function ($activity) {
                    $activity->action_time = $activity->created_at->toDateTimeString();
                    $activity->by = $activity->creator->preferred_name;

                    return $activity;
                });
//
            $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
            $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

            $offset = ($page * $perPage) - $perPage;
            $data = $activity_stream->slice($offset, $perPage, true);
            $activity_stream = new LengthAwarePaginator($data, $activity_stream->count(), $perPage, $page);
            $activity_stream->setPath('/api/get/projects');

            return $activity_stream;
        }
    }