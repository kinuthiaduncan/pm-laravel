<?php
    /**
     * Date: 30/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Http\Controllers\Api;


    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use PM\Models\Project;
    use PM\Models\Task;
    use PM\Models\User;

    class GlobalSearchController extends Controller
    {
        /**
         * Global search simple implementation
         * @param Request $request
         * @return array|string
         */
        public function search(Request $request)
        {
            if (!is_null($request->get('search_input'))) {
                $users = User::where('active', 1)->search($request->get('search_input'))
                    ->get();

                $tasks = Task::whereHas('assignedUsers', function ($query) {
                    $query->where('user_id', Auth::id());
                })->orWhere('created_by', Auth::id())
                    ->orWhere('task_access', 'public')->search($request->get('search_input'))
                    ->get();

                $projects = Project::where('project_access', 'public')->orWhere('project_lead', Auth::id())
                    ->orWhere('created_by', Auth::id())->search($request->get('search_input'))
                    ->get();

                if (!is_null($users) && !is_null($tasks)  && !is_null($projects)) {

                    $users = $users->map(function ($user) {
                        $user->title = $user->preferred_name;
                        $user->details = $user->email;
                        $user->url = '/user/summary/' . $user->id;

                        return $user;
                    })->toArray();

                    $tasks = $tasks->map(function ($task) {

                        $task->details = 'Assigned by ' . $task->creator->preferred_name;
                        $task->url = '/tasks/my-tasks/' . $task->id;
                        return $task;
                    })->toArray();

                    $projects = $projects->map(function ($project) {
                        $project->title = $project->name;
                        $project->details = strip_tags($project->project_description);
                        $project->url = '/project/' . $project->id;

                        return $project;
                    })->toArray();

                    $results = array_merge($projects, $tasks);
                    $results = array_merge($results, $users);

                } elseif (!is_null($users) && is_null($tasks) && is_null($projects)) {

                    $users = $users->map(function ($user) {
                        $user->title = $user->preferred_name;
                        $user->details = $user->email;
                        $user->url = '/user/summary/' . $user->id;

                        return $user;
                    })->toArray();

                    $results = $users;

                } elseif (is_null($users) && is_null($projects) && !is_null($tasks)) {

                    $tasks = $tasks->map(function ($task) {
                        $task->details = 'Assigned to ' . $task->assignee->preferred_name;
                        $task->url = '/tasks/my-tasks/' . $task->id;

                        return $task;
                    })->toArray();

                    $results = $tasks;

                } elseif (is_null($users) && !is_null($projects) && is_null($tasks)) {

                    $projects = $projects->map(function ($project) {
                        $project->title = $project->name;
                        $project->details = strip_tags($project->project_description);
                        $project->url = '/project/' . $project->id;

                        return $project;
                    })->toArray();

                    $results = $projects;

                } else {
                    $results = 'No search results found';
                }

                return $results;
            };
        }
    }