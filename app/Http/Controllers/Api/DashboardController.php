<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use PM\Models\Task;

class DashboardController extends Controller
{
    /**
     * Get user completed tasks
     * @param $user
     * @return int
     */
    public function getCompletedTasks($user)
    {
        $tasks = Task::where('status_id',4)->whereHas('assignedUsers',function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->get();

        $data = count($tasks);
        return $data;
    }

    /**
     * Get user ongoing tasks
     * @param $user
     * @return int
     */
    public function getOngoingTasks($user)
    {
        $tasks = Task::where('status_id',2)->whereHas('assignedUsers',function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->get();

        $data = count($tasks);
        return $data;
    }

    /**
     * User pending tasks
     * @param $user
     * @return int
     */
    public function getPendingTasks($user)
    {
        $tasks = Task::where('status_id',1)->whereHas('assignedUsers',function($assignedUser) use ($user){
            $assignedUser->where('user_id',$user);
        })->get();

        $data = count($tasks);
        return $data;
    }
}