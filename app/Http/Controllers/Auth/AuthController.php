<?php

    namespace App\Http\Controllers\Auth;

    use App\Http\Controllers\Controller;
    use App\Http\Requests;
    use Exception;
    use Illuminate\Http\Response;
    use Illuminate\Support\Facades\Auth;
    use Laracasts\Flash\Flash;
    use Laravel\Socialite\Facades\Socialite;
    use PM\Models\User;

    class AuthController extends Controller
    {

        /**
         * Redirect the user to the Google authentication page.
         *
         * @return Response
         */
        public function redirectToProvider()
        {
            return Socialite::driver('google')
                ->scopes(['openid', 'profile', 'email'])
                ->with(['hd' => 'cytonn.com'])
                ->redirect();
        }

        /**
         * Obtain the user information from Google.
         *
         * @return Response
         */
        public function handleProviderCallback()
        {
            try {
                $user = Socialite::driver('google')->user();
            } catch (Exception $e) {
                return redirect('/');
            }

            $authUser = $this->findOrCreateUser($user);

            $user_array = $authUser->toArray();
            if ($user_array['role_id'] == 4) {
                Auth::login($authUser, true);

                //Flash::error('Contact the administrator to activate your account');

                return redirect('/dashboard');
            } else {
                Auth::login($authUser, true);

                Flash::success('Successfully authenticated from Cytonn\'s Google account.');

                return redirect('/dashboard');
            }
        }

        /**
         * Return user if exists; create and return if doesn't
         *
         * @param $GoogleUser
         * @return User
         */
        private function findOrCreateUser($googleUser)
        {
            if ($authUser = User::where('email', $googleUser->email)->first()) {
                $authUser->update([
                    'google_id' => $googleUser->id,
                ]);
                return $authUser;
            }

            $names = explode(' ', $googleUser->name);

            return User::create([
                'firstname' => $names[0],
                'lastname' => $names[1],
                'email' => $googleUser->email,
                'google_id' => $googleUser->id,
                'role_id' => 4,
                'avatar' => $googleUser->avatar
            ]);
        }

    }
