<?php
/**
 * Cytonn Technologies
 *
 * @author: Timothy Kimathi <tkimathi@cytonn.com>
 *
 * Project: crm.
 *
 */

namespace App\Http\Controllers;

use PM\SearchOperations\ElasticOperations;

class BuildIndexesController extends Controller
{
    /*
     * Get the elactic operations class
     */
    private $elasticOperations;

    /*
     * Setup the constructor for the controller
     */
    public function __construct(ElasticOperations $elasticOperations)
    {
        $this->elasticOperations = $elasticOperations;
    }

    /*
     * Create the initial index to hold all the types from the tables,
     * NB: Run this once only
     * To rerun first delete the index first on the terminal
     */
    public function newIndex()
    {
        $response = $this->elasticOperations->createIndex();

        return view('buildindexes.new', [
            'response' => $response,
            'title'=>'Elastic Search Index'
        ]);
    }

    /*
     * Build an Index for the issues Table
     */
    public function issues()
    {
        $this->elasticOperations->indexIssuesTable();

        return view('buildindexes.index', [
            'table' => 'Issues',
            'title'=>'Elastic Search Index'
        ]);
    }

    /*
     * Build an Index for the projects Table
     */
    public function projects()
    {
        $this->elasticOperations->indexProjectsTable();

        return view('buildindexes.index', [
            'table' => 'Projects',
            'title'=>'Elastic Search Index'
        ]);
    }
}