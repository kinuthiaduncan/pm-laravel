<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PM\Models\Authorization;
use PM\Models\Permissions;
use PM\Models\Roles;

class AuthorizationController extends Controller
{
    public function index()
    {
        $permissions = Permissions::paginate(10);
        $roles = Roles::paginate(10);
        return view('authorization.index',['permissions'=>$permissions,'roles'=>$roles]);
    }
}
