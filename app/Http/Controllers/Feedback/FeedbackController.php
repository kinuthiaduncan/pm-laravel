<?php

namespace App\Http\Controllers\Feedback;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use PM\ActivityLogs\ActivityLogRepository;
use PM\Feedback\FeedbackRepository;
use PM\Issues\IssueRepository;
use PM\Models\Feedback;
use PM\Models\FeedbackFile;
use PM\Models\File;
use PM\Models\Project;
use PM\Models\ProjectComponent;
use PM\Projects\ProjectRepository;
use PM\Users\UserRepository;

class FeedbackController extends Controller
{
    private $feedbackRepository;
    protected $userRepository;
    protected $issueRepository;
    protected $projectRepository;

    /**
     * FeedbackController constructor.
     * @param FeedbackRepository $feedbackRepository
     * @param ProjectRepository $projectRepository
     * @param UserRepository $userRepository
     * @param IssueRepository $issueRepository
     */
    function __construct(FeedbackRepository $feedbackRepository,
                         ProjectRepository $projectRepository, UserRepository $userRepository,
                          IssueRepository $issueRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
        $this->issueRepository = $issueRepository;
        $this->userRepository = $userRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $project = Project::where('id',$id)->first();
        $feedback = Feedback::where('project_id',$id)->paginate(10);
        return view('projects.feedback.index',['project'=>$project,'feedback'=>$feedback]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $project = Project::where('id',$id)->first();
        return view('projects.feedback.create',['project'=>$project]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return Redirect
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'subject'=>'required|min:5|max:255',
            'description'=>'required|min:10',
            'files.*' => 'nullable|max:5000'
        ]);
        $files = $request->file('files');
        $this->feedbackRepository->saveFeedback($request, $files,$id);

        Flash::success('The feedback has been forwarded');
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param $project_id
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($project_id,$id)
    {
        $feedback = Feedback::findOrFail($id);
        $project_components = ProjectComponent::where('project_id',$project_id)->get();
       return view('projects.feedback.show',[
           'feedback'=>$feedback,
           'project'=>$feedback->project_id,
           'project_components'=>$project_components,
           'file_id' => $feedback->file_id
       ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Feedback::findOrFail($id)->forceDelete();

        Flash::success('The feedback has been deleted');
        return Redirect::back();
    }

    public function assignIssue($id,$projectId,$component)
    {
        $project = Project::find($projectId);
        $feedback = Feedback::where('id',$id)->first();
        $feedbackFile = File::findOrFail($feedback->file_id);
        $issues_array = $this->issueRepository->getParentIssues($projectId);
        $issue_types_array = $this->issueRepository->getIssueTypeSelect();
        $priorities_array = $this->issueRepository->getPrioritiesSelect();
        $users_array = $this->userRepository->allUsersSelect();

        return view('projects.feedback.partials.assign_issue',
            [
                'feedbackFile'=>$feedbackFile,
                'feedback'=>$feedback,
                'lead' => $project->project_lead,
                'title' => 'Project: ' . $project->name,
                'priorities' => $priorities_array,
                'users' => $users_array,
                'issues' => $issues_array,
                'issue_type' => $issue_types_array,
                'project_id' => $projectId,
                'component_id' => $component
            ]);
    }
}
