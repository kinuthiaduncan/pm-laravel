<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Api\Transformers;


    use League\Fractal\TransformerAbstract;
    use PM\Models\Project;

    class ProjectTransformer extends TransformerAbstract
    {

        public function transform(Project $project)
        {
            return [
                'id'        =>$project->id,
                'name'      =>$project->name,
                'access'    =>$project->project_access,
                'type'      =>$project->projectType->name,
                'key'       =>$project->project_key,
                'url'       =>$project->project_url,
                'lead'      =>$project->projectLead->preferred_name,
                'created_at'=>$project->present()->date,
            ];
        }
    }
