<?php
    /**
     * Date: 27/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Api\Transformers;


    use League\Fractal\TransformerAbstract;
    use PM\Models\User;

    class UserTransformer extends TransformerAbstract
    {

        public function transform(User $user)
        {
            return [
                'id' => $user->id,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'preferred_name'=>$user->preferred_name,
                'phone_number' => $user->phone_number,
                'email' => $user->email,
                'department' => $user->department->name,
                'job_level' => $user->job_level
            ];
        }
    }