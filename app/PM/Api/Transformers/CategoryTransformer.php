<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 06/12/2016
 * Time: 10:06
 */

namespace PM\Api\Transformers;


use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use PM\Models\ProjectCategory;
use PM\Presenters\UserPresenter;

class CategoryTransformer extends TransformerAbstract
{
    public function transform(ProjectCategory $category)
    {
        return [
            'id'        =>$category->id,
            'slug'      =>$category->slug,
            'name'      =>$category->name,
            'description'=>$category->description,
            'created_by'=>UserPresenter::presentFullNames($category->created_by),
            'created_at'=>Carbon::parse($category->date)->toDateString(),
        ];

    }

}