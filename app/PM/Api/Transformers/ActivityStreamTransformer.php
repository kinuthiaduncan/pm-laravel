<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Api\Transformers;


    use PM\Models\ActivityStream;
    use League\Fractal\TransformerAbstract;

    class ActivityStreamTransformer extends TransformerAbstract {

        public function transform(ActivityStream $activityStream)
        {
            return [
                'text'=>$activityStream->activity_description,
                'path'=>$activityStream->path,
                'action_time'=>$activityStream->created_at->toDateTimeString(),
                'by'=>$activityStream->creator->preferred_name
            ];
        }
    }