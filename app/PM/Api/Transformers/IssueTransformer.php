<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Api\Transformers;

    use Carbon\Carbon;
    use League\Fractal\TransformerAbstract;
    use PM\Models\Issue;


    class IssueTransformer extends TransformerAbstract
    {

        public function transform(Issue $issue)
        {
            return [
                'id'        =>$issue->id,
                'key'       =>$issue->projects->project_key . '-' . $issue->id,
                'project'   =>$issue->projects->name,
                'project_id'=>$issue->project_id,
                'title'     =>$issue->title,
                'type'      =>$issue->issueTypes->name,
                'priority'  =>$issue->priorities,
                'created_at'=>$issue->created_at->toDateString(),
                'due_date'  =>$issue->date_due,
                'lead'      =>$issue->projects->projectLead->id,
                'overdue_from'=>Carbon::parse($issue->date_due)->endOfDay()->toDateTimeString(),
                'status'    =>$issue->statuses->name,
                'status_change_date'=>$issue->status_change_date,
                'status_id'    =>$issue->status_id,
                'project_component'=>$issue->component->component_name,
                'done'      =>$issue->percentage_done . '%',
                'by'        =>$issue->creator->preferred_name,
                'by_id'        =>$issue->created_by,
                'assigned'  =>$issue->assignedTo->preferred_name,
                'comments_count'  =>$issue->comments()->count(),
                'created'       => calculate_date_diff($issue->updated_at),
                'resolved'  => (bool)$issue->completed_at
            ];
        }
    }
