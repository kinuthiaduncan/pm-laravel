<?php
/**
 * Date: 25/08/2016
 * Time: 12:51 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: ct-project-management
 * Cytonn Technologies
 */

namespace PM\Api\Transformers;


use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

class ModelTransformer extends TransformerAbstract
{

    public function transform(Model $model)
    {
        return $model->toArray();
    }

}