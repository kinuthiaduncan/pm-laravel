<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 11/27/16
 * Time: 1:29 PM
 */

namespace PM\Api\Transformers;
use League\Fractal\TransformerAbstract;
use PM\Models\Comment;
use Carbon\Carbon;

class CommentTransformer extends TransformerAbstract
{

    public function transform(Comment $comment)
    {
        return [
            'comment_id' => $comment->id,
            'avatar' => $comment->creator->avatar,
            'comment' => $comment->name,
            'image' => $comment->image,
            'file' => $comment->file,
            'filename' => $comment->filename,
            'created_by' => $comment->created_by,
            'username' => $comment->creator->preferred_name,
            'time' => $comment->created_at->toDateTimeString(),
        ];
    }
}