<?php
/**
 * Date: 28/01/2016
 * Time: 5:14 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace PM\Api\DataTables;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use PM\Api\Transformers\ModelTransformer;

/**
 * Class SortFilterPaginateTrait
 * @package Cytonn\Api\DataTables
 */
trait SortFilterPaginateTrait
{

    /**
     * @var int
     */
    protected $perPage = 10;
    protected $stateArray = [];

    /*
     * Get the filter, sort and paginate data from the
     */
    public function getTableStateData()
    {
        $state = Input::get('tableState');

        $stateArray = array();

        //Get the Search Query
        if(isset($state['search']['predicateObject']['$']))
        {
            $stateArray['query'] = $state['search']['predicateObject']['$'];
        }
        else
        {
            $stateArray['query'] = null;
        }

        //Get the Sort Query
        $dir = 'desc';
        $term = 'created_at';
        if(isset($state['sort']))
        {
            if(isset($state['sort']['reverse']))
            {
                $state['sort']['reverse'] == 'true' ? $dir = 'desc' : $dir = 'asc';
            }

            if(isset($state['sort']['predicate']))
            {
                $term = $state['sort']['predicate'];
            }
        }
        $stateArray['sortDirection'] = $dir;
        $stateArray['sortItem'] = $term;


        //Get the Pagination Details
        if($state['pagination'])
        {
            if(isset($state['pagination']['start']))
            {
                $stateArray['offset'] = (int)$state['pagination']['start'];
            }
            else
            {
                $stateArray['offset'] = 0;
            }

            if(isset($state['pagination']['number']))
            {
                $stateArray['itemsPerPage'] = $state['pagination']['number'];
            }
            else
            {
                $stateArray['itemsPerPage'] = $this->perPage;
            }
        }
        else
        {
            $stateArray['offset'] = 0;

            $stateArray['itemsPerPage'] = $this->perPage;
        }

        return $stateArray;
    }

    /**
     * filter, sort and paginate the model
     * @param $model
     * @return array
     */
    public function sortFilterPaginate($model)
    {
        $filtered = $this->filter($model);

        $sorted = $this->sort($filtered);

        return $this->paginate($sorted);
    }

    /**
     * filter the model
     * @param $model
     * @return mixed
     */
    public function filter($model, $filterFunc)
    {
        $originalModel = $model->sortedSearch($this->stateArray['query'], $this->stateArray['sortItem'], $this->stateArray['sortDirection']);

        $model = $originalModel;
        
        if(!is_null($filterFunc))
        {
            $model = $filterFunc($model);
        }

        $paginatedModel = $model->slice($this->stateArray['offset'],$this->stateArray['itemsPerPage'] );

        return [
            'model' => $paginatedModel,
            'offset' => $this->stateArray['offset'],
            'total_pages'=>ceil(count($model)/$this->stateArray['itemsPerPage']),
            'total_results' => count($model)
        ];
    }

    /**
     * Sort the model
     * @param $model
     * @return mixed
     */
    public function sort($model)
    {
        $model = $model->orderBy($this->stateArray['sortItem'], $this->stateArray['sortDirection']);

        return $model;
    }

    /**
     * Paginate the model
     * @param $model
     * @return array
     */
    public function paginate($model)
    {
        $page = $model->get();

        $model = $model->offset($this->stateArray['offset'])->take($this->stateArray['itemsPerPage'])->get();

        $totalPages = ceil(count($page)/$this->stateArray['itemsPerPage']);

        return [
            'model' => $model,
            'offset' => $this->stateArray['offset'],
            'total_pages' => $totalPages,
            'total_results' => count($page)
        ];
    }

    /**
     * @param $model
     * @return array
     */
    public function sortAndPaginate($model)
    {
        return $this->paginate($this->sort($model));
    }

    /**
     * Add the pagination information to the resource
     * @param $paginatedModel
     * @param \League\Fractal\Resource\Collection $resource
     * @return mixed
     */
    public function addPaginationToResource($paginatedModel, $resource)
    {
        return $resource->setMetaValue('pagination', [
            'offset'=>$paginatedModel['offset'],
            'total_pages'=>$paginatedModel['total_pages'],
            'count' => $paginatedModel['total_results']
        ]);
    }


    /**
     * Combine all functions into one simple API
     * @param $model
     * @param TransformerAbstract|null $transformer
     * @param \Closure|null $filterFunc
     * @param \Closure|null $modifyResource
     * @return string
     */
    public function processTable($model, TransformerAbstract $transformer = null, \Closure $filterFunc = null, \Closure $modifyResource = null)
    {
        $this->stateArray = $this->getTableStateData();

        if(! is_null($this->stateArray['query']))
        {
            $paginated = $this->filter($model, $filterFunc);
        }
        else
        {
            if(!is_null($filterFunc))
            {
                $filtered = $filterFunc($model)->latest();
            }
            else
            {
                $filtered = $model->latest();
            }
            
            $paginated = $this->sortAndPaginate($filtered);
        }

        if(is_null($transformer))
        {
            $transformer = new ModelTransformer();
        }

        $resource = new Collection($paginated['model'], $transformer);

        $this->addPaginationToResource($paginated, $resource);

        if(!is_null($modifyResource))
        {
            $modifyResource($resource);
        }

        return (new Manager())->createData($resource)->toJson();
    }

    protected function searchParam($name)
    {
        $state = Input::get('tableState');

        //Get the Search Query
        if(isset($state['search']['predicateObject'][$name]))
        {
            return $state['search']['predicateObject'][$name];
        }


        return null;
    }
}