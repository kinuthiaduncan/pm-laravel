<?php
/**
 * Date: 28/01/2016
 * Time: 5:14 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace PM\Api\DataTables;

use Illuminate\Database\QueryException;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use PM\Api\Transformers\ModelTransformer;

/**
 * Class SortFilterPaginateTrait
 * @package Cytonn\Api\DataTables
 */
trait SortFilterPaginateTraitOld
{

    /**
     * @var int
     */
    protected $perPage = 10;

    /**
     * filter, sort and paginate the model
     * @param $model
     * @return array
     */
    public function sortFilterPaginate($model)
    {
        $filtered = $this->filter($model);

        $sorted = $this->sort($filtered);

        return $this->paginate($sorted);
    }

    /**
     * filter the model
     * @param $model
     * @return mixed
     */
    public function filter($model)
    {
        $state = \Input::get('tableState');

        //filter
        if(isset($state['search']['predicateObject']['$']))
        {
            $query = $state['search']['predicateObject']['$'];

            $model = $model::search($query);
        }

        return $model;
    }

    /**
     * Sort the model
     * @param $model
     * @return mixed
     */
    public function sort($model)
    {
        $state = \Input::get('tableState');

        //sort
        if(isset($state['sort']))
        {
            try
            {
                $state['sort']['reverse'] == 'true' ? $dir = 'DESC' : $dir = 'ASC' ;

                $model = $model->orderBy($state['sort']['predicate'], $dir);
            }
            catch(QueryException $e)
            {

            }
        }

        return $model;
    }

    /**
     * Paginate the model
     * @param $model
     * @return array
     */
    public function paginate($model)
    {
        $state = \Input::get('tableState');

        $model_count = count($model->get());

        //pagination
        if($state['pagination'])
        {
            $offset = (int)$state['pagination']['start'];
            $this->perPage = $state['pagination']['number'];
            $model = $model->offset($offset)->take($this->perPage)->get();
        }
        else
        {
            $offset = 0;
            $model = $model->offset($offset)->take($this->perPage)->get();
        }

        $totalPages = ceil($model_count/$this->perPage);

        return [
            'model'=>$model,
            'offset'=>$offset,
            'total_pages'=>$totalPages
        ];
    }

    /**
     * @param $model
     * @return array
     */
    public function sortAndPaginate($model)
    {
        return $this->paginate($this->sort($model));
    }

    /**
     * Add the pagination information to the resource
     * @param $paginatedModel
     * @param \League\Fractal\Resource\Collection $resource
     * @return mixed
     */
    public function addPaginationToResource($paginatedModel, $resource)
    {
        return $resource->setMetaValue('pagination', ['offset'=>$paginatedModel['offset'], 'total_pages'=>$paginatedModel['total_pages']]);
    }


    /**
     * Combine all functions into one simple API
     * @param $model
     * @param TransformerAbstract|null $transformer
     * @param \Closure|null $filterFunc
     * @param \Closure|null $modifyResource
     * @return string
     */
    public function processTable($model, TransformerAbstract $transformer = null, \Closure $filterFunc = null, \Closure $modifyResource = null)
    {
        dd("Hre");
//        $filtered = $this->filter($model);
//
//        if(!is_null($filterFunc))
//        {
//            $filtered = $filterFunc($filtered);
//        }
//
//        if(is_null($transformer))
//        {
//            $transformer = new ModelTransformer();
//        }
//
//        $paginated = $this->sortAndPaginate($filtered);
//
//        $resource = new Collection($paginated['model'], $transformer);
//
//        $this->addPaginationToResource($paginated, $resource);
//
//        if(!is_null($modifyResource))
//        {
//            $modifyResource($resource);
//        }
//
//        return (new Manager())->createData($resource)->toJson();
    }
}