<?php
/**
 * Date: 28/01/2016
 * Time: 5:12 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace PM\Api\DataTables;


interface SortFilterPaginateInterface
{
    public function sortFilterPaginate($model);

}