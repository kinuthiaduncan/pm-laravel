<?php
    /**
     * Date: 20/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Tasks;


    use Illuminate\Support\Facades\Auth;
    use PM\Models\TaskCategory;
    use PM\Models\TaskSubCategories;
    use PM\Models\User;

    class TaskCategoriesRepository
    {
        /**
         * Save a category
         * @param $request
         */
        public function save($request)
        {
            $request['created_by'] = Auth::id();
            $request['department_id'] = Auth::user()->department->id;

            TaskCategory::create($request);
        }

        /**
         * Edit a category
         * @param $request
         */
        public function edit($request, $id)
        {
            $category = TaskCategory::findOrFail($id);

            $category->update($request);
        }

        /**
         * Get an array of categories
         * @return mixed
         */
        public function getTaskCategories()
        {

            $task_categories = TaskCategory::get()
                ->mapWithKeys(function ($task_category) {
                return [$task_category['id'] => $task_category['name']];
            })->toArray();

            return $task_categories;
        }

        /**
         * @param $category
         * @return mixed
         */
        public function getTaskSubCategories($id)
        {
            $subcategories = TaskSubCategories::where('task_category_id',$id)->get()
                ->mapWithKeys(function ($subcategory){
                    return [$subcategory['id'] => $subcategory['subcategory']];
                })->toArray();
            return $subcategories;
        }
    }