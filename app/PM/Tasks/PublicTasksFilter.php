<?php
    /**
     * Date: 01/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Tasks;


    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Support\Facades\Auth;
    use Laracasts\Presenter\PresentableTrait;
    use PM\Models\Task;
    use PM\Models\User;

    class PublicTasksFilter
    {
        use PresentableTrait;

        public static function filter($request, $pagination)
        {
            $query = self::query();
            $methods = self::scopes();

            foreach ($request as $key => $param) {

                // If a scope exists in the provided array, add it to the query.
                if (array_key_exists($key, $methods)) {
                    switch ($key) {
                        case "title":
                            $query->OfTaskTitle($param);
                            break;
                        case "status":
                            $query->OfTaskStatus($param);
                            break;
                        case "task_access":
                            $query->OfTaskAccess($param);
                            break;
                        case "department":
                            $query->OfTaskDepartment($param);
                            break;
                        case "user":
                            $query->OfTaskAssignedTo($param);
                            break;
                        default:
                            $query;
                    }
                }
            }

            $tasks = $query->get();

            $tasks = $tasks->map(function ($task) {
                $task->creator = $task->creator->preferred_name;
                $task->created_at_x = $task->present()->date;
                $task->status = $task->status->name;
                $task->priority = $task->priority->name;
                $task->description = strip_tags($task->description);
                $assigned_user = $task->assignedUsers()->first();
                $task->assigned = (count($assigned_user)) ? $assigned_user->user->preferred_name : '';

                return $task;
            });

            $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
            $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

            $offset = ($page * $perPage) - $perPage;
            $data = $tasks->slice($offset, $perPage, true);
            $tasks = new LengthAwarePaginator($data, $tasks->count(), $perPage, $page);
            $tasks->setPath('/api/get/tasks');

            return $tasks;
        }

        private static function scopes()
        {
            $methods = [
                'title' => 'OfTaskTitle',
                'status' => 'OfTaskStatus',
                'task_access' => 'OfTaskAccess',
                'department' => 'OfTaskDepartment',
                'user' => 'OfTaskAssignedTo'
            ];

            return $methods;
        }

        private static function query()
        {
            $auth_user_hr_id = User::whereNotNull('hr_id')->where('id', Auth::id())->pluck('hr_id')->toArray();

            $supervised = User::where('supervisor_id', $auth_user_hr_id[0])
                ->orWhere('manager_id', $auth_user_hr_id[0])
                ->get()->map(function ($user) {
                    return $user->id;
                })->toArray();

            if (Auth::user()->job_level == 'Manager') {
                $query = Task::whereHas('assignedUsers', function ($query) {
                    $query->where('user_id', Auth::id());
                })->orWhere('department_id', Auth::user()->department_id)
                    ->orWhere('task_access', 'public')
                    ->latest()
                    ->orderBy('id');
            } elseif (!empty($supervised)) {
                $query = Task::whereHas('assignedUsers', function ($query) use ($supervised) {
                    $query->whereIn('user_id', $supervised);
                })->orWhereIn('created_by', $supervised)
                    ->orWhere('task_access', 'public')
                    ->latest()
                    ->orderBy('id');
            } else {
                $query = Task::whereHas('assignedUsers', function ($query) {
                    $query->where('user_id', Auth::id());
                })->orWhere('created_by', Auth::id())
                    ->orWhere('task_access', 'public')
                    ->latest()
                    ->orderBy('id');

            }

            return $query;
        }
    }