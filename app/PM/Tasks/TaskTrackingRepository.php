<?php

namespace PM\Tasks;

use App\Notifications\TaskTrackingCreated;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PM\Models\IssueTracking;
use PM\Models\Task;
use PM\Models\TaskTracking;
use PM\Models\User;

class TaskTrackingRepository
{

    /**
     * Save task tracking
     * @param $request
     * @param $task_id
     */
    public function save($request, $task_id)
    {
        $tracking = TaskTracking::create([
            'date'=>$request->input('date'),
            'start_time'=>Carbon::parse($request->input('start_time'))->toTimeString(),
            'end_time'=>Carbon::parse($request->input('end_time'))->toTimeString(),
            'description'=>$request->input('description'),
            'task_id'=>$task_id,
            'created_by'=> Auth::user()->id
        ]);

        $task = Task::where('id',$task_id)->first();

        // Notifications
        $assigned_users = $task->assignedUsers()->get();

        $assigned_users->map(function ($assignee) use ($task, $tracking, $assigned_users) {
            $user = User::findOrFail($assignee->user_id);

            $user->notify(new TaskTrackingCreated($task, $tracking));
        });
    }

    public function saveFromTask($request, $task_id)
    {
        $tracking = TaskTracking::create([
            'date' => $request['date'],
            'start_time' => Carbon::parse($request['start_time'])->toDateString(),
            'end_time' => Carbon::parse($request['end_time'])->toDateString(),
            'task_id' => $task_id,
            'created_by' => Auth::user()->id,
            'description'=>$request['schedule_description'],
        ]);

        $task = Task::where('id', $task_id)->first();

        // Notifications
        $assigned_users = $task->assignedUsers()->get();

        $assigned_users->map(function ($assignee) use ($task, $tracking, $assigned_users) {
            $user = User::findOrFail($assignee->user_id);

            $user->notify(new TaskTrackingCreated($task, $tracking));
        });
    }

    /**
     * Fetch schedules to display to timeline
     * @param $user
     * @return array
     */
    public function fetchAllSchedules($user)
    {
        if ($user->department_id == 2) {
            $task_schedules = TaskTracking::where('created_by', $user->id)->get();
            $issue_schedules = IssueTracking::where('created_by', $user->id)->get();

            $task_schedules = $task_schedules->map(function ($event) {
                $event->title = 'Task: '.$event->task->title;
                $event->start = $event->date . ' ' . $event->start_time;
                $event->end = $event->date . ' ' . $event->end_time;
                return $event;
            })->toArray();

            $issue_schedules = $issue_schedules->map(function ($event) {
                $event->title = 'Issue: '.$event->issue->title;
                $event->start = $event->date . ' ' . $event->start_time;
                $event->end = $event->date . ' ' . $event->end_time;
                return $event;
            })->toArray();
           $events  = array_merge($task_schedules,$issue_schedules);

            return $events;
        }
        else {
            $events = TaskTracking::where('created_by', $user->id)->get();

            $events = $events->map(function ($event) {
                $event->title = $event->task->title;
                $event->start = $event->date . ' ' . $event->start_time;
                $event->end = $event->date . ' ' . $event->end_time;
                return $event;
            });
            return $events;
        }

    }
}