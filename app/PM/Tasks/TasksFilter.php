<?php
    /**
     * Date: 23/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Tasks;

    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Support\Facades\Auth;
    use Laracasts\Presenter\PresentableTrait;
    use PM\Models\Task;

    class TasksFilter
    {
        use PresentableTrait;

        public static function filter($request, $pagination)
        {
            $query = self::query();
            $methods = self::scopes();

            foreach ($request as $key => $param) {

                // If a scope exists in the provided array, add it to the query.
                if (array_key_exists($key, $methods)) {
                    switch ($key) {
                        case "title":
                            $query->OfTaskTitle($param);
                            break;
                        case "status":
                            $query->OfTaskStatus($param);
                            break;
                        case "task_access":
                            $query->OfTaskAccess($param);
                            break;
                        default:
                            $query;
                    }
                }
            }

            $tasks = $query->get();

            $tasks = $tasks->map(function ($task) {
                $task->creator = $task->creator->preferred_name;
                $task->created_at_x = $task->present()->date;
                $task->status = $task->status->name;
                $task->priority = $task->priority->name;
                $task->description = strip_tags($task->description);
                $assigned_user = $task->assignedUsers()->first();
                $task->assigned = (count($assigned_user)) ? $assigned_user->user->preferred_name : '';

                return $task;
            });

            $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
            $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

            $offset = ($page * $perPage) - $perPage;
            $data = $tasks->slice($offset, $perPage, true);
            $tasks = new LengthAwarePaginator($data, $tasks->count(), $perPage, $page);
            $tasks->setPath('/api/get/tasks');

            return $tasks;
        }

        private static function scopes()
        {
            $methods = [
                'title' => 'OfTaskTitle',
                'status' => 'OfTaskStatus',
                'task_access' => 'OfTaskAccess'
            ];

            return $methods;
        }

        private static function query()
        {
            $query = Task::whereHas('assignedUsers', function ($query) {
                $query->where('user_id', Auth::id());
            })->orWhere('created_by', Auth::id())->latest()->orderBy('id');

            return $query;
        }
    }