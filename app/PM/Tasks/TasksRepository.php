<?php
    /**
     * Date: 20/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Tasks;


    use App\Notifications\TaskCompleted;
    use App\Notifications\TaskCreated;
    use App\Notifications\TaskCreatedFromEmail;
    use App\Notifications\TaskUpdated;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Storage;
    use PM\ActivityLogs\ActivityLogRepository;
    use PM\Library\Mailer;
    use PM\Models\AssignedUser;
    use PM\Models\EmailGroups;
    use PM\Models\Follow;
    use PM\Models\Task;
    use PM\Models\TaskEmailGroup;
    use PM\Models\TaskFile;
    use PM\Models\TaskGroup;
    use PM\Models\TaskGroupMember;
    use PM\Models\TaskUserGroup;
    use PM\Models\User;
    use Webpatser\Uuid\Uuid;


    class TasksRepository
    {
        private $taskTrackingRepository;
        /**
         * @var Mailer
         */
        private $mailer;

        /**
         * TasksRepository constructor.
         * @param Mailer $mailer
         * @param TaskTrackingRepository $taskTrackingRepository
         */
        public function __construct(Mailer $mailer, TaskTrackingRepository $taskTrackingRepository)
        {
            $this->mailer = $mailer;
            $this->activityLogRepository = new ActivityLogRepository();
            $this->taskTrackingRepository = $taskTrackingRepository;
        }

        /**
         * Fetch all tasks
         * @return \Illuminate\Database\Eloquent\Collection|static[]
         */
        public function getAllTasks()
        {
            $all_tasks = Task::all();

            return $all_tasks;
        }

        /**
         * Store a task
         * @param $request
         * @param $files
         */
        public function save($request, $files)
        {
            $request['created_by'] = Auth::id();
            $request['department_id'] = Auth::user()->department->id;
            $request['status_id'] = 1;
            $request['assign_date'] = Carbon::parse($request->assign_date)->toDateString();
            $request['due_date'] = Carbon::parse($request->due_date)->toDateString();
            $task = Task::create($request->except('file'));

            // Save assigned users
            $this->saveAssignedUsers($request, $task->id);

            // Save the user group
            $this->saveUserGroup($request, $task->id);
            //save email group
            $this->saveEmailGroup($request, $task->id);
            // Save the files
            $this->saveFiles($files, $task->id);

//            if ($task->task_access == 'public') {
//                $this->activityLogRepository->logTaskCreation($task);
//            }

            // Notifications
            $assigned_users = $task->assignedUsers()->get();

            $assigned_users->map(function ($assignee) use ($task, $assigned_users) {
                $user = User::findOrFail($assignee->user_id);

                $user->notify(new TaskCreated($task, $assigned_users));

                //send a notification to the users following the assignee
                Follow::where('followed_id', $assignee->user_id)->get()
                    ->map(function ($follower) use ($task, $assigned_users) {
                        $id = $follower->follower_id;
                        $user = User::findOrFail($id);
                        $user->notify(new TaskCreated($task, $assigned_users));
                    });

                //send notification to task group
                TaskUserGroup::where('task_id', $task->id)->where('user_id', '!=', $assignee->user_id)->get()
                    ->map(function ($user_group) use ($task, $assigned_users) {
                        $id = $user_group->users->id;
                        $user = User::findOrFail($id);
                        $user->notify(new TaskCreated($task, $assigned_users));
                    });
            });
            TaskEmailGroup::where('task_id', $task->id)->get()->map(function($email_group) use ($task, $assigned_users){
                $id = $email_group->emailGroups->id;
                $group = EmailGroups::findOrFail($id);
                $group->notify(new TaskCreated($task, $assigned_users));
            });
            $creator = User::findOrFail($request['created_by']);

            $creator->notify(new TaskCreated($task, $assigned_users));
        }

        /**
         * Save task email group
         * @param $request
         * @param $task_id
         */
        public function saveEmailGroup($request, $task_id)
        {
            if($request->file) {
                if($request->email_group){
                    $emailGroup = explode(',', $request->email_group);
                    $request['email_group'] = $emailGroup;
                }
            }
            $email_group = $request['email_group'];
            if(count($email_group) > 0) {
                foreach ($email_group as $email)
                {
                    $email_array[] = $email;
                    $email_group_input['email_group_id'] = $email;
                    $email_group_input['task_id'] = $task_id;
                    $present_group = TaskEmailGroup::where('task_id', $task_id)
                        ->where('email_group_id', $email)->get()->toArray();
                    if (empty($present_group)) {
                        TaskEmailGroup::create($email_group_input);
                    }
                }
            }
        }
        /**
         * Save a user group
         * @param $request
         * @param $task_id
         */
        public function saveUserGroup($request, $task_id)
        {
            if($request->file) {
                if($request->user_group){
                    $userGroup = explode(',', $request->user_group);
                    $request['user_group'] = $userGroup;
                }
            }
            $user_group = $request['user_group'];
            if(count($user_group) > 0) {
                foreach ($user_group as $user) {
                    $user_array[] = $user;

                    $user_group_input['user_id'] = $user;
                    $user_group_input['task_id'] = $task_id;

                    $present_user = TaskUserGroup::where('task_id', $task_id)
                        ->where('user_id', $user)
                        ->get()->toArray();

                    if (empty($present_user)) {
                        TaskUserGroup::create($user_group_input);
                    }
                }
            }
        }

        /**
         * Save assigned users
         * @param $request
         * @param $task_id
         */
        public function saveAssignedUsers($request, $task_id)
        {
            if($request->file) {
                if($request->assign_to){
                    $assigned = explode(',', $request->assign_to);
                    $request['assign_to'] = $assigned;
                }
            }
            foreach ($request->get('assign_to') as $assignee) {

                $user = User::where('id', $assignee)->first();

                if (!is_null($user)) {
                    $user_input['user_id'] = $user->id;
                    $user_input['task_id'] = $task_id;

                    $present_user = AssignedUser::where('task_id', $task_id)
                        ->where('user_id', $user->id)
                        ->get()->toArray();

                    if (empty($present_user)) {
                        AssignedUser::create($user_input);
                    }
                } else {
                    $grp = TaskGroup::where('name', $assignee)->first();

                    TaskGroupMember::where('task_group_id', $grp->id)
                        ->get()
                        ->map(function ($assigned) use ($task_id, $grp) {
                            $user_input['user_id'] = $assigned->user_id;
                            $user_input['task_id'] = $task_id;

                            $present_user = AssignedUser::where('task_id', $task_id)
                                ->where('user_id', $assigned->user_id)
                                ->get()->toArray();

                            if (empty($present_user)) {
                                AssignedUser::create($user_input);
                            }
                        });
                }
            }
        }

        /**
         * Save files
         * @param $files
         * @param $task_id
         * @return $this|\Illuminate\Http\RedirectResponse
         * @throws \Exception
         */
        public function saveFiles($files, $task_id)
        {
            if (!is_null($files)) {
                    $filename = $files->getClientOriginalName();
                    $file_name = Uuid::generate();
                    $saved_file = Storage::put('public/task_files/' . $file_name, $files);

                    $file_input['name'] = $filename;
                    $file_input['url'] = Storage::url($saved_file);
                    $file_input['created_by'] = Auth::id();
                    $file_input['task_id'] = $task_id;

                TaskFile::create($file_input);
            }
        }

        /**
         * Update a task
         * @param $request
         * @param $files
         * @param $id
         */
        public function edit($request, $files, $id)
        {

            $task = Task::findOrFail($id);

            $assigned_users_array = $task->assignedUsers()->pluck('user_id')->toArray();

            $removed_assigned_users_ids = array_diff($assigned_users_array, $request->get('assign_to'));

            AssignedUser::whereIn('user_id', $removed_assigned_users_ids)
                ->where('task_id', $task->id)->delete();

            $creator = User::findOrFail($task->created_by);

            $updatedBy = Auth::user();

            if ($request['status_id'] == 4) {
                $request['completed_at'] = Carbon::now();
                $request['status_change_date'] = Carbon::now();

            } elseif ($request['status_id'] != 1) {
                $request['status_change_date'] = Carbon::now();

            } elseif ($request['status_id'] != 4) {
                $request['completed_at'] = NULL;
            }

            if ($request['repetitive'] == 0) {
                $request['interval'] = NULL;
            }

            $task->update($request->toArray());

            // Save assigned users
            $this->saveAssignedUsers($request, $task->id);

            // save the user group
            $this->saveUserGroup($request, $task->id);
            // save the email group
            $this->saveEmailGroup($request, $task->id);
            // save the files
            $this->saveFiles($files, $task->id);

            // Notifications
            $assigned_users = $task->assignedUsers()->get();

            $assigned_users->map(function ($assignee) use ($task, $assigned_users,$updatedBy) {
                $user = User::findOrFail($assignee->user_id);

                $user->notify(new TaskUpdated($task, $assigned_users,$updatedBy));

                //send a notification to the users following the assignee
                Follow::where('followed_id', $assignee->user_id)->get()
                    ->map(function ($follower) use ($task, $assigned_users,$updatedBy) {
                        $id = $follower->follower_id;
                        $user = User::findOrFail($id);
                        $user->notify(new TaskUpdated($task, $assigned_users,$updatedBy));
                    });
                //send notification to email groups
                TaskEmailGroup::where('task_id', $task->id)->get()->map(function($email_group) use ($task, $assigned_users,$updatedBy){
                    $id = $email_group->emailGroups->id;
                    $group = EmailGroups::findOrFail($id);
                    $group->notify(new TaskUpdated($task, $assigned_users,$updatedBy));
                });
                //send notification to task group
                TaskUserGroup::where('task_id', $task->id)->where('user_id', '!=', $assignee->user_id)->get()
                    ->map(function ($user_group) use ($task, $assigned_users,$updatedBy) {
                        $id = $user_group->users->id;
                        $user = User::findOrFail($id);
                        $user->notify(new TaskUpdated($task, $assigned_users,$updatedBy));
                    });
            });

            $creator->notify(new TaskUpdated($task, $assigned_users,$updatedBy));

            if ($task->status_id == 4) {
                $assigned_users->map(function ($assignee) use ($task, $assigned_users) {
                    //send a notification to the users following the assignee on completion
                    Follow::where('followed_id', $assignee->user_id)->get()
                        ->map(function ($follower) use ($task, $assigned_users) {
                            $id = $follower->follower_id;
                            $user = User::findOrFail($id);
                            $user->notify(new TaskCompleted($task, $assigned_users));
                        });
                });

                $creator->notify(new TaskCompleted($task, $assigned_users));
            }
        }
        public function getTasks($id)
        {
            $today = Carbon::today()->toDateString();

            $tasks = Task::where('due_date','>=',$today)->whereHas('status', function ($status) {
                $status->where('resolution', '!=', true);

            })->whereHas('assignedUsers', function ($query) use ($id) {
                $query->where('user_id', $id);
            })->get()->mapWithKeys(function ($task) {
                    return [$task['id'] => $task['title']];
                })->toArray();

            return $tasks;
        }

        /**
         * Save tasks assigned from email
         * @param $taskData
         */
        public function saveFromEmail($taskData)
        {
            $assignor = User::where('email',$taskData['creator'])->first();
            if(!is_null($assignor)) {
                $assigned = $taskData['assignee'];

                $task = Task::create([
                    'title' => $taskData['title'],
                    'task_access' => 'public',
                    'due_date' => Carbon::today()->addDays(3)->toDateString(),
                    'status_id' => 1,
                    'priority_id' => 3,
                    'created_by' => $assignor->id,
                    'description' => $taskData['description'],
                ]);
                //save assigned users
                $this->saveAssignedFromEmail($assigned, $task->id);
                //get assigned users
                $assigned_users = $task->assignedUsers()->get();
                //notify assignor and assigned users
                $assignor->notify(new TaskCreatedFromEmail($task, $assigned_users, $assignor));

                $assigned_users->map(function ($assignee) use ($task, $assigned_users, $assignor) {
                    $user = User::findOrFail($assignee->user_id);
                    $user->notify(new TaskCreatedFromEmail($task, $assigned_users, $assignor));
                });
            }
        }

        /**
         * Save users assigned from email
         * @param $assigned
         * @param $task_id
         */
        public function saveAssignedFromEmail($assigned, $task_id)
        {
            foreach ($assigned as $assignee) {

                $user = User::where('email', $assignee)->first();

                if (!is_null($user)) {
                    $user_input['user_id'] = $user->id;
                    $user_input['task_id'] = $task_id;
                    $present_user = AssignedUser::where('task_id', $task_id)
                        ->where('user_id', $user->id)
                        ->get()->toArray();
                    if (empty($present_user)) {
                        AssignedUser::create($user_input);
                    }
                }
            }
        }
    }