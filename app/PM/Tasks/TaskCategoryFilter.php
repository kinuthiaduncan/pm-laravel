<?php
namespace PM\Tasks;

use Illuminate\Pagination\LengthAwarePaginator;
use Laracasts\Presenter\PresentableTrait;
use PM\Models\TaskCategory;

class TaskCategoryFilter
{
    use PresentableTrait;

    public static function filter($request, $pagination)
    {
        $query = self::query();
        $methods = self::scopes();

        foreach ($request as $key => $param) {
            if (array_key_exists($key, $methods)) {
                switch ($key) {
                    case "department":
                        $query->OfTaskCategoryDepartment($param);
                        break;
                    default:
                        $query;
                }
            }
        }
        $categories = $query->get();

        $categories = $categories->map(function ($category)
        {
            $category->creator = $category->user->preferred_name;
            $category->department = $category->department->name;
            $category->description = strip_tags($category->description);
            return $category;
        });

        $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
        $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

        $offset = ($page * $perPage) - $perPage;
        $data = $categories->slice($offset, $perPage, true);

        $categories= new LengthAwarePaginator($data, $categories->count(), $perPage, $page);

        $categories->setPath('/api/get/filter/task-categories');

        return $categories;
    }

    /**
     * @return array
     */
    private static function scopes()
    {
        $methods = [
            'department' => 'OfTaskCategoryDepartment',
        ];

        return $methods;
    }

    /**
     * @return mixed
     */
    private static function query()
    {
        $query = TaskCategory::latest()->orderBy('id');

        return $query;
    }
}