<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Projects;


    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Collection;
    use Maatwebsite\Excel\Facades\Excel;
    use PM\Models\Project;
    use PM\Models\ProjectCategory;
    use PM\Models\ProjectComponent;
    use PM\Models\ProjectPhase;
    use PM\Models\ProjectType;
    use PM\Models\ProjectUser;
    use PM\Presenters\ProjectPhasePresenter;
    use Sofa\Eloquence\Model;

    class ProjectRepository {

        public function getProjectsSelect()
        {
            $projects=Project::all();
            $projects_array=[];
            foreach ($projects as $project)
            {
                $projects_array[ $project->id ]=$project->name;
            }

            return $projects_array;
        }

        public function projectsTypesSelect()
        {
            $project_types=ProjectType::all();
            $project_types_array=array(''=>'Select project type');
            foreach ($project_types as $project_type)
            {
                $project_types_array[ $project_type->id ]=$project_type->name;
            }

            return $project_types_array;
        }
        public function categoriesSelect()
        {
            $categories=ProjectCategory::all();
            $categories_array=array(''=>'Select project category');
            foreach ($categories as $category)
            {
                $categories_array[ $category->id ]=$category->name;
            }

            return $categories_array;
        }


        public function generateProjectKey($project_name)
        {
            $array=explode(' ', $project_name, 3);
            $project_key=strtoupper($array[0]);
            $project_keys=Project::where('project_key', $project_key)->get();
            if (count($project_keys) != 0)
            {
                $project_key=$project_key . '1';
                $project_keys=Project::where('project_key', $project_key)->get();
                if (count($project_keys) != 0)
                {
                    $project_key=$project_key . '1';
                    $project_keys=Project::where('project_key', $project_key)->get();
                    if (count($project_keys) != 0)
                    {
                        $project_key=$project_key . '1';
                    }
                }
            }

            return $project_key;
        }

        /**
         * Provide a list of all existing project components
         *
         * @return array
         */
        public function projectsComponentsSelect()
        {
            $project_components=ProjectComponent::all();
            $project_components_array=array(''=>'Select project component');
            foreach ($project_components as $project_component)
            {
                $project_components_array[ $project_component->id ]=$project_component->component_name;
            }

            return $project_components_array;
        }

        /**
         * Export project summaries using selected lists
         *
         * @param $request
         */
        public function exportSummaries($request)
        {
            $now = Carbon::now()->toFormattedDateString();

            $categories = isset($request['all_categories']) ? ProjectCategory::all() : ProjectCategory::whereIn('id', $request['category_id'])->get();

            $types = isset($request['all_types']) ? ProjectType::all(['id'])->toArray() : ProjectType::whereIn('id', $request['type_id'])->get(['id'])->toArray();

            //Retrieve projects for each categories
            $projects = Project::where('project_access', 'public')
                ->whereHas('projectType', function ($type) use ($types){
                    return $type->whereIn('id', $types);
                })
                ->get();

            Excel::create('Selected Project Summaries as at ' . $now, function($excel) use ($categories, $now, $projects) {

                $excel->sheet($now, function($sheet) use ($categories, $projects, $now) {

                        $sheet->setFontFamily('Comic Sans MS');

                        $sheet->setStyle(array(
                            'font' => array(
                                'name'      =>  'Calibri',
                                'size'      =>  13,
                            )
                        ));

                        $sheet->loadView('projects.exports.project_summaries_export', ['categories'=>$categories, 'projects'=>$projects]);

                    });

                })->export('xlsx');

        }

        /*
         * Get Project Members
         */
        public function getProjectMembers($id)
        {
            $members = ProjectUser::where('project_id', $id)->get();

            $members_array = array();

            foreach ($members as $member)
            {
                $members_array[ $member->user_id ]= $member->members->preferred_name;
            }

            return $members_array;
        }
        public function getProjectPhases($projectID)
        {
            $phases = ProjectPhase::where('project_id',$projectID)->where('approval_status',1)->get();
            $phase_array = array(''=>'Select Project Phase');
            foreach ($phases as $phase) {
                $percentage = ProjectPhasePresenter::phasePercentDone($phase->id);
                if($percentage < 100) {
                    $phase_array[$phase->id] = $phase->name;
                }
            }
            return $phase_array;
        }



    }