<?php
    /**
     * Date: 13/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Projects;


    use Illuminate\Pagination\LengthAwarePaginator;
    use Laracasts\Presenter\PresentableTrait;
    use PM\Models\Project;

    class ProjectsFilter
    {
        use PresentableTrait;

        public static function filter($request, $pagination)
        {
            $query = self::query($request);
            $methods = self::scopes();

            foreach ($request as $key => $param) {

                // If a scope exists in the provided array, add it to the query.
                if (array_key_exists($key, $methods)) {
                    switch ($key) {
                        case "user":
                            $query->OfUser($param);
                            break;
                        case "name":
                            $query->OfName($param);
                            break;
                        default:
                            $query;
                    }
                }
            }

            $projects = $query->get();

            $projects = $projects->map(function ($project) {
                $project->project_type = $project->projectType->name;
                $project->project_lead = $project->projectLead->preferred_name;
                $project->created_at_x = $project->present()->date;

                return $project;
            });

            $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
            $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

            $offset = ($page * $perPage) - $perPage;
            $data = $projects->slice($offset, $perPage, true);
            $projects = new LengthAwarePaginator($data, $projects->count(), $perPage, $page);
            $projects->setPath('/api/get/projects');

            return $projects;
        }

        private static function scopes()
        {
            $methods = [
                'user' => 'OfUser',
                'name' => 'OfName'
            ];

            return $methods;
        }

        private static function query($request)
        {
            $query = Project::orderBy('id');

            return $query;
        }
    }