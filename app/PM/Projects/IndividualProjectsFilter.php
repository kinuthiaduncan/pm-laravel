<?php
/**
 * Created by PhpStorm.
 * User: dkinuthia
 * Date: 7/3/17
 * Time: 10:02 AM
 */

namespace PM\Projects;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Laracasts\Presenter\PresentableTrait;
use PM\Models\Project;


class IndividualProjectsFilter
{
    use PresentableTrait;

    public static function filter($request, $pagination)
    {
        $query = self::query($request);
        $methods = self::scopes();

        foreach ($request as $key => $param) {
            // If a scope exists in the provided array, add it to the query.
            if (array_key_exists($key, $methods)) {
                switch ($key) {
                    case "name":
                        $query->OfName($param);
                        break;
                    default:
                        $query;
                }
            }
        }
        $projects = $query->get();

        $projects = $projects->map(function ($project) {
            $project->project_type = $project->projectType->name;
            $project->project_lead = $project->projectLead->preferred_name;
            $project->created_at_x = $project->present()->date;

            return $project;
        });

        $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
        $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

        $offset = ($page * $perPage) - $perPage;
        $data = $projects->slice($offset, $perPage, true);
        $projects = new LengthAwarePaginator($data, $projects->count(), $perPage, $page);
        $projects->setPath('/api/get/projects/individual');

        return $projects;
    }

    private static function scopes()
    {
        $methods = [
            'name' => 'OfName'
        ];

        return $methods;
    }

    private static function query($request)
    {
        $query = Project::whereHas('projectUsers',function($user){
            $user->where('user_id',Auth::user()->id);
        });
        return $query;
    }

}