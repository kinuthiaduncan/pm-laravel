<?php
/**
 * Created by PhpStorm.
 * User: dkinuthia
 * Date: 6/17/17
 * Time: 12:19 PM
 */

namespace PM\Projects;


use Illuminate\Pagination\LengthAwarePaginator;
use Laracasts\Presenter\PresentableTrait;
use PM\Models\ProjectPhase;
use PM\Models\ProjectPhaseUpdates;

class PhasesFilter
{
    use PresentableTrait;

    /**
     * @param $pagination
     * @return LengthAwarePaginator
     */
    public static function newApprovals($pagination)
    {
        $new_approvals = ProjectPhase::where('approval_status',0)->get();
        $new_approvals = $new_approvals->map(function($approval){
            $approval->project = $approval->project->name;
            $approval->created_by = $approval->creator->preferred_name;
            $approval->decisions = strip_tags($approval->decisions);

            return $approval;
        });
        $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
        $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;
        $offset = ($page * $perPage) - $perPage;
        $data = $new_approvals->slice($offset, $perPage, true);
        $new_approvals = new LengthAwarePaginator($data, $new_approvals->count(), $perPage, $page);
        $new_approvals->setPath('/api/phases/new-approvals');

        return $new_approvals;
    }

    public static function updatedApprovals($pagination)
    {
        $updated_approvals = ProjectPhaseUpdates::get();
        $updated_approvals = $updated_approvals->map(function($approval){
            $approval->project = $approval->project->name;
            $approval->created_by = $approval->creator->preferred_name;
            $approval->decisions = strip_tags($approval->decisions);

            return $approval;
        });
        $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
        $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;
        $offset = ($page * $perPage) - $perPage;
        $data = $updated_approvals->slice($offset, $perPage, true);
        $updated_approvals = new LengthAwarePaginator($data, $updated_approvals->count(), $perPage, $page);
        $updated_approvals->setPath('/api/phases/updated-approvals');

        return $updated_approvals;
    }
}