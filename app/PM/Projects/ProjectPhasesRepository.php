<?php
/**
 * Created by PhpStorm.
 * User: dkinuthia
 * Date: 6/7/17
 * Time: 9:42 AM
 */

namespace PM\Projects;


use App\Notifications\PhaseActivityCreated;
use App\Notifications\PhaseActivityUpdated;
use App\Notifications\ProjectPhaseCreated;
use App\Notifications\ProjectPhaseUpdated;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use PM\Models\Project;
use PM\Models\ProjectPhase;
use PM\Models\ProjectPhaseActivity;
use PM\Models\ProjectPhaseUpdates;

class ProjectPhasesRepository
{
    /**
     * Save project phase
     * @param $request
     * @param $id
     */
    public function save($request, $id)
    {
        $user = Auth::user();
        $input = $request->all();
        $input['created_by'] = $user->id;
        $input['project_id'] = $id;
        //save project phase
        $phase = ProjectPhase::create([
            'name'=>$input['title'],
            'start_date'=>$input['start_date'],
            'end_date'=>$input['end_date'],
            'decisions'=>$input['decisions'],
            'created_by'=>$input['created_by'],
            'project_id'=>$input['project_id']
        ]);
        //notify user and project lead
        $project = Project::findOrFail($id);
        $project->projectLead->notify(new ProjectPhaseCreated($phase));
        $user->notify(new ProjectPhaseCreated($phase));
        //request approval
        Mail::send('emails.projects.approval_required',['phase'=>$phase], function($message) use ($project){
            $message->from(['support@cytonn.com' => 'Task Management']);
            $message->to(['mchaka@cytonn.com']);
            $message->subject("Project Phase Approval Requested for " . $project->name);
        });
    }

    /**
     * Update Project Phase
     * @param $request
     * @param $project
     * @param $id
     */
    public function update($request, $project, $id)
    {
        $user = Auth::user();
       //Update Project phase
        $phase = ProjectPhase::findOrFail($id);
        $updated  = ProjectPhaseUpdates::create([
            'project_phase'=>$id,
            'project_id'=>$project,
            'name'=>$request->title,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'decisions'=>$request->decisions,
            'created_by'=>Auth::id()
        ]);
        //notify user and project lead
        $project = Project::findOrFail($project);
        $project->projectLead->notify(new ProjectPhaseUpdated($phase,$project));
        $user->notify(new ProjectPhaseUpdated($phase,$project));
        //notify approver
        Mail::send('emails.projects.approval_required',['phase'=>$phase], function($message) use ($project){
            $message->from(['support@cytonn.com' => 'Task Management']);
            $message->to(['machaka@cytonn.com']);
            $message->subject("Project Phase Approval Requested for " . $project->name);
        });
    }

    /**
     * Save a project phase activity
     * @param $request
     * @param $phase
     */
    public  function saveActivity($request, $phase)
    {
        $projectPhase = ProjectPhase::findOrFail($phase);
        $project = Project::findOrFail($projectPhase->project_id);
        //create activity
        $activity = ProjectPhaseActivity::create([
            'project_phase'=>$phase,
            'name'=>$request->name,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'percent_complete'=>$request->percent_complete,
            'comments'=>$request->comments,
            'created_by'=>Auth::id()
        ]);
        //notify users
        $activity->creator->notify(new PhaseActivityCreated($activity,$projectPhase));
        $project->projectLead->notify(new PhaseActivityCreated($activity,$projectPhase));
    }

    /**
     * Update activity
     * @param $request
     * @param $phase
     * @param $id
     */
    public function updateActivity($request,$phase,$id)
    {
        $activity = ProjectPhaseActivity::findOrFail($id);
        $projectPhase = ProjectPhase::findOrFail($phase);
        $project = Project::findOrFail($projectPhase->project_id);
        //Update activity
        $activity->update([
            'name'=>$request->name,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'percent_complete'=>$request->percent_complete,
            'comments'=>$request->comments
        ]);
        //notify users
        $activity->creator->notify(new PhaseActivityUpdated($activity,$projectPhase));
        $project->projectLead->notify(new PhaseActivityUpdated($activity,$projectPhase));
    }

    public function summary($request)
    {
        $projects = $request->project_id;
        $start = $request->start_date;
        $end = $request->end_date;
        foreach ($projects as $project)
        {
            $workPlans = ProjectPhase::where('project_id',$project)->whereBetween('start_date',[$start, $end])
                ->orderBy('project_id','ASC')->get();
           return $workPlans;
        }
    }
}