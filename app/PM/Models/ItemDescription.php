<?php

    namespace PM\Models;

    use Illuminate\Database\Eloquent\Model;

    class ItemDescription extends Model
    {
        /*
         * Mass assignable fields
         */
        protected $fillable = ['description', 'date', 'department_id', 'item_id', 'created_by'];

        protected $table = 'daily_report_descriptions';

        /**
         * Belongs to a user
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function user()
        {
            return $this->belongsTo(User::class, 'created_by');
        }
    }
