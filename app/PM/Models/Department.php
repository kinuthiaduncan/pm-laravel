<?php
    /**
     * Date: 15/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class Department extends Model
    {
        /*
         * Mass assignable fields
         */
        protected $fillable = ['name', 'department_email', 'hr_department_id'];
        protected $table='departments';

        public function user(){
            return $this->hasMany('PM\Models\User','department_id');
        }

        public function dailyReport(){
            return $this->hasMany(DailyReport::class);
        }

    }