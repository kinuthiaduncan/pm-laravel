<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 06/12/2016
 * Time: 08:27
 */

namespace PM\Models;


use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class ProjectCategory extends Model
{

    use PresentableTrait;

    /**
     * Get the presenter class
     */
    protected $presenter = 'PM\Presenters\CategoryPresenter';

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table='project_categories';

    /**
     * The attributes excluded from the model's JSON form.
     * @var array
     */
    protected $guarded=['id'];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable=[
        'slug',
        'name',
        'description',
        'created_by',
        'edited_by'
    ];

    /**
     * A category may have many projects
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->hasMany('PM\Models\Project', 'category_id');
    }

    /**
     * A category belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('PM\Models\User', 'created_by');
    }

    /**
     * A user can edit a category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function editor()
    {
        return $this->belongsTo('PM\Models\User', 'edited_by');
    }

    /**
     * Specify the items to be indexed
     *
     * @return array
     */
    public function getIndexDocumentData()
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name,
            'description' => $this->description,
            'created_by'=>$this->created_by,
            'created_at' => $this->created_at->toDateTimeString()
        ];
    }


}