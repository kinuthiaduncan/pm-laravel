<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EmailGroups extends Model
{
    use Notifiable;

    protected $fillable = ['name','email'];
    protected $table = 'email_groups';

}
