<?php
    /**
     * Date: 18/04/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class TaskGroup extends Model
    {
        /*
         * Mass assignable fields
         */
        protected $fillable = ['name', 'details', 'created_by'];
    }