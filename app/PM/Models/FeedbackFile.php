<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackFile extends Model
{
    protected $fillable = ['name','url','feedback_id'];
    protected $table = 'feedback_files';
}
