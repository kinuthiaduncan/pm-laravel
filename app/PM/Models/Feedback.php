<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['project_id','subject','description','created_by', 'source', 'client_id', 'client_email'];
    protected $table = 'project_feedback';

    /**
     * Relationship between feedback and project
     */
    public function project()
    {
        return $this->belongsTo('PM\Models\Project','project_id');
    }

    /**
     *Relationship between user and feedback
     */
    public function creator()
    {
        return $this->belongsTo('PM\Models\User','created_by');
    }

    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }
}
