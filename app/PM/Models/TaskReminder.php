<?php
    /**
     * Date: 01/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class TaskReminder extends Model
    {
        /*
         * Mass assignable fields
         */
        protected $fillable = ['content', 'time', 'task_id', 'created_by'];

        /**
         * Comment belongs to a user
         * @return mixed
         */
        public function creator()
        {
            return $this->belongsTo('PM\Models\User', 'created_by');
        }
    }