<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Models;

    use Illuminate\Database\Eloquent\Model;
    use Roketin\Auditing\AuditingTrait;
    use Laracasts\Presenter\PresentableTrait;

    class Status extends Model {

        
        use PresentableTrait;
        /**
         * Audit Trait.
         */
        use AuditingTrait;

        /**
         * Disables the log record in this model.
         *
         */
        protected $auditEnabled  = true;

        /**
         * Disables the log record after 500 records.
         *
         */
        protected $historyLimit = 500;

        /**
         * Fields that should not be registered.
         *
         */
        protected $dontKeepLogOf = ['created_at', 'updated_at'];

        /**
         * Tell what actions should be edited audit.
         *
         */
        protected $auditableTypes = ['created', 'saved', 'deleted'];

        /**
         * The database table used by the model.
         * @var string
         */
        protected $table='status';

        /**
         * The attributes excluded from the model's JSON form.
         * @var array
         */
        protected $guarded=['id'];

        /**
         * The attributes that are mass assignable.
         * @var array
         */
        protected $fillable=[
            'name'
        ];

        public function task()
        {
            return $this->hasMany('PM\Models\Task');
        }
    }