<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPhaseActivity extends Model
{
    protected $table = 'project_phase_activities';
    protected $fillable = ['project_phase','name','start_date','end_date','percent_complete','comments','created_by'];
    /**
     * Relationship between project phases and their activities
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo('PM\Models\ProjectPhase','project_phase');
    }

    /**
     * Relationship between users and project phase activities
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('PM\Models\User','created_by');
    }
}
