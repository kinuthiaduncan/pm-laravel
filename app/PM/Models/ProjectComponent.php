<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 11/24/16
 * Time: 9:12 AM
 */

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectComponent extends Model
{

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table='project_components';

    /**
     * The attributes excluded from the model's JSON form.
     * @var array
     */
    protected $guarded=['id'];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable=[
        'project_id',
        'component_name',
        'description',
        'component_lead'
    ];

    /**
     * A component belongs to a project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('PM\Models\Project', 'project_id');
    }
    
    /**
     * A component can have many issues
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function issues()
    {
        return $this->hasMany('PM\Models\Issue', 'component_id');
    }

    /*
     * Relationship between a component lead and a user
     */
    public function componentLead()
    {
        return $this->belongsTo('PM\Models\User', 'component_lead');
    }

    /*
     * Relationship between a component and a component user
     */
    public function componentUser()
    {
        return $this->hasMany('PM\Models\ComponentUser');
    }
}