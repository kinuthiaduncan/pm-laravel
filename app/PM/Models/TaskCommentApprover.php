<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class TaskCommentApprover extends Model
{
    protected $table = 'task_comment_approvers';
    protected $fillable = ['user_id','comment_id','status'];

    /**
     * relationship between users and comment approvers
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approver()
    {
        return $this->belongsTo('PM\Models\User','user_id');
    }

    /**
     * Relationship between comment approvals and comments
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comment()
    {
        return $this->belongsTo('PM\Models\TaskComment','comment_id');
    }
}
