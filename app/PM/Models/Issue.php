<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Models;

    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Model;
    use Nicolaslopezj\Searchable\SearchableTrait;
    use PM\SearchOperations\ElasticOperationsTrait;
    use PM\SearchOperations\ModelObserver;
    use Sofa\Eloquence\Eloquence;
    use Roketin\Auditing\AuditingTrait;

    /**
     * Class Issue
     * @package PM\Models
     */
    class Issue extends Model {
        use SearchableTrait;
        /*
         * Get the elastic operations trait
         */
        use ElasticOperationsTrait;

        /**
         * Audit Trait.
         */
        use AuditingTrait;

        /**
         * Disables the log record in this model.
         *
         */
        protected $auditEnabled  = true;

        /**
         * Disables the log record after 500 records.
         *
         */
        protected $historyLimit = 500;

        /**
         * Fields that should not be registered.
         *
         */
        protected $dontKeepLogOf = ['created_at', 'updated_at'];

        /**
         * Tell what actions should be edited audit.
         *
         */
        protected $auditableTypes = ['created', 'saved', 'deleted'];

        /**
         * @var array
         */
        protected $dates = ['completed_at'];

        protected $searchable = [
            'columns' => [
                'title'=>10,
                'description'=>10
            ],
            'joins' => [

            ],
        ];

        /*
         * Setup the mapping for the issues table elasticsearch indexing
         */
        /**
         * @var array
         */
        public $mappingProperties = [
        ];

        /**
         * Issue constructor.
         */
        public function __construct(array $attributes = [])
        {
            parent::__construct($attributes);
        }

        /*
        * Observer method when the issue is updated
        */
        /**
         *
         */
        public static function boot()
        {
            parent::boot();

            Issue::observe(new ModelObserver());
            
            static::saving(function (Issue $model)
            {
                $old = new static($model->getOriginal());

                if(!$model->statuses) return;

                if($model->statuses->resolution == true && $old->statuses->resolution != true)
                {
                    $model->completed_at = Carbon::now();
                }
            });
        }

        /*
         * Specify the items to be indexed
        */
        /**
         * @return array
         */
        public function getIndexDocumentData()
        {
            return [
                'id' => $this->id,
                'title' => $this->title,
                'project_id' => $this->project_id,
                'project' => $this->projects->name,
                'description' => $this->description,
                'status_id' => $this->status_id,
                'status' => $this->statuses->name,
                'assigned_to' => $this->assigned_to,
                'assigned' => $this->assignedTo->present()->fullName,
                'type' => $this->issueTypes->name,
                'created_by' => $this->created_by,
                'creatorBy' => $this->creator->present()->fullName,
                'priority' => $this->priorities->name,
                'priority_id' => $this->priority_id,
                'issue_type' => $this->issue_type,
                'created_at' => $this->created_at->toDateTimeString(),
                'updated_at' => $this->updated_at->toDateTimeString(),
                'percentage_done' => $this->percentage_done,
                'date_due' => $this->date_due
            ];
        }

        /**
         * The database table used by the model.
         * @var string
         */
        protected $table='issues';

        /**
         * The attributes excluded from the model's JSON form.
         * @var array
         */
        protected $guarded=['id'];

        /**
         * The attributes that are mass assignable.
         * @var array
         */
        protected $fillable=[
            'title',
            'project_id',
            'component_id',
            'description',
            'image',
            'status_id',
            'parent_id',
            'assigned_to',
            'issue_type',
            'date_due',
            'created_by',
            'priority_id',
            'percentage_done',
            'status_change_date',
            'project_phase'
        ];

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function comments()
        {
            return $this->hasMany('PM\Models\Comment', 'issue_id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function statuses()
        {
            return $this->belongsTo('PM\Models\Status', 'status_id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function issueTypes()
        {
            return $this->belongsTo('PM\Models\IssueType', 'issue_type');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function priorities()
        {
            return $this->belongsTo('PM\Models\Priority', 'priority_id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function projects()
        {
            return $this->belongsTo('PM\Models\Project', 'project_id');
        }

        /**
         * An issue belongs to a project component
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function component()
        {
            return $this->belongsTo('PM\Models\ProjectComponent', 'component_id');
        }


        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function creator()
        {
            return $this->belongsTo('PM\Models\User', 'created_by');
        }

        /**
         * Relationship between project issues and project phases
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function projectPhase()
        {
            return $this->belongsTo('PM\Models\ProjectPhase','project_phase');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function assignedTo()
        {
            return $this->belongsTo('PM\Models\User', 'assigned_to');
        }

        /**
         * @param $query
         * @param $date
         * @return mixed
         */
        public function scopeCompletedAfter($query, $date)
        {
            return $query->whereHas('statuses', function ($s)
            {
                $s->where('resolution', true);

            })->where('completed_at', '>=', $date);
        }

        /**
         * @param $query
         * @param $date
         * @return mixed
         */
        public function scopeCreatedAfter($query, $date)
        {
            return $query->where('created_at', '>=', $date);
        }

        /**
         * @param $query
         * @param $date
         * @param $project
         * @param $status
         * @return mixed
         */
        public function scopeStatusChangedAfter($query, $date,$project,$status)
        {
            return $query->where('status_change_date','>=',$date)
                ->where('project_id',$project)->where('status_id',$status);
        }

        /**
         * @param $query
         * @param $date
         * @param $project
         * @return mixed
         */
        public function scopeIssuesCreatedAfter($query, $date,$project)
        {
            return $query->where('created_at','>=',$date)->where('project_id',$project);
        }
    }