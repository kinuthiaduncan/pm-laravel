<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    //
    protected $table = 'roles';

    protected $fillable = ['role','description'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
