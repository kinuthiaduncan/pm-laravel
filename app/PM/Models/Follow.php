<?php
    /**
     * Date: 28/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;
    use Nicolaslopezj\Searchable\SearchableTrait;

    class Follow extends Model
    {
        use SearchableTrait;
        /*
         * Mass assignable fields
         */
        protected $fillable = ['follower_id', 'followed_id', 'task_id'];


        protected $searchable = [
            'columns' => [
                'follower_id'=>10,
                'followed_id'=>10,
                'task_id'=>10,
            ],
            'joins' => [

            ],
        ];

        public function follows()
        {
            return $this->belongsTo('PM\Models\User', 'follower_id');
        }

        public function followed()
        {
            return $this->belongsTo('PM\Models\User', 'followed_id');
        }

        public function followed_task()
        {
            return $this->belongsTo('PM\Models\Task', 'task_id');
        }
    }