<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Models;

    use Illuminate\Database\Eloquent\Model;
    use Laracasts\Presenter\PresentableTrait;
    use Nicolaslopezj\Searchable\SearchableTrait;
    use PM\SearchOperations\ModelObserver;
    use Roketin\Auditing\AuditingTrait;

    /**
     * Class Project
     * @package PM\Models
     */
    class Project extends Model {

        /*
         * Get the presentable trait
         */
        use PresentableTrait, SearchableTrait;

        /*
         * Specify the presenter
         */
        protected $presenter = 'PM\Presenters\ProjectPresenter';

        /*
         * Get the elastic operations trait
         */
//        use ElasticOperationsTrait;

        /**
         * Audit Trait.
         */
        use AuditingTrait;

        /**
         * Disables the log record in this model.
         *
         */
        protected $auditEnabled  = true;

        /**
         * Disables the log record after 500 records.
         *
         */
        protected $historyLimit = 500;

        /**
         * Fields that should not be registered.
         *
         */
        protected $dontKeepLogOf = ['created_at', 'updated_at'];

        /**
         * Tell what actions should be edited audit.
         *
         */
        protected $auditableTypes = ['created', 'saved', 'deleted'];

//        /*
//         * Setup the mapping for the projects table elasticsearch indexing
//         */
//        public $mappingProperties = [
//        ];

        protected $searchable = [
            'columns' => [
                'name'=>10,
                'project_description'=>3,
            ],
            'joins' => [

            ],
        ];


        /*
        * Observer method when the issue is updated
        */
        public static function boot()
        {
            parent::boot();

            Project::observe(new ModelObserver());
        }

        /*
         * Specify the items to be indexed
        */
        public function getIndexDocumentData()
        {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'project_access' => $this->project_access,
                'project_key' => $this->project_key,
                'project_url' => $this->project_url,
                'type' => $this->projectType->name,
                'project_type' => $this->project_type,
                'project_lead' => $this->project_lead,
                'lead' => $this->projectLead->present()->fullName,
                'created_by' => $this->created_by,
                'createdBy' => $this->creator->present()->fullName,
                'created_at' => $this->created_at->toDateTimeString()
            ];
        }

        /**
         * The database table used by the model.
         * @var string
         */
        protected $table='projects';

        /**
         * The attributes excluded from the model's JSON form.
         * @var array
         */
        protected $guarded=['id'];

        /**
         * The attributes that are mass assignable.
         * @var array
         */
        protected $fillable=[
            'name',
            'project_type',
            'project_key',
            'project_access',
            'project_description',
            'project_url',
            'project_lead',
            'created_by',
            'category_id',
            'status',
            'active',
            'priority',
            'resources_required',
            'resources_assigned'
        ];

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function creator()
        {
            return $this->belongsTo('PM\Models\User', 'created_by');
        }

        /**
         * A project belongs to a category
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function category()
        {
            return $this->belongsTo('PM\Models\ProjectCategory', 'category_id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function projectLead()
        {
            return $this->belongsTo('PM\Models\User', 'project_lead');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function projectType()
        {
            return $this->belongsTo('PM\Models\ProjectType', 'project_type');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function issues()
        {
            return $this->hasMany('PM\Models\Issue', 'project_id');
        }

        public function progress()
        {
            return $this->hasMany(ProjectProgress::class, 'project_id');
        }
        public function milestones(){
            return $this->hasMany('PM\Models\Milestone', 'project_id');
        }

        public function projectUsers()
        {
            return $this->hasMany(ProjectUser::class);
        }

        public function scopeType($query, $name)
        {
            return $query->whereHas('projectType', function ($q) use ($name)
            {
                $q->where('name', $name);
            });
        }

        /**
         * filter by name
         * @param $query
         * @param $type
         * @return mixed
         */
        public function scopeOfName($query, $type)
        {
            if ($type == "") {
                return $query->whereNotNull('name');
            }

            return $query->where('name', 'LIKE', "%$type%");
        }

        /**
         *
         * Filter by members
         * @param $query
         * @param $type
         * @return mixed
         */
        public function scopeOfUser($query, $type)
        {
            $user_projects = ProjectUser::where('user_id', $type)->get();

            $user_projects_ids = $user_projects->map(function($user_project){
                return $user_project->project_id;
            })->toArray();

            if ($type == "") {
                return $query->whereNotNull('project_lead');
            }

            return $query->whereIn('id', $user_projects_ids);
        }
    }