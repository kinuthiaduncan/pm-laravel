<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class ReportItem extends Model
{
    //
    protected $fillable = ['item', 'department_id', 'created_by'];

    protected $table='daily_reports_items';

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }
    public function subItems(){
        return $this->hasMany(ReportSubItem::class);
    }
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function reports()
    {
        return $this->hasMany(DailyReport::class);
    }
}
