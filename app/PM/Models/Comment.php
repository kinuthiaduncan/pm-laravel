<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Models;

    use Illuminate\Database\Eloquent\Model;
    use Roketin\Auditing\AuditingTrait;

    /**
     * Class Comment
     * @package PM\Models
     */
    class Comment extends Model {

        /**
         * Audit Trait.
         */
        use AuditingTrait;

        /**
         * Comment constructor.
         */
        public function __construct(array $attributes = [])
        {
            parent::__construct($attributes);
        }

        /**
         * Disables the log record in this model.
         *
         */
        protected $auditEnabled  = true;

        /**
         * Disables the log record after 500 records.
         *
         */
        protected $historyLimit = 500;

        /**
         * Fields that should not be registered.
         *
         */
        protected $dontKeepLogOf = ['created_at', 'updated_at'];

        /**
         * Tell what actions should be edited audit.
         *
         */
        protected $auditableTypes = ['created', 'saved', 'deleted'];

        /**
         * The database table used by the model.
         * @var string
         */
        protected $table='comments';

        /**
         * The attributes excluded from the model's JSON form.
         * @var array
         */
        protected $guarded=['id'];

        /**
         * The attributes that are mass assignable.
         * @var array
         */
        protected $fillable=[
            'name',
            'image',
            'file',
            'filename',
            'issue_id',
            'created_by'
        ];

        /**
         * Comment Issue
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function issues()
        {
            return $this->belongsTo('PM\Models\Issue', 'issue_id');
        }

        /**
         * Comment Creator
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function creator()
        {
            return $this->belongsTo('PM\Models\User', 'created_by');
        }
    }