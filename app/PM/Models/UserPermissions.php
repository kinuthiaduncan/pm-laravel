<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class UserPermissions extends Model
{
    //
    protected $table = 'user_has_permissions';

    protected $fillable = ['permission_id','user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function permissions()
    {
        return $this->belongsTo(Permissions::class,'permission_id');
    }
}
