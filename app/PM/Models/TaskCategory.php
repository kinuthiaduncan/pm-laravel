<?php
    /**
     * Date: 20/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class TaskCategory extends Model
    {
        /*
         * Mass Assignable field
         */
        protected $fillable = ['name', 'description', 'department_id', 'created_by'];

        /**
         * Created by a user
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function user()
        {
            return $this->belongsTo('PM\Models\User', 'created_by');
        }


        /**
         * Belongs to a department
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function department()
        {
            return $this->belongsTo('PM\Models\Department', 'department_id');
        }

        public function scopeOfTaskCategoryDepartment($query, $type)
        {
            if ($type == "") {
                return $query->whereNotNull('department_id');
            }

            return $query->where('department_id', $type);
        }
    }