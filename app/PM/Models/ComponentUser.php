<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 2/3/17
 * Time: 2:14 PM
 */

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class ComponentUser extends Model
{

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table='component_users';

    /**
     * The attributes excluded from the model's JSON form.
     * @var array
     */
    protected $guarded=['id'];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable=[
        'project_component_id',
        'component_user_id',
    ];

    /*
     * Relationship between a component user and a project_component
     */
    public function projectComponent()
    {
        return $this->belongsTo('PM\Models\ProjectComponent', 'project_component_id');
    }

    /*
     * Relationship between a component user and a user
     */
    public function user()
    {
        return $this->belongsTo('PM\Models\User', 'component_user_id');
    }
}