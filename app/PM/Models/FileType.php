<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class FileType extends Model
{
    protected $fillable = ['slug','name'];
    protected $table = 'file_types';

    public function folder()
    {
        return $this->slug;
    }
}
