<?php
    /**
     * Date: 22/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class TaskFile extends Model
    {
        /*
         * Mass assignable fields
         */

        protected $fillable = ['name', 'url', 'task_id', 'task_comment_id', 'created_by'];
    }