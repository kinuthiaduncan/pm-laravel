<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    //
    protected $table = 'permissions';

    protected $fillable = ['name','description'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function userPermissions()
    {
        return $this->hasMany(UserPermissions::class);
    }

}
