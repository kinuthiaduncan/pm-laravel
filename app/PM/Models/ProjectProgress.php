<?php
/**
 * Date: 02/09/2016
 * Time: 12:03 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: ct-project-management
 * Cytonn Technologies
 */

namespace PM\Models;


use Illuminate\Database\Eloquent\Model;
use Roketin\Auditing\AuditingTrait;

/**
 * Class ProjectProgress
 * @package PM\Models
 */
class ProjectProgress extends Model
{

    /**
     * Audit Trait.
     */
    use AuditingTrait;

    /**
     * Disables the log record in this model.
     *
     */
    protected $auditEnabled  = true;

    /**
     * Disables the log record after 500 records.
     *
     */
    protected $historyLimit = 500;

    /*
     * mass assignable
     */
    protected $fillable = ['title', 'description', 'added_by', 'project_id'];
    /**
     * Fields that should not be registered.
     *
     */
    protected $dontKeepLogOf = ['created_at', 'updated_at'];

    /**
     * Tell what actions should be edited audit.
     *
     */
    protected $auditableTypes = ['created', 'saved', 'deleted'];

    /**
     * @var string
     */
    protected $table = 'project_progress';

    /**
     * @var array
     */
    protected $guarded = ['id', 'added_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function scopeFiledAfter($query, $date)
    {
        return $query->where('created_at', '>=', $date);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function addedBy()
    {
        return $this->belongsTo(User::class, 'added_by');
    }

}