<?php
    /**
     * Date: 18/04/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class TaskGroupMember extends Model
    {
        /*
         * Mass Assignable Fields
         */
        protected $fillable = ['task_group_id', 'user_id'];

        public function user()
        {
            return $this->hasMany('PM\Models\User');
        }
    }