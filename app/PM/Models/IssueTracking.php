<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;
use PM\Models\Issue;

class IssueTracking extends Model
{
    protected $fillable = ['issue_id','date','start_time','end_time','description','created_by'];
    protected $table = 'issue_trackings';

    /**
     * Relationship between issue tracking and users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('PM\Models\User','created_by');
    }

    /**
     * Relationship between issues tracking and issues
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function issue()
    {
        return $this->belongsTo(Issue::class);
    }
}
