<?php
    /**
     * Date: 28/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class TaskProgress extends Model
    {
        /*
         * Mass assignable fields
         */
        protected $fillable = ['task_id', 'title','percentage_done', 'description', 'created_by'];

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function task()
        {
            return $this->belongsTo(Task::class, 'task_id');
        }

        public function scopeFiledAfter($query, $date)
        {
            return $query->where('created_at', '>=', $date);
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function addedBy()
        {
            return $this->belongsTo(User::class, 'created_by');
        }
    }