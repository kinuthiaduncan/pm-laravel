<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class TaskEmailGroup extends Model
{
    protected $fillable = ['task_id','email_group_id'];
    protected $table = 'task_email_groups';

    public function emailGroups()
    {
        return $this->belongsTo(EmailGroups::class,'email_group_id');
    }
}
