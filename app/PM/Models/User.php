<?php

    namespace PM\Models;

    use Illuminate\Auth\Authenticatable;
    use Illuminate\Auth\Passwords\CanResetPassword;
    use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
    use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
    use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Foundation\Auth\Access\Authorizable;
    use Illuminate\Notifications\Notifiable;
    use Laracasts\Presenter\PresentableTrait;
    use Laravel\Passport\HasApiTokens;
    use Nicolaslopezj\Searchable\SearchableTrait;
    use Roketin\Auditing\AuditingTrait;
    use Spatie\Permission\Traits\HasRoles;

    class User extends Model implements AuthenticatableContract,
        AuthorizableContract,
        CanResetPasswordContract
    {
        use Authenticatable, Authorizable, CanResetPassword,HasRoles,
            PresentableTrait, AuditingTrait, HasApiTokens, SearchableTrait,Notifiable;

        /*
         * Get the presenter class
         */
        protected $presenter = 'PM\Presenters\UserPresenter';

        /**
         * Disables the log record in this model.
         *
         */
        protected $auditEnabled = true;

        /**
         * Disables the log record after 500 records.
         *
         */
        protected $historyLimit = 500;

        /**
         * Fields that should not be registered.
         *
         */
        protected $dontKeepLogOf = ['created_at', 'updated_at'];

        /**
         * Tell what actions should be edited audit.
         *
         */
        protected $auditableTypes = ['created', 'saved', 'deleted'];

        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'users';

        protected $searchable = [
            'columns' => [
                'firstname'=>10,
                'preferred_name'=>10,
                'lastname'=>10,
                'email'=>10,
            ],
            'joins' => [

            ],
        ];

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'firstname',
            'lastname',
            'preferred_name',
            'email',
            'password',
            'role_id',
            'google_id',
            'avatar',
            'department_id',
            'job_level',
            'job_title',
            'manager_id',
            'hr_id',
            'supervisor_id',
            'phone_number',
            'active'
        ];

        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = ['password', 'remember_token'];

        protected $guarded = ['id'];

        public function projects()
        {
            return $this->hasMany('PM\Models\Project', 'created_by');
        }

        /**
         * A user may create multiple categories
         *
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function createCategories()
        {
            return $this->hasMany('PM\Models\ProjectCategory', 'created_by');
        }

        /**
         * A user may create multiple categories
         *
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function editCategories()
        {
            return $this->hasMany('PM\Models\ProjectCategory', 'edited_by');
        }


        /**
         * Relationship between  users and roles
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function roles()
        {
            return $this->belongsTo('PM\Models\Roles', 'role_id');
        }

        /**
         * Relationship between  users and the issues assigned to them
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function issues()
        {
            return $this->hasMany(Issue::class, 'assigned_to');
        }

        /**
         * User who creates a BDA daily report
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function dailyReports()
        {
            return $this->hasMany(DailyReport::class, 'created_by');
        }

        /*
         * Relationship between a user and a component lead
         */
        public function componentLead()
        {
            return $this->hasMany('PM\Models\ProjectComponent');
        }

        /**
         * Relationship between  users and permissions
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function userPermissions()
        {
            return $this->hasMany('PM\Models\UserPermissions');
        }

        /**
         * Relationship between users and their comments on tasks
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function taskComments()
        {
            return $this->hasMany('PM\Models\TaskComment','created_by');
        }

        /**
         * relationship between task tracking and users
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function taskTracking()
        {
            return $this->hasMany('PM\Models\TaskTracking','created_by');
        }

        /**
         * relationship between issue tracking and users
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function issueTracking()
        {
            return $this->hasMany('PM\Models\IssueTracking','created_by');
        }

        /*
         * Relationship between a user and a component user
         */
        public function componentUser()
        {
            return $this->hasMany('PM\Models\ComponentUser');
        }

        /**
         * A user belongs to a department
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function department()
        {
            return $this->belongsTo('PM\Models\Department','department_id', 'hr_department_id');
        }



        /**
         * A user has a manager
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function manager()
        {
            return $this->belongsTo('PM\Models\User','manager_id', 'hr_id');
        }

        /**
         * A user has a supervisor
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function supervisor()
        {
            return $this->belongsTo('PM\Models\User','supervisor_id', 'hr_id');
        }

        /**
         *Relationship between a user and task approvers
         */
        public function taskCommentApprover()
        {
            return $this->hasMany('PM\Models\TaskCommentApprover','user_id');
        }

        /**
         * A user has many tasks
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */

        public function itemDescription(){
            return $this->hasMany('PM\Models\ItemDescription','created_by');
        }

        /**
         * User permissions
         * @param $permission
         * @return bool
         */
        public function may($permission)
        {
            return $this->hasPermissionTo($permission);
        }

        /**
         * Relationship between  users and tasks assigned to them
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function taskAssigned()
        {
            return $this->hasMany('PM\Models\AssignedUser');
        }

        public function taskCreated()
        {
            return $this->hasMany('PM\Models\Task','created_by');
        }
    }
