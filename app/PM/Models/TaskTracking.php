<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class TaskTracking extends Model
{

    protected $fillable = ['title','task_id','date','start_time','end_time','description','created_by'];
    protected $table = 'task_tracking';

    public function task()
    {
        return $this->belongsTo(Task::class,'task_id');
    }
    public function creator()
    {
        return $this->belongsTo(User::class,'created_by');
    }
}
