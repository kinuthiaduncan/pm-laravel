<?php

namespace PM\Models;

use App\Providers\FileServiceProvider;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Webpatser\Uuid\Uuid;

class File extends Model
{
    protected $fileProvider;
    protected $fillable = ['title','filename','type_id'];
    protected $table = 'files';

    /**
     * File constructor.
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fileProvider = app('pm.storage');
    }

    public function type()
    {
        return $this->belongsTo(FileType::class, 'type_id');
    }


    public static function upload($contents, $ext, $title, $file_type)
    {
        $type = FileType::where('slug', $file_type)->first();
        if(!$type)
        {
            $type = FileType::create(['slug'=>$file_type]);
        }
        return DB::transaction(function() use ($title, $type, $ext, $contents)
        {
            $file = static::create([
                'title' => $title,
                'filename' => Uuid::generate()->string.'.'.$ext,
                'type_id' => $type->id
            ]);

            $provider = (new static())->getFileProvider();
            $provider->putFileAs($type->slug, $contents, $file->filename);

            return $file;
        });
    }

    public function path()
    {
        return $this->type->folder().'/'.$this->filename;
    }

    /**
     * @return Filesystem
     */
    public function getFileProvider()
    {
        return $this->fileProvider;
    }
}
