<?php
    /**
     * Date: 20/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;
    use Laracasts\Presenter\PresentableTrait;
    use Nicolaslopezj\Searchable\SearchableTrait;

    class Task extends Model
    {
        use SearchableTrait;
        /*
         * Mass assignable fields
         */
        protected $fillable = ['title', 'description', 'file', 'task_access', 'priority_id', 'due_date','assign_date',
            'repetitive','interval','status_change_date', 'task_category_id','task_subcategory_id','cancellation_reason',
            'status_id', 'created_by','department_id', 'completed_at', 'created_at'];

        /*
         * Get the presentable trait
         */
        use PresentableTrait;


        protected $searchable = [
            'columns' => [
                'title'=>10,
                'description'=>7,
            ],
            'joins' => [

            ],
        ];

        /*
         * Specify the presenter
         */
        protected $presenter = 'PM\Presenters\ProjectPresenter';

        /**
         * Created by a user
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function creator()
        {
            return $this->belongsTo('PM\Models\User', 'created_by');
        }
        /**
         * Assigned to a user
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */

        /**
         * Assigned to many user
         */
        public function assignedUsers()
        {
            return $this->hasMany('PM\Models\AssignedUser');
        }

        public function emailGroups()
        {
            return $this->hasMany('PM\Models\TaskEmailGroup');
        }
        /**
         * Relationship between task and time tracked on task
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function tracking()
        {
            return $this->hasMany('PM\Models\TaskTracking','task_id');
        }

        /**
         * Belongs to a priority level
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function priority()
        {
            return $this->belongsTo('PM\Models\Priority', 'priority_id');
        }

        /**
         * Belongs to a status level
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function status()
        {
            return $this->belongsTo('PM\Models\Status', 'status_id');
        }

        /**
         * Belongs to a category
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function taskCategory()
        {
            return $this->belongsTo('PM\Models\TaskCategory', 'task_category_id');
        }
        public function taskSubCategory()
        {
            return $this->belongsTo('PM\Models\TaskSubCategories','task_subcategory_id');
        }
        /**
         * Fetch all ongoing tasks for a user
         * @param $query
         * @param $type
         * @return mixed
         */
        public function scopeOfTaskStatus($query, $type)
        {
            if ($type == "") {
                return $query->whereNotNull('status_id');
            }

            return $query->where('status_id', $type);
        }
        /**
         * Fetch tasks based on access
         * @param $query
         * @param $type
         * @return mixed
         */
        public function scopeOfTaskAccess($query, $type)
        {
            if ($type == "") {
                return $query->whereNotNull('task_access');
            }

            return $query->where('task_access', $type);
        }

        /**
         * filter by title and description
         * @param $query
         * @param $type
         * @return mixed
         */
        public function scopeOfTaskTitle($query, $type)
        {
            if ($type == "") {
                return $query->whereNotNull('title');
            }

            return $query->where('title', 'LIKE', "%$type%")->orWhere('description', 'LIKE', "%$type");
        }

        /**
         * filter by departments
         * @param $query
         * @param $type
         * @return mixed
         */
        public function scopeOfTaskDepartment($query, $type)
        {
            if ($type == "") {
                return $query->whereNotNull('department_id');
            }

            return $query->where('department_id', $type);
        }

        /**
         * filter by assigned user
         * @param $query
         * @param $type
         * @return mixed
         */
        public function scopeOfTaskAssignedTo($query, $type)
        {
            if ($type == "") {
                return $query;
            }

            return $query->whereHas('assignedUsers', function ($q) use($type){
                $q->where('user_id', $type);
            });
        }
    }