<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPhase extends Model
{
    protected $table = 'project_phases';

    protected $fillable = ['project_id','name','start_date','end_date','decisions','created_by','approval_status'];

    /**
     * Relationship between projects and project phases.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project(){
        return $this->belongsTo('PM\Models\Project','project_id');
    }

    /**
     * Relationship between users and project phases
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(){
        return $this->belongsTo('PM\Models\User','created_by');
    }
}
