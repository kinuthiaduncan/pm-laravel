<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class ReportSubItem extends Model
{
    //
    protected $fillable = ['name', 'description','report_item_id'];

    protected $table='daily_reports_subitems';

    public function reportItem(){
        return $this->belongsTo(ReportItem::class,'report_item_id');
    }
}
