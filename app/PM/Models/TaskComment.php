<?php
    /**
     * Date: 03/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class TaskComment extends Model
    {
        /*
         * Mass assignable fields
         */
        protected $fillable = ['percentage_done', 'comment', 'task_id', 'created_by'];

        /**
         * A comment belongs to a user
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function user()
        {
            return $this->belongsTo('PM\Models\User', 'created_by');
        }

        /**
         * Relationship between tasks and comments
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function task()
        {
            return $this->belongsTo('PM\Models\Task','task_id');
        }
    }