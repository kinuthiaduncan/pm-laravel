<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class ReportUserGroup extends Model
{
    //
    protected $table='report_user_groups';

    protected $fillable = ['report_id', 'user_id'];

    /**
     * Belongs to a user
     */
    public function users()
    {
        return $this->belongsTo('PM\Models\User', 'user_id');
    }
}
