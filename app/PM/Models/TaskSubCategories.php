<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class TaskSubCategories extends Model
{
    //
    protected $fillable = ['task_category_id', 'subcategory','description','created_by'];

    protected $table='task_sub_categories';

    public function creator()
    {
        return $this->belongsTo('PM\Models\User','created_by');
    }
}
