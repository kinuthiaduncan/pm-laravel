<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPhaseUpdates extends Model
{
    protected $table = 'project_phase_updates';

    protected $fillable = ['project_phase','project_id','name','start_date','end_date','decisions','created_by'];

    /**
     * Relationship with project phase
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function phase()
    {
        return $this->belongsTo('PM\Models\ProjectPhase','project_phase');
    }

    /**
     * Relationship with user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('PM\Models\User','created_by');
    }

    public function project()
    {
        return $this->belongsTo('PM\Models\Project','project_id');
    }
}
