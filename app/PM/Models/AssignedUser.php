<?php
    /**
     * Date: 18/04/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class AssignedUser extends Model
    {
        /*
         * Mass assignable fields
         */
        protected $fillable = ['task_id', 'user_id'];

        /**
         * Belongs to a user
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function user()
        {
            return $this->belongsTo('PM\Models\User', 'user_id');
        }

        public function tasks()
        {
            return $this->belongsTo('PM\Models\Task','task_id');
        }
    }