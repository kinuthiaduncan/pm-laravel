<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;
    use Nicolaslopezj\Searchable\SearchableTrait;
    use Sofa\Eloquence\Eloquence;

    class ActivityStream extends Model
    {

        /**
         * The database table used by the model.
         * @var string
         */
        protected $table = 'activity_stream';

        /**
         * The attributes excluded from the model's JSON form.
         * @var array
         */
        protected $guarded = ['id'];

        /**
         * The attributes that are mass assignable.
         * @var array
         */
        protected $fillable = [
            'activity_description',
            'activity_by',
            'project_id',
            'path'
        ];

        public function creator()
        {
            return $this->belongsTo('PM\Models\User', 'activity_by');
        }
    }