<?php

namespace PM\Models;

use Illuminate\Database\Eloquent\Model;

class DailyReport extends Model
{
    //
    protected $fillable = ['description', 'date','item_id','subitem_id','department_id', 'created_by'];

    protected $table='daily_report_descriptions';

    public function creator(){
        return $this->belongsTo(User::class, 'created_by');
    }
    public function department(){
        return $this->belongsTo(Department::class,'department_id');
    }
    public function item()
    {
        return $this->belongsTo(ReportItem::class,'item_id');
    }
    public function subitem()
    {
        return $this->belongsTo(ReportSubItem::class,'subitem_id');
    }

    public function scopeOfReportDate($query, $type)
    {
        if ($type == "") {
            return $query->whereNotNull('date');
        }

        return $query->where('date', $type);
    }

    public function scopeOfReportDepartment($query, $type)
    {
        if ($type == "") {
            return $query->whereNotNull('department_id');
        }

        return $query->where('department_id', $type);
    }
}

