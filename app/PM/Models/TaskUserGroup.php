<?php
    /**
     * Date: 23/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Models;


    use Illuminate\Database\Eloquent\Model;

    class TaskUserGroup extends Model
    {
        /*
         * Mass assignable fields
         */
        protected $fillable = ['task_id', 'user_id', 'task_comment_id'];

        /**
         * Belongs to a user
         */
        public function users()
        {
            return $this->belongsTo('PM\Models\User', 'user_id');
        }
    }