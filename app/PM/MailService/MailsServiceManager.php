<?php
namespace PM\MailService;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Created by PhpStorm.
 * User: dkinuthia
 * Date: 6/14/17
 * Time: 9:11 AM
 */
class MailsServiceManager
{
    /**
     * Authorize request
     * @return null
     */
    public function authorization()
    {
        $client = new Client();
        $accessToken = null;
        try
        {
            $response = $client->request('POST', getenv('MailService_LOGIN_URL'), [
                'form_params' => [
                    'email' => 'task@cytonn.com',
                    'password' => 'iamnumberfour2',
                ]
            ]);
        }
        catch(RequestException $e)
        {
            $response = $e->getResponse();
        }
        catch(\Exception $e)
        {
            $response = null;
        }

        if($response && $response->getStatusCode() == 200)
        {
            $body = $response->getBody();
            $accessTokenObject = (object) \GuzzleHttp\json_decode($body);
            $accessToken = $accessTokenObject->token;
        }
        return $accessToken;
    }
    /*
     * Get the email signatures from the Mail service system
     */
    public function getMails()
    {
        $accessToken = MailsServiceManager::authorization();
        if(! is_null($accessToken))
        {
            $client = new Client();
            $response = $client->request('GET',getenv('MailService_URL'), [
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken
                ],
                'json' => [
                    'identifier' => getenv('MailService_Identifier'),
                    'mailGroup' => 'tasks@cytonn.com',
                    'parameter' => 'NEW'
                ]
            ]);
            try
            {
                $data = \GuzzleHttp\json_decode($response->getBody());
                return $data->message;
            }
            catch(RequestException  $e)
            {
            }
        }
    }
}