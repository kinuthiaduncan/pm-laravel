<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\ActivityLogs;


    use Illuminate\Support\Facades\Auth;
    use PM\Models\ActivityStream;
    use PM\Models\Issue;
    use PM\Models\Project;
    use PM\Models\Task;

    /**
     * Class ActivityLogRepository
     * @package PM\ActivityLogs
     */
    class ActivityLogRepository {

        /**
         * Log project creation
         *
         * @param $created_project
         * @return mixed
         */
        public function logProjectCreation($created_project)
        {
            $log['path']='/project/' . $created_project->id;
            $log['activity_by']=Auth::user()->id;
            $log['activity_description']=$created_project->name . ' project created';
            $log['project_id']=$created_project->id;
            ActivityStream::create($log);

            return $created_project;
        }

        /**
         * @param $project_name
         * @param $project_url
         * @param $original_name
         * @return mixed
         */
        public function logProjectEditing($project_name, $project_url, $original_name)
        {
            $created_project=Project::where('name', $project_name)->where('project_url', $project_url)->first();
            $log['path']='/project/' . $created_project->id;
            $log['activity_by']=Auth::user()->id;
            $log['activity_description']=$original_name . ' project edited';
            $log['project_id']=$created_project->id;
            ActivityStream::create($log);

            return $created_project;
        }
        public function logIssueEditing($title, $description, $project_id, $original_title)
        {
            $new_issue=Issue::where('title', $title)->where('description', $description)->where('project_id', $project_id)->first();
            $log['path']='/issue/' . $new_issue->id;
            $log['activity_by']=Auth::user()->id;
            $log['activity_description']=$original_title . ' issue edited';
            $log['project_id']=$project_id;
            ActivityStream::create($log);

            return $new_issue;
        }

        /**
         * @param $title
         * @param $description
         * @param $project_id
         * @return mixed
         */
        public function logIssueCreation($title, $description, $project_id)
        {
            $created_issue=Issue::where('title', $title)->where('description', $description)->where('project_id', $project_id)->first();
            $log['path']='/issue/' . $created_issue->id;
            $log['activity_by']=Auth::user()->id;
            $log['activity_description']=$created_issue->title . ' issue created';
            $log['project_id']=$project_id;
            ActivityStream::create($log);

            return $created_issue;
        }

        /**
         * @param $title
         * @param $description
         * @param $project_id
         * @param $original_title
         * @return mixed
         */

        public function logTaskCreation($created_task)
        {
            $log['path']='/task/' . $created_task->id;
            $log['activity_by']=Auth::user()->id;
            $log['activity_description']=$created_task->name . ' task created';
            $log['task_id']=$created_task->id;
            ActivityStream::create($log);

            return $created_task;
        }

        public function logTaskEditing($title, $description, $project_id, $original_title)
        {
            $new_task=Task::where('title', $title)->where('description', $description)->where('project_id', $project_id)->first();
            $log['path']='/task/' . $new_task->id;
            $log['activity_by']=Auth::user()->id;
            $log['activity_description']=$original_title . ' task edited';
            $log['project_id']=$project_id;
            ActivityStream::create($log);

            return $new_task;
        }
    }