<?php

namespace PM\Feedback;

use App\Notifications\FeedbackCreated;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PM\Models\Feedback;
use PM\Models\FeedbackFile;
use PM\Models\File;
use PM\Models\Project;
use PM\Models\User;
use Webpatser\Uuid\Uuid;

class FeedbackRepository
{
    /**
     * Save feedback
     * @param $request
     * @param $files
     * @param $id
     */
    public function saveFeedback($request, $files, $id)
    {
        $request['created_by'] = Auth::id();
        $request['project_id'] = $id;

        $feedback = Feedback::create($request->toArray());

        $this->saveFeedbackFiles($files, $feedback->id);

        $creator = User::findOrFail($request['created_by']);
        $project = Project::findorFail($request['project_id']);
        $lead = User::findOrFail($project->project_lead);

        $creator->notify(new FeedbackCreated($feedback));
        $lead->notify(new FeedbackCreated($feedback));
    }

    /**
     * Save feedback files
     * @param $files
     * @param $id
     */
    public function saveFeedbackFiles($files, $id)
    {
        if (!is_null($files))
        {
            foreach ($files as $file)
            {
                $filename = $file->getClientOriginalName();
                $file_name = Uuid::generate();
                $saved_file = Storage::put('public/feedback_files/' . $file_name, $file);

                $fileInput['name'] = $filename;
                $fileInput['url'] = Storage::url($saved_file);
                $fileInput['feedback_id'] = $id;

                FeedbackFile::create($fileInput);

            }
        }
    }

    /**
     * Save external feedback
     * @param $request
     */
    public function saveExternalFeedback($request)
    {
        return Feedback::create($request);
    }
}