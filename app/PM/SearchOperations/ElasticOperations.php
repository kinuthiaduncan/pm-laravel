<?php
/**
 * Cytonn Technologies
 *
 * @author: Timothy Kimathi <tkimathi@cytonn.com>
 *
 * Project: pm.
 *
 */

namespace PM\SearchOperations;

use PM\Models\Issue;
use PM\Models\Project;

class ElasticOperations
{
    /*
     * First we create the index; Should only be run once
     */
    public function createIndex()
    {
        try{
            Project::createIndex($shards = null, $replicas = null);

            return 1;
        }
        catch(\Exception $e)
        {
            return 0;
        }
    }

    /*
     * Index all the tables
     */
    public function indexAllTables()
    {
        $this->createIndex();
        $this->indexIssuesTable();
        $this->indexProjectsTable();
    }

    /*
     * ReIndex all the tables
     */
    public function reIndexAllTables()
    {
        $this->reIndexIssuesTable();
        $this->reIndexProjectsTable();
    }

    /*
     * Index the issues table
     */
    public function indexIssuesTable()
    {
        Issue::putMapping($ignoreConflicts = true);

        Issue::addAllToIndex();
    }

    /*
     * Reindex the issues Table
     */
    public function reIndexIssuesTable()
    {
        Issue::reindex();
    }

    /*
     * Index the projects table
     */
    public function indexProjectsTable()
    {
        Project::putMapping($ignoreConflicts = true);

        Project::addAllToIndex();
    }

    /*
     * Reindex the projects Table
     */
    public function reIndexProjectsTable()
    {
        Project::reindex();
    }
}