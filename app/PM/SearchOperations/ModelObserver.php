<?php
/**
 * Cytonn Technologies
 *
 * @author: Timothy Kimathi <tkimathi@cytonn.com>
 *
 * Project: pm.
 *
 */

namespace PM\SearchOperations;

use Illuminate\Support\Facades\Log;

class ModelObserver
{
    /*
     * Add model to elasticsearch index when created
     */
    public function saved($model)
    {
        try
        {
            $model->addToIndex();
        }catch (\Exception $e)
        {
        }
    }

    /*
     * Add model to elastic search index when updated
     */
    public function updated($model)
    {
        try{
            $model->addToIndex();
        }
        catch (\Exception $e)
        {
        }
    }

    /*
     * Remove model from elasticsearch index when deleted
     */
    public function deleted($model)
    {
        try
        {
            $model->removeFromIndex();
        }catch (\Exception $e)
        {
        }
    }
}