<?php
/**
 * Cytonn Technologies
 *
 * @author: Timothy Kimathi <tkimathi@cytonn.com>
 *
 * Project: pm.
 *
 */

namespace PM\SearchOperations;

use Elasticquent\ElasticquentTrait;

trait ElasticOperationsTrait
{
    /*
     * Get the main ela
     */
    use ElasticquentTrait;

    /*
     * Basic Search in Elasticsearch
     */
    public static function search($term = null)
    {
        $instance = new static;

        $params = $instance->getBasicEsParams();

        $params['size'] = 1000;

        $params['body']['query']['match']['_all'] = $term;

        $result = $instance->getElasticSearchClient()->search($params);

        return static::hydrateElasticsearchResult($result);
    }

    /*
     * Search while sorting by id
     */
    public static function sortedSearchById($term = null)
    {
        $instance = new static;

        $params = $instance->getBasicEsParams();

        $params['size'] = 1000;

        $params['body']['sort'] = 'id';

        $params['body']['query']['match']['_all'] = $term;

        $result = $instance->getElasticSearchClient()->search($params);

        return static::hydrateElasticsearchResult($result);
    }

    /*
     * Search while sorting by another column
     */
    public static function sortedSearch($term = null, $sort = null, $direction = 'asc')
    {
        $instance = new static;

        $params = $instance->getBasicEsParams();

        $params['size'] = 1000;

        if(! is_null($sort))
        {
            $sortArray = [
                $sort => [
                    'order' => $direction
                ]
            ];
        }
        else
        {
            $sortArray = 'id';
        }

        $params['body']['sort'] = $sortArray;

        $params['body']['query']['match']['_all'] = $term;

        $result = $instance->getElasticSearchClient()->search($params);

        return static::hydrateElasticsearchResult($result);
    }

}