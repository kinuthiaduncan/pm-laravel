<?php

namespace PM\ComponentUsers;

use PM\Models\ComponentUser;

/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 2/3/17
 * Time: 2:28 PM
 */
class ComponentUserRepository
{

    /*
     * Get the users for a specific component
     */
    public function getComponentUsers($componentId)
    {
        $componentUsers = ComponentUser::where('project_component_id', $componentId)->get();

        return $componentUsers->map(function($componentUser)
        {
            return[
                'user_id' => $componentUser->component_user_id,
                'name' => $componentUser->user->preferred_name,
            ];
        });

    }

    /*
     * save component users
     */
    public function save($users, $projectComponent)
    {
        $users = json_decode($users);

        foreach($users as $user)
        {
            $componentUser = ComponentUser::where('project_component_id', $projectComponent->id)->where('component_user_id', $user->id)->first();

            if(is_null($componentUser))
            {
                ComponentUser::create([
                    'project_component_id' => $projectComponent->id,
                    'component_user_id' => $user->id
                ]);
            }
        }
    }

    /*
     * delete a component user
     */
    public function delete($componentId, $componentUser)
    {
        $user = ComponentUser::where('project_component_id', $componentId)->where('component_user_id', $componentUser)->delete();

        if($user == 1)
        {
            return true;
        }

        return false;
    }
}