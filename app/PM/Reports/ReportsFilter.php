<?php
namespace PM\Reports;

use Illuminate\Pagination\LengthAwarePaginator;
use Laracasts\Presenter\PresentableTrait;
use PM\Models\DailyReport;

class ReportsFilter
{
    use PresentableTrait;

    /**
     * Reports filter function
     * @param $request
     * @param $pagination
     */
    public static function filter($request, $pagination)
    {
        $query = self::query();
        $methods = self::scopes();

        foreach ($request as $key => $param) {
            if (array_key_exists($key, $methods)) {
                switch ($key) {
                    case "date":
                        $query->OfReportDate($param);
                        break;
                    case "department":
                        $query->OfReportDepartment($param);
                        break;
                    default:
                        $query;
                }
            }
        }
        $reports = $query->groupBy('date','department_id')->get();

        $reports = $reports->map(function ($report)
        {
            $report->creator = $report->creator->preferred_name;
            $report->department = $report->department->name;
            $report->item = $report->item->item;
            return $report;
        });

        $page = isset($pagination['current_page']) ? $pagination['current_page'] : 1;
        $perPage = isset($pagination['per_page']) ? $pagination['per_page'] : 10;

        $offset = ($page * $perPage) - $perPage;
        $data = $reports->slice($offset, $perPage, true);

        $reports= new LengthAwarePaginator($data, $reports->count(), $perPage, $page);

        $reports->setPath('/api/get/filter/reports');

        return $reports;
    }

    /**
     * @return array
     */
    private static function scopes()
    {
        $methods = [
            'date' => 'OfReportDate',
            'department' => 'OfReportDepartment',
        ];

        return $methods;
    }

    /**
     * @return mixed
     */
    private static function query()
    {
            $query = DailyReport::latest()
                ->orderBy('id','DESC');

        return $query;
    }
}