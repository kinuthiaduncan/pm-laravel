<?php
    /**
     * Date: 01/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Departments;


    use Illuminate\Support\Facades\Auth;
    use PM\Models\Department;

    class DepartmentsRepository
    {
        /**
         * Get an array of departments
         * @return mixed
         */
        public function getDepartments()
        {
            $departments = Department::all()
                ->mapWithKeys(function ($department) {
                    return [$department['id'] => $department['name']];
                })->toArray();

            return $departments;
        }
    }