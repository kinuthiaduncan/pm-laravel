<?php
namespace PM\ReportItems;

use PM\Models\ReportItem;
use PM\Models\ReportSubItem;

class ReportItemsRepository
{
    /**
     * Get Items for items drop down
     * @param $department
     * @return mixed
     */
    public function getItems($department)
    {
        $items = ReportItem::with('subItems')->where('department_id', $department)->get()
            ->mapWithKeys(function ($items) {
                return [$items['id'] => $items['item']];
            })->toArray();

        return $items;
            }

    /**
     *Get sub items for sub items drop down
     * @param $item
     * @return mixed
     */
    public function getSubItems($item)
    {
        $subItems = ReportSubItem::where('report_item_id',$item)->get()
            ->mapWithKeys(function ($subItems) {
                return [$subItems['id'] => $subItems['name']];
            })->toArray();
        return $subItems;
    }
}