<?php
    use PM\Models\User;


    /**
     * Function to get two dates and return their difference
     * @param $change_date
     * @param $created_at
     * @return string
     */
    function calculate_date_diff($updated_at)
    {
        if (is_null($updated_at))
        {
            return '';
        }

        return Carbon\Carbon::parse($updated_at)->diffForHumans();
    }

    function userFullName($user_id)
    {
        $user=User::find($user_id);

        return $user->preferred_name;
    }
