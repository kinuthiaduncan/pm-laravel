<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Library;


    use Illuminate\Support\Facades\Mail;
    use Illuminate\Bus\Queueable;
    use Illuminate\Queue\SerializesModels;

    /**
     * Class Mailer
     * @package PM\Library
     */
    class Mailer {
        use Queueable, SerializesModels;

        /**
         * Function to send email to project lead on project creation
         * @param $data
         */
//        public function projectCreationNotifier($data)
//        {
//            Mail::send('emails.project', ['data'=>$data, 'access'=>$data->project_access], function ($message) use ($data)
//            {
//                $message->to($data->projectLead->email);//->cc('emukiri@cytonn.com')
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject($data->name . ' Project Created');
//            });
//        }

        /**
         * @param $data
         */
        public function issueCreationNotifier($data)
        {
//            Mail::send('emails.issue', ['data'=>$data], function ($message) use ($data)
//            {
//                $message->to($data->assignedTo->email);
//                $message->cc($data->creator->email);
//                $message->cc($data->projects->projectLead->email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject('[' . $data->projects->project_key . '-' . $data->id . '] ' . $data->title . ' Issue Created');
//            });
        }

        /**
         * @param $data
         */
//        public function issueEditingNotifier($data)
//        {
//            Mail::send('emails.issuechange', ['data'=>$data], function ($message) use ($data)
//            {
//                $message->to($data->assignedTo->email);
//                $message->cc($data->creator->email);
//                $message->cc($data->projects->projectLead->email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject('[' . $data->projects->project_key . '-' . $data->id . '] ' . $data->title . ' Issue Edited');
//            });
//        }

        /**
         * @param $data
         */
//        public function issueCreatedMembersNotifier($data)
//        {
//            Mail::send('emails.issuemembers', ['data'=>$data], function ($message) use ($data)
//            {
//                $message->to($data->member_email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject('[' . $data->projects->project_key . '-' . $data->id . '] ' . $data->title . ' Issue Created');
//            });
//        }

        /**
         * @param $data
         */
//        public function issueEditedMembersNotifier($data)
//        {
//            Mail::send('emails.issuemembers', ['data'=>$data], function ($message) use ($data)
//            {
//                $message->to($data->member_email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject('[' . $data->projects->project_key . '-' . $data->id . '] ' . $data->title . ' Issue Edited');
//            });
//        }

        /**
         * @param $data
         */
//        public function commentCreationNotifier($data)
//        {
//            Mail::send('emails.comment', ['creator'=>$data->comment_creator,'data'=>$data, 'comment'=>$data->comment_name, 'member'=>$data->member_firstname], function ($message) use ($data)
//            {
//                $message->to($data->member_email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject('A New Comment for ' . $data->title . ' Issue Created');
//            });
//        }

        /**
         * @param $data
         */
//        public function milestoneCreationNotifier($data)
//        {
//            Mail::send('emails.milestone', ['date'=>$data->milestone_date, 'description'=>$data->milestone_description, 'firstname'=>$data->member_firstname, 'data'=>$data, 'title'=>$data->milestone_title], function ($message) use ($data)
//            {
//                $message->to($data->member_email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject('[ ' . $data->project_key . '-M' . $data->milestone_id . ' ] ' . $data->milestone_title . ' Milestone Created');
//            });
//        }

        /**
         * @param $data
         */
//        public function milestoneEditedNotifier($data)
//        {
//            Mail::send('emails.milestone', ['date'=>$data->milestone_date, 'description'=>$data->milestone_description, 'firstname'=>$data->member_firstname, 'data'=>$data, 'title'=>$data->milestone_title], function ($message) use ($data)
//            {
//                $message->to($data->member_email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject('[ ' . $data->project_key . '-M' . $data->milestone_id . ' ] ' . $data->milestone_title . ' Milestone Edited');
//            });
//        }

        /**
         * @param $data
         */
//        public function memberAddNotifier($data)
//        {
//            Mail::send('emails.addmember', ['member_firstname'=>$data->member_firstname, 'added_by'=>$data->added_by, 'data'=>$data], function ($message) use ($data)
//            {
//                $message->to($data->member_email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject('You have been added to ' . $data->name . ' Project');
//            });
//        }

        /**
         * @param $data
         */
//        public function memberRemovedNotifier($data)
//        {
//            Mail::send('emails.removemember', ['member_firstname'=>$data->member_firstname, 'added_by'=>$data->added_by, 'data'=>$data], function ($message) use ($data)
//            {
//                $message->to($data->member_email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject('You have been removed from ' . $data->name . ' Project');
//            });
//        }

        /**
         * Notify the user of a task assigned to them
         * @param $data
         */
//        public function taskCreationNotifier($data)
//        {
//            Mail::queue('emails.tasks.created_task', ['data'=>$data], function ($message) use ($data)
//            {
//                $message->to($data->assignee->email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject($data->title . ' Task Created');
//            });
//        }

        /**
         * Notify the user group of tasks they have been added to
         * @param $data
         * @param $user_group_emails
         */
//        public function taskCreatedUserGroupNotifier($data, $user_group_emails)
//        {
//            Mail::queue('emails.tasks.usergroups', ['data'=>$data], function ($message) use ($data, $user_group_emails)
//            {
//                $message->to($user_group_emails);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject($data->title . ' Task Created');
//            });
//        }

        /**
         * Notify a user when a task has been updated
         * @param $data
         */
//        public function taskUpdateNotifier($data)
//        {
//            Mail::queue('emails.tasks.updated_task', ['data'=>$data], function ($message) use ($data)
//            {
//                $message->to($data->assignee->email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject($data->title . ' Task Updated');
//            });
//        }
        
        /**
         * Send a Reminder Email
         * @param $data
         */
//        public function taskReminder($data)
//        {
//            Mail::queue('emails.tasks.reminder', ['data'=>$data], function ($message) use ($data)
//            {
//                $message->to($data->user_email);
//                $message->from('support@cytonn.com', 'Cytonn Project Management');
//                $message->subject($data->title . ' Task Reminder');
//            });
//        }
    }