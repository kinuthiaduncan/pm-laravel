<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Presenters;



    use Laracasts\Presenter\Presenter;
    use PM\Models\User;

    class UserPresenter extends Presenter
    {
        /*
         * Get the fullname
         */
        public function fullName()
        {
            return $this->preferred_name;
        }

        /**
         * Present full names
         *
         * @param $userId
         * @return string
         */
        public static function presentFullNames($userId)
        {
            $user = User::findOrFail($userId);

            return $user->preferred_name;
        }
    }