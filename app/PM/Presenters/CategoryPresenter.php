<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 09/12/2016
 * Time: 09:03
 */

namespace PM\Presenters;

use Laracasts\Presenter\Presenter;
use PM\Models\ProjectCategory;

class CategoryPresenter extends Presenter
{
    /**
     * Present category by name
     *
     * @param $id
     * @return mixed
     */
    public static function presentName($id)
    {
        return ProjectCategory::where('id', $id)->first()->name;

    }

}