<?php
/**
 * Cytonn Technologies
 *
 * @author: Timothy Kimathi <tkimathi@cytonn.com>
 *
 * Project: pm.
 *
 */

namespace PM\Presenters;

use Carbon\Carbon;
use Laracasts\Presenter\Presenter;
use PM\Models\Project;

class ProjectPresenter extends Presenter
{
    /*
     * Get the date for the project
     */
    public function date()
    {
        return Carbon::parse($this->created_at)->toDateString();
    }

    /**
     * Present project by title
     *
     * @param $id
     * @return mixed
     */
    public static function presentTitle($id)
    {
        $project = Project::where('id', $id)->first();

        return $project['name'];
    }
}