<?php
/**
 * Created by PhpStorm.
 * User: dkinuthia
 * Date: 6/7/17
 * Time: 2:34 PM
 */

namespace PM\Presenters;


use Carbon\Carbon;
use Laracasts\Presenter\Presenter;
use PM\Models\Issue;
use PM\Models\ProjectPhase;
use PM\Models\ProjectPhaseActivity;

class ProjectPhasePresenter extends Presenter
{
    /**
     * Calculate percentage of project phase done
     * @param $id
     * @return float|int
     */
    public static function phasePercentDone($id)
    {
        $percent_done = null;
        $activities = Issue::where('project_phase', $id)->get();
        $activity_number = count($activities);
        if ($activity_number > 0) {
            foreach ($activities as $activity) {
                $done = $activity->percentage_done;
                $percent_done += $done;
            }
            $overall = $percent_done / $activity_number;
            return $overall;
        }
        else return 0;
    }

    public static function phaseApprovalStatus($id)
    {
        $phaseStatus = ProjectPhase::findOrFail($id);
        $status = ($phaseStatus->approval_status == 1) ? 'Approved' : 'Awaiting Approval';
        return $status;
    }

    public static function timeLeft($due_date)
    {
        $today = Carbon::today();
        $time_left = $today->diffInDays(Carbon::parse($due_date),0);
        $time = $time_left > 0 ?  $time_left :  0;
        return $time;
    }
}