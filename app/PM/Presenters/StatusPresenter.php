<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 01/12/2016
 * Time: 14:54
 */

namespace PM\Presenters;


use Laracasts\Presenter\Presenter;
use PM\Models\Status;

class StatusPresenter extends Presenter
{
    /**
     * Present status by title
     *
     * @param $id
     * @return mixed
     */
    public static function presentTitle($id)
    {
        $status = Status::where('id', $id)->first();

        return $status['name'];
    }

}