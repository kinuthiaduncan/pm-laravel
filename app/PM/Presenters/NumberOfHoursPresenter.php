<?php

namespace PM\Presenters;

use Carbon\Carbon;
use Laracasts\Presenter\Presenter;
use PM\Models\IssueTracking;
use PM\Models\TaskTracking;

class NumberOfHoursPresenter extends Presenter
{
    /**
     * Calculate time in hours taken for a task, and present it to blade.
     * @param $start
     * @param $end
     * @return float|int
     */
    public static function presentNumberOfHours($start, $end)
    {
        $hours = (Carbon::parse($start)->diffInMinutes(Carbon::parse($end)))/60;
        return $hours;
    }

    /**
     * Total hours worked in a week
     * @param $user
     * @param $startDate
     * @param $endDate
     * @return float|int|null
     */
    public static function presentTotalHours($user, $startDate, $endDate)
    {
        $totalHours = null;
        $tasks = TaskTracking::where('created_by',$user)->where('date','>=', $startDate)
            ->where('date','<=', $endDate)->get();

        foreach ($tasks as $task)
        {
            $hours = (Carbon::parse($task->start_time)->diffInMinutes(Carbon::parse($task->end_time)))/60;
            $totalHours += $hours;
        }
        return $totalHours;
    }

    /**
     * Daily total hours on tasks per user
     * @param $user
     * @return float|int|null
     */
    public static function presentDailyTotalHours($user)
    {
        $totalHours = null;
        $today = Carbon::today()->format('Y-m-d');
        $tasks = TaskTracking::where('created_by',$user)->where('date', $today)->get();

        foreach ($tasks as $task)
        {
            $hours = (Carbon::parse($task->start_time)->diffInMinutes(Carbon::parse($task->end_time)))/60;
            $totalHours += $hours;
        }
        return $totalHours;
    }

    /**
     * Daily total hours on issues per user
     * @param $user
     * @return float|int|null
     */
    public static function presentIssuesDailyTotalHours($user)
    {
        $totalHours = null;
        $today = Carbon::today()->format('Y-m-d');
        $issues = IssueTracking::where('created_by',$user)->where('date', $today)->get();

        foreach ($issues as $issue)
        {
            $hours = (Carbon::parse($issue->start_time)->diffInMinutes(Carbon::parse($issue->end_time)))/60;
            $totalHours += $hours;
        }
        return $totalHours;
    }

    public static function presentIssuesTotalHours($user, $startDate, $endDate)
    {
        $totalHours = null;
        $issues = IssueTracking::where('created_by',$user)->where('date','>=', $startDate)
            ->where('date','<=', $endDate)->get();

        foreach ($issues as $issue)
        {
            $hours = (Carbon::parse($issue->start_time)->diffInMinutes(Carbon::parse($issue->end_time)))/60;
            $totalHours += $hours;
        }
        return $totalHours;
    }
}