<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Issues;


    use Illuminate\Support\Facades\Auth;
    use PM\Models\Issue;
    use PM\Models\IssueType;
    use PM\Models\Priority;
    use PM\Models\Status;
    use PM\Models\Task;

    class IssueRepository
    {


        public function getIssueTypeSelect()
        {
            $issue_types = IssueType::all();
            $issue_types_array = array('' => 'Select issue type');
            foreach ($issue_types as $issue_type) {
                $issue_types_array[$issue_type->id] = $issue_type->name;
            }

            return $issue_types_array;
        }

        public function getPrioritiesSelect()
        {
            $priorities = Priority::all();
            $priorities_array = [];
            foreach ($priorities as $priority) {
                $priorities_array[$priority->id] = $priority->name;
            }

            return $priorities_array;
        }

        public function getParentIssues($id)
        {
            $issues = Issue::where('project_id', $id)->get();
            $issues_array = array('' => 'Select parent issue');
            $issues_array[0] = 'None';
            foreach ($issues as $issue) {
                $issues_array[$issue->id] = $issue->title;
            }

            return $issues_array;
        }

        public function getStatusTypesSelect()
        {
            $status_types = Status::all();
            $status_type_array = array('' => 'Select status level');
            foreach ($status_types as $status_type) {
                $status_type_array[$status_type->id] = $status_type->name;
            }

            return $status_type_array;
        }




        public function getStatusEdit($id)
        {
            $task = Task::where('id',$id)->first();

            if($task->created_by != Auth::user()->id){
                $status_types = Status::all()->take(4);
                $status_type_array = array('' => 'Select issue status level');
                foreach ($status_types as $status_type) {
                    $status_type_array[$status_type->id] = $status_type->name;
                }
                return $status_type_array;
            }
            else{
                $status_types = Status::all();
                $status_type_array = array('' => 'Select issue status level');
                foreach ($status_types as $status_type) {
                    $status_type_array[$status_type->id] = $status_type->name;
                }

                return $status_type_array;
            }

        }


    }