<?php
/**
 * Created by PhpStorm.
 * User: dkinuthia
 * Date: 6/5/17
 * Time: 10:42 AM
 */

namespace PM\Issues;


use App\Notifications\IssueTimeCreated;
use App\Notifications\IssueTimeUpdated;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PM\Models\Issue;
use PM\Models\IssueTracking;
use PM\Models\User;

class IssueTrackingRepository
{
    /**
     * Save an issue schedule
     * @param $request
     * @param $id
     */
    public function save($request, $id)
    {
       $issue = Issue::findOrFail($id);
       $schedule = IssueTracking::create([
           'issue_id'=>$id,
           'date'=>$request->date,
           'start_time'=>Carbon::parse($request->start_time)->toTimeString(),
            'end_time'=>Carbon::parse($request->end_time)->toTimeString(),
            'created_by'=>Auth::id(),
           'description'=>$request->description
       ]);
      $issue_creator = User::findOrFail($issue->created_by);
      $schedule_creator = Auth::user();

      $issue_creator->notify(new IssueTimeCreated($schedule,$issue));
      $issue_creator->notify(new IssueTimeCreated($schedule,$issue));
    }

    /**
     * Update an issue schedule
     * @param $request
     * @param $data
     * @param $id
     */
    public function update($request,$data,$id)
    {
        $schedule = IssueTracking::findOrFail($id);
        $issue_creator = User::findOrFail($data->created_by);
        $schedule_editor = Auth::user();
        $schedule->update([
        'date'=>$request->date,
        'start_time'=>$request->start_time,
        'end_time'=>$request->end_time,
        'description'=>$request->description
    ]);
        $issue_creator->notify(new IssueTimeUpdated($schedule,$data));
        $schedule_editor->notify(new IssueTimeUpdated($schedule,$data));
    }
}