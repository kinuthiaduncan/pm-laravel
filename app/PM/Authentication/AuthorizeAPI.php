<?php
    namespace PM\Authentication;

    use GuzzleHttp\Client;
    use GuzzleHttp\Exception\RequestException;

    /**
     * Date: 15/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */
    class AuthorizeAPI
    {
        /**
         * Fetch data from an HR API
         * @param $url
         * @return mixed
         */
        public function apiRequest($url)
        {
            $client = new Client;
            $token = $this->getAccessToken();
            $response = $client->request('GET', $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]);

            try {
                $data = \GuzzleHttp\json_decode($response->getBody());
            } catch (\Exception $e) {
                throw new RequestException($e->getMessage(), $response);
            }

            return $data;
        }

        /**
         * Get the token from Cache
         * @return mixed
         * @throws Exception
         */
//        public function token()
//        {
//            return cache()->get('accessToken', function () {
//                return $this->getAccessToken();
//            });
//        }

        /**
         * Get the access token from the HR API
         * @return null
         * @throws Exception
         */
        private function getAccessToken()
        {
            $client = new Client;
            $accessToken = null;
            try {
                $response = $client->request('POST', getenv('HR_AUTH_URL'), [
                    'form_params' => [
                        'grant_type' => 'client_credentials',
                        'client_id' => getenv('HR_CLIENT_ID'),
                        'client_secret' => getenv('HR_CLIENT_SECRET')
                    ]
                ]);
            } catch (RequestException $e) {

                $response = $e->getResponse();
            } catch (\Exception $e) {
                $response = null;
            }

            if ($response && $response->getStatusCode() == 200) {
                $body = $response->getBody();
                $accessTokenObject = (object) \GuzzleHttp\json_decode($body);
                $accessToken = $accessTokenObject->access_token;
//                cache(compact('accessToken'), ($accessTokenObject->expires_in / 60));
            }
            return $accessToken;
        }
    }