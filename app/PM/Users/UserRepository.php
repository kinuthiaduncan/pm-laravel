<?php
    /**
     * Cytonn Technologies
     * @author: Edwin Mukiri <emukiri@cytonn.com>
     */

    namespace PM\Users;

    use PM\Models\Department;
    use PM\Models\EmailGroups;
    use PM\Models\Permissions;
    use PM\Models\Roles;
    use PM\Models\User;

    class UserRepository
    {
        public function allUsersSelect()
        {
            $users = User::where('active', 1)->orderBy('preferred_name')->get();
            $users_array = [];
            foreach ($users as $user) {
                $users_array[$user->id] = $user->preferred_name;
            }

            return $users_array;
        }

//        public function usersForSelect()
//        {
//            $users = User::where('active', 1)->orderBy('preferred_name')->get();
//            $users_array = [];
//            foreach ($users as $user) {
//                $users_array[] = ['value' => $user->id,
//                    'label' => $user->preferred_name];
//            }
//
//            return $users_array;
//            }



        public function allUsersSelectId()
        {
            $users = User::where('active', 1)->orderBy('preferred_name')->get();
            $users_array = [];
            foreach ($users as $user) {
                $users_array[] = ['id' => [$user->id],
                    'name' => $user->preferred_name];
            }

            return $users_array;
        }

        public function getRolesSelect()
        {
            $roles = Roles::all();
            $roles_array = array('' => 'Select role');
            foreach ($roles as $role) {
                $roles_array[$role->id] = $role->role;
            }

            return $roles_array;
        }

        public function getPermissionsSelect()
        {
            $permissions = Permissions::all();
            $permissions_array = array('' => 'Select Permission');
            foreach ($permissions as $permission) {
                $permissions_array[$permission->id] = $permission->name;
            }

            return $permissions_array;
        }

        public function userFullName($id)
        {
            $user = User::find($id);
            $user_name = $user->preferred_name;

            return $user_name;
        }

        /*
         * Get all users in the database
         */
        public function allUsers($query)
        {
            $users = User::where('active', 1)->where('firstname', 'LIKE', "%$query%")->orWhere('preferred_name', 'LIKE', "%$query%")->orWhere('lastname', 'LIKE', "%$query%")->get(['id', 'firstname', 'lastname','preferred_name','email']);

            return $users->each(function ($user) {
                $user->fullName = $user->preferred_name . ' (' . $user->email . ')';
            });
        }

        public function emailGroupsSelect()
        {
            $email_groups = EmailGroups::all();
            $groups_array = [];
            foreach ($email_groups as $email_group) {
                $groups_array[$email_group->id] = $email_group->email;
            }
            return $groups_array;
        }
    }