<?php
    /**
     * Date: 28/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace PM\Users;


    use Illuminate\Support\Facades\Auth;
    use PM\Models\Follow;

    class FollowsRepository
    {
        /**
         * Follow a user
         * @param $followed_id
         * @return mixed
         */
        public function followUser($followed_id)
        {
            $input['follower_id'] = Auth::id();
            $input['followed_id'] = $followed_id;

            $followed = Follow::create($input);

            return $followed;
        }

        /**
         * UnFollow a user
         * @param $followed_id
         * @return mixed
         */
        public function unFollowUser($followed_id)
        {
            $followed = Follow::where('follower_id', Auth::id())->where('followed_id', $followed_id)->first();

            $followed->delete();

            return 'Success';
        }

        /**
         * Follow a task
         * @param $task_id
         * @return mixed
         */
        public function followTask($task_id)
        {
            $input['follower_id'] = Auth::id();
            $input['task_id'] = $task_id;

            $followed = Follow::create($input);

            return $followed;
        }

        /**
         * UnFollow a task
         * @param $task_id
         * @return mixed
         */
        public function unFollowTask($task_id)
        {
            $followed = Follow::where('follower_id', Auth::id())->where('task_id', $task_id)->first();

            $followed->delete();

            return 'Success';
        }

        public function getAllFollowedTasksByAuthUser()
        {
            $followed_tasks = Follow::with('followed_task')->whereNotNull('task_id')->where('follower_id', Auth::id())
                ->latest()
                ->get()
                ->map(function ($task) {
                    $task->task_id = $task->followed_task->id;
                    $task->title = $task->followed_task->title;
                    $task->due_date = $task->followed_task->due_date;
                    $task->creator = $task->followed_task->creator->preferred_name;
                    $task->followed = true;

                    return $task;
                });

            return $followed_tasks;
        }
    }