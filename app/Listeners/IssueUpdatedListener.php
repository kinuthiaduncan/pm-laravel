<?php

namespace App\Listeners;

use App\Events\IssueUpdated;
use App\Mail\IssueEdited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use PM\Library\Mailer;
use PM\ActivityLogs\ActivityLogRepository;

class IssueUpdatedListener
{
    protected $mailer;


    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        $this->mailer = new Mailer();
        $this->activityLogRepository = new ActivityLogRepository();
    }

    /**
     * Handle the event.
     *
     * @param  IssueUpdated  $event
     * @return void
     */
    public function handle(IssueUpdated $event)
    {
//        $this->mailer->issueEditingNotifier($event->issue);

        if ($event->issue->projects->project_access == 'public') {
            $this->activityLogRepository->logIssueEditing($event->issue->title, $event->issue->description,
                $event->issue->project_id, $event->issue->original_title);
        }

        Mail::to($event->issue->assignedTo)

            ->cc($event->issue->creator->email)
            ->bcc($event->issue->projects->projectLead->email)

            ->queue(new IssueEdited($event));
    }
}
