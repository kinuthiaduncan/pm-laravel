<?php

    namespace App\Listeners;

    use App\Events\IssueCreated;
    use PM\ActivityLogs\ActivityLogRepository;
    use PM\Library\Mailer;

    class IssueCreatedListener
    {
        protected $activityLogRepository;

        protected $mailer;

        /**
         * Create the event listener.
         *
         */
        public function __construct()
        {
            $this->activityLogRepository = new ActivityLogRepository();
            $this->mailer = new Mailer();
        }

        /**
         * Handle the event.
         *
         * @param  IssueCreated $event
         * @return void
         */
        public function handle(IssueCreated $event)
        {
            $issue = $event->issue;

            //Log this activity if public
            if ($issue->projects->project_access == 'public') {
                $this->activityLogRepository->logIssueCreation($issue->title, $issue->description, $issue->project_id);
            }

            //send mail
            if ($issue->assigned_to == $issue->created_by && $issue->assigned_to == $issue->projects->project_lead) {
                $issue->assignedTo->notify(new \App\Notifications\IssueCreated($issue));
            } elseif ($issue->assigned_to == $issue->created_by && $issue->assigned_to != $issue->projects->project_lead) {
                $issue->assignedTo->notify(new \App\Notifications\IssueCreated($issue));
                $issue->projects->projectLead->notify(new \App\Notifications\IssueCreated($issue));
            } elseif ($issue->assigned_to != $issue->created_by && $issue->assigned_to == $issue->projects->project_lead) {
                $issue->creator->notify(new \App\Notifications\IssueCreated($issue));
                $issue->assignedTo->notify(new \App\Notifications\IssueCreated($issue));
            } elseif ($issue->assigned_to != $issue->created_by && $issue->created_by == $issue->projects->project_lead) {
                $issue->creator->notify(new \App\Notifications\IssueCreated($issue));
                $issue->assignedTo->notify(new \App\Notifications\IssueCreated($issue));
            } else {
                $issue->assignedTo->notify(new \App\Notifications\IssueCreated($issue));
                $issue->creator->notify(new \App\Notifications\IssueCreated($issue));
                $issue->projects->projectLead->notify(new \App\Notifications\IssueCreated($issue));
            }
        }
    }
