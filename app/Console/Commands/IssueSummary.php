<?php

    namespace App\Console\Commands;

    use Barryvdh\Snappy\Facades\SnappyPdf;
    use Carbon\Carbon;
    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Mail;
    use PM\Models\Issue;
    use PM\Models\Project;
    use PM\Models\User;
    use PDF;

    class IssueSummary extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'project:issuesummary';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Weekly Issues summary';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {

            $this->sendSummary();

        }

        protected function sendSummary()
        {
            //
            $startDate = Carbon::today()->startOfWeek()->toDateString();

            $endDate = Carbon::today()->endOfWeek()->toDateString();

            $issuesCreated = Project::whereHas('issues',function($issue) use ($startDate,$endDate){
                $issue->where('created_at', '>=', $startDate)
                    ->where('created_at', '<=', $endDate);
            })->get();

            $issuesUpdatedReview = Project::whereHas('issues',function($issue) use ($startDate,$endDate){
                $issue->where('status_change_date', '<=', $endDate)->where('status_change_date','>=', $startDate)
                    ->where('status_id', 3);
            })->get();

            $issuesUpdatedDone = Project::whereHas('issues',function($issue) use ($startDate,$endDate){
                $issue->where('status_change_date', '<=', $endDate)->where('status_change_date','>=', $startDate)
                    ->where('status_id', 4);
            })->get();

               $data = [
                   'issuesCreated' => $issuesCreated,
                   'issuesUpdatedReview' => $issuesUpdatedReview,
                   'issuesUpdatedDone' => $issuesUpdatedDone,
                   'startDate' => $startDate,
                   'endDate' => $endDate
               ];

            $pdf = SnappyPdf::loadView('issues.exports.issue_summary', $data);

            Mail::send('emails.issue_summary',$data,function ($m) use($pdf){
                    $m->attachData($pdf->output(), "Issue_summary.pdf");
                    $m->from('support@cytonn.com', 'CT Project Management');
                    $m->to(['mchaka@cytonn.com'])->subject('Issues Summary for the Week');
                });
        }
    }
