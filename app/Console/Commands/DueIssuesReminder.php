<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 01/12/2016
 * Time: 11:11
 */

namespace App\Console\Commands;


use Carbon\Carbon;
use Illuminate\Console\Command;
use PM\Models\Issue;
use PM\Models\Project;
use PM\Models\User;

class DueIssuesReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:dueissues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind users of issues due and overdue issues';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Get all users with unresolved issues
        $users = User::whereHas('issues', function ($issue)
        {
            $issue->whereHas('statuses', function ($status)
            {
                $status->where('resolution', '!=', true);
            });
        })->get();

        $users->each(function ($user)
        {
            //send reminder
            $this->sendReminder($user);
        });


    }

    protected function sendReminder(User $user)
    {
        $issues = Issue::whereHas('statuses', function ($status)
        {
            $status->where('resolution', '!=', true);
        })->where('assigned_to', $user->id)
            ->orderBy('project_id')
            ->get();

        $cutOff = Carbon::today()->addDays(2)->toDateString();
        $now = Carbon::now()->toDateString();

        //Issues overdue
        $overdue = $issues->filter(function ($issue) use ($now) {
            return $issue->date_due < $now;
        });

        //Issues due today
        $today = $issues->filter(function ($issue) use ($now) {
            return $issue->date_due == $now;
        });

        //Issues due in next two days
        $next2days = $issues->filter(function ($issue) use ($now, $cutOff) {
            return ($issue->date_due > $now) && ($issue->date_due <= $cutOff);
        });

        //Notify each user
        $viewData = [
            'overdue'=>$overdue,
            'today'=>$today,
            'next2days'=>$next2days,
            'user'=>$user
        ];

        $projects = Project::whereIn('id', $issues->unique('project_id')->pluck('project_id'))->get();

        $leads_emails = User::whereIn('id', $projects->unique('project_lead')->pluck('project_lead'))->pluck('email');

        \Mail::send('emails.due_issues_reminder', $viewData, function ($message) use ($user, $now, $leads_emails)
        {
            $name = $user->present()->fullName;


            $message->from(['devs@cytonn.com'=>'CT Project Management']);
            $message->to([$user->email]);
            $leads_emails->each(function ($email) use ($message)
            {
                $message->cc($email);
            });

            $message->subject("[PM] [$name] Issues Reminder for ". Carbon::parse($now)->toFormattedDateString());
        });
    }

}
