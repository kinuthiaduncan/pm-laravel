<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use PM\Models\Department;
use PM\Models\TaskTracking;
use PM\Models\User;

class DailyTaskTrackingSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:dailytrackingsummary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily task tracking summary to Departments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today()->toDateString();

        //Tasks tracked for the day
        $departmentsTracked = Department::whereHas('user', function($user) use($today){
            $user->whereHas('taskTracking',function($tracking) use($today){
                $tracking->where('date', $today);
            });
        })->get();

        $departmentsNotTracked = Department::where('hr_department_id','!=',2)->whereHas('user', function($user) use($today){
            $user->where('active',1)->whereDoesntHave('taskTracking',function($tracking) use($today){
                $tracking->where('date', $today);
            });
        })->get();

        $departmentsTracked->each(function ($department){
            $this->sendUsersTracked($department);
        });

        $departmentsNotTracked->each(function($department){
            $this->sendUsersNotTracked($department);
        });
    }

    /**
     * Send tasks tracked summary
     * @param $department
     */
    protected function sendUsersTracked($department)
    {
        $today = Carbon::today()->toDateString();


        $schedules = TaskTracking::where('date', $today)->whereHas('creator', function ($user) use($department){
            $user->where('department_id',$department->hr_department_id);
        })->groupBy('created_by')->get();

        Mail::send('emails.tasks.daily_tracking_summary', ['schedules'=>$schedules], function ($message) use ($department) {
            $message->from(['support@cytonn.com' => 'CT Project Management']);
            $message->to([$department->department_email]);
            $message->subject('Daily Task Tracking Summary');
        });
    }
    protected function sendUsersNotTracked($department)
    {
        $today = Carbon::today()->toDateString();

        $users = User::where('department_id',$department->hr_department_id)->whereDoesntHave('taskTracking',
            function($tracking) use($today){
            $tracking->where('date', $today);
        })->get();

        Mail::send('emails.tasks.daily_untracked_users', ['users'=>$users], function ($message) use ($department) {
            $message->from(['support@cytonn.com' => 'CT Project Management']);
            $message->to([$department->department_email]);
            $message->subject('Staff Without Task Schedules Today');
        });
    }
}
