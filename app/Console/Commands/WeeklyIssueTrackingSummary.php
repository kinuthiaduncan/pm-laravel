<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use PM\Models\Department;
use PM\Models\User;

class WeeklyIssueTrackingSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'issue:weeklytrackingsummary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Report on weekly issue time tracking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $department = Department::where('id',2)->first();
        $this->sendSummary($department);
    }

    /**
     * Send summary to department
     * @param $department
     */
    protected function sendSummary($department)
    {
        $startDate = Carbon::today()->startOfWeek()->toDateString();
        $endDate = Carbon::today()->endOfWeek()->toDateString();

        $usersTracked = User::where('active',1)->where('department_id',$department->id)->whereHas('issueTracking',
            function($tracking) use ($startDate,$endDate){
            $tracking->whereBetween('date',[$startDate,$endDate]);
            })->get();

        $tasksTracked = User::where('active',1)->where('department_id',$department->id)->whereHas('taskTracking',
            function($tracking) use ($startDate,$endDate){
                $tracking->whereBetween('date',[$startDate,$endDate]);
            })->get();

        $usersNotTracked = User::where('active',1)->where('department_id',$department->id)->whereDoesntHave('issueTracking',
            function($tracking) use ($startDate,$endDate){
                $tracking->whereBetween('date',[$startDate,$endDate]);
            })->whereDoesntHave('taskTracking', function($tracking) use ($startDate,$endDate){
                $tracking->whereBetween('date',[$startDate,$endDate]);
            })->get();

        Mail::send('emails.issues.weekly_tracking_summary', [
            'tracked'=>$usersTracked,
            'notTracked'=>$usersNotTracked,
            'tasksTracked'=>$tasksTracked,
            'startDate'=>$startDate,
            'endDate'=>$endDate
        ],
            function ($message) use ($department) {
            $message->from(['support@cytonn.com' => 'CT Project Management']);
            $message->to(['mchaka@cytonn.com']);
            $message->cc(['dkinuthia@cytonn.com']);
            $message->subject('Weekly Issue and Task Tracking Summary');
        });

    }
}
