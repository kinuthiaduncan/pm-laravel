<?php
    /**
     * Date: 21/01/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Console\Commands;


    use Carbon\Carbon;
    use Illuminate\Console\Command;
    use PM\Models\Issue;
    use PM\Models\User;

    class IssueDueTodayReminder extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'project:issuesduetoday';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Remind users of issues that are due today';

        /**
         * Create a new command instance.
         *
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            //Get all users with unresolved issues
            $users = User::whereHas('issues', function ($issue)
            {
                $now = Carbon::today()->toDateString();

                $issue->where('date_due', '=', $now)->whereHas('statuses', function ($status) {
                    $status->where('resolution', '!=', true);
                });
            })->get();

            $users->each(function ($user) {
                //send reminder
                $this->sendReminder($user);
            });
        }

        protected function sendReminder(User $user)
        {
            $issues = Issue::whereHas('statuses', function ($status)
            {
                $status->where('resolution', '!=', true);
            })->where('assigned_to', $user->id)
                ->get();

            $now = Carbon::now()->toDateString();

            //Issues due today
            $today = $issues->filter(function ($issue) use ($now) {
                return $issue->date_due == $now;
            });

            //Notify each user
            $viewData = [
                'today' => $today,
            ];

            \Mail::send('emails.issues_due_today', $viewData, function ($message) use ($user, $now) {
                $message->from(['devs@cytonn.com' => 'CT Project Management']);
                $message->to([$user->email]);
                $message->subject('Issues Reminder for ' . $now);
            });
        }
    }