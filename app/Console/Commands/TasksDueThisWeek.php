<?php

    namespace App\Console\Commands;

    use Carbon\Carbon;
    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Mail;
    use PM\Models\Task;
    use PM\Models\User;

    class TasksDueThisWeek extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'task:tasksduethisweek';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Remind users of tasks that are due this week';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            //Get all users with undone tasks

            $users = User::whereHas('taskAssigned', function ($taskAssigned) {

                $taskAssigned->whereHas('tasks', function($task){

                    $startDate = Carbon::today()->startOfWeek()->toDateString();

                    $endDate = Carbon::today()->endOfWeek()->toDateString();

                    $task->where('due_date', '>=', $startDate)->where('due_date', '<=', $endDate)->whereHas('status', function ($status) {
                        $status->where('resolution', '!=', true);
                    });
                });
            })->get();


            $users->each(function ($user) {
                //send reminder
                $this->sendReminder($user);
            });
        }

        protected function sendReminder(User $user)
        {
            $tasks = Task::whereHas('status', function ($status) {
                $status->where('resolution', '!=', true);
            })->whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })
                ->get();

            $startDate = Carbon::today()->startOfWeek()->toDateString();
            $endDate = Carbon::today()->endOfWeek()->toDateString();

            $this_week = $tasks->filter(function ($task) use ($startDate, $endDate) {
                return ($task->due_date >= $startDate) && ($task->due_date <= $endDate);
            });

            //Notify each user
            $viewData = [
                'this_week' => $this_week,
            ];

            Mail::send('emails.tasks.tasks_due_this_week', $viewData, function ($message) use ($user) {
                $message->from(['support@cytonn.com' => 'Task Management']);
                $message->to([$user->email]);
                $message->subject('Tasks Reminder for This Week');
            });
        }
    }
