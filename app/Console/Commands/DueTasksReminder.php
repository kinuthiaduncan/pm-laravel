<?php

    namespace App\Console\Commands;

    use Carbon\Carbon;
    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Mail;
    use PM\Models\Task;
    use PM\Models\User;

    class DueTasksReminder extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'task:duetasks';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Remind users of tasks due and overdue tasks';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $users = User::whereHas('taskAssigned', function ($taskAssigned) {

                $taskAssigned->whereHas('tasks', function($task){
                    $today = Carbon::now()->toDateString();
                    $nextTwoDays = Carbon::now()->addDays(2)->toDateString();
                    $task->where('due_date','<=',$today)->where('due_date','<=',$nextTwoDays)
                        ->whereHas('status', function ($status) {
                        $status->where('resolution', '!=', true);
                    });
                });
            })->get();
            
            $users->each(function ($user) {
                //send reminder
                $this->sendReminder($user);
            });
        }

        protected function sendReminder(User $user)
        {
            $tasks = Task::whereHas('status', function ($status) {
                $status->where('resolution', '!=', true);

            })->whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })
                ->get();

            $cutOff = Carbon::today()->addDays(2)->toDateString();
            $now = Carbon::now()->toDateString();

            //Tasks overdue
            $overdue = $tasks->filter(function ($task) use ($now) {
                return $task->due_date < $now;
            });

            //Tasks due today
            $today = $tasks->filter(function ($task) use ($now) {
                return $task->due_date == $now;
            });

            //Tasks due in next two days
            $next2days = $tasks->filter(function ($task) use ($now, $cutOff) {
                return ($task->due_date > $now) && ($task->due_date <= $cutOff);
            });

            $viewData = [
                'overdue' => $overdue,
                'today' => $today,
                'next2days' => $next2days
            ];

            Mail::send('emails.tasks.due_tasks_reminder', $viewData, function ($message) use ($user, $now) {
                $name = $user->present()->fullName;


                $message->from(['support@cytonn.com' => 'Task Management']);
                $message->to([$user->email]);

                $message->subject("[$name] Tasks Reminder for " . Carbon::parse($now)->toFormattedDateString());
            });
        }
    }
