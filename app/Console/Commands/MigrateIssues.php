<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PM\Models\Issue;
use PM\Models\Project;
use PM\Models\ProjectComponent;

class MigrateIssues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate-issues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate and allow issues to belong to a specified project-component';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $projects = Project::all();

        foreach ($projects as $p){

            $p->project_access = 'public';
            $p->save();

            //Create a component for each of the existing projects
            $component['project_id'] = $p->id;
            $component['component_name'] = 'Development';
            $component['description'] = 'A component for issues belonging to software development';
            ProjectComponent::create($component);

            //Get all issues for each of the existing projects
            $p_component = ProjectComponent::where('project_id', $p->id)->where('component_name', $component['component_name'])->where('description', $component['description'])->first();

            $issues = Issue::where('project_id', $p->id)->get();
            foreach ($issues as $iss){
                $iss['component_id'] = $p_component->id;
                $iss->save();
            }

        }

    }
}
