<?php
    /**
     * Date: 19/04/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Console\Commands;


    use Illuminate\Console\Command;
    use PM\Models\AssignedUser;
    use PM\Models\Task;

    class TransferAssignedUsers extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'transfer:assigned';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Transfer all assigned Issues';

        /**
         * Create a new command instance.
         *
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         */
        public function handle()
        {
            Task::all()->map(function ($task){
                $data = ['task_id'=>$task->id, 'user_id'=>$task->assigned_to ];
                AssignedUser::create($data);
            });
        }

    }