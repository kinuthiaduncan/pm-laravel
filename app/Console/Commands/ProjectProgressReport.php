<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use PM\Models\Project;

class ProjectProgressReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:progress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate and mail progress report for software projects';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cutOff = Carbon::today()->subWeek();
        $end = Carbon::now();

        $progress = Project::type('Software')->where('active',1)
            ->whereHas('progress', function ($progress) use ($cutOff)
        {
            $progress->where('created_at', '>=', $cutOff);

        })->get();

        $noProgress = Project::type('Software')->where('active',1)
            ->whereDoesntHave('progress', function ($progress) use ($cutOff)
        {
            $progress->where('created_at', '>=', $cutOff);

        })->get();

        $viewData = [
            'progress'=>$progress,
            'no_progress'=>$noProgress,
            'cut_off'=>$cutOff,
            'end'=>$end
        ];

        \Mail::send('emails.progress_report', $viewData, function ($message) use ($end)
        {
            $message->from(['devs@cytonn.com'=>'CT Project Management']);
            $message->to(['nolwero@cytonn.com','mchaka@cytonn.com', 'cytonntechnologies@cytonn.com', 'techleads@cytonn.com']);
            $message->subject('Project progress report - '.$end->toFormattedDateString());
        });
    }
}
