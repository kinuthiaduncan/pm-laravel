<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use PM\Models\User;
use PM\Models\Task;
use PM\Models\Project;
use Mail;
use Carbon\Carbon;

class RepetitiveTasksReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:repetitivereminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind Users of Repetitive tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get users with repetitive tasks
        $users = User::whereHas('taskAssigned', function ($taskAssigned) {

            $taskAssigned->whereHas('tasks', function ($task) {
            $task->where('repetitive', 1);
            });
        })->get();

        $users->each(function ($user) {
            //send reminder
            $this->setNewDueDate($user);
        });
    }

    protected function setNewDueDate(User $user)
    {
        $yesterday = Carbon::yesterday()->toDateString();

        $tasks = Task::whereHas('status', function ($status) {
            $status->where('resolution', '!=', true);
        })->where('repetitive', 1)->where('due_date', '=', $yesterday)
            ->get();

        $tasks->map(function ($task){
            if ($task->interval == 1){
                $due_date = Carbon::parse($task->due_date);
                $toRemind = new Carbon($due_date->addDays(1)->toDateString());
                Task::where('id', $task->id)->update(['due_date' => $toRemind, 'status_id' => 1]);
            } elseif ($task->interval == 7){
                $due_date = Carbon::parse($task->due_date);
                $toRemind = new Carbon($due_date->addDays(7)->toDateString());
                Task::where('id', $task->id)->update(['due_date' => $toRemind, 'status_id' => 1]);
            } elseif ($task->interval == 30){
                $due_date = Carbon::parse($task->due_date);
                $toRemind = new Carbon($due_date->addDays(28)->toDateString());
                Task::where('id', $task->id)->update(['due_date' => $toRemind, 'status_id' => 1]);
            };

            return $task;
        });
    }
}
