<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PM\MailService\MailsServiceManager;
use PM\Tasks\TasksRepository;

class GetEmailTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:fromemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get new tasks created via email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $taskRepository;

    public function __construct(TasksRepository $tasksRepository)
    {
        parent::__construct();
        $this->taskRepository = $tasksRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mailServiceManager = new MailsServiceManager();
        $data = $mailServiceManager->getMails();

        if($data != "No Emails") {
            foreach ($data as $item) {
                $this->getDetails($item);
            }
        }
    }

    /**
     * Get all details from message
     * @param $item
     */
    protected function getDetails($item)
    {
        $body = $this->getBody($item->thread);

        $taskData =[
        'title' => $item->subject,
        'description' => $body['content'][0],
        'assignee' => $body['assigned'],
        'creator' => $body['from'],
        ];

        $this->taskRepository->saveFromEmail($taskData);
    }

    /**
     * Get message body
     * @param $thread
     * @return mixed
     */
    protected function getBody($thread)
    {

        foreach ($thread as $value)
        {
            $message = [
            'from' => $value->from,
            'assigned' => $this->getAssigned($value),
            'content' =>  list($value->message) = explode('<br clear="all">',$value->message),
            ];
            return $message;
        }
    }

    /**
     * Get assigned from all emails
     * @param $value
     * @return array
     */
    protected function getAssigned($value)
    {
        $from = $value->from;
        $cc = explode(',',$value->cc);
        $to = explode(',',$value->to);
        $bcc = explode(',',$value->bcc);
        $assignee_emails = array_filter(array_merge($cc,$to,$bcc));
        $emails = array_diff($assignee_emails, array('dkinuthia@cytonn.com'));
        $assigned = (count($emails) > 0) ? $emails :  $from;
       return $assigned;
    }

}
