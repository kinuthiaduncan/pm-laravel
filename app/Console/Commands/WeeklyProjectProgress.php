<?php

namespace App\Console\Commands;

use App\Mail\ProjectProgressReminder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use PM\Library\Mailer;
use PM\Models\Project;
use Carbon\Carbon;
use PM\Models\ProjectUser;
use PM\Models\User;

class WeeklyProjectProgress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:weeklyreminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weekly Project Progress Reminders';


    /**
     * WeeklyProjectProgress constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     * Send project progress update reminders to project members
     * @return mixed
     */
    public function handle()
    {
        $cutOff = Carbon::today()->subWeek();
        $end = Carbon::now();

        $noProgress = Project::type('Software')->where('active',1)->whereDoesntHave('progress', function ($progress) use ($cutOff)
        {
            $progress->where('created_at', '>=', $cutOff);

        })->pluck('id');

        foreach ($noProgress as $project)
        {
            $this_week = Project::where('id', $project)->get();
            $lead = Project::where('id', $project)->pluck('project_lead');
            $user = User::where('id', $lead)->get();
            Mail::to($user)->send(new ProjectProgressReminder($this_week));
        }

    }

}
