<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 09/12/2016
 * Time: 10:12
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use PM\Models\Project;
use PM\Models\ProjectCategory;
use PM\Models\User;

class AddProjectsToCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:migrateprojects';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add projects to a common category';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->ask('Please provide your Cytonn email: ');

        $user = User::where('email', $email)->first();
        if ($user === null) {
            $this->error('Invalid email!');
           die();
        }

        //Create a category
        $category = new ProjectCategory();
        $category->slug = 'AUTOMATION OF INTERNAL PROCESSES';
        $category->name = 'AUTOMATION OF INTERNAL PROCESSES';
        $category->description = 'These are important to the smooth running of the business, therefore they are of high priority';
        $category->created_by = $user->id;
        $category->save();
        $projects = Project::all();

        foreach ($projects as $p){

            $p->category_id = $category->id;
            $p->update();
        }

    }
}
