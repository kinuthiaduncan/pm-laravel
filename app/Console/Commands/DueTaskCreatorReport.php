<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use PM\Models\Task;
use PM\Models\User;

class DueTaskCreatorReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:creatorduetasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send all due tasks to the task creators';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $taskCreators = User::whereHas('taskCreated',function ($task){
            $today = Carbon::now()->toDateString();
            $nextTwoDays = Carbon::now()->addDays(2)->toDateString();
            $task->where('due_date','<=',$today)->where('due_date','<=',$nextTwoDays)
                ->whereHas('status', function ($status) {
                    $status->where('resolution', '!=', true);
                });
        })->get();

        $taskCreators->each(function($creator){
           $this->sendReport($creator);
        });
    }

    /**
     * Send report to task creators or assignors
     * @param $creator
     */
    public function sendReport($creator)
    {
        $tasks = Task::whereHas('status', function ($status) use ($creator) {
            $status->where('resolution', '!=', true);
            })->where('created_by',$creator->id)->get();

        $cutOff = Carbon::today()->addDays(2)->toDateString();
        $now = Carbon::now()->toDateString();

        //Tasks overdue
        $overdue = $tasks->filter(function ($task) use ($now) {
            return $task->due_date < $now;
        });

        //Tasks due today
        $today = $tasks->filter(function ($task) use ($now) {
            return $task->due_date == $now;
        });

        //Tasks due in next two days
        $next2days = $tasks->filter(function ($task) use ($now, $cutOff) {
            return ($task->due_date > $now) && ($task->due_date <= $cutOff);
        });

        $viewData = [
            'overdue' => $overdue,
            'today' => $today,
            'next2days' => $next2days
        ];

        Mail::send('emails.tasks.due_tasks_creator_report', $viewData, function ($message) use ($creator, $now) {
            $message->from(['support@cytonn.com' => 'Task Management']);
            $message->to([$creator->email]);

            $message->subject("Tasks Assigned Reminder for " . Carbon::parse($now)->toFormattedDateString());
        });
    }
}
