<?php
    /**
     * Date: 15/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Console\Commands;


    use Illuminate\Console\Command;
    use PM\Authentication\AuthorizeAPI;
    use PM\Models\User;

    class CallHRUsersAPI extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'users:fetch';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Fetch Users From HR';
        /**
         * @var AuthorizeAPI
         */
        private $authorizeAPI;

        /**
         * Create a new command instance.
         *
         * @param AuthorizeAPI $authorizeAPI
         */
        public function __construct(AuthorizeAPI $authorizeAPI)
        {
            parent::__construct();
            $this->authorizeAPI = $authorizeAPI;
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $hr_users = $this->authorizeAPI->apiRequest(getenv('HR_STAFF_URL'));

            $pm_users_hr_ids = User::pluck('hr_id')->toArray();

            collect($hr_users->data)->map(function ($user) use ($pm_users_hr_ids) {
                $user_info = [
                    'firstname' => $user->firstname,
                    'lastname' => $user->lastname,
                    'preferred_name'=>$user->signature_name,
                    'email' => $user->email,
                    'job_level' => $user->job_level_id->name,
                    'job_title' => $user->job_title,
                    'phone_number' => $user->mobile_no,
                    'avatar' => $user->image_url,
                    'department_id' => $user->departments->id,
                    'manager_id' => $user->manager_id,
                    'supervisor_id' => $user->leave_approver_id,
                    'hr_id' => $user->id,
                    'active' => 1,
                    'role_id' => 3
                ];

                if (in_array($user->id, $pm_users_hr_ids)) {
                    $pm_user = User::where('hr_id', $user->id)->first();

                    $pm_user->update($user_info)->except('role_id');
                } else {
                    User::create($user_info);
                }
            });

            $users_hr_ids = User::pluck('hr_id');

            $hr_users = collect($hr_users->data)->toBase()->map(function ($user) {
                return $user->id;
            });

            $inactive_users = $users_hr_ids->diff($hr_users);

            User::whereIn('hr_id', $inactive_users)->get()->map(function ($user){
                $user->update(['active' => 0]);
            });

            $this->info('Users Updated Successfully');
        }
    }