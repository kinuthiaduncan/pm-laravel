<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use PM\Models\Department;
use PM\Models\TaskTracking;
use PM\Models\User;

class WeeklyTaskTrackingSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:weeklytrackingsummary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send weekly task tracking summary to Departments';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startDate = Carbon::today()->startOfWeek()->toDateString();
        $endDate = Carbon::today()->endOfWeek()->toDateString();
        //Tasks tracked for the week

        $departmentsTracked = Department::whereHas('user', function($user) use($startDate,$endDate){
            $user->whereHas('taskTracking',function($tracking) use($startDate,$endDate){
                $tracking->where('date','>=', $startDate)->where('date','<=', $endDate);
            });
        })->get();

        $departmentsNotTracked = Department::where('hr_department_id','!=',2)->whereHas('user', function($user) use($startDate,$endDate){
            $user->where('active',1)->whereDoesntHave('taskTracking',function($tracking) use($startDate,$endDate){
                $tracking->where('date','>=', $startDate)->where('date','<=', $endDate);
            });
        })->get();

        $departmentsTracked->each(function ($department){
            $this->sendUsersTracked($department);
        });

        $departmentsNotTracked->each(function($department){
            $this->sendUsersNotTracked($department);
        });
    }

    /**
     * Send tasks tracked summary
     * @param $department
     */
    protected function sendUsersTracked($department)
    {
        $startDate = Carbon::today()->startOfWeek()->toDateString();
        $endDate = Carbon::today()->endOfWeek()->toDateString();

        $schedules = TaskTracking::where('date','>=', $startDate)->where('date','<=', $endDate)
            ->whereHas('creator', function ($user) use($department){
            $user->where('department_id',$department->hr_department_id);
        })->groupBy('created_by')->get();


        Mail::send('emails.tasks.weekly_task_tracking_summary', ['schedules'=>$schedules,'startDate'=>$startDate,'endDate'=>$endDate], function ($message) use ($department) {
            $message->from(['support@cytonn.com' => 'CT Project Management']);
            $message->to([$department->department_email]);
            $message->subject('Task Tracking Summary for the Week');
        });
    }
    protected function sendUsersNotTracked($department)
    {
        $startDate = Carbon::today()->startOfWeek()->toDateString();
        $endDate = Carbon::today()->endOfWeek()->toDateString();

        $users = User::where('department_id',$department->hr_department_id)->whereDoesntHave('taskTracking',
            function($tracking) use($startDate,$endDate){
            $tracking->where('date','>=', $startDate)->where('date','<=', $endDate);
        })->get();

        Mail::send('emails.tasks.untracked_users', ['users'=>$users], function ($message) use ($department) {
            $message->from(['support@cytonn.com' => 'CT Project Management']);
            $message->to([$department->department_email]);
            $message->subject('Users Without Task Schedules This Week');
        });
    }
}
