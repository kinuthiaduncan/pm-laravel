<?php
    /**
     * Date: 15/02/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Console\Commands;


    use Illuminate\Console\Command;
    use PM\Authentication\AuthorizeAPI;
    use PM\Models\Department;

    class CallHRDepartmentsAPI extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'departments:fetch';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Fetch Departments From HR';
        /**
         * @var AuthorizeAPI
         */
        private $authorizeAPI;

        /**
         * Create a new command instance.
         *
         * @param AuthorizeAPI $authorizeAPI
         */
        public function __construct(AuthorizeAPI $authorizeAPI)
        {
            parent::__construct();
            $this->authorizeAPI = $authorizeAPI;
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $hr_departments = $this->authorizeAPI->apiRequest(getenv('HR_DEPARTMENT_URL'));

            $department_emails = Department::all('department_email')->map(function ($department) {
                return $department->department_email;
            })->toArray();

            collect($hr_departments)->map(function ($department) use ($department_emails) {
                $department_info = [
                    'name' => $department->name,
                    'department_email' => $department->department_email,
                    'hr_department_id' => $department->id
                ];

                if (in_array($department->department_email, $department_emails)) {
                    $pm_department = Department::where('department_email', $department->department_email)->first();

                    $pm_department->update($department_info);
                } else {
                    Department::create($department_info);
                }
            });

            $this->info('Departments Updated Successfully');
        }
    }