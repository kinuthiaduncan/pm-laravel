<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use PM\Models\AssignedUser;
use PM\Models\Department;
use PM\Models\Task;

class TaskSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:summary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily task summaries per department';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $departmentCreated = Department::whereHas('user' ,function($user){
            $user->whereHas('taskAssigned', function($task){

                $today = Carbon::today()->toDateString();
                $tomorrow = Carbon::today()->addDays(1)->toDateString();

                $task->where('created_at','>=',$today)->where('created_at','<=',$tomorrow);
            });
        })->get();

        $departmentCreated->each(function($department){
            $this->sendCreatedSummary($department);
        });

    }

    /**
     * Send summary
     * @param $department
     */
    protected function sendCreatedSummary($department)
    {
        $today = Carbon::today()->toDateString();
        $tomorrow = Carbon::today()->addDays(1)->toDateString();

        $createdTasks = Task::where('created_at','>=',$today)->where('created_at','<=',$tomorrow)
            ->whereHas('assignedUsers',function($user) use ($department){
            $user->where('department_id',$department->id);
        })->get();

        $toSend=[
           'createdTasks' => $createdTasks
        ];

        Mail::send('emails.tasks.task_summary', $toSend, function ($message) use ($department,$today) {
            $message->from(['support@cytonn.com' => 'Task Management']);
            $message->to([$department->department_email]);
            $message->subject("$department->name Created Tasks Summary For " .$today);
        });
    }

}
