<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use PM\Models\Department;
use PM\Models\User;

class DailyIssueTrackingSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'issue:dailytrackingsummary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily Issue Time Tracking Summary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $department = Department::where('id',2)->first();
       $this->sendSummary($department);
    }

    /**
     * Send summary
     * @param $department
     */
    protected function sendSummary($department)
    {
        $today = Carbon::today()->toDateString();

        $usersTracked = User::where('department_id',2)->whereHas('issueTracking',function($schedule) use ($today){
            $schedule->where('date',$today);
        })->get();

        $tasksTracked = User::where('department_id',2)->whereHas('taskTracking',function($schedule) use ($today){
            $schedule->where('date',$today);
        })->get();

        $usersNotTracked = User::where('active',1)->where('department_id',2)->whereDoesntHave('issueTracking',function($schedule) use ($today){
            $schedule->where('date',$today);
        })->whereDoesntHave('taskTracking',function($schedule) use ($today){
            $schedule->where('date',$today);
        })->get();

        Mail::send('emails.issues.daily_tracking_summary', [
            'tracked'=>$usersTracked,
            'notTracked'=>$usersNotTracked,
            'tasksTracked'=>$tasksTracked
        ],
            function ($message) use ($department) {
            $message->from(['support@cytonn.com' => 'CT Project Management']);
            $message->to(['mchaka@cytonn.com']);
            $message->cc(['dkinuthia@cytonn.com']);
            $message->subject('Daily Issue and Task Tracking Summary');
        });
    }
}
