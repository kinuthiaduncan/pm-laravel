<?php

    namespace App\Console\Commands;

    use Carbon\Carbon;
    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Mail;
    use PM\Models\Task;
    use PM\Models\User;

    class TasksDueToday extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'task:tasksduetoday';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Remind users of tasks that are due today';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            //Get all users with undone tasks

            $users = User::whereHas('taskAssigned', function ($taskAssigned) {

                $taskAssigned->whereHas('tasks', function($task){
                    $now = Carbon::today()->toDateString();

                    $task->where('due_date', '=', $now)->whereHas('status', function ($status) {
                    $status->where('resolution', '!=', true);
                    });
                });
            })->get();

           // dd($users);

            $users->each(function ($user) {
                //send reminder
                $this->sendReminder($user);
            });

        }
        /**
         * Send reminder to users
         * @param User $user
         */
        protected function sendReminder(User $user)
        {
            $tasks = Task::whereHas('status', function ($status) {
                $status->where('resolution', '!=', true);

            })->whereHas('assignedUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->get();

            $now = Carbon::now()->toDateString();

            //Tasks due today
            $today = $tasks->filter(function ($task) use ($now) {
                return $task->due_date == $now;
            });

            //Notify each user
            $viewData = [
                'today' => $today,
            ];

            Mail::send('emails.tasks.tasks_due_today', $viewData, function ($message) use ($user, $now) {
                $message->from(['support@cytonn.com' => 'Task Management']);
                $message->to([$user->email]);
                $message->subject('Tasks Reminder for ' . $now);
            });
        }
    }
