<?php
    /**
     * Date: 23/01/17
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Console\Commands;


    use Carbon\Carbon;
    use Illuminate\Console\Command;
    use PM\Models\Issue;
    use PM\Models\User;

    class IssuesDueThisWeek extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'project:issuesdueweekly';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Remind users of issues that are due the current week';

        /**
         * Create a new command instance.
         *
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            //Get all users with unresolved issues
            $users = User::whereHas('issues', function ($issue)
            {
                $startDate = Carbon::today()->startOfWeek()->toDateString();

                $endDate = Carbon::today()->endOfWeek()->toDateString();

                $issue->where('date_due', '>=', $startDate)->where('date_due', '<=', $endDate)->whereHas('statuses', function ($status) {
                    $status->where('resolution', '!=', true);
                });
            })->get();

            $users->each(function ($user) {
                //send reminder
                $this->sendReminder($user);
            });
        }

        /**
         * @param User $user
         */
        protected function sendReminder(User $user)
        {
            $issues = Issue::whereHas('statuses', function ($status)
            {
                $status->where('resolution', '!=', true);
            })->where('assigned_to', $user->id)
                ->get();

            $startDate = Carbon::today()->startOfWeek()->toDateString();
            $endDate = Carbon::today()->endOfWeek()->toDateString();

            $this_week = $issues->filter(function ($issue) use ($startDate, $endDate) {
                return ($issue->date_due >= $startDate) && ($issue->date_due <= $endDate);
            });

            //Notify each user
            $viewData = [
                'this_week' => $this_week,
            ];

            \Mail::send('emails.issues_due_this_week', $viewData, function ($message) use ($user) {
                $message->from(['devs@cytonn.com' => 'CT Project Management']);
                $message->to([$user->email]);
                $message->subject('Issues Reminder for This Week');
            });
        }
    }