<?php
    /**
     * Date: 03/03/2017
     * Cytonn Technologies
     * @author: Phillis Kiragu pkiragu@cytonn.com
     */


    namespace App\Console\Commands;


    use App\Mail\TaskReminderNotifier;
    use Carbon\Carbon;
    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Mail;
    use PM\Library\Mailer;
    use PM\Models\Task;
    use PM\Models\TaskReminder;
    use PM\Models\User;

    class SendTaskReminder extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'task:reminder';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Send users their reminders';

        /**
         * Create a new command instance.
         *
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $reminders = TaskReminder::where('time', Carbon::now()->format('y:m:d h:i'))->get();

            $this->sendReminderEmails($reminders);
        }

        /**
         * Send reminder emails
         * @param $reminders
         */
        protected function sendReminderEmails($reminders)
        {
            $reminders->map(function ($reminder) {
                $task = Task::findOrFail($reminder->task_id);

                $task->reminder = $reminder->content;
                $task->user_email = User::findOrFail($reminder->created_by)->email;

                //send mail
                Mail::to($task->user_email)
                    ->queue(new TaskReminderNotifier($task, $reminder));
            });
        }
    }