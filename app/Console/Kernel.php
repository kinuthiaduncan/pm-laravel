<?php

namespace App\Console;

use App\Console\Commands\AddProjectsToCategory;
use App\Console\Commands\CallHRDepartmentsAPI;
use App\Console\Commands\CallHRUsersAPI;
use App\Console\Commands\DueIssuesReminder;
use App\Console\Commands\IssueDueTodayReminder;
use App\Console\Commands\IssuesDueThisWeek;
use App\Console\Commands\RepetitiveTasksReminder;
use App\Console\Commands\MigrateIssues;
use App\Console\Commands\ProjectProgressReport;
use App\Console\Commands\SendTaskReminder;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\ProjectProgressReport::class,
        Commands\MigrateIssues::class,
        Commands\DueIssuesReminder::class,
        Commands\AddProjectsToCategory::class,
        Commands\IssueDueTodayReminder::class,
        Commands\IssuesDueThisWeek::class,
        Commands\CallHRUsersAPI::class,
        Commands\CallHRDepartmentsAPI::class,
        Commands\SendTaskReminder::class,
        Commands\RepetitiveTasksReminder::class,
        Commands\TasksDueToday::class,
        Commands\TasksDueThisWeek::class,
        Commands\DueTasksReminder::class,
        Commands\IssueSummary::class,
        Commands\TransferAssignedUsers::class,
        Commands\WeeklyProjectProgress::class,
        Commands\WeeklyTaskTrackingSummary::class,
        Commands\TaskSummary::class,
        Commands\DailyTaskTrackingSummary::class,
        Commands\DueTaskCreatorReport::class,
        Commands\DailyIssueTrackingSummary::class,
        Commands\WeeklyIssueTrackingSummary::class,
        Commands\GetEmailTasks::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('project:progress')->saturdays()->at('14:00');
        $schedule->command('project:dueissues')->dailyAt('17:00');
        $schedule->command('project:issuesduetoday')->dailyAt('05:00');
        $schedule->command('project:issuesdueweekly')->mondays()->at('05:00');
        $schedule->command('project:migrateprojects');
        $schedule->command('users:fetch')->dailyAt('17:00');
        $schedule->command('departments:fetch')->dailyAt('16:55');
        $schedule->command('task:reminder')->everyMinute();
        $schedule->command('task:repetitivereminder')->dailyAt('00:15');
        $schedule->command('task:repetitivereminder')->dailyAt('12:15');
        $schedule->command('task:tasksduetoday')->dailyAt('06:00');
        $schedule->command('task:tasksduethisweek')->mondays()->at('06:00');
        $schedule->command('task:duetasks')->dailyAt('17:00');
        $schedule->command('project:issuesummary')->saturdays()->at('06:00');
        $schedule->command('project:weeklyreminder')->fridays()->at('15.00');
        $schedule->command('task:weeklytrackingsummary')->saturdays()->at('17:00');
        $schedule->command('issue:weeklytrackingsummary')->saturdays()->at('17:00');
//        $schedule->command('task:fromemail')->everyMinute();
        $schedule->command('issue:dailytrackingsummary')->dailyAt('17:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
