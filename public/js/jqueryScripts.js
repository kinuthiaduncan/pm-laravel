$(function () {
    // today = new Date();

    d = new Date();

    month = d.getMonth()+1;
    day = d.getDate();

    output = d.getFullYear() + '-' +
        ((''+month).length<2 ? '0' : '') + month + '-' +
        ((''+day).length<2 ? '0' : '') + day;

    // today.setHours(0,0,0);
    //
    // todayNow = new Date();

    //Date and time
    $('.datetime').fdatepicker({
        format: 'yyyy-mm-dd hh:ii',
        disableDblClickSelection: true,
        pickTime: true,
        // onRender: function (date) {
        //     return date.valueOf() < today.valueOf() ? 'disabled' : '';
        // }
    });

    $('.time').fdatepicker({
        format: 'hh:ii',
        disableDblClickSelection: true,
        pickTime: true,
        pickDate: false,
    });

    //Time picker
    let issue_start = $('#issue_start').data("issue-id");
    let issue_stop = $('#issue_stop').data("issue-id");

    $('.timepicker').wickedpicker({
        twentyFour: true,
        title: 'Select Time',
        timeSeparator: ':'
    });

    let start = $('#start').data("field-id");
   let stop = $('#stop').data("field-id");

    $('.timepicker2').wickedpicker({
        twentyFour: true,
        title: 'Select Time',
        timeSeparator: ':',
        showSeconds: true,
        now:start
    });

    $('.timepicker3').wickedpicker({
        twentyFour: true,
        title: 'Select Time',
        timeSeparator: ':',
        showSeconds: true,
        now:stop
    });


    $(document).on("click", "#edit", function() {
        let data = $(this).data("rowid");
        $('#id').val(data.id);
        $('#title').val(data.title);
        $('#description').val(data.description);
        $('#percentage').val(data.percentage_done);
    });

    $('.startTimepicker').wickedpicker({
        twentyFour: true,
        title: 'Select Time',
        timeSeparator: ':',
        now:issue_start
    });
    $('.stopTimepicker').wickedpicker({
        twentyFour: true,
        title: 'Select Time',
        timeSeparator: ':',
        now:issue_stop
    });
    //Date only
    $('.date').fdatepicker({
        format: 'yyyy-mm-dd',
        disableDblClickSelection: false
        // onRender: function (date) {
        //     return date.valueOf() < today.valueOf() ? 'disabled' : '';
        // }
    });
    $('.date').css({'border': '0','color': 'black','font-size': '1em','background-color': 'white','background-image': 'none'});
    $('.date').attr("placeholder", "📅 Set Date");//Date only

    $('.filterdate').fdatepicker({
        format: 'yyyy-mm-dd',
        disableDblClickSelection: false
    });


    $('.due-date').fdatepicker({
        format: 'yyyy-mm-dd',
        disableDblClickSelection: true
        // onRender: function (date) {
        //     return date.valueOf() < today.valueOf() ? 'disabled' : '';
        // }
    });
    $('.due-date').css({'border': '0','color': 'black','font-size': '1em','background-color': 'white','background-image': 'none'});
    $('.due-date').attr("placeholder", output);

    //     .on('changeDate', function (ev) {
    //     today = new Date(ev.date);
    //     today.setHours(0,0,0);
    // }).keydown(function(event) {event.preventDefault();});

    $("#schedule").click(function(){
        $("#add_schedule").toggle();
    });

    //Date  end
    $('#endDate').fdatepicker({
        format: 'yyyy-mm-dd',
        disableDblClickSelection: true,
        onRender: function (date) {
            return date.valueOf() < today.valueOf() ? 'disabled' : '';
        }
    }).keydown(function(event) {event.preventDefault();});
});