/**
 * Created by PKiragu on 02/03/2017.
 */

tinymce.init({
    class: "tinymce",
    selector: "textarea",
    plugins: [
        "advlist autolink lists link charmap anchor",
        "searchreplace visualblocks code",
        "insertdatetime  contextmenu paste jbimages"
    ],
    toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
    "bullist numlist outdent indent ",
    relative_urls: false,
});