<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::group(['middleware' => 'guest'], function () {
        Route::get('/', [
            'uses' => 'IndexController@index'
        ]);

        Route::get('/auth/google', [
            'as' => 'google_login',
            'uses' => 'Auth\AuthController@redirectToProvider'
        ]);

        Route::get('/auth/google/callback', 'Auth\AuthController@handleProviderCallback');
    });

    Route::post('/api/post/feedback','Api\FeedbackController@postFeedback');
    Route::post('/api/post/task','Tasks\TasksController@store');

    Route::get('/users/role-summary','RolesController@exportRoles');
    /*
     * APIs
     */

    Route::group(['middleware' => 'auth', 'namespace' => 'Api', 'csrf'], function () {
        Route::get('/api/activitystream', [
            'uses' => 'ActivityStreamController@index'
        ]);

        Route::get('/api/allissues', [
            'uses' => 'IssueController@index'
        ]);

        Route::get('/api/projects', [
            'uses' => 'ProjectController@index'
        ]);

        Route::get('/api/categories', [
            'uses' => 'CategoriesController@index'
        ]);

        Route::get('/api/my-projects', [
            'uses' => 'ProjectController@myProjects'
        ]);

        Route::get('/api/myissues', [
            'uses' => 'IssueController@myIssues'
        ]);

        Route::get('/api/myopenissues', [
            'uses' => 'IssueController@myOpenIssues'
        ]);

        Route::get('/export/issues', [
            'uses' => 'IssueController@exportIssuesToExcel'
        ]);

        Route::get('/test', [
            'uses' => 'IssueController@exportIssuesToExcel'
        ]);

        Route::get('/api/issuesComments/{id}', [
            'uses' => 'CommentController@getIssueComments'
        ]);
        Route::post('/api/get/tasks', 'TaskController@getTasks');
        Route::post('/api/get/public/tasks', 'TaskController@getPublicTasks');
        Route::post('/api/get/task/{id}/comments', 'TaskController@getComments');
        Route::post('/api/get/issue/{id}/comments','IssueController@getComments');
        Route::post('/api/comments/{id}/approve','TaskController@approveComments');
        Route::get('/api/project/{project_id}/issues/{status_id?}', 'IssueController@projectIssues');
        Route::get('/api/get/task/{id}','TaskController@getTaskDetails');
        Route::get('/api/get/task-fields','TaskController@getTaskFields');
        Route::post('/api/post/task-file','TaskController@uploadTaskFile');
        Route::get('/api/issues/{issue_id}/moved/{section}', 'IssueController@updateMoved');

    });

    Route::group(['middleware' => 'auth', 'csrf'], function () {
        Route::get('/logout', function () {
            Auth::logout();
            Session::flush();

            return redirect('/');
        });

        Route::resource('/api/projects/{id}/board', 'Board\BoardController');
        Route::post('/api/issue/{id}/tracking','Board\BoardController@saveSchedule');
        Route::get('/api/getAllUsers/{query?}', [
            'uses' => 'UserController@getAllUsers'
        ]);


        Route::get('/users-boards', [
            'uses' => 'UserController@board'
        ]);

        Route::post('/user-board', [
            'as' => 'user_board',
            'uses' => 'UserController@userBoard'
        ]);

        Route::get('/map', function () {
            return view('index.map');
        });

        Route::get('/dashboard', [
            'uses' => 'DashboardController@index'
        ]);

        Route::get('/dashboard/completed',[
            'uses'=>'DashboardController@completedTasks'
        ]);

        Route::get('/dashboard/ongoing',[
            'uses'=>'DashboardController@ongoingTasks'
        ]);

        Route::get('/dashboard/pending',[
            'uses'=>'DashboardController@pendingTasks'
        ]);

        Route::get('/dashboard/assigned',[
            'uses'=>'DashboardController@assignedTasks'
        ]);

        Route::get('/dashboard/past-due',[
            'uses'=>'DashboardController@pastDueTasks'
        ]);

        Route::get('/notifications', [
            'uses' => 'IndexController@notifications'
        ]);

        Route::get('/delete/notifications', [
            'uses' => 'IndexController@deleteNotifications'
        ]);

        Route::get('/user/summary/{id}', 'Summary\SummaryController@userSummary');
        Route::get('/api/user/summary/{id}', 'Summary\SummaryController@getUserSummary');

        Route::get('api/summary', 'Summary\SummaryController@getSummary');

        Route::get('/activity-stream', [
            'uses' => 'IndexController@dashboard'
        ]);

        Route::get('/boards', [
            'uses' => 'IndexController@boards'
        ]);

        Route::resource('files', 'FileController', ['only'=>['show']]);

        /*
         * Projects Routes
         */
        Route::resource('project/{id}/phases', 'ProjectPhases\ProjectPhasesController', [
            'names' => [
                'index' => 'phase.index',
                'create' => 'phase.create',
                'store' => 'phase.store',
                'edit' => 'phase.edit',
                'update' => 'phase.update',
                'show' => 'phase.show'
            ]]);

        Route::post('/phase/summary', [
            'as' => 'project_phase.summary',
            'uses' => 'ProjectPhases\ProjectPhasesController@exportSummary'
        ]);

        Route::get('/phase/{id}/delete', [
            'as' => 'project_phase.delete',
            'uses' => 'ProjectPhases\ProjectPhasesController@delete'
        ]);

        Route::get('/phase/{id}/pdf', [
            'as' => 'project_phase.pdf',
            'uses' => 'ProjectPhases\ProjectPhasesController@exportWorkPlanPDF'
        ]);

        Route::get('/phase/{id}/excel', [
            'as' => 'project_phase.excel',
            'uses' => 'ProjectPhases\ProjectPhasesController@exportWorkPlanExcel'
        ]);

        Route::resource('phases/{id}/activity', 'ProjectPhases\PhaseActivitiesController', [
            'names' => [
                'create' => 'activity.create',
                'store' => 'activity.store',
                'edit' => 'activity.edit',
                'update' => 'activity.update',
            ]]);

        Route::get('/activity/{id}/delete', [
            'as' => 'phase_activity.delete',
            'uses' => 'ProjectPhases\PhaseActivitiesController@delete'
        ]);

        Route::get('/api/phases/new-approvals', 'Api\ProjectController@getNewApprovals');

        Route::get('/api/phases/updated-approvals', 'Api\ProjectController@getUpdatedApprovals');

        Route::post('/phase/{id}/approve/{status}','ProjectPhases\ProjectPhasesController@approveNew');

        Route::post('/updated/{id}/approve/','ProjectPhases\ProjectPhasesController@approveUpdated');

        Route::post('/phase/{id}/reject/','ProjectPhases\ProjectPhasesController@rejectNew');

        Route::post('/updated/{id}/reject/','ProjectPhases\ProjectPhasesController@rejectUpdated');

        Route::resource('/categories', 'CategoriesController', ['names' => [
            'index' => 'get_categories_index',
            'create' => 'get_categories_create',
            'update' => 'post_categories_edit',
            'postRemoveProject' => 'post_categories_remove_project'
        ], 'except' => [
            'edit', 'show', 'store'
        ]]);

        Route::resource('/phases/approvals', 'ProjectPhases\PhaseApprovalController', [
            'names' => [
                'index' => 'phase_approval.index',
                'create' => 'phase_approval.create',
                'store' => 'phase_approval.store',
                'edit' => 'phase_approval.edit',
                'update' => 'phase_approval.update',
                'show' => 'phase_approval.show'
            ]]);

        Route::resource('/project/{id}/feedback', 'Feedback\FeedbackController', [
            'names' => [
                'index' => 'feedback.index',
                'create' => 'feedback.create',
                'show'=>'feedback.show',
                'store' => 'feedback.store',
            ]]);

        Route::post('/categories/save', [
            'as' => 'projects_categories.store',
            'uses' => 'CategoriesController@store'
        ]);

        Route::post('/categories', [
            'as' => 'download_summaries',
            'uses' => 'ProjectController@downloadSummaries'
        ]);

        Route::get('/categories/edit/{id}', 'CategoriesController@edit');

        Route::get('/categories/show/{id}', 'CategoriesController@show');

        Route::post('/categories/', [
            'as' => 'post_categories_add_projects',
            'uses' => 'CategoriesController@postAddProjects'
        ]);

        Route::post('/projects', [
            'as' => 'download_summaries',
            'uses' => 'ProjectController@downloadSummaries'
        ]);

        Route::get('/projects', [
            'as' => 'projects_index',
            'uses' => 'ProjectController@index'
        ]);

        Route::get('/mypublicprojects', [
            'as' => 'my_public_projects',
            'uses' => 'ProjectController@index'
        ]);

        Route::get('/myprivateprojects', [
            'as' => 'my_private_projects',
            'uses' => 'ProjectController@myPrivateProjects'
        ]);

        Route::get('/project/public', [
            'as' => 'add_project',
            'uses' => 'ProjectController@create'
        ]);

        Route::get('/project/{id}/edit', [
            'uses' => 'ProjectController@edit'
        ]);

        Route::resource('/project/{id}/progress', 'ProjectProgressController', ['names' => [
            'store' => 'store_project_progress',
            'update' => 'update_project_progress'
        ]]);

        Route::get('/project/{id}', [
            'uses' => 'ProjectController@showProject'
        ]);

        Route::get('/project/{id}/board/{component?}', [
            'as' => 'project_board',
            'uses' => 'ProjectController@projectBoard'
        ]);

        Route::post('/project/store', [
            'as' => 'create_project',
            'uses' => 'ProjectController@store'
        ]);

        Route::post('/project/edit', [
            'as' => 'edit_project',
            'uses' => 'ProjectController@update'
        ]);

        Route::get('/project/create', [
            'uses' => 'ProjectController@create'
        ]);

        Route::post('/project/store', [
            'as' => 'create_project',
            'uses' => 'ProjectController@store'
        ]);

        Route::get('/project/{id}/watch', [
            'uses' => 'ProjectController@watchProject'
        ]);

        Route::get('/project/{id}/members/add', [
            'uses' => 'ProjectController@addMembers'
        ]);

        Route::post('/project/members/store', [
            'as' => 'add_members',
            'uses' => 'ProjectController@storeMembers'
        ]);

        Route::get('/project/{project_id}/member/{id}/remove', [
            'uses' => 'ProjectController@removeMember'
        ]);


        /*
        * Components Routes
        */

        Route::post('/project/{id}', [
            'as' => 'post_component',
            'uses' => 'ComponentsController@store'
        ]);

        Route::get('/api/deleteComponentUser/component/{componentId}/user/{userId}', [
            'uses' => 'ComponentsController@deleteComponentUser'
        ]);

        /*
        * Milestones Routes
        */

        Route::get('/project/{id}/milestone/create', [
            'uses' => 'MilestoneController@createMilestone'
        ]);

        Route::post('/project/milestone/store', [
            'as' => 'create_milestone',
            'uses' => 'MilestoneController@milestoneStore'
        ]);

        Route::get('/edit/project/{project_id}/milestone/{id}', [
            'uses' => 'MilestoneController@editMilestone'
        ]);

        Route::post('/update/project/milestone', [
            'as' => 'update_milestone',
            'uses' => 'MilestoneController@milestoneUpdate'
        ]);

        Route::post('/api/get/projects/individual',[
            'uses' => 'Api\ProjectController@getIndividualProjects'
        ]);

        Route::post('/api/get/projects/private',[
            'uses' => 'Api\ProjectController@getPrivateProjects'
        ]);


        /*
         * Issues Routes
         */

        Route::get('/issue/create', [
            'uses' => 'IssueController@createIssue'
        ]);

        Route::get('/project/{id}/issue/create/{component}', [
            'as' => 'add_issue',
            'uses' => 'IssueController@create'
        ]);

        Route::post('/issue/store/{id?}', [
            'as' => 'create_issue',
            'uses' => 'IssueController@store'
        ]);

        Route::get('/{project_id}/issue/{id}/edit', [
            'uses' => 'IssueController@edit'
        ]);

        Route::get('/{project_id}/issue/{id}/board/edit', [
            'uses' => 'IssueController@boardEdit'
        ]);

        Route::post('/issue/{id}/update', [
            'as' => 'edit_issue',
            'uses' => 'IssueController@update'
        ]);

        Route::get('issue/{id}', [
            'uses' => 'IssueController@showIssue'
        ]);

        Route::get('issues', [
            'uses' => 'IssueController@showAllIssues'
        ]);

        Route::get('my-issues', [
            'uses' => 'IssueController@showMyIssues'
        ]);

        Route::get('my-issues', [
            'uses' => 'IssueController@showMyIssues'
        ]);

        Route::get('my-open-issues', [
            'uses' => 'IssueController@showMyOpenIssues'
        ]);

        /*
        * Issue tracking routes
        */
        Route::resource('/issue/{id?}/track', 'IssueTrackingController', [
            'names' => [
                'index' => 'issue_track.index',
                'create' => 'issue_track.create',
                'store' => 'issue_track.store',
                'edit' => 'issue_track.edit',
                'update' => 'issue_track.update',
            ]]);

        Route::get('/issue-track/{id}/delete',[
            'as'=>'delete_issue_schedule',
            'uses'=>'IssueTrackingController@delete'
        ]);


        /*
        * Comments Routes
        */

        Route::get('/issue/{id}/comment/create', [
            'uses' => 'CommentController@create'
        ]);

        Route::post('/comment/store', [
            'as' => 'comment_issue',
            'uses' => 'CommentController@store'
        ]);

        Route::get('/comment/{id}/edit', [
            'uses' => 'CommentController@edit'
        ]);

        Route::post('/comment/update', [
            'as' => 'comment_update',
            'uses' => 'CommentController@update'
        ]);

        /*
         * ELASTIC SEARCH SITE INDEX ROUTES
         */
        Route::get('/buildindex/new', [
            'as' => 'buildindex.new',
            'uses' => 'BuildIndexesController@newIndex'
        ]);

        Route::get('/buildindex/issues', [
            'as' => 'buildindex.issues',
            'uses' => 'BuildIndexesController@issues'
        ]);

        Route::get('/buildindex/projects', [
            'as' => 'buildindex.projects',
            'uses' => 'BuildIndexesController@projects'
        ]);
        Route::post('/tasks/timeline/search',[
            'as'=>'timelines.search',
            'uses'=>'Tasks\TaskTimelineController@search'
        ]);
        Route::get('/export/projects', 'ExportController@tableProjectsExport');
        Route::get('/export/users', 'ExportController@tableUsersExport');
        Route::get('/export/status', 'ExportController@tableStatusExport');
        Route::get('/export/roles', 'ExportController@tableRolesExport');
        Route::get('/export/project-users', 'ExportController@tableProjectUsersExport');
        Route::get('/export/project-progress', 'ExportController@tableProjectProgressExport');
        Route::get('/export/project-milestones', 'ExportController@tableProjectMilestonesExport');
        Route::get('/export/project-components', 'ExportController@tableProjectComponentsExport');
        Route::get('/export/project-types', 'ExportController@tableProjectTypesExport');
        Route::get('/export/priorities', 'ExportController@tablePrioritiesExport');
        Route::get('/export/issues', 'ExportController@tableIssuesExport');
        Route::get('/export/issue-types', 'ExportController@tableIssueTypesExport');
        Route::get('/export/groups', 'ExportController@tableGroupsExport');
        Route::get('/export/comments', 'ExportController@tableCommentsExport');
        Route::get('/export/activity-stream', 'ExportController@tableActivityStreamExport');
        Route::get('/api/users', [
            'uses' => 'Api\UserController@getAllUsers'
        ]);
        Route::get('/api/fetch/users', [
            'uses' => 'Api\UserController@allUsersSelect'
        ]);
        Route::get('/api/get/complete-tasks/{id}', [
            'uses'=>'Api\DashboardController@getCompletedTasks'
        ]);
        Route::get('/api/get/ongoing-tasks/{id}', [
            'uses'=>'Api\DashboardController@getOngoingTasks'
        ]);
        Route::get('/api/get/pending-tasks/{id}', [
            'uses'=>'Api\DashboardController@getPendingTasks'
        ]);
        Route::resource('daily-reports','Reports\ReportsController');
        Route::resource('report-items','Reports\ItemsController', ['names' => [
            'index' => 'report_items.index']]);
        Route::resource('roles','RolesController');
        Route::resource('authorization','AuthorizationController');
        Route::resource('permissions','PermissionsController');
        Route::get('/delete/{id}','PermissionsController@deleteUserPermission');
        Route::post('permissions-assign', 'PermissionsController@assign');
        Route::get('daily-personal-summary', 'Reports\ReportsController@PersonalDailySummary');
        Route::get('weekly-personal-summary', 'Reports\ReportsController@PersonalWeeklySummary');
        Route::get('task-filter-report', 'Reports\ReportsController@exportTasks');
        Route::get('personal-task-report', 'Reports\ReportsController@exportPersonalTasks');
        Route::get('departmental-tasks', 'Reports\ReportsController@departmentHeadReport');
        Route::get('api/global/search', 'Api\GlobalSearchController@search');
        Route::get('/daily-report-export/{id}/{date}', 'Reports\ReportsController@exportDailyReport');
        Route::get('/daily-report-send/{id}/{date}', 'Reports\ReportsController@sendDailyReport');
        Route::get('export-summary/{id?}', 'Summary\SummaryController@exportSummary');
        Route::get('export-summary', 'Summary\SummaryController@exportSummary');

        Route::get('export-daily-summary/{id?}', 'Summary\SummaryController@userDailySummary');
        Route::get('export-weekly-summary/{id?}','Summary\SummaryController@userWeeklySummary');

        Route::post('/export-issues/{id}','Board\BoardController@export');
        Route::post('/api/get/filter/reports', 'Api\ReportController@getReports');
        Route::post('/api/get/filter/task-categories','Api\TaskCategoriesController@getCategories');
        Route::get('sub-items-delete/{id}','Reports\SubItemsController@delete');
        Route::get('delete-report/{id}','Reports\ReportsController@delete');
        Route::resource('sub-items','Reports\SubItemsController');
        Route::get('/api/items/{id}', 'Reports\ReportsController@getItems');
        Route::get('/view-reports/{id}/{date}','Reports\ReportsController@showReport');
        Route::get('api/get-sub-items','Api\ReportController@getSubItems');
        Route::get('api/get-items','Api\ReportController@getItems');
        Route::get('/api/get/task-subcategories', 'Api\TaskController@getSubCategories');
        Route::get('api/get-reports','Api\ReportController@getDailyReports');
        Route::get('/project/{id}/activate','ProjectController@activateProject');
        Route::get('/project/{id}/deactivate','ProjectController@deactivateProject');
        Route::get('/feedback/{id}/delete','Feedback\FeedbackController@delete');
        Route::get('/feedback/{id}/project/{project}/issue/{component}','Feedback\FeedbackController@assignIssue');
        Route::get('/tasks/tasks_categories/{id}/delete','Tasks\TaskCategoriesController@delete');
        Route::get('/delete-sub-categories/{id}','Tasks\TaskSubCategoriesController@delete');
        Route::get('/task/{id}/track/{track}','Tasks\TasksTimeTrackingController@delete');
        Route::post('/task/{id}/done/{status}','Tasks\TasksController@markTaskDone');
    });


    /*
    * Tasks Routes
    */
    Route::group(['middleware' => 'auth', 'namespace' => 'Tasks', 'prefix' => 'tasks', 'csrf'], function () {

        Route::resource('/my-tasks', 'TasksController', [
            'names' => [
                'index' => 'task.index',
                'create' => 'task.create',
                'store' => 'task.store',
                'edit' => 'task.edit',
                'update' => 'task.update',
                'show' => 'task.show'
            ]]);

        Route::resource('/task/{id}/track', 'TasksTimeTrackingController', [
            'names' => [
                'index' => 'task_track.index',
                'create' => 'task_track.create',
                'store' => 'task_track.store',
                'edit' => 'task_track.edit',
                'update' => 'task_track.update',
                'show' => 'task_track.show'
            ]]);

        Route::resource('/timelines', 'TaskTimelineController', [
            'names' => [
                'index' => 'task_timeline.index',
                'create' => 'task_timeline.create',
                'store' => 'task_timeline.store',
                'edit' => 'task_timeline.edit',
                'update' => 'task_timeline.update',
                'show' => 'task_timeline.show'
            ]]);

        Route::resource('/tasks_categories', 'TaskCategoriesController', [
            'names' => [
                'index' => 'task_category.index',
                'create' => 'task_category.create',
                'store' => 'task_category.store',
                'edit' => 'task_category.edit',
                'update' => 'task_category.update'
            ]]);

        Route::resource('/tasks_categories/{id}/subcategories','TaskSubCategoriesController',[
            'names'=>[
                'index'=>'task_subcategories.index',
                'create'=>'task_subcategories.create',
                'store'=>'task_subcategories.store',
                'edit'=>'task_subcategories.edit',
                'update'=>'task_subcategories.update'
            ]]);

        Route::resource('/task/{id}/progress', 'TaskProgressController', [
            'names' => [
                'index' => 'task_progress.index',
                'store' => 'task_progress.store',
            ], 'except' => [
               'update'
            ]]);

        Route::post('/task/{id}/progress/update',[
            'as' => 'update_progress',
            'uses'=>'TaskProgressController@update'
        ]);

        Route::resource('/task/{id}/reminder', 'TaskReminderController', [
            'names' => [
                'index' => 'task_reminder.index',
                'store' => 'task_reminder.store',
                'update' => 'task_reminder.update'
            ]]);

        Route::resource('/task/{id}/comments', 'TaskCommentsController', [
            'names' => [
                'store' => 'task_comment.store',
                'update' => 'task_comment.update'
            ]]);

        Route::resource('/public', 'DepartmentalTasksController', [
            'names' => [
                'index' => 'public_tasks.index',
                'store' => 'public_tasks.store',
                'update' => 'public_tasks.update'
            ]]);

        Route::resource('/my-task-groups', 'TaskGroupsController', [
            'names' => [
                'index' => 'task_group.index',
                'create' => 'task_group.create',
                'store' => 'task_group.store',
                'edit' => 'task_group.edit',
                'update' => 'task_group.update',
                'show' => 'task_group.show',
                'destroy' => 'task_group.destroy'
            ]]);
    });

    /*
     * User Management Routes
     */
    Route::group(['middleware' => 'auth', 'namespace' => 'Users', 'csrf'], function () {
        Route::resource('/users', 'UserController', [
            'names' => [
                'index' => 'users.index',
            ]
        ]);

        Route::get('api/follow/user/{id}', 'FollowsController@followUser');
        Route::get('api/un-follow/user/{id}', 'FollowsController@unFollowUser');

        Route::get('tasks/my-tasks/{id}/follow', [
            'as' => 'follow_task',
            'uses' => 'FollowsController@followTask'
        ]);

        Route::get('tasks/my-tasks/{id}/un-follow', [
            'as' => 'un_follow_task',
            'uses' => 'FollowsController@UnFollowTask'
        ]);

        Route::get('/api/tasks/followed', 'FollowsController@followedTasks');
    });

