@extends('layouts.default')
@section('content')
    <div class="small-12 column">
        <h4 class="page-title">Report Items

            <a class="button pull-right" href="{{url('daily-reports/create')}}" style="margin-right: 15px;">
                Add Daily Report</a>
        </h4>
    </div>
    <div class="project-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Add an Item</h3>
    {{ Form::open(['url' => 'report-items']) }}
    @include('layouts.partials.errors')

    {{ Form::label('item','Item Name') }}
    {{ Form::text('item',NULL,['required']) }}
    {{ Form::label('department_id','Department') }}
    {!! Form::select('department_id', $departments,'' ,['required'=>'required'])!!}

    {{ Form::submit('Add Item', ['class' => 'button button-success'])}}

    {{ Form::close() }}
                </div>
            </div>


        </div>
    </div>
    @stop