@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="small-7 large-8 columns">
            <h5 class="perm-title">Sub Items under Item: <b>{{ $item->item }}</b></h5>
        </div>
        <div class="small-2 large-2 column">
            <a href="{{url('report-items')}}"  class="button pull-right">Back</a>
        </div>
        <div class="small-3 large-2 column">
            <a href="" data-open="createSubItemModal{{ $item->id }}" class="button pull-right">Add Sub Item</a>
        </div>
        <div id="createSubItemModal{{ $item->id }}" class="reveal pm-modal" data-reveal style="padding: 20px;">
            <div class="row">
                <div class="small-12 large-12 columns">
                    {{ Form::open(['url' => 'sub-items']) }}
                    @include('layouts.partials.errors')
                    {{ Form::label('name','Sub Item Name') }}
                    {{ Form::hidden('report_item_id',$item->id) }}
                    {{ Form::text('name',NULL,['required']) }}
                    <div class="tinymce-label">
                        {!! Form::label('description','Description') !!}
                        <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                        ='content'></tinymce>
                        <br>
                    </div>
                    {{ Form::submit('Add Sub Item', ['class' => 'button button-success'])}}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="small-12 large-12 column">
            <table class="responsive table">
                <thead>
                <th>Sub Item Name</th>
                <th>Description</th>
                <th></th>
                </thead>
                <tbody>
                    @foreach($subItems as $subItem)
                        <tr>
                            <td>{!! $subItem->name !!}</td>
                            <td>{!! $subItem->description !!}</td>
                            <td><a href="{{url('sub-items-delete/'.$subItem->id)}}"><i class="fa fa-trash-o pull-left" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop