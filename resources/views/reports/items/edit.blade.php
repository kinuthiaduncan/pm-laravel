@extends('layouts.default')
@section('content')

    <div class="project-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Edit Item</h3>
                    {{ Form::open(['url' => '/report-items/'.$item->id,'method' => 'PUT']) }}
                    @include('layouts.partials.errors')

                    {{ Form::label('item','Item Name') }}
                    {{ Form::text('item',$item->item,['required']) }}
                    {{ Form::label('department_id','Department') }}
                    {!! Form::select('department_id', $departments,'$item->department->name' ,['required'=>'required'])!!}

                    {{ Form::submit('Add Item', ['class' => 'button button-success'])}}

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop