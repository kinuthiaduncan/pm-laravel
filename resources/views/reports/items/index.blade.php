@extends('layouts.default')
@section('content')
    <div class="small-12 column" style="margin-top: 3%;">
        <h4 class="page-title">All Items
            <a href="{{url('report-items/create')}}" class="button pull-right">Add
                Report Items</a>
            <a href="{{url('daily-reports')}}" class="button pull-right">Back</a>
        </h4>
    </div>
    <div class="row">
        <div class="small-12 large-12 column">
            <table class="responsive table">
                <thead>
                <tr>
                    <th>Item Name</th>
                    <th>Department</th>
                    <th>Created By</th>
                    <th>Edit</th>
                    <th>Sub Items</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dailyItems as $dailyItem)
                    <tr>
                        <td>{!! $dailyItem->item !!}</td>
                        <td>{!! $dailyItem->department->name !!}</td>
                        <td>{!! $dailyItem->user->preferred_name !!}</td>
                        <td><a href="{{url('/report-items/'.$dailyItem->id.'/edit')}}">Edit</a></td>
                        <td><a href="{{url('/sub-items/'.$dailyItem->id)}}">SubItems</a></td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot style="background-color: #ffffff;border:none;box-shadow: none">
                <tr><td> {{ $dailyItems->links() }}</td></tr>
                </tfoot>
            </table>
        </div>
    </div>
    @stop