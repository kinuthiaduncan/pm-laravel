@extends('layouts.pdf')

@section('content')
<h3 class="title">DAILY REPORT</h3>

<table class="table">
    <thead class="summary">
    <tr>
        <th>Item Name</th><th>Sub Item</th><th>Description</th><th>Department</th><th>Date</th><th>Created By</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $d)
        <tr>
        <td>{{$d->item->item}}</td>
        <td>
            @if($d->subitem != null)
            {{$d->subitem->name}}
            @else
                N/A
            @endif
        </td>
        <td>{!!$d->description !!}</td>
        <td>{{$d->department->name}}</td>
        <td>{{$d->date}}</td>
        <td>{{$d->creator->preferred_name}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection
