@extends('layouts.default')
@section('content')
    <div class="row" style="padding-left: 10px; padding-right: 10px">
            {{ Form::open(['url' => 'daily-reports','class'=>'daily-form','novalidate']) }}
            @include('layouts.partials.errors')
            <div class="small-12 columns">
                {{ Form::label('department_id','Select Department') }}
                {!! Form::select('department_id', $departments ,'',['placeholder'=>'Select Department','required'=>'true','v-model'=>'department_id','@change' => 'itemsPicker()'])!!}
            </div>
                <i  v-show="loading" class="fa fa-spinner fa-spin" style="margin-top: 0;"></i>
            <div v-if="department_id != '' && department_items == 0">
                <p style="color:red"><b>The Department you have selected does not have report items.<a href="{{url('report-items')}}"> Add items</a></b> </p>
             </div>
            <div class="small-12 columns">
                {{ Form::label('date','Date') }}
                {{Form::text('date',\Carbon\Carbon::today()->format('Y-m-d'),['required'=>'required','class'=>'due-date'])}}
            </div>
            <div class="small-12 large-6 columns">
                {{ Form::label('item_id', 'Select Item') }}
                <select name="item_id" v-model="item" @change="subItemsPicker()" required>
                    <option v-for="department_item, index in department_items" :value="index">
                        @{{ department_item }}
                    </option>
                </select>
            </div>
            <div v-if="subItems != 0" class="small-12 large-6 columns">
                {{ Form::label('subItem_id', 'Select Sub Item') }}
                <select name="subItem">
                    <option>Select a sub item</option>
                    <option v-for="subItem, index in subItems" :value="index">
                        @{{ subItem }}
                    </option>
                </select>
            </div>
            <div class="small-12 columns">
                <div class="tinymce-label">
                    {{ Form::label('description', 'Description') }}
                    <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                    ='content'></tinymce>
                    <br>
                </div>
                <br />
            </div>
            <div class="small-12 columns">
                {{ Form::submit('Add Daily Report', ['class' => 'submit-task button button-success'])}}
            </div>
            {{ Form::close() }}
            <br/>

                    <table v-for="daily_report in daily_reports" class="table progress-table">
                        <tr class="report-header"><th colspan="3">@{{daily_report.item}}</th></tr>
                        <tr>
                            <th>Sub Item</th>
                            <th>Date</th>
                            <th>Description</th>
                        </tr>
                        <tr>
                            <td>@{{daily_report.subitem}}</td>
                            <td>@{{daily_report.date}}</td>
                            <td>@{{ daily_report.description }} </td>
                        </tr>
                    </table>
    </div>

@stop
