@extends('layouts.default')
@section('content')
<div class="row" style="padding-left: 10px;">
    @if($items->count()==0)
        <p>You do not have any report items for the department. Please add them first</p>
    @else
        {{ Form::open(['url' => 'daily-reports','class'=>'daily-form','novalidate']) }}
        @include('layouts.partials.errors')
            {{Form::hidden('department_id',$department,['required'])}}
            {{ Form::label('date','Date') }}
            {{Form::text('date','',['required'=>'required','class'=>'due-date'])}}

            @foreach($items as $item)
                 {{ Form::label($item->id, $item->item) }}
                    <div style="width: 80%;">
                        {{Form::textarea($item->id,null,['class'=>'', 'rows' => 2, 'cols' => 4,'required'=>'required'])}}
                    </div>
            @endforeach
                    <br />
                     {{ Form::submit('Add Daily Report', ['class' => 'submit-task button button-success'])}}
                    {{ Form::close() }}
    @endif
    </div>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        plugins: [
            "advlist autolink lists link charmap anchor",
            "searchreplace visualblocks code",
            "insertdatetime  contextmenu paste jbimages"
        ],
        toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
        "bullist numlist outdent indent "
    });
</script>
@stop
