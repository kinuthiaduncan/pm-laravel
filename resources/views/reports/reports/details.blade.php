@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="small-12 column">
            <h4 class="page-title"> Report for <b>{{$department->name}}</b> for date: <b>{{$date}}</b>
                <a class="button pull-right" href="{{url('daily-reports')}}" style="margin-right: 15px;">
                    Back</a>
            </h4>
        </div>
        <div class="small-12 large-2 columns pull-right">
            <a href="{{url('/daily-report-export/'.$department->id.'/'.$date)}}" class="button">Export Report</a>
        </div>
        <div class="small-12 large-2 columns pull-right">
            <a href="{{url('/daily-report-send/'.$department->id.'/'.$date)}}" class="button">Send Report</a>
        </div>
        <div class="large-12 small-12 columns" style="float: left; padding: 10px;">
            <table class="table responsive">
                <thead class="details-header">
                <th>Item</th>
                <th>Sub Item</th>
                <th>Report Description</th>
                <th>Created By</th>
                <th>Action</th>
                </thead>
                <tbody>

                @foreach($report as $r)
                    <tr>
                        <td>{{$r->item->item}}</td>
                        <td>
                            @if($r->subitem != null)
                                {{$r->subitem->name}}
                            @endif
                        </td>
                        <td>{!! $r->description !!}</td>
                        <td>{{$r->creator->preferred_name}}</td>
                        <td>
                        <a href="{{url('delete-report/'.$r->id)}}"><i class="fa fa-trash-o edit-item" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop