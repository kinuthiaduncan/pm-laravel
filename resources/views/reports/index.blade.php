@extends('layouts.default')
@section('content')

    <div style="padding: 20px">
        <div class="content-section-header row">
            <div class="small-6 large-6  column add-item">
                <span> <a href="{{url('report-items')}}" class="button pull-left margin-left-10">
                Report Items</a> </span>
            </div>
            <div class="small-6 large-6  column">
                <span> <a href="{{url('daily-reports/create/')}}" class="button pull-right margin-left-10">Add Report</a> </span>
            </div>
        </div>
        <div class="row">
                <div class="small-12 large-5 columns">
                {{Form::text('date',null,['class'=>'filterdate department-filter','placeholder'=>'Select Date',
                            'v-model'=>'date', 'v-on:select'=>'getReports()'])}}
                </div>
                <div class="small12 large-5 columns">
                    {!! Form::select('department_id', $departments ,'',['placeholder'=>'Select Department','class'=>'department-filter','v-model'=>'department', 'v-on:change'=>'getReports()'])!!}
                </div>
            </div>

            <reports-table :departments="{{json_encode($departments)}}"></reports-table>
        </div>
    </div>

@stop