<div class="mynav">
<ul class="vertical menu" data-accordion-menu>
    <li class="{{ Request::is('dashboard*') ? 'current' : '' }}">
        <a href="/dashboard">
            Dashboard <i class="fa fa-home pull-left" aria-hidden="true"></i>
        </a>
    </li>
    <li>
        <a href="/projects">
            Projects <i class="fa fa-briefcase pull-left" aria-hidden="true"></i>
        </a>
        <ul class="menu vertical nested"  v-cloak>
            <li class="{{ Request::is('project*') ? 'current' : '' }} {{ Request::is('mypublicprojects*') ? 'current' : '' }}"><a href="/projects">All Projects</a></li>
            <li class="{{ Request::is('categories*') ? 'current' : '' }}"><a href="/categories">Project Categories</a></li>
             </ul>
    </li>

    <li>
        <a href="/tasks/my-tasks">
            Tasks <i class="fa fa-area-chart pull-left" aria-hidden="true"></i>
        </a>
        <ul class="menu vertical nested"  v-cloak>
            <li class="{{ Request::is('tasks/my-tasks*') ? 'current' : '' }}"><a href="{{route('task.index')}}">All Tasks</a></li>
            <li class="{{ Request::is('tasks/tasks_categories*') ? 'current' : '' }}"><a href="{{route('task_category.index')}}">Task Categories</a></li>
            <li class="{{ Request::is('tasks/my-task-groups*') ? 'current' : '' }}"><a href="{{route('task_group.index')}}">My Task Groups</a></li>
            <li class="{{ Request::is('tasks/timelines*') ? 'current' : '' }}"><a href="{{route('task_timeline.index')}}">Timelines</a></li>

        </ul>
    </li>
    {{--<li class="{{ Request::is('daily_reports*') ? 'current' : '' }}">--}}
        {{--<a href="{{url('daily-reports')}}">Daily Reports <i class="fa fa-file pull-left" aria-hidden="true"></i></a>--}}
    {{--</li>--}}
    <li class="{{ Request::is('user*') ? 'current' : '' }}">
        <a href="{{route('users.index')}}">
            Users <i class="fa fa-users pull-left" aria-hidden="true"></i>
        </a>
    </li>
    <li class="{{ Request::is('feedback*') ? 'current' : '' }}">
        <a style="color: #EA9E00" href="{{route('feedback.create',12)}}"><i class="fa fa-comments" aria-hidden="true"></i> Feedback</a></li>

    {{--@if(Auth::user()->can('access_user_permissions') || Auth::user()->role_id == 1)--}}
    {{--<li class="{{ Request::is('authorization*') ? 'current' : '' }}">--}}
        {{--<a href="{{url('authorization')}}">Authorization <i class="fa fa-shield pull-left" aria-hidden="true"></i></a>--}}
    {{--</li>--}}
    {{--@endif--}}
    {{--<li class="{{ Request::is('activity*') ? 'current' : '' }}">--}}
        {{--<a href="/activity-stream">--}}
            {{--Activity Stream <i class="fa fa-line-chart pull-left" aria-hidden="true"></i>--}}
        {{--</a>--}}
    {{--</li>--}}
</ul>
</div>
