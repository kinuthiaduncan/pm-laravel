@if ($errors->any())
    <div style=" color: red; list-style-type: none">
        <div class="callout alert form-errors">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    </div>
@endif