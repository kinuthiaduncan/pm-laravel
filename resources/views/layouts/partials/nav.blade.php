<div class="contain-to-grid" data-sticky-container>
    <div class="top-bar" data-options="marginTop:0;" style="width:100%">

        <div class="top-bar-title">
            <ul class="menu">
                <li style="padding-right: 8px;">
                    <a data-toggle="mobile-nav" class="show-for-small-only  menu-icon dark mobile-menu-btn"
                       type="button">
                    </a>
                </li>
                <li class="logo"><a id="logo" href="/"><img src="/site_images/cytonn-logo.jpg"></a></li>
            </ul>
        </div>

        <div id="responsive-menu" class="top-bar-section">
            <div class="top-bar-left my-title">
                <ul class="menu">
                    <li>
                        <span class="project-title">Project Management</span>
                    </li>
                </ul>
            </div>

            <div class="top-bar-right row" v-cloak>
                <div class="small-12 large-12 column" style="padding-right: 0">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li>
                            <a href="#">
                                <input class= "global-search" type="text" placeholder="Search" v-model="search_input" @keyup="globalSearch()">
                            </a>
                            <ul class="menu vertical notifications">
                                <li class="search-item" v-for="result in global_search_results">
                                    <a :href="result.url">
                                        <h6>@{{ result.title}}</h6>
                                        <p>@{{ result.details}}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="/notifications" id="fa-reminder">
                                <i class="fa fa-bell" aria-hidden="true"></i>
                                @if (count(Auth::user()->unreadNotifications) != 0)
                                    <sup></sup>
                                @endif
                            </a>
                            <ul class="menu vertical notifications">
                                @forelse(Auth::user()->unreadNotifications()->paginate(5) as $notification)
                                    <li class="notifications-item">
                                        <a href="{{$notification->data['url']}}">
                                            <h6>
                                                @if(array_key_exists('subject', $notification->data))
                                                {{$notification->data['subject']}}
                                                    @else
                                                    {{snake_case(class_basename($notification->type))}}
                                                @endif
                                                ~ {{$notification->data['title']}}
                                            </h6>
                                            <p>{{$notification->created_at->toDayDateTimeString()}}</p>
                                        </a>
                                    </li>
                                @empty
                                    <li class="notifications-item">You have no new notifications</li>
                                @endforelse
                                <li><a class="more-notifications" href="/notifications">View All Notifications</a></li>
                            </ul>
                        </li>
                        <li class="hide-for-small-only">
                            <a href="#"
                               class="user-name">{!! Auth::user()->preferred_name !!}</a>
                            <ul class="menu vertical">
                                <li><a id="logout" href="/logout">Logout</a></li>
                            </ul>
                        </li>
                        <li>
                            @if (Auth::user()->avatar == null)
                                <i class="fa fa-user-circle" aria-hidden="true"></i>
                            @else
                                <img src="{{Auth::user()->avatar}}"
                                     class="profile-pic">
                            @endif
                            <ul class="menu vertical show-for-small-only">
                                <li><a id="logout" href="{{route('feedback.create',12)}}">Feedback</a></li>
                                <li><a id="logout" href="/logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>