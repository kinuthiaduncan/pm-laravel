@if (Session::has('flash_notification.message'))
    {{--<div data-closable class="callout  success {{ Session::get('flash_notification.level') }}">--}}
    <div class="callout success {{ Session::get('flash_notification.level') }}" data-closable>
        {{ Session::get('flash_notification.message') }}
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif