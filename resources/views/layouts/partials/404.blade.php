<html>
<head>
    <link rel="shortcut icon" href="/site_images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/assets/bower/foundation-sites/dist/css/foundation.min.css"/>
    <link rel="stylesheet" href="/assets/bower/ng-tags-input/ng-tags-input.min.css"/>
    <link rel="stylesheet" href="{{mix('/css/app.css')}}"/>
    <link rel="stylesheet" href="/assets/foundation-icons/foundation-icons.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/assets/bower/font-awesome/css/font-awesome.min.css">

</head>
<body>
<div class="contain-to-grid" data-sticky-container >
    <div class="top-bar" data-options="marginTop:0;" style="width:100%">

        <div class="top-bar-title">
            <ul class="menu">
                <li style="padding-right: 8px;">
                    <a data-toggle="mobile-nav" class="show-for-small-only  menu-icon dark mobile-menu-btn"
                       type="button">
                    </a>
                </li>
                <li class="logo">
                    <a id="logo" href="/">
                        <img src="/site_images/cytonn-logo.jpg">
                    </a>
                </li>
            </ul>
        </div>

        <div id="responsive-menu" class="top-bar-section">
            <div class="top-bar-left my-title">
                <ul class="menu">
                    <li>
                        <span class="project-title">Project Management</span>
                    </li>
                </ul>
            </div>

            <div class="top-bar-right row">
                <div class="small-12 large-12 column" style="padding-right: 0">
                    <ul class="dropdown menu" data-dropdown-menu>

                        <li class="hide-for-small-only">
                            <a href="/"
                               class="user-name">Dashboard</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row myContent">
        <div class="small-12 large-12 columns">
            <h2 class="title">Oops!!</h2>
            <p class="details">We can't seem to find the page you're looking for.</p>
            <p class="details">You may have mistyped the address, or the page may have moved.</p>
        <div class="small-12 large-12 columns">
            <br />
            <p><a style="color:#82CA9C" href="/">Back to the Dashboard <i class="fa fa-arrow-right" aria-hidden="true"></i></a></p>
        </div>
    </div>
</div>
</body>
</html>