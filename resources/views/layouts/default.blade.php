<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Development Company" content="Cytonn Technologies">
    <meta name="Developer" content="Duncan Kinuthia">
    <title>Project Management</title>

    {{--CSS FIles Inclusion--}}

    <link rel="shortcut icon" href="/site_images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/assets/bower/foundation-sites/dist/css/foundation.min.css"/>
    <link rel="stylesheet" href="/assets/bower/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/bower/foundation-datepicker/css/foundation-datepicker.min.css"/>
    {{--<link rel="stylesheet" href="/assets/bower/ng-tags-input/ng-tags-input.min.css"/>--}}
    <link rel="stylesheet" href="{{mix('/css/app.css')}}"/>
    {{--<link rel="stylesheet" href="/assets/foundation-icons/foundation-icons.css"/>--}}
    <link rel="stylesheet" href="/assets/responsive-tables/responsive-tables.css">
    {{--<link rel="stylesheet" href="/assets/timepicker/stylesheets/wickedpicker.css">--}}
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-default/index.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>


    <script type="text/javascript">

        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) {
                return false;
            }
        }

        document.onkeypress = stopRKey;

    </script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment.min.js"></script>
    <script src="/assets/bower/tinymce/tinymce.min.js"></script>
    <script src="/assets/bower/jquery/dist/jquery.min.js"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>--}}
    {{--<script src="/assets/bower/angular/angular.min.js"></script>--}}
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.5.9/angular-sanitize.js"></script>--}}
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    {{--<script src="/assets/bower/angular-smart-table/dist/smart-table.min.js"></script>--}}
    {{--<script src="/assets/js/app.min.js"></script>--}}
    {{--<script src="/assets/js/scripts.min.js"></script>--}}
    <script src="/assets/responsive-tables/responsive-tables.js"></script>
    <script src="/assets/timepicker/src/wickedpicker.js"></script>


</head>

<body>
<div ng-app="pmApp">
    <div id="app">

        @include('layouts.partials.nav')

        <div class="main-container">
            <div class="row" data-equalizer="default">
                <div class="off-canvas-wrapper">
                    <div class="off-canvas position-left" id="mobile-nav" data-off-canvas data-equalizer-watch="default">
                        @include('layouts.partials.left_nav')
                    </div>
                    <div class="small-3 medium-2 columns  side-nav show-for-medium" data-equalizer-watch="default">
                        @include('layouts.partials.left_nav')
                    </div>
                    <div class="small-12 medium-10 columns middle-content" data-equalizer-watch="default" v-cloak>
                        <div class="off-canvas-content" data-off-canvas-content>
                            <div class="content-section main-block">
                                @include('layouts.partials.flash')
{{--                                @include('layouts.partials.errors')--}}
                                <div v-cloak>
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Javascript Files--}}

<script>
    window.Laravel = {csrfToken: '{{ csrf_token() }}'};
</script>

<script src="/assets/bower/foundation-sites/dist/js/foundation.min.js"></script>
<script src="/assets/bower/foundation-datepicker/js/foundation-datepicker.min.js"></script>
<script src="/assets/bower/foundation-datepicker/js/locales/foundation-datepicker.en-GB.js"></script>
{{--<script src="/assets/bower/ng-tags-input/ng-tags-input.min.js"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="/js/jqueryScripts.js"></script>

<script src="{{mix('/js/app.js')}}"></script>

{{--<script type="text/javascript">--}}
{{--$('.selected_user').select2();--}}
{{--</script>--}}
<script>
    $(document).foundation();

</script>

</body>
</html>