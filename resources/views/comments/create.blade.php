@extends('layouts.default')
@section('content')

    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false,
        });
    </script>

    <div class="issue-create">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Create a Comment</h3>
                    {!! Form::open(['route' => 'comment_issue', 'files' => 'true']) !!}
                    @include('layouts.partials.errors')

                    <div class="form-group">
                        {!! Form::hidden('issue_id', $issue_id) !!}
                        {!! Form::label('name','Comment*') !!}
                        {!! Form::textarea('name',NULL,['rows'=> '3',  'class' => 'form-control']) !!}<br/>
                        {!! Form::label('image','Upload Image') !!}
                        {!! Form::file('image', NULL) !!}
                        {!! Form::submit('Create', ['class' => 'button button-success'])!!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>

@stop
