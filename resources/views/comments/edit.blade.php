@extends('layouts.default')
@section('content')

    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false,
        });
    </script>

    <div class="issue-create">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Edit Comment</h3>
                    {!! Form::open(['route' => 'comment_update', 'files' => 'true']) !!}
                    @include('layouts.partials.errors')

                    <div class="form-group">
                        {!! Form::hidden('issue_id', $comment->issue_id) !!}
                        {!! Form::hidden('comment_id', $comment->id) !!}
                        {!! Form::label('name','Comment*') !!}
                        {!! Form::textarea('name',$comment->name,['rows'=> '3',  'class' => 'form-control']) !!}<br/>
                        {!! Form::label('image','Upload Image') !!}
                        {!! Form::file('image', NULL) !!}
                        {!! Form::submit('Edit', ['class' => 'button button-success'])!!}
                        <a class="button success" href="/issue/{!! $comment->issue_id !!}">Cancel</a>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>

@stop
