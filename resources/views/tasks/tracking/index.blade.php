@extends('layouts.default')

@section('content')
    <div class="row content-section-text team-tasks">
        <div class="small-8 large-8 columns content-section-header">
            <h1 class="page-title"><b>{{$task->title}}</b> Task Schedule</h1>
        </div>
        <div class="small-2 large-2 columns">
            <a href="{{url('/tasks/my-tasks/'.$task->id)}}" class="button pull-right">Back</a>
        </div>
        <div class="small-2 large-2 columns">
            @if($task->due_date >= $today)
                <a href="{{route('task_track.create', $task->id)}}" class="button pull-left">Add Time</a>
            @endif
        </div>
        <div class="small 12 large-12 columns">
            @if(count($tracks)==null)
                <p>The timesheet for this task is empty</p>
            @else
                <table class="table">
                    <thead>
                    <th>Description</th>
                    <th>Date</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Added By</th>
                    <th style="text-align: center">Number of Hours</th>
                    <th colspan="2">Action</th>
                    </thead>
                @foreach($tracks as $track)
                    <tr>
                        <td>{!! $track->description !!}</td>
                        <td>{!! $track->date !!}</td>
                        <td>{!! $track->start_time !!}</td>
                        <td>{!! $track->end_time !!}</td>
                        <td>{!! $track->creator->preferred_name !!}</td>
                        <td style="text-align: center">{!! number_format((\PM\Presenters\NumberOfHoursPresenter::presentNumberOfHours($track->start_time,$track->end_time)),2) !!}</td>
                       <td><a href="{{route('task_track.edit',[$task->id,$track->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        <td><a href="{{url('/task/'.$task->id.'/track/'.$track->id)}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                @endforeach
                </table>
            @endif
        </div>
    </div>
@stop