@extends('layouts.default')

@section('content')
    <div class="row content-section-text team-tasks">
        <div class="small-9 large-9 columns content-section-header">
            <h1 class="page-title">Add Task Time</h1>
        </div>
        <div class="small-3 large-3 columns">
            <a href="{{route('task_track.index', $task_id)}}" class="button pull-right">Back</a>
        </div>
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                {!! Form::open(['route' => ['task_track.store',$task_id]]) !!}
                <br>
                @include('layouts.partials.errors')
                    <div class="task-form-controls">
                        <div class="row create-task">
                            {!! Form::label('date','Date*') !!}
                            {!! Form::text('date',\Carbon\Carbon::today()->format('Y-m-d'),['required','placeholder'=>'Date','class'=>'due-date']) !!}
                        </div>
                        <div class="row create-task">
                            <div class="small-12 medium-6 columns create-task-col-2">
                                {!! Form::label('start_time','Start*') !!}
                                <start-time></start-time>
                             </div>
                            <div class="small-12 medium-6 columns create-task-col">
                                {!! Form::label('end_time','Stop*') !!}
                                <end-time></end-time>
                            </div>
                        </div>
                <div class="tinymce-label">
                    {!! Form::label('description','Description') !!}
                    <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                    ='content'></tinymce>
                    <br>
                </div>
                {!! Form::submit('Create Schedule', ['class' => 'submit-task button button-success'])!!}
                 </div>
                </div>
            </div>
        </div>
    </div>
@stop