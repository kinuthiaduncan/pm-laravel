@extends('layouts.default')

@section('content')
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>
    <div class="row content-section-text team-tasks">
        <div class="small-9 large-9 columns content-section-header">
            <h1 class="page-title">Edit Task Schedule</h1>
        </div>
        <div class="small-3 large-3 columns">
            <a href="{{route('task_track.index', $task->id)}}" class="button pull-right">Back</a>
        </div>
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    {!! Form::open(['route' => ['task_track.update',$schedule->id,$task->id] ,'method'=>'PUT']) !!}
                    <br>
                    @include('layouts.partials.errors')
                    <div class="task-form-controls">
                        <div class="row create-task">
                            {!! Form::label('date','Date*') !!}
                            {!! Form::text('date',$schedule->date,['required','placeholder'=>'Date','class'=>'due-date']) !!}
                        </div>
                        <div class="row create-task">
                            <div class="small-12 medium-6 columns create-task-col-2">
                                {!! Form::label('start_time','Start*') !!}
                                {!! Form::text('start_time',$schedule->start_time,['required','placeholder'=>'Started at','id'=>'start','data-field-id'=>$schedule->start_time,'class'=>'timepicker2 start']) !!}
                            </div>
                            <div class="small-12 medium-6 columns create-task-col">
                                {!! Form::label('end_time','Stop*') !!}
                                {!! Form::text('end_time',$schedule->end_time,['required','placeholder'=>'Ended at','id'=>'stop','data-field-id'=>$schedule->end_time,'class'=>'timepicker3 end']) !!}
                            </div>
                        </div>
                        <div class="tinymce-label">
                            {!! Form::label('description','Schedule Description*') !!}
                            {!! Form::textarea('description',$schedule->description,['rows'=> '3',  'class' => 'form-control']) !!}
                            <br/>
                        </div>
                        {!! Form::submit('Update Schedule', ['class' => 'submit-task button button-success'])!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop