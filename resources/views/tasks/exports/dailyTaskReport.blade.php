@extends('layouts.pdf')

@section('content')
<h3 class="title"> {{$user->preferred_name}} ~ DAILY TASKS REPORT</h3>
@if(count($data) > 0)
    <h5>Tasks Assigned Today:</h5>
<table class="table">
    <thead class="summary">
    <tr>
        <th>Title</th><th>Description</th><th>Access</th><th>Priority</th><th>Due Date</th><th>Category</th>
        <th>Status</th><th>Assigned By</th>
    </tr>
    </thead>
    @foreach ($data as  $myData)
        <tr>
            <td>{{ $myData->title }}</td>
            <td>{!! $myData->description !!}</td>
            <td>{!! $myData->task_access !!}</td>
            <td>{!! $myData->priority->name !!}</td>
            <td>{!! $myData->due_date !!}</td>
            @if ( !empty ( $myData->taskCategory->name ) )
            <td>{!! $myData->taskCategory->name !!}</td>
                @else
                <td></td>
            @endif
            <td>{!! $myData->status->name  !!}</td>
            <td>{!! $myData->creator->preferred_name !!}</td>
        </tr>
    @endforeach
</table>
    @else
    <p>No new Tasks Assigned Today</p>
    @endif
    @if(count($logged) > 0 )
        <br />
        <h5>Task Hours Logged Today:</h5>
        <table class="table">
            <thead class="summary">
            <tr>
                <th>Task</th><th>Schedule Description</th><th>Start Time</th><th>End Time</th><th>Hours</th>
            </tr>
            </thead>
            <tbody>
            @foreach($logged as $time)
            <tr>
                <td>{!! $time->task->title !!}</td>
                <td>{!! $time->description !!}</td>
                <td>{!! $time->start_time !!}</td>
                <td>{!! $time->end_time !!}</td>
                <td>{!! number_format((\PM\Presenters\NumberOfHoursPresenter::presentNumberOfHours($time->start_time,$time->end_time)),2) !!}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
        @else
        <p>No Task Hours logged Today</p>
    @endif
@endsection