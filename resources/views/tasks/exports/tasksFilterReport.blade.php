@extends('layouts.pdf')

@section('content')
<h3 class="title">EXPORTED TASK REPORT</h3>

<table class="table">
    <thead class="summary">
    <tr>
        <th>Title</th>
        {{--<th>Description</th>--}}
        <th>Access</th>
        <th>Priority</th>
        <th>Date Due</th>
        <th>Status</th>
        <th>Assigned By</th>
        <th>Assigned To</th>
    </tr>
    </thead>
    @foreach ($data as  $mydata)
        <tr>
            <td>{{ $mydata->title }}</td>
{{--            <td>{!! $mydata->description !!}</td>--}}
            <td>{!! $mydata->task_access !!}</td>
            <td>{!! $mydata->priority->name !!}</td>
            <td>{!! $mydata->due_date !!}</td>
            <td>{!! $mydata->status->name  !!}</td>
            <td>{!! $mydata->creator->preferred_name !!}</td>
            <td>
                <ul>
                    @foreach($mydata->assignedUsers as $assignee)
                        <li>
                            {{$assignee->user->preferred_name}}
                        </li>
                    @endforeach
                </ul>
            </td>
        </tr>
    @endforeach
</table>
@endsection