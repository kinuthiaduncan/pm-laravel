@extends('layouts.default')
@section('content')
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>
    <div class="row">
        <div class="content-section-header">
            <h1 class="page-title">Edit Sub Category
                <span> <a href="{{ route('task_subcategories.index',[$category]) }}" class="button pull-right margin-left-10">
                    Back</a> </span>
            </h1>
        </div>
        <div class="large-8 large-centered columns">
            <div class="project-create-form">
                {{ Form::open(['method'=>'PUT','route' => ['task_subcategories.update',$category, $subcategory->id]]) }}
                @include('layouts.partials.errors')

                {{ Form::label('subcategory','Name*') }}
                {{ Form::text('subcategory',$subcategory->subcategory,['required']) }}

                {{ Form::label('description','Sub Category Description*') }}
                {!! Form::textarea('description',$subcategory->description,['rows'=> '3',  'class' => 'form-control']) !!}
                <br>

                {{ Form::submit('Update', ['class' => 'button button-success'])}}

                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop