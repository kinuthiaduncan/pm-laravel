@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="content-section-header">
            <h1 class="page-title">Add Sub Categories
                <span> <a href="{{ route('task_subcategories.index',[$category->id]) }}" class="button pull-right margin-left-10">
                    Back</a> </span>
            </h1>
        </div>
        <div class="large-8 large-centered columns">
            <div class="project-create-form">
                <h3>Create Sub Category</h3>
                {{ Form::open(['route' => ['task_subcategories.store',$category->id]]) }}
                @include('layouts.partials.errors')

                {{ Form::label('subcategory','Name*') }}
                {{ Form::text('subcategory',NULL,['required']) }}

                {{ Form::label('description','Sub Category Description*') }}
                <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                ='content'></tinymce>
                <br>

                {{ Form::submit('Create', ['class' => 'button button-success'])}}

                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop