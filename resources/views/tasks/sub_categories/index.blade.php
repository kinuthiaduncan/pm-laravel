@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="content-section-header">
            <h1 class="page-title">{{$category->name}} Sub Categories
                <span> <a href="{{ route('task_subcategories.create',[$category->id]) }}" class="button pull-right margin-left-10">Add
                    Sub Categories</a> </span>
                <a href="{{ route('task_category.index') }}" class="button pull-right margin-left-10">
                Back</a>
            </h1>
        </div>
        <div class="content-section-text small-12 large-12 column">
            @if(count($subcategories)==null)
                <p>No subcategories in this task category</p>
            @else
                <table class="responsive">
                    <thead>
                        <th>Sub category Name</th>
                        <th>Description</th>
                        <th>Created By</th>
                    </thead>
                    <tbody>
                    @foreach($subcategories as $subcategory)
                        <tr>
                            <td>{{$subcategory->subcategory}}</td>
                            <td>{!! $subcategory->description !!}</td>
                            <td>{{ $subcategory->creator->preferred_name}}</td>

                                @if(Auth::user()->id == $subcategory->creator->id)
                                <td>
                                    <a href="{{route('task_subcategories.edit',[$category->id,$subcategory->id])}}">Edit</a>
                                </td>
                                <td>
                                    <a href="{{url('/delete-sub-categories/'.$subcategory->id)}}">Delete</a>
                                </td>
                                @else
                                    <td></td><td></td>
                                @endif

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@stop