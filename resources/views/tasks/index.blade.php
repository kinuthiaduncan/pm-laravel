@extends('layouts.default')

@extends('layouts.partials.tabs')
@section('team-shared')
    @include('tasks.partials.team_tasks')
@endsection
@section('individual')
    @include('tasks.partials.individual_tasks')
@endsection