@extends('layouts.default')

@section('content')
    <div class="row reminders">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="small-12 columns">
            <h5 class="pull left">Reminders
                <a data-open="createReminderModal" class="button pull-right">Add a reminder</a>
                <a href="{{route('task.show', $task_id)}}" class="button pull-right">Back</a>
            </h5>
        </div>

        <div id="createReminderModal" class="row entry-modal" data-reveal>
            <div class="title-modal small-12 large-4 column">
                <div class="title-text hide-for-small-only">
                    <h3 style="margin-top: 70%;">Add Reminder</h3>
                </div>
                <div class="title-text-small show-for-small-only">
                    <h4>Add Reminder</h4>
                </div>
            </div>

            <div class="small-12 large-8 column form-modal">
                {!!  Form::open(['route'=>['task_reminder.store', $task_id]]) !!}

                {!! Form::textarea('content', null, ['required'=>'required', 'rows'=>3, 'placeholder'=>'Add the message']) !!}

                {!! Form::text('time', null, ['required'=>'required', 'class'=>'datetime', 'placeholder'=>'Select time']) !!}

                {!! Form::submit('Save', ['class'=>'button']) !!}
                {!! Form::close() !!}
            </div>
        </div>

        <div class="reminders-info">
            @forelse($task_reminders as $reminder)
                <div class="small-12 medium-6 large-4 columns" style="padding: 10px;">
                    <div class="card reminder-card" >
                        <div class="card-divider">
                            <h6>{{ $reminder->content }}
                                {{--<h6>{{ $reminder->created_at->diffForHumans() }}</h6>--}}
                                <a href="#" data-open="editReminderModal{{ $reminder->id }}"><i
                                            class="fa fa-pencil pull-right"></i></a>
                            </h6>
                        </div>
                        <div class="card-section">
                            <p>{{ Carbon\Carbon::parse($reminder->time)->format('d-m-Y h:i') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div id="editReminderModal{{ $reminder->id }}" class="row entry-modal" data-reveal>
                    <div class="title-modal small-12 large-4 column">
                        <div class="title-text hide-for-small-only">
                            <h3 style="margin-top: 70%;">Edit Reminder</h3>
                        </div>
                        <div class="title-text-small show-for-small-only">
                            <h4>Edit Reminder</h4>
                        </div>
                    </div>
                    <div class="small-12 large-8 column form-modal">
                        {!!  Form::open(['method'=>'PUT','route'=>['task_reminder.update', $task_id, $reminder->id]]) !!}

                        {!! Form::textarea('content', $reminder->content, ['required'=>'required', 'rows'=>3, 'placeholder'=>'Add the message']) !!}

                        {!! Form::text('time', $reminder->time, ['required'=>'required', 'class'=>'datetime', 'placeholder'=>'Select time']) !!}
                        {!! Form::submit('Edit', ['class'=>'button']) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
            @empty
                <p>You do not have any reminders set for this task</p>
            @endforelse

        </div>
    </div>
@endsection