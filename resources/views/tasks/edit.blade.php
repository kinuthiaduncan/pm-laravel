@extends('layouts.default')
@section('content')
    <div class="project-create">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form" id="tinymce">
                    <h3>Edit Task ~ {{$task->title}}</h3>
                    {!! Form::open(['method'=>'PUT', 'route' => ['task.update', $task->id], 'files'=>true]) !!}
                    <br>
                    @include('layouts.partials.errors')
                    {!! Form::label('title','Title*') !!}
                    {!! Form::text('title',$task->title,['required','class'=>'task-title']) !!}
                    <div class="task-form-controls">
                        <div class="row create-task">
                            {{--<hr>--}}
                            <div class="small-12 medium-6 columns create-task-col-2">
                                {!! Form::label('due_date', 'Due Date*')!!}
                                {!! Form::text('due_date', $task->due_date, ['required'=>'required','class'=>'due-date']) !!}
                            </div>
                            <div class="small-12 medium-6 columns create-task-col">
                                {!! Form::label('priority_id', 'Priority*')!!}
                                {!! Form::select('priority_id', $priorities,$task->priority_id,[ 'placeholder'=>'Priority','required'])!!}
                            </div>
                        </div>
                            <div class="row create-task">
                                <div :class="[hasSubCategory ? medium6 : medium12]">
                                    {!! Form::label('task_category_id','Task Category')!!}
                                    {!! Form::select('task_category_id', $task_categories ,$task->task_category_id, ['v-model'=>'task_category_id','@change' => 'subCategoriesPicker()','placeholder'=>'Task Category'])!!}
                                </div>
                                    <div v-if ="subcategories !=0" class="" :class="[hasSubCategory ? medium6 : medium12]">
                                        {{ Form::label('task_subcategory_id', 'Task Subcategory') }}
                                        <select name="task_subcategory_id" required="required">
                                            @if($task->task_subcategory_id != null)
                                                <option value="{{$task->task_subcategory_id}}">{{$task->taskSubCategory->subcategory}}</option>
                                            @endif
                                                <option v-for="subcategory, index in subcategories" :value="index">
                                                @{{ subcategory }}
                                            </option>
                                        </select>
                                    </div>
                            </div>

                        <div class="row create-task">
                            <div class="small-12 medium-12 columns create-task-col">
                                {!! Form::label('assigned_to[]', 'Assign To*')!!}
                                {!! Form::select('assigned_to[]', $assign_to, $task->assigned_users ,['required'=>'required', 'v-select'=>'', 'multiple'])!!}
                            </div>
                        </div>
                        <div class="row create-task">
                            <div class="small-8 medium-3 columns create-task-col-2">
                                {!! Form::label('repetitive', 'Is Task Repetitive?')!!}
                                {!! Form::select( 'repetitive',['1'=>'Yes', '0'=>'No'],$task->repetitive ,
                                 ['@change' => 'isRepetitiveChecker()','required'])!!}
                            </div>
                            @if ($task->repetitive == 1)
                                <div :class="repetitive">
                                    {!! Form::label('interval', 'Repetition interval')!!}
                                    {!! Form::select('interval', ['1'=>'Daily', '7'=>'Weekly','30'=>'Monthly', 'null'=>'None'],$task->interval ,
                                    ['placeholder'=>'Select Interval'])!!}
                                </div>
                                <div :class="medium3">
                                    {!! Form::label('files[]', 'Upload file') !!}
                                    <div class="file-upload">
                                        {!! Form::file('files[]',['multiple'=>true]) !!}
                                    </div>
                                </div>
                            @else
                                <div :class="[isRepetitive ? repetitive : notRepetitive]">
                                    {!! Form::label('interval', 'Repetition interval')!!}
                                    {!! Form::select('interval', ['1'=>'Daily', '7'=>'Weekly','30'=>'Monthly'], null ,
                                    ['placeholder'=>'Select Interval'])!!}
                                </div>
                                <div :class="[isRepetitive ? medium3 : medium6]">
                                    {!! Form::label('files[]', 'Upload file') !!}
                                    <div class="file-upload">
                                        {!! Form::file('files[]',['multiple'=>true]) !!}
                                    </div>
                                </div>
                            @endif
                            <div class="small-12 medium-3 columns create-task-col">
                                {!! Form::label('task_access', 'Access Level*')!!}
                                {!! Form::select('task_access', ['public'=>'Public', 'private'=>'Private'],$task->task_access,['placeholder'=>'↓ Task Type', 'required'])!!}
                            </div>
                        </div>
                        <div class="row create-task">
                            <div class="small-12 medium-6 columns create-task-col">

                                {!! Form::label('status_id', 'Task Status*')!!}
                                {!! Form::select('status_id', $status , $task->status_id)!!}

                            </div>
                            <div class="small-12 medium-6 columns create-task-col">

                                {!! Form::label('cancellation_reason', 'Reason (If Status changed to Cancelled')!!}
                                {!! Form::text('cancellation_reason',$task->cancellation_reason,['class'=>'task-title']) !!}

                            </div>
                        </div>
                        <div class="row create-task">
                            <div class="small-12 columns create-task-col-2">
                                {!! Form::label('task_access', 'Share With*')!!}
                                {!! Form::select('user_group[]', $assign_to,$task_groups_users_array ,['required'=>'required', 'v-select'=>'', 'multiple'])!!}
                            </div>
                        </div>
                        <div class="tinymce-label">
                            {!! Form::label('description','Task Description') !!}
                            {!! Form::textarea('description',$task->description,['rows'=> '3',  'class' => 'form-control']) !!}
                            <br/>
                            <br>
                        </div>
                        <div id="schedule" class="task-timeline"><i class="fa fa-plus-square" aria-hidden="true"></i>
                            Add Task Schedule</div>
                        <div id="add_schedule" style="display:none">
                            <br />
                            <h6>Add Task Schedule</h6>
                            @include('tasks.partials.task_schedule')
                        </div>
                        <br />
                        {!! Form::submit('Edit', ['class' => 'button button-success'])!!}

                        <a href="{{route('task.show', $task->id)}}" class="button button-success">Back</a>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
