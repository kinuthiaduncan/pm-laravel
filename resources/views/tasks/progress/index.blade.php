@extends('layouts.default')

@section('content')
    <div class="task-progress">
        @if (count($errors) > 0)
            <div class="callout alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h5>Progress report for task: {{$task->title}}
            <a href="#" data-open="createProgressModal" class="button pull-right">Add a progress report</a>
            <a href="{{route('task.show', $task->id)}}" class="button pull-right">Back</a>
        </h5>
        <hr>
        <div class="progress-info">
            <table>
                <tbody>
                @foreach($progress as $report)
                    <tr>
                        <td>
                            {{$report->title}}
                            <a class="pull-right" data-open="updateProgressModal" id="edit"
                               data-rowid="{{ json_encode($report) }}"><i class="fa fa-pencil"></i></a>
                        </td>
                        <td>{{ $report->description }}</td>
                        <td>{{ $report->percentage_done }}%</td>
                        <td>{{ $report->created_at->diffForHumans() }}</td>
                        <td>By {{ $report->addedBy->preferred_name }}
                            on {{ $report->created_at->toFormattedDateString() }}</td>
                    </tr>


                @endforeach
                @if($progress->count() == 0)
                    <tr>
                        <td>There are no progress reports recorded for this task</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {!! $progress->links() !!}

        </div>

            <div id="updateProgressModal" class="row entry-modal" data-reveal>

                <div class="title-modal small-12 large-4 column">
                    <div class="title-text hide-for-small-only">
                        <h3 style="margin-top: 120%;">Edit the Task Progress</h3>
                    </div>
                    <div class="title-text-small show-for-small-only">
                        <h4>Edit the Task Progress</h4>
                    </div>
                </div>

                <div class="small-12 large-8 column form-modal">
                    {!!  Form::open(['route'=>['update_progress',$task->id]]) !!}
                    <div>
                        {!! Form::hidden('id',null, ['required'=>'required','id'=>'id']) !!}
                        {!! Form::label('title', 'Title*') !!}
                        {!! Form::text('title',null, ['required'=>'required','id'=>'title']) !!}
                    </div>
                    <div>
                        {!! Form::label('description', 'Description*') !!}
                        {!! Form::textarea('description',null,['required'=>'required','id'=>'description', 'rows'=>4]) !!}
                    </div>
                    <div class="percentage-done">
                        {!! Form::label('percentage_done', 'Percentage Done*') !!}
                        {!! Form::number('percentage_done',0, ['max'=>100,'id'=>'percentage', 'min'=>0]) !!}
                    </div>
                    <div>
                        {!! Form::submit('Save', ['class'=>'button']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        <!-- Reveal Modals begin -->
        <div id="createProgressModal" class="row entry-modal" data-reveal>

            <div class="title-modal small-12 large-4 column">
                <div class="title-text hide-for-small-only">
                    <h3 style="margin-top: 120%;">Create a Task Progress</h3>
                </div>
                <div class="title-text-small show-for-small-only">
                    <h4>Create a Task Progress</h4>
                </div>
            </div>

            <div class="small-12 large-8 column form-modal">
                {!!  Form::open(['route'=>['task_progress.store', $task->id]]) !!}
                <div>
                    {!! Form::label('title', 'Title*') !!}
                    {!! Form::text('title', null, ['required'=>'required']) !!}
                </div>
                <div>
                    {!! Form::label('description', 'Description*') !!}
                    {!! Form::textarea('description', null, ['required'=>'required', 'rows'=>4]) !!}
                </div>
                {{--class="small-3 large-3 column percentage-done comment-col"--}}
                <div class="percentage-done">
                    {!! Form::label('percentage_done', 'Percentage Done*') !!}
                    {!! Form::number('percentage_done', 0, ['max'=>100, 'min'=>0]) !!}
                </div>
                <div>
                    {!! Form::submit('Save', ['class'=>'button']) !!}
                </div>
                {!! Form::close() !!}
            </div>

            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

    </div>
@endsection