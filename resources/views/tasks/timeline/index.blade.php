@extends('layouts.default')
@section('content')

    <div class="row">
        <div class="small-12 large-12 columns content-section-header">
            <h1 class="page-title">Timelines
                <a class="button pull-right" href="{{route('task.create')}}"
                   style="margin-right: 15px;"><strong>+</strong>
                    Add Task</a>
                <a href="{{route('task_timeline.create')}}" class="button pull-right"><i class="fa fa-clock-o" aria-hidden="true"></i> Add Schedule</a>
                <div class="small-12 large-7 centered columns">
                {!! Form::open(['route'=>'timelines.search']) !!}
                {!! Form::select('user_id',$users,'',['class'=>'timeline-search','placeholder'=>'Select User','required']) !!}
                {!! Form::submit('View Timeline',['class'=>'button']) !!}
                {!! Form::close() !!}
                </div>
            </h1>
        </div>
        <full-calendar :events="{{ json_encode($events) }}" :editable="false"></full-calendar>
    </div>

@endsection


