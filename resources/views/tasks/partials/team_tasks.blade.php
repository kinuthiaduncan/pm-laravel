<div class="content-section-header">
    <div class="small-12 column">
        <h1 class="page-title">All Tasks

            @if(Auth::user()->job_level == 'Manager')
                <a href="{{url('departmental-tasks')}}" class="button margin-left-10 pull-right">Export Departmental
                    Tasks</a>
            @endif

        </h1>
    </div>
</div>

<div class="content-section-text">
    <div class="row team-tasks">

        <div class="small-12 large 12 columns">

            <div class="row select-boxes">

                {{ Form::open(['method' => 'get','url' => url('task-filter-report')]) }}

                <div class="small-2 columns">
                    {{ Form::text('title', null,['placeHolder'=>'Enter task title', 'v-model'=>'title', 'v-on:change'=>'getPublicTasks()']) }}
                </div>
                <div class="small-2 columns">
                    {{ Form::select('status_id',$status,null, ['v-model'=>'status', 'v-on:change'=>'getPublicTasks()' ]) }}
                </div>
                <div class="small-2 columns">
                    {{ Form::select('task_access',['public' => 'Public', 'private'=>'Private'],null, ['placeholder'=>'Select Access','v-model'=>'task_access', 'v-on:change'=>'getPublicTasks()' ]) }}
                </div>
                <div class="small-2 columns">
                    {{ Form::select('department_id',$departments,null, ['placeholder'=>'Select Department', 'v-model'=>'department', 'v-on:change'=>'getPublicTasks()' ]) }}
                </div>
                <div class="small-2 columns">
                    {{ Form::select('assigned_to',$users,null, ['placeholder'=>'Assigned To', 'v-model'=>'user', 'v-on:change'=>'getPublicTasks()']) }}
                    {{--<select2 v-model="user">--}}
                        {{--<option disabled value="0">Select one</option>--}}
                    {{--</select2 >--}}
                </div>
                <div class="small-2 large-2 column">
                    <button class="button pull-right margin-left-10">Export Tasks</button>
                </div>
                {{ Form::close() }}

                <public-tasks-table today="{{ $today}}"></public-tasks-table>
            </div>
        </div>
    </div>
</div>