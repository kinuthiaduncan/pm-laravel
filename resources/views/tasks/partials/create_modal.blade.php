<div id="createTaskModal" class="task-modal" data-reveal
     aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
        <div class="row task-modal-header">
            <div class="small-11 large-11 task-modal-create  columns">
                <h6>Create Task</h6>
            </div>
            <div class="small-1 large-1 columns task-modal-close">
                <a data-close aria-label="Close modal"><i class="fa fa-times" aria-hidden="true" ></i></a>
            </div>
        </div>
    <div class="row task-modal-body">
        {!! Form::open() !!}
            <div class="small-6 large-6 columns">
                {!! Form::label('title','Task Name*') !!}
                {!! Form::text('title',NULL,['v-model'=>'title']) !!}
            </div>
            <div class="small-6 large-6 columns">
                {!! Form::label('status_id', 'Task Status*')!!}
                {!! Form::select('status_id',$status,'',['v-model'=>'status_id'])!!}
            </div>
        <div class="small-6 large-6 columns">
            {!! Form::label('assign_date', 'Assign Date*')!!}
            <el-date-picker
                    v-model="assign_date"
                    type="date"
                    placeholder="Pick a day">
            </el-date-picker>
        </div>
        <div class="small-6 large-6 columns">
            {!! Form::label('due_date', 'Due Date*')!!}
            <el-date-picker
                    v-model="due_date"
                    type="date"
                    placeholder="Pick a day">
            </el-date-picker>
        </div>
        <div class="small-12 large-12 columns">
            {!! Form::label('assigned_to', 'Assign To*')!!}

            {!! Form::select('assigned_to[]', $assign_to,null,['id' => 'assigned_to','v-select'=>'assigned_to', 'multiple','v-model'=>'assigned_to'])!!}
        </div>
        <div class="small-12 large-12 more-fields columns">
            <a @click="moreFields()">
            <span v-if="!more_fields">More Fields <i class="fa fa-angle-down" aria-hidden="true"></i></span>
            <span v-if="more_fields">Less Fields <i class="fa fa-angle-up" aria-hidden="true"></i></span>
            </a>
            <hr>
        </div>
        <div v-if="more_fields">
            <div class="small-6 large-6 columns">
                {!! Form::label('task_category_id','Task Category')!!}
                {!! Form::select('task_category_id', $task_categories ,'', ['v-model'=>'task_category_id','placeholder'=>'Task Category'])!!}
            </div>
            <div class="small-6 large-6 columns">
                {!! Form::label('interval', 'Occurrence')!!}
                {!! Form::select('interval', ['1'=>'Daily', '7'=>'Weekly','30'=>'Monthly'],'null' ,
                ['placeholder'=>'Select Interval','v-model'=>'interval'])!!}
            </div>
            <div class="small-6 large-6 columns">
                {!! Form::label('task_access', 'Access Level*')!!}
                {!! Form::select('task_access', ['public'=>'Public', 'private'=>'Private'],'public' ,['placeholder'=>'↓ Task Type','v-model'=>'task_access'])!!}
            </div>
            <div class="small-6 large-6 columns">
                {!! Form::label('task_access', 'Share With*')!!}
                {!! Form::select('user_group[]', $assign_to,'' ,['v-select'=>'user_group', 'multiple','v-model'=>'user_group'])!!}

            </div>
            <div class="small-12 large-12 columns">
                {!! Form::label('description', 'Description')!!}
                {!! Form::textarea('description',NULL, ['rows'=>'3','v-model'=>'description'])!!}
            </div>
            <div class="small-12 large-12 columns">
                {!! Form::label('file', 'Attach File')!!}
                <div class="files">
                    {!! Form::file('file')!!}
                </div>
            </div>
        </div>
        <div>
            <div class="small-12 large-12 columns" v-if="!loading">
                <a class="button pull-right" @click="createTask()">Create Task</a>
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    <div class="task-loading " v-if="loading">
        <i class="fa fa-spinner fa-spin"></i> <h4> Submitting ...</h4>
    </div>
</div>

