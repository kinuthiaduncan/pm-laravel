<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        plugins: [
            "advlist autolink lists link charmap anchor",
            "searchreplace visualblocks code",
            "insertdatetime  contextmenu paste jbimages"
        ],
        toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
        "bullist numlist outdent indent ",
        relative_urls: false
    });
</script>
<div class="task-form-controls">
    <div class="row create-task">
        {!! Form::label('date','Schedule Date*') !!}
        <date-picker></date-picker>
    </div>
</div>
<div class="row create-task">
    <div class="small-12 medium-6 columns create-task-col-2">
        {!! Form::label('start_time','Start*') !!}
        <start-time></start-time>
    </div>
    <div class="small-12 medium-6 columns create-task-col">
        {!! Form::label('end_time','Stop*') !!}
        <end-time></end-time>
    </div>
</div>
<div class="tinymce-label">
    {!! Form::label('schedule_description','Task Schedule Description') !!}
    {!! Form::textarea('schedule_description',null,['rows'=> '3',  'class' => 'form-control']) !!}
</div>