<div class="content-section-header">
    <h1 class="page-title">My Tasks
        <a class="button pull-right" @click="taskFormVisible = true" style="margin-right: 15px;"><strong>+</strong>
            Add Task</a>

        <a class="button pull-right" href="{{route('task_group.index')}}" style="margin-right: 15px;">
            My Task Groups</a>
    </h1>
</div>
<task-create v-if="taskFormVisible"></task-create>
<div class="content-section-text">
    <div class="row team-tasks">
        <div class="row select-boxes">

            {{ Form::open(['method' => 'get','url' => url('personal-task-report')]) }}

            <div class="small-4 large-2 columns">
                {{ Form::text('title', null,['placeHolder'=>'Enter task title', 'v-model'=>'title', 'v-on:input'=>'getTasks()']) }}
            </div>
            <div class="small-4 large-3 columns">
                {{ Form::select('status_id',$status,null, ['v-model'=>'status', 'v-on:change'=>'getTasks()' ]) }}
            </div>
            <div class="small-4 medium-3 columns">
                {{ Form::select('task_access',['public' => 'Public', 'private'=>'Private'],null, ['placeholder'=>'Select Access','v-model'=>'task_access', 'v-on:change'=>'getTasks()' ]) }}
            </div>
            <div class="small-6 large-2 column">
                <button class="button pull-right margin-left-10">Export Tasks</button>
            </div>

            {{ Form::close() }}

        </div>

        <div class="small-12 large 12 columns">
            {{--<a class="button " href="{{route('task_category.index')}}">View Categories</a>--}}
            {{--&nbsp;&nbsp;--}}
            {{--<a class="button" href="{{route('public_tasks.index')}}">Departmental Tasks</a>--}}
            <div class="small-12 columns">
                <div class="row">
                    <tasks-table today="{{$today}}"></tasks-table>
                </div>
            </div>
        </div>
    </div>
</div>