@extends('layouts.default')
@section('content')
    <div class="project-create">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    {!! Form::open(['route' => 'task.store', 'files'=>true]) !!}
                    <br>

                    {!! Form::label('title','Title*') !!}
                    {!! Form::text('title',NULL,['required','placeholder'=>'Task Title','class'=>'task-title']) !!}
                    <div class="task-form-controls">
                        <div class="row create-task">
                            {{--<hr>--}}
                            <div class="small-12 medium-6 columns create-task-col-2">
                                {!! Form::label('due_date', 'Due Date*')!!}
                                {!! Form::text('due_date', '', ['required'=>'required','class'=>'due-date']) !!}
                            </div>
                            <div class="small-12 medium-6 columns create-task-col">
                                {!! Form::label('priority_id', 'Priority*')!!}
                                {!! Form::select('priority_id', $priorities,3,[ 'placeholder'=>'Priority','required'])!!}
                            </div>
                        </div>
                        <div class="row create-task">
                            <div :class="[hasSubCategory ? medium6 : medium12] ">
                                {!! Form::label('task_category_id','Task Category')!!}
                                {!! Form::select('task_category_id', $task_categories ,'' , ['v-model'=>'task_category_id','@change' => 'subCategoriesPicker()','placeholder'=>'Task Category'])!!}
                            </div>
                        <div v-if ="subcategories !=0" class="" :class="[hasSubCategory ? medium6 : medium12]">
                            {{ Form::label('task_subcategory_id', 'Task Subcategory') }}
                             <select name="task_subcategory_id" required>
                                <option value="">Select a Task Subcategory</option>
                                <option v-for="subcategory, index in subcategories" :value="index">
                                    @{{ subcategory }}
                                </option>
                            </select>
                          </div>
                        </div>
                        <div class="row create-task">
                            <div class="small-12 medium-12 columns create-task-col">
                                {!! Form::label('assigned_to', 'Assign To*')!!}
                                {!! Form::select('assigned_to[]', $assign_to,'' ,['required'=>'required', 'v-select'=>'assigned_to', 'multiple'])!!}
                            </div>
                        </div>

                        <div class="row create-task">
                            <div class="small-12 medium-3 columns create-task-col-2">
                                {!! Form::label('repetitive', 'Is Task Repetitive?')!!}
                                {!! Form::select( 'repetitive',['1'=>'Yes', '0'=>'No'],'0' ,
                                 ['@change' => 'isRepetitiveChecker()','required'])!!}
                            </div>
                            <div v-if="isRepetitive" class="" :class="[isRepetitive ? repetitive : notRepetitive]">
                                {!! Form::label('interval', 'Repetition interval')!!}
                                {!! Form::select('interval', ['1'=>'Daily', '7'=>'Weekly','30'=>'Monthly'],'null' ,
                                ['required'=>'required','placeholder'=>'Select Interval'])!!}
                            </div>
                            <div :class="[isRepetitive ? medium3 : medium6]">
                                {!! Form::label('files[]', 'Upload file') !!}
                                <div class="file-upload">
                                    {!! Form::file('files[]',['multiple'=>true]) !!}
                                </div>
                            </div>

                            <div class="small-12 medium-3 columns create-task-col">

                                {!! Form::label('task_access', 'Access Level*')!!}
                                {!! Form::select('task_access', ['public'=>'Public', 'private'=>'Private'],'public' ,['placeholder'=>'↓ Task Type', 'required'])!!}

                            </div>
                        </div>
                        <div class="row create-task">
                            <div class="small-12 columns create-task-col-2">
                                {!! Form::label('task_access', 'Share With*')!!}
                                {!! Form::select('user_group[]', $assign_to,'' ,['required'=>'required', 'v-select'=>'', 'multiple'])!!}
                            </div>
                        </div>

                        <div class="tinymce-label">
                            {!! Form::label('description','Task Description') !!}
                            {!! Form::textarea('description',null,['rows'=> '3',  'class' => 'form-control']) !!}
                            <br>
                        </div>
                        <div id="schedule" class="task-timeline"><i class="fa fa-plus-square" aria-hidden="true"></i>
                            Add Task Schedule</div>
                        <div id="add_schedule" style="display:none">
                            <br />
                            <h6>Add Task Schedule</h6>
                            @include('tasks.partials.task_schedule')
                        </div>
                        <br />
                        {!! Form::submit('Create Task', ['class' => 'submit-task button button-success'])!!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
