@extends('layouts.default')

@section('content')

    <div class="content-section-header">
        <h1 class="page-title">Tasks</h1>
    </div>

    <div class="row content-section-text team-tasks">
        <div class="large-12 columns">
            <a class="button pull-left" href="{{route('task.index')}}">Back</a>

            @if ( $followed == true)
                <a class="button pull-left" href="{{route('un_follow_task', $task->id)}}">Un Follow Task</a>
            @elseif ($followed == false)
                <a class="button pull-left" href="{{route('follow_task', $task->id)}}">Follow Task</a>
            @endif

            <a class="button pull-right" style="right: 10px;"
               href="{{route('task_reminder.index', $task->id)}}">
                <i class="fa fa-bell-o"></i>&nbsp;Set A Reminder</a> &nbsp;&nbsp;

            @if ($task->edit == true)
                <a class="button pull-right" href="{{route('task_progress.index', $task->id)}}">
                    <i class="fa fa-pencil-square-o"></i>&nbsp;Progress Report
                </a>
            @endif
            <a class="button pull-right" style="right: 10px;"
               href="{{route('task_track.index', $task->id)}}">
                <i class="fa fa-clock-o"></i> Task Schedule</a> &nbsp;&nbsp;
        </div>

        <div class="task-show">
            <h3 class="task-heading">{{$task->title}}
            </h3>
            <span>Assigned by <strong>{{$task->creator->preferred_name}}</strong></span>
            <table style="margin-top: 10px">
                <tbody>
                <tr>
                    <td>Task Lead(s)</td>

                    <td>
                        @foreach($assigned_users as $assignee)
                            <span class="button task-member">{{$assignee}}</span>
                        @endforeach
                    </td>
                </tr>
                @if(!is_null($task->percentage_done))
                    <tr>
                        <td>Percentage Done</td>
                        <td>{{$task->percentage_done}}</td>
                    </tr>
                @endif
                <tr>
                    <td>Status</td>
                    <td>{{$task->status->name}}</td>
                </tr>
                <tr>
                    <td>Priority</td>
                    <td>{{$task->priority->name}}</td>
                </tr>
                <tr>
                    <td>Due Date</td>
                    <td>{{$task->due_date}}</td>
                </tr>
                @if(!is_null($task->task_category_id))
                    <tr>
                        <td>Task Category</td>
                        <td>{{$task->taskCategory->name}}</td>
                    </tr>
                @else
                    <tr>
                        <td>Task Category</td>
                        <td>General</td>
                    </tr>
                @endif

                <tr>
                    <td style="width: 180px">Task description</td>
                    <td class="task-description">{!! $task->description !!}</td>
                </tr>

                <tr>
                    <td>Task Members</td>
                    <td>
                        @forelse($task_members as $member)
                            <span class="button task-member">{{$member}}</span>
                        @empty
                            <span class="button task-member">{{$task->creator->preferred_name}}</span>
                        @endforelse
                    </td>
                </tr>
                <tr>
                    <td>Task Files</td>
                    <td>
                        @if(count($task->files) > 0)
                            @foreach($task->files as $file)
                                <p><a href="{{URL::to($file->url)}}" download>{{$file->name}}</a></p>
                            @endforeach
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        {{--@forelse($task->files as $file)--}}
        {{--<div class="small-3 column task-files-display">--}}
        {{--<iframe src='https://docs.google.com/viewer?url={!! $file !!}&embedded=true' frameborder='0'></iframe>--}}
        {{--<iframe src='{!! $file !!}'></iframe>--}}
        {{--<img class="thumbnail" src="{!! $file!!}">--}}
        {{--</div>--}}
        {{--@empty--}}
        {{--<p></p>--}}
        {{--@endforelse--}}

        <div class="row">
            <div class="row">
                <div class="small-12 column">
                    <comments id="{{ $task->id }}" user="{{Auth::id()}}"></comments>
                </div>
            </div>

            <div class="row new-comment">

                {!! Form::open(['route' => ['task_comment.store', $task->id], 'files' => 'true']) !!}
                <div class="row">
                    <div v-if="showApproversField" class="small-12 column">
                        <label class="typo__label">Select Approvers</label>
                        {!! Form::select('user_group[]', $users,'' ,['required'=>'required', 'v-select'=>'', 'multiple'])!!}
                    </div>
                </div>
                <div class="small-9 large-9 column comment-col">
                    {!! Form::textarea('comment', null, ['placeholder' => 'Type Something to Send...', 'rows'=>1, 'class' => 'mycomment-box', 'required']) !!}
                </div>

                <div class="small-1 large-1 column comment-col">
                    <input type="file" name="files[]" id="file" multiple class="inputfile"/>
                    <label for="file"><i style="color: #2B3A40" class="fa fa-paperclip myclip"
                                         aria-hidden="true"></i></label>

                </div>
                <div class="small-1 large-1 column comment-col">
                    <a @click="showApproversField = !showApproversField"><i style="color: #2B3A40"
                                                                            class="fa fa-address-book-o myclip"
                                                                            aria-hidden="true"></i></a>
                </div>
                <div class="small-1 large-1 column mysendbox send-div">
                    {!! Form::button('<i class="fa fa-paper-plane send-icon" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'send', 'style'=> "margin-left: -5px;"])!!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection