@extends('layouts.default')
@section('content')
    <div class="content-section-header">
        <h1 class="page-title">All Public Tasks</h1>
    </div>

    <div class="content-section-text">
        <div class="row">
            <div class="small-12 large 12 columns">
                <a class="button pull-right" href="{{route('task.create')}}" style="margin-right: 15px;">Create Task</a>

                <a class="button " href="{{route('task_category.index')}}">View Category</a>

                <div class="small-12 large 12 columns">

                    <div class="row">

                        {{ Form::open() }}

                        <div class="small-2 columns">
                            {{ Form::text('title', null,['placeHolder'=>'Enter task title', 'v-model'=>'title', 'v-on:input'=>'getPublicTasks()']) }}
                        </div>
                        <div class="small-3 columns">
                            {{ Form::select('status_id',$status,null, ['v-model'=>'status', 'v-on:change'=>'getPublicTasks()' ]) }}
                        </div>
                        <div class="small-3 columns">
                            {{ Form::select('department_id',$departments,null, ['placeholder'=>'Select Department', 'v-model'=>'department', 'v-on:change'=>'getPublicTasks()' ]) }}
                        </div>
                        <div class="small-3 columns">
                            {{ Form::select('assigned_to',$users,null, ['v-model'=>'user', 'v-on:change'=>'getPublicTasks()' ]) }}
                        </div>

                        {{ Form::close() }}

                        <public-tasks-table></public-tasks-table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection