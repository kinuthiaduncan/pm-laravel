@extends('layouts.default')
@section('content')

    <div class="project-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Create Category</h3>
                    {{ Form::open(['route' => 'task_category.store']) }}
                    @include('layouts.partials.errors')

                    {{ Form::label('name','Name*') }}
                    {{ Form::text('name',NULL,['required']) }}

                    {{ Form::label('description','Category Description*') }}
                    <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                    ='content'></tinymce>
                    <br>

                    {{ Form::submit('Create', ['class' => 'button button-success'])}}

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
