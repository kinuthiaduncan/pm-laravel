@extends('layouts.default')
@section('content')

    <div style="padding: 20px">
        <div class="content-section-header">
            <h1 class="page-title">Department Categories
                <span> <a href="{{ route('task_category.create') }}" class="button pull-right margin-left-10">Add
                Categories</a> </span>
            </h1>
        </div>
        <div class="row">
            <div class="content-section-text large-12 small-12 column">
                <div class="small-12 large-3 columns pull-right">
                    {!! Form::select('department_id',$departments,null, ['placeholder'=>'Select Department', 'v-model'=>'department', 'v-on:change'=>'getTaskCategories()' ]) !!}
                </div>
                <task-category-table :user="{{Auth::user()->id}}"></task-category-table>
            </div>
        </div>
    </div>
@endsection