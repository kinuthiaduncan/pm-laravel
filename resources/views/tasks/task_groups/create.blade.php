@extends('layouts.default')
@section('content')

    <div class="project-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Create Task Group</h3>
                    {{ Form::open(['route' => 'task_group.store']) }}
                    @include('layouts.partials.errors')

                    {{ Form::label('name','Name*') }}
                    {{ Form::text('name',NULL,['required']) }}

                    {{ Form::label('details','Task Group Details*') }}
                    <tinymce name="details" id="editor" v-model="editor" :options="options" :content
                    ='content'></tinymce>
                    <br>

                    {!! Form::label('users[]', 'Add users to this group*') !!}
                    {!! Form::select('users[]',  $users, '' ,['required'=>'required', 'v-select'=>'', 'multiple']) !!}
                    <br>
                    <br>

                    {{ Form::submit('Save', ['class' => 'button button-success'])}}

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
