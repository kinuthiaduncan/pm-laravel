@extends('layouts.default')
@section('content')
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>
    <div class="project-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Edit Task Group</h3>
                    {{ Form::open(['method'=>'PUT','route' => ['task_group.update', $task_group->id]]) }}
                    @include('layouts.partials.errors')

                    {{ Form::label('name','Name*') }}
                    {{ Form::text('name',$task_group->name,['required']) }}

                    {{ Form::label('details','Task Group Details*') }}
                    {!! Form::textarea('details',$task_group->details,['rows'=> '3',  'class' => 'form-control']) !!}<br/>
                    <br>

                    {!! Form::label('users[]', 'Add users to this group*') !!}
                    {!! Form::select('users[]',  $users, $task_group_members,['required'=>'required', 'v-select'=>'', 'multiple']) !!}
                    <br>
                    <br>

                    {{ Form::submit('Save', ['class' => 'button button-success'])}}

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
