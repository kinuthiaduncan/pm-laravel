@extends('layouts.default')

@section('content')

    <div class="content-section-header">
        <h1 class="page-title">Tasks Groups
            <a class="button pull-right" href="{{route('task_group.index')}}">Back</a>
        </h1>
    </div>

    <div class="row content-section-text team-tasks">
        <div class="large-12 columns">
            <div class="task-show">
                <h3 class="task-heading">{{$task_group->name}}
                    <a class="pull-right" href="{{route('task_group.edit', $task_group->id)}}">
                        <i class="fa fa-pencil"></i>&nbsp;Edit
                    </a>
                </h3>
                <table style="margin-top: 10px">
                    <tbody>
                    <tr>
                        <td style="width: 180px">Task group details</td>
                        <td class="task-description">{!! $task_group->details !!}</td>
                    </tr>

                    <tr>
                        <td>Task Group Members</td>
                        <td>
                            @foreach($task_group_members as $member)
                                <span class="button task-member">{{$member}}</span>
                            @endforeach
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection