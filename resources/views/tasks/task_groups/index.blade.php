@extends('layouts.default')
@section('content')

    <div style="padding: 20px">
        <div class="content-section-header">
            <h1 class="page-title">My Task Groups
                <span> <a class="button pull-right" href="{{route('task_group.create')}}"
                          style="margin-right: 15px;"><strong>+</strong>
            Add Task Group</a> </span>
            </h1>
        </div>
        <div class="row">
            <div class="content-section-text large-12 small-12 column">
                <table class="responsive">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Details</th>
                        <th>Edit</th>
                        <th>View</th>
                        {{--<th>Delete</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($my_task_groups as $my_task_group)
                        <tr>
                            <td>{{$my_task_group->name}}</td>
                            <td>{!! $my_task_group->details !!}</td>
                            <td><a href="{{route('task_group.edit', [$my_task_group->id])}}">Edit</a></td>
                            <td><a href="{{route('task_group.show', [$my_task_group->id])}}">View</a></td>
                            {{--<td>--}}
                                {{--<a data-open="deleteGroup{{ $my_task_group->id }}"><i--}}
                                            {{--class="fa fa-trash"></i></a>--}}
                            {{--</td>--}}
                        </tr>

                        {{--<div id="deleteGroup{{ $my_task_group->id }}" class="reveal" data-reveal>--}}
                            {{--<div class="lead">--}}
                                {{--<button class="close-button" data-close aria-label="Close modal" type="button">--}}
                                    {{--<span aria-hidden="true">&times;</span>--}}
                                {{--</button>--}}
                                {{--<h4>Are you sure you want to delete Task Group</h4>--}}
                            {{--</div>--}}
                            {{--<div>--}}
                                {{--<div>--}}
                                    {{--{{ Form::open(['method'=>'DELETE', 'route' => ['task_group.destroy', $my_task_group->id]]) }}--}}
                                    {{--{{ Form::submit('Yes', ['class' => 'button success']) }}--}}
                                    {{--{{ Form::close() }}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <!-- Reveal Modals begin -->
                        {{--<div id="deleteGroup{{$my_task_group->id}}" class="reveal pm-modal" data-reveal>--}}
                            {{--<div class="modal-heading">--}}
                                {{--<h6>Are you sure you want to delete group?</h6>--}}
                            {{--</div>--}}
                            {{--<div class="modal-form">--}}
                                {{--<div>--}}
                                    {{--<a class="button button-success" href="{{route('task_group.index')}}">No</a>--}}
                                    {{--<a class="button button-danger" href="{{route('task_group.destroy', [$my_task_group->id])}}">Yes</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<button class="close-button" data-close aria-label="Close modal" type="button">--}}
                                {{--<span aria-hidden="true">&times;</span>--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    @empty
                        <tr>
                            <td>You have not created any task groups</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{$my_task_groups->links()}}
            </div>
        </div>
    </div>
@endsection