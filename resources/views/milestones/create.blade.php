@extends('layouts.default')
@section('content')

    <div class="milestone-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="milestone-create-form">
                    <h3>Create a milestone</h3>
                    {!! Form::open(['route' => 'create_milestone']) !!}
                    @include('layouts.partials.errors')

                    <div class="form-group">
                        {!! Form::hidden('project_id', $project->id) !!}
                        {!! Form::label('title','Milestone Title*') !!}
                        {!! Form::text('title',NULL,['class' => 'form-control','required']) !!}
                        {!! Form::label('description','Milestone Description*') !!}
                        <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                        ='content'></tinymce>
                        <br>
                        {!! Form::label('due_date','Due Date*') !!}
                        {!! Form::text('due_date', NULL, array('class' => 'form-control date', 'required')) !!}
                        {!! Form::submit('Create', ['class' => 'button button-success'])!!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop
