@extends('layouts.default')
@section('content')

    <div class="milestone-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="milestone-create-form">
                    <h3>Edit milestone</h3>
                    {!! Form::open(['route' => 'update_milestone']) !!}
                    @include('layouts.partials.errors')

                    <div class="form-group">
                        {!! Form::hidden('project_id', $project->id) !!}
                        {!! Form::hidden('milestone_id', $milestone->id) !!}
                        {!! Form::label('title','Milestone Title*') !!}
                        {!! Form::text('title',$milestone->title,['class' => 'form-control','required']) !!}
                        {!! Form::label('description','Milestone Description*') !!}
                        <tinymce name="description" id="editor" v-model="editor" :options="options" content
                        ={{$milestone->description}}></tinymce>
                        <br>
                        {!! Form::label('due_date','Due Date*') !!}
                        {!! Form::text('due_date', $milestone->due_date, array('class' => 'form-control date', 'required')) !!}
                        {!! Form::submit('Edit', ['class' => 'button'])!!}
                        &nbsp;&nbsp; <a class="button" href="/project/{!! $project->id !!}">Cancel</a>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop
