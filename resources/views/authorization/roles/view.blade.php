@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="small-6 large-8 columns">
            <h4 class="perm-title">User Role</h4>
        </div>
        <div class="small-3 large-2 column">
            <a href="{{url('authorization')}}" class="button">Back</a>
        </div>
        <div class="small-3 large-2 columns">
            <a href="{{url('/users/role-summary')}}" class="button">User Role Report</a>
        </div>
        <div class="small-12 large-8 perm-create-form">
            {!! Form::open(['url' => '/roles/'.$user->id,'method' => 'PUT']) !!}
            <br>
            @include('layouts.partials.errors')
            {!! Form::label('name','User') !!}
            {!! Form::text('name',$user->preferred_name,['required','class'=>'task-title','readonly']) !!}
            {!! Form::label('role_id', 'Assign Role')!!}
            {!! Form::select('role_id', $roles, $user->roles->name ,['required'=>'required'])!!}
            <br/>
            {!! Form::submit('Edit User Role', ['class' => 'submit-task button button-success'])!!}
            {!! Form::close() !!}
        </div>
    </div>
@stop