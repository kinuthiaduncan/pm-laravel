@section('content')
    <ul class="tabs" data-tabs id="p-tabs">
        <li class="tabs-title  is-active"><a href="#permissions">Permissions</a></li>
        <li class="tabs-title"><a href="#roles" aria-selected="true">Roles</a></li>
    </ul>
    <div class="tabs-content" data-tabs-content="p-tabs">
        <div class="tabs-panel is-active" id="permissions">
            @yield('permissions')
        </div>
        <div class="tabs-panel" id="roles">
            @yield('roles')
        </div>
    </div>
@endsection