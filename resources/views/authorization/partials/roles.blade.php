<div class="row">
    <div class="small-9 large-10 columns" style="padding: 1%;">
        <h4>Roles</h4>
    </div>
    <div class="small-12 large-12 columns" style="padding: 1%;">
        <table class="table responsive">
            <thead>
            <th>Name</th>
            <th>Description</th>
            </thead>
            <tbody>
            @foreach($roles as $role)
                <tr>
                    <td>{{$role->role}}</td>
                    <td>{!! $role->description !!}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoor>
                <tr><td>{{$roles->links()}}</td></tr>
            </tfoor>
        </table>
    </div>
</div>