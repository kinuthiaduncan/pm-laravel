<div class="row">
<div class="small-9 large-10 columns">
    <h4>Permissions</h4>
</div>
    <div class="small-12 large-12 columns">
        <table class="table responsive">
            <thead>
            <th>Name</th>
            <th>Description</th>
            </thead>
            <tbody>
            @foreach($permissions as $permission)
            <tr>
            <td>{{$permission->name}}</td>
            <td>{!! $permission->description !!}</td>
          </tr>
                @endforeach
            </tbody>
            <tfoor>
                <tr><td>{{$permissions->links()}}</td></tr>
            </tfoor>
        </table>
    </div>
</div>