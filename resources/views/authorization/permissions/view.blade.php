@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="small-9 large-10 columns">
            <h4 class="perm-title">{{$user->preferred_name}} Permissions</h4>
        </div>
        <div class="small-3 large-2 column">
            <a href="{{url('authorization')}}" class="button">Back</a>
        </div>
        <div class="small-12 large-12 column">
            <table class="responsive table">
                <thead>
                <th>Permission Name</th>
                <th>Description</th>
                <th>Action</th>
                </thead>
                <tbody>
                @foreach($permissions as $permission)
                    <tr>
                        <td>{{$permission->permissions->name}}</td>
                        <td>{!! $permission->permissions->description !!}</td>
                        <td><a href="{{url('/delete/'.$permission->id)}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot class="link_footer">
                <tr><td>{{ $permissions->render() }}</td></tr>
                </tfoot>
            </table>
            <br />
            <h4 class="perm-title">Assign Permissions to {{$user->preferred_name}}</h4>

            <div class="small-12 large-8 column perm-create-form" style="float: left;">
                    {!! Form::open(['url' => 'permissions-assign']) !!}
                    <br>
                    @include('layouts.partials.errors')
                    {!! Form::hidden('id',$user->id,['required']) !!}
                    {!! Form::label('permission_id','Select Permission to Assign') !!}
                    {!! Form::select('permission_id', $all_permissions , null ,['class' => 'form-control','required'])!!}
                    {!! Form::submit('Assign Permission', ['class' => 'button button-success'])!!}
                     {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop