@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="small-9 large-10 columns">
            <h4 class="perm-title">Permissions</h4>
        </div>
        <div class="small-3 large-2 column">
            <a href="{{url('authorization')}}" class="button">Back</a>
        </div>
        <div class="small-12 large-8 perm-create-form">
            {!! Form::open(['url' => 'permission_store', 'files'=>true]) !!}
            <br>
        @include('layouts.partials.errors')
            {!! Form::label('name','Name*') !!}
            {!! Form::text('name',NULL,['required','placeholder'=>'Permission Name','class'=>'task-title']) !!}
            <div class="tinymce-label">
                {!! Form::label('description','Permission Description*') !!}
                <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                ='content'></tinymce>
                <br>
                {!! Form::submit('Create Permission', ['class' => 'submit-task button button-success'])!!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop