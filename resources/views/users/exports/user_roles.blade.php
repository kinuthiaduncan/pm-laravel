@extends('users.exports.issue_pdf')

@section('content')
    <h3 style="text-align: center; font-size: 16px;"> PM System Active User Roles </h3>
    <br />
    <table class="table">
        <thead>
        <tr>
            <td>Name</td>
            <td>Job Title</td>
            <td>Role</td>
        </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{!! $user->preferred_name !!}</td>
                    <td>{!! $user->job_title !!}</td>
                    <td>
                        {!! $user->roles->role !!}
                        @if(in_array($user->id,$supervisors))
                            ,<span> Supervisor</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection