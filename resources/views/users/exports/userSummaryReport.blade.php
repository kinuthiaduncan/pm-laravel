@extends('layouts.pdf')

@section('content')
<h3 class="title"style="text-align: center;">{{$data['user']->preferred_name}} ~ SUMMARY REPORT</h3>
@if($data['user']->department_id == 2)
    <p class="my-intro">Team Issues</p>
    <table class="table">
        <tr>
            <td>{{$data['team_total_issues']}}<p>Total Issues</p></td>
            <td>{{$data['team_completed_issues']}}<p>Solved Issues</p></td>
            <td>{{$data['team_ongoing_issues']}}<p>Ongoing Issues</p></td>
            <td>{{$data['team_review_issues']}}<p>Issues Under Review</p></td>
            <td>{{$data['team_pending_issues']}}<p>Pending Issues</p></td>
            <td>{{$data['team_overdue_issues']}}<p>Overdue Issues</p></td>
        </tr>
    </table>
    <p class="my-intro">My Issues</p>
    <table class="table">
        <tr>
            <td>{{$data['total_issues']}}<p>Total Issues</p></td>
            <td>{{$data['completed_issues']}}<p>Solved Issues</p></td>
            <td>{{$data['ongoing_issues']}}<p>Ongoing Issues</p></td>
            <td>{{$data['review_issues']}}<p>Issues Under Review</p></td>
            <td>{{$data['pending_issues']}}<p>Pending Issues</p></td>
            <td>{{$data['overdue_issues']}}<p>Overdue Issues</p></td>
        </tr>
    </table>
@else
    <p class="my-intro">Team Tasks</p>
    <table class="table">
        <tr>
            <td>{{$data['team_total_tasks']}}<p>Total Tasks</p></td>
            <td>{{$data['team_completed_tasks']}}<p>Completed Tasks</p></td>
            <td>{{$data['team_ongoing_tasks']}}<p>Ongoing Tasks</p></td>
            <td>{{$data['team_review_tasks']}}<p>Tasks Under Review</p></td>
            <td>{{$data['team_pending_tasks']}}<p>Pending Tasks</p></td>
            <td>{{$data['team_overdue_tasks']}}<p>Overdue Tasks</p></td>
        </tr>
    </table>
    <p class="my-intro">My Tasks</p>
    <table class="table">
        <tr>
            <td>{{$data['total_tasks']}}<p>Total Tasks</p></td>
            <td>{{$data['completed_tasks']}}<p>Completed Tasks</p></td>
            <td>{{$data['ongoing_tasks']}}<p>Ongoing Tasks</p></td>
            <td>{{$data['review_tasks']}}<p>Tasks Under Review</p></td>
            <td>{{$data['pending_tasks']}}<p>Pending Tasks</p></td>
            <td>{{$data['overdue_tasks']}}<p>Overdue Tasks</p></td>
        </tr>
    </table>
@endif
@endsection