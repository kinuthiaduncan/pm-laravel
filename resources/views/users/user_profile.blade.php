@extends('layouts.default')
@section('content')
    <div class="content-section-header">
        <div class="small-12 column">
            <h1 class="page-title">{{$user->name}}'s Profile
                <a class="button pull-right hide-for-small-only" href="{{route('task.create')}}"
                   style="margin-right: 15px;"><strong>+</strong>
                    Add Task</a>
            </h1>
        </div>
        <div class="row">
            <div class="small-6 large-4 column hide-for-small-only">
                <a href="{{url('export-summary', [$user->id])}}" class="button pull-right">
                    Export User Summary</a>
            </div>
            <div class="small-6 large-4 columns">
                <a href="{{url('export-daily-summary', [$user->id])}}" class="button pull-right">
                    Export Daily Summary</a>
            </div>
            <div class="small-6 large-4 columns">
                <a href="{{url('export-weekly-summary', [$user->id])}}" class="button  pull-right" style="margin-right: 10px;">
                    Export Weekly Summary</a>
            </div>
        </div>
        <div class="small-12 column show-for-small-only">
            <a class="button pull-right" href="{{route('task.create')}}"
            ><strong>+</strong>
                Add Task</a>
            <a href="{{url('export-summary', [$user->id])}}" class="button" style="margin-left: 57px;">
                Export User Summary</a>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <user-profile :users="{{json_encode($users)}}" :user_id="{{$id}}"></user-profile>
        </div>
    </div>
@endsection




{{--<div class="user-summary">--}}
{{--<div class="row">--}}
{{--<div class="small-3 medium-3 column" style="padding-left: 0;">--}}
{{--@if($user->avatar == null)--}}
{{--<div class="prof-pic">--}}
{{--<i class="fa fa-user-circle profile-avatar" aria-hidden="true"></i>--}}
{{--</div>--}}
{{--@else--}}
{{--<div>--}}
{{--<img src="{{$user->avatar}}" class="profile-avatar">--}}
{{--</div>--}}
{{--@endif--}}
{{--</div>--}}
{{--<div class="small-3 medium-3 column profile-details">--}}
{{--<p class="prof-name">{{$user->firstname}} {{$user->lastname}}</p>--}}
{{--<p class="gen-info">General Information</p>--}}
{{--<table class="profile-info-table">--}}
{{--<tr>--}}
{{--<th>Email:</th>--}}
{{--<td>{{$user->email}}</td>--}}
{{--</tr>--}}
{{--<tr>--}}
{{--<th>Phone:</th>--}}
{{--<td>{{$user->phone_number}}</td>--}}
{{--</tr>--}}
{{--<tr>--}}
{{--<th>Department:</th>--}}
{{--<td>{{$user->department}}</td>--}}
{{--</tr>--}}
{{--<tr>--}}
{{--<th>Job Title:</th>--}}
{{--<td>{{$user->job_level}}</td>--}}
{{--</tr>--}}
{{--</table>--}}
{{--</div>--}}
{{--<div class="small-6 columns"></div>--}}

{{--@if($user->department_id==2)--}}
{{--<div class="small-3 medium-3 columns">--}}
{{--<summary-pie-chart :height="200" :width="200" :data="ct_pie_data"--}}
{{--:options="pie_options"></summary-pie-chart>--}}
{{--</div>--}}
{{--<div class="small-3 medium-3 columns">--}}

{{--<summary-pie-chart :height="200" :width="200" :data="pie_data"--}}
{{--:options="pie_options"></summary-pie-chart>--}}
{{--</div>--}}
{{--@else--}}
{{--<div class="small-6 medium-5 columns">--}}
{{--<summary-pie-chart :height="200" :width="200" :data="pie_data"--}}
{{--:options="pie_options"></summary-pie-chart>--}}
{{--</div>--}}
{{--@endif--}}
{{--</div>--}}

{{--<div class="row summary-data">--}}
{{--@if($user->department_id==2)--}}
{{--<div class="small-12 columns">--}}
{{--<h6>My Tasks</h6>--}}
{{--<div class="summary-values">--}}
{{--<div class="small-2 columns">--}}
{{--<h6>{{$total_tasks}}</h6>--}}
{{--<p>Total Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns summary-values-center">--}}
{{--<h6>{{$completed_tasks}}</h6>--}}
{{--<p>Solved Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns">--}}
{{--<h6>{{$review_tasks}}</h6>--}}
{{--<p>In-Review Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns summary-values-center">--}}
{{--<h6>{{$ongoing_tasks}}</h6>--}}
{{--<p>Ongoing Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns summary-values-center" style="border-left: none">--}}
{{--<h6>{{$pending_tasks}}</h6>--}}
{{--<p>Pending Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns">--}}
{{--<h6>{{$overdue_tasks}}</h6>--}}
{{--<p>Overdue Tasks</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<br>--}}
{{--<div class="small-12 columns">--}}
{{--<h6>My Issues</h6>--}}
{{--<div class="summary-values">--}}
{{--<div class="small-2 column">--}}
{{--<h6>{{$total_issues}}</h6>--}}
{{--<p>Total Issues</p>--}}
{{--</div>--}}
{{--<div class="small-2 column summary-values-center">--}}
{{--<h6>{{$completed_issues}}</h6>--}}
{{--<p>Solved Issues</p>--}}
{{--</div>--}}
{{--<div class="small-2 column">--}}
{{--<h6>{{$review_issues}}</h6>--}}
{{--<p>In-Review Issues</p>--}}
{{--</div>--}}
{{--<div class="small-2 column summary-values-center">--}}
{{--<h6>{{$ongoing_issues}}</h6>--}}
{{--<p>Ongoing Issues</p>--}}
{{--</div>--}}
{{--<div class="small-2 column summary-values-center" style="border-left: none">--}}
{{--<h6>{{$pending_issues}}</h6>--}}
{{--<p>Pending Issues</p>--}}
{{--</div>--}}
{{--<div class="small-2 column">--}}
{{--<h6>{{$overdue_issues}}</h6>--}}
{{--<p>Overdue Issues</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--@else--}}
{{--<div class="small-12 columns">--}}
{{--<h6>Team Tasks</h6>--}}
{{--<div class="summary-values">--}}
{{--<div class="small-2 columns">--}}
{{--<h6>{{$team_total_tasks}}</h6>--}}
{{--<p>Total Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns summary-values-center">--}}
{{--<h6>{{$team_completed_tasks}}</h6>--}}
{{--<p>Solved Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns">--}}
{{--<h6>{{$team_review_tasks}}</h6>--}}
{{--<p>In-Review Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns summary-values-center">--}}
{{--<h6>{{$team_ongoing_tasks}}</h6>--}}
{{--<p>Ongoing Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns summary-values-center" style="border-left: none">--}}
{{--<h6>{{$team_pending_tasks}}</h6>--}}
{{--<p>Pending Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns">--}}
{{--<h6>{{$team_overdue_tasks}}</h6>--}}
{{--<p>Overdue Tasks</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<br>--}}
{{--<div class="small-12 columns">--}}
{{--<h6>My Tasks</h6>--}}
{{--<div class="summary-values">--}}
{{--<div class="small-2 columns">--}}
{{--<h6>{{$total_tasks}}</h6>--}}
{{--<p>Total Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns summary-values-center">--}}
{{--<h6>{{$completed_tasks}}</h6>--}}
{{--<p>Solved Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns">--}}
{{--<h6>{{$review_tasks}}</h6>--}}
{{--<p>In-Review Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns summary-values-center">--}}
{{--<h6>{{$ongoing_tasks}}</h6>--}}
{{--<p>Ongoing Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns summary-values-center" style="border-left: none">--}}
{{--<h6>{{$pending_tasks}}</h6>--}}
{{--<p>Pending Tasks</p>--}}
{{--</div>--}}
{{--<div class="small-2 columns">--}}
{{--<h6>{{$overdue_tasks}}</h6>--}}
{{--<p>Overdue Tasks</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--@endif--}}
{{--</div>--}}