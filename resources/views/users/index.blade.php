@extends('layouts.default')

@section('content')
    <ul class="tabs" data-tabs id="p-tabs">
        <li class="tabs-title is-active"><a href="#users" aria-selected="true">People</a></li>
        <li class="tabs-title"><a href="#tasks">Tasks</a></li>
    </ul>
    <div class="tabs-content content-section-text" data-tabs-content="p-tabs">
        <div class="tabs-panel is-active" id="users">
            <div class="row">
                <div class="large-11 large-centered columns">
                    <users :user_role = "{{Auth::user()->role_id}}" :departments="{{json_encode($departments)}}"></users>
                </div>
            </div>
        </div>
        <div class="tabs-panel" id="tasks">
            <div class="row">
                <div class="large-11 large-centered columns">
                    <followed-tasks></followed-tasks >
                </div>
            </div>
        </div>
    </div>
@endsection