@extends('layouts.default')
@section('content')
    <div class="project-create">
        <div class="row">
            <div class="small-11 large-11 columns">
                <a href="{{ url('/') }}" class="button pull-right">Back</a>
            </div>
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h5>Give feedback on any issues with the system </h5>
                    {!! Form::open(['route' => ['feedback.store',$project->id], 'files'=>true]) !!}
                    <br>
                    {!! Form::label('subject','Subject*') !!}
                    {!! Form::text('subject',NULL,['required','placeholder'=>'Feedback Subject','class'=>'task-title']) !!}
                    <div class="tinymce-label">
                        {!! Form::label('description','Feedback Description*') !!}
                        <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                        ='content'></tinymce>
                        <br>
                    </div>
                    {!! Form::label('files[]', 'Upload file') !!}
                    {!! Form::file('files[]',['multiple'=>true]) !!}
                    {!! Form::submit('Send Feedback', ['class' => 'submit-task button button-success'])!!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop