@extends('layouts.default')

@section('content')
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>
    <div class="issue-create">
        <div class="row">
            {!! Form::open(['route' => 'create_issue', 'files' => 'true']) !!}
            {{ Form::hidden('project_id', $project_id) }}
            {{ Form::hidden('component_id', $component_id) }}
            <div class="small-12 medium-6 columns" style="padding: 10px;">
                <div class="form-group">
                    {!! Form::label('title','Issue Title*') !!}
                    {!! Form::text('title', $feedback->subject,['class' => 'form-control','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description','Issue Description') !!}
                    {!! Form::textarea('description',$feedback->description,['rows'=> '3',  'class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    @if(count($feedbackFile) > 0 )
                        {!! Form::hidden('image', $feedbackFile->id,['class' => 'form-control','readonly'=>'true']) !!}
                    @endif
                </div>
            </div>
            <div class="small-12 medium-6 columns" style="padding: 10px;">
                <div class="form-group">
                    {!! Form::label('assigned_to','Assigned To*') !!}
                    {!! Form::select('assigned_to', $users , $lead ,['required', 'v-select'=>''])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('issue_type','Issue Type*') !!}
                    {!! Form::select('issue_type', $issue_type , 2 ,['class' => 'form-control','required'])!!}
                </div>

                <div class="form-group">
                    {!! Form::label('priority_id','Priority Type*') !!}
                    {!! Form::select('priority_id', $priorities , 3 ,['class' => 'form-control','required'])!!}
                </div>

                <div class="form-group">
                    {!! Form::label('date_due','Due Date*') !!}
                    {!! Form::text('date_due', Carbon\Carbon::today()->format('Y-m-d'), array('class' => 'form-control date', 'required')) !!}
                </div>
            </div>
            <div class="small-12 columns">
                <div class="form-group">
                    {!! Form::submit('Save', ['class' => 'button'])!!}&nbsp;&nbsp;
                    <a class="button pull-right" href=" /project/{!! $project_id !!}/feedback/{!! $feedback->id !!}">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
