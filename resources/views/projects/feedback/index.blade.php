@extends('layouts.default')
@section('content')
<div class="row">
    <div class="small-12 large-11 large-centered column">
        <h5 class="page-title">User Feedback on {{$project->name}}
        <a href="{{url('/project/'.$project->id)}}" class="button pull-right">Back</a>
        </h5>
    </div>
    <div class="content-section-text large-centered large-11 small-12 column">
        <table class="responsive">
            <thead>
            <th>Subject</th><th>Description</th><th colspan="2" style="text-align: center">Action</th>
            </thead>
            <tbody>
                @foreach($feedback as $data)
                    <tr>
                        <td>{!! $data->subject !!}</td>
                        <td>{!! $data->description !!}</td>
                        <td><a href="{{route('feedback.show',[$project->id,$data->id])}}"><i class="fa fa-eye pull-right" aria-hidden="true"></i></a></td>
                        <td><a href="{{url('/feedback/'.$data->id.'/delete')}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div style="background-color: #ffffff; text-align: right;">
            {{ $feedback->links() }}
        </div>
    </div>
</div>
@endsection