@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="small-12 large-11 large-centered column">
            <h5 class="page-title">Details
                <a href="{{route('feedback.index',$project)}}" class="button pull-right">Back</a>
            </h5>
        </div>
        <div class="small-12 large-11 content-section-text large-centered column">
            <table>
                <thead>
                    <tr>
                        <td><b>Subject</b></td>
                        <td>{!! $feedback->subject !!}</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><b>Created By</b></td>
                        <td>{!! $feedback->client_email !!}</td>
                    </tr>
                    <tr>
                        <td><b>Description</b></td>
                        <td>{!! $feedback->description !!}</td>
                    </tr>
                    @if($file_id)
                        <tr>
                            <td><b>Attachments</b></td>
                            <td><a href="/files/{{ $file_id }}">View attachment</a></td>
                        </tr>
                    @endif
                </tbody>
                <tfoot style="background-color: #f3f3f3; text-align: center">
                <tr><td colspan="2" style="text-align: center">
                        <ul class="dropdown menu" style="margin-left: 37%;" data-dropdown-menu>
                            <li>
                                <a class="button">Add as an Issue on PM</a>
                                <ul class="menu">
                                    @foreach($project_components as $project_component)
                                        <li><a style="font-size: 0.75rem;" href="{!! url('/feedback/'.$feedback->id.'/project/'.$project.'/issue/'.$project_component->id) !!}"
                                               class="transitions"> {!! $project_component->component_name !!} </a></li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </td></tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection