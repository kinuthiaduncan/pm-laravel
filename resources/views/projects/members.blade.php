@extends('layouts.default')

@section('content')

    <div class="project-members content-section-text">
        <div class="row">
            <div class="large-12 columns">
                <h3>Add Project Members</h3>
                {!! Form::open(['route' => 'add_members']) !!}
                @include('layouts.partials.errors')

                <div class="form-group">
                    {!! Form::hidden('project_id',$project_id) !!}
                    @if(!$none_members)
                        <p>There are no users available to add to this project</p>
                        <a class="button success" href="/project/{!! $project_id !!}">Go Back</a>
                    @elseif($none_members)

                        {!! Form::label('users[]', 'Type the name of the user*') !!}
                        {!! Form::select('users[]',  $none_members, '' ,['required'=>'required', 'v-select'=>'', 'multiple']) !!}
                        <br>
                        <br>
                </div>
                {!! Form::submit('Add', ['class' => 'button button-success'])!!} &nbsp;
                <a class="button" href="/project/{!! $project_id !!}">Cancel</a>
                {!! Form::close() !!}
                @endif
            </div>
        </div>
    </div>
@stop