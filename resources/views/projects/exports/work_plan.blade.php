@extends('layouts.pdf')

@section('content')
    <h3 class="title">{{$data['phase']->name}} Work Plan</h3>
    <table class="table">
        <thead class="summary">
        <tr>
            <th>Project</th>
            <th>Work Plan Title</th>
            <th>Start Date</th>
            <th>Due Date</th>
            <th>Percent Complete</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{!! $data['phase']->project->name !!}</td>
            <td>{!! $data['phase']->name !!}</td>
            <td>{!! $data['phase']->start_date !!}</td>
            <td>{!! $data['phase']->end_date !!}</td>
            <td>{!! \PM\Presenters\ProjectPhasePresenter::phasePercentDone($data['phase']->id) !!}%</td>
        </tr>
        </tbody>
    </table>
    <br />
    @if($data['activities']->count() > 0)
        <h3 class="title">Activities under the Work Plan:</h3>
        <table class="table">
            <thead class="summary">
            <tr>
                <th>Issue Title</th>
                <th>Due Date</th>
                <th>Days Left</th>
                <th>Percent Complete</th>
                <th>Assigned To</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data['activities'] as $issue)
                <tr>
                    <td>{!! $issue->title !!}</td>
                    <td>{!! $issue->date_due !!}</td>
                    <td>{!! \PM\Presenters\ProjectPhasePresenter::timeLeft($issue->date_due) !!} days</td>
                    <td>{!! $issue->percentage_done !!}%</td>
                    <td>{!! \PM\Presenters\UserPresenter::presentFullNames($issue->assigned_to) !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>This work plan has no issues</p>
    @endif
@stop