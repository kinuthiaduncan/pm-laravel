
<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <style TYPE="text/css" MEDIA=screen>
       body {
           background: #f0f0f5;
       }

    </style>

</head>
<body>
<div >
    <div class="widget-body no-padding">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Project Description</th>
                <th>Status</th>
                <th>Priority</th>
                <th>Resources Required</th>
                <th>Resources Assigned</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)

                <tr><td colspan="6" style="background-color: #006666; color: #ffffff;  text-transform: capitalize;"><strong> {!! $category->name !!}</strong></td></tr>
                <tr><td colspan="6" style="background-color: #006666; color: #ffffff;  text-transform: capitalize;"><h5> {!! $category->description !!} </h5></td></tr>

                @foreach($projects as $project)
                    @if($project->category_id == $category->id)
                        <tr>
                            <td>{!! $project->name !!}</td>
                            <td>{!! $project->project_description !!}</td>
                            <td>{!! $project->status !!}</td>
                            <td>{!! $project->priority !!}</td>
                            <td>{!! $project->resources_required!!}</td>
                            <td>{!! $project->resources_assigned!!}</td>
                        </tr>
                    @endif
                @endforeach
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
</html>