@extends('users.exports.issue_pdf')

@section('content')
    <h3 style="text-align: center; font-size: 16px;"> Project Work Plan Summary for {{$start}} to {{$end}} </h3>
    <br />
    @if($summaries->count())
        <table class="table">
            <thead>
            <tr>
                <td>Project</td>
                <td>WorkPlan Title</td>
                <td>Start Date</td>
                <td> Due Date</td>
                <td>Percent Complete</td>
            </tr>
            </thead>
            <tbody>
            @foreach($summaries as $summary)
                <tr>
                    <td>{!! $summary->project->name !!}</td>
                    <td>{!! $summary->name !!}</td>
                    <td>{!! $summary->start_date !!}</td>
                    <td>{!! $summary->end_date !!}</td>
                    <td style="text-align: center;">{!! \PM\Presenters\ProjectPhasePresenter::phasePercentDone($summary->id) !!}%</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
        <p>No Work Plans for the selection</p>
    @endif
@endsection