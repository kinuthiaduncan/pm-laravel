@extends('layouts.default')
@section('content')

    <div class="project-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Edit Project</h3>
                    {!! Form::open(['route' => 'edit_project']) !!}
                    @include('layouts.partials.errors')

                    <div class="form-group">
                        {!! Form::hidden('project_id', $project->id) !!}
                        {!! Form::label('name','Project Name*') !!}
                        {!! Form::text('name',$project->name,['class' => 'form-control','required']) !!}

                        <div class="form-group">
                            <div class="small-12 medium-6 columns" style="padding-left: 0">
                                {!! Form::label('project_access','Project Access*') !!}
                                {!! Form::select('project_access', ['public'=>'Public', 'private'=>'Private'] ,$project->project_access ,['placeholder'=>'Please select project access level', 'class' => 'form-control', 'required'])!!}
                            </div>
                            <div class="small-12 medium-6  columns">
                                {!! Form::label('project_type','Project Type*') !!}
                                {!! Form::select('project_type', $project_types , $project->project_type ,['class' => 'form-control','required'])!!}
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        {!! Form::label('project_description','Project Description') !!}
                        <tinymce name="project_description" id="editor" v-model="editor" :options="options" content
                        ={{$project->project_description}}></tinymce>
                        <br>
                        {!! Form::label('project_url','Project Url*') !!}
                        {!! Form::text('project_url',$project->project_url,['class' => 'form-control','required']) !!}
                        {!! Form::label('project_lead','Project Lead*') !!}
                        {!! Form::select('project_lead', $users , $project->project_lead ,['class' => 'form-control','required'])!!}
                        {!! Form::label('category_id','Project Category') !!}
                        {!! Form::select('category_id', $categories , $project->category_id,[ 'class' => 'form-control'])!!}

                        <div class="form-group">
                            <div class="small-12 medium-6 columns" style="padding-left: 0">
                                {!! Form::label('status','Status*') !!}
                                {!! Form::select('status', ['concept'=>'Concept', 'In Progress'=>'In Progress', 'Deployed'=>'Deployed', 'Complete'=>'Complete'] ,$project->status ,['placeholder'=>'Please select current project status', 'class' => 'form-control', 'required'])!!}
                            </div>
                            <div class="small-12 medium-6  columns">
                                {!! Form::label('priority',' Priority*') !!}
                                {!! Form::select('priority', [ 'high'=>'High', 'medium'=>'Medium', 'low'=>'Low'] ,$project->priority ,['class' => 'form-control', 'placeholder'=>'Please select project priority','required'])!!}
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        {!! Form::label('resources_required','Resources Required*') !!}
                        {!! Form::text('resources_required',$project->resources_required,['class' => 'form-control','required']) !!}
                        {!! Form::label('resources_assigned','Resources Assigned*') !!}
                        {!! Form::text('resources_assigned',$project->resources_assigned,['class' => 'form-control','required']) !!}

                        {!!Form::submit('Edit', ['class' => 'button'])!!}
                        &nbsp;&nbsp; <a class="button" href="/projects">Cancel</a>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop
