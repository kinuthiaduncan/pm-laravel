@extends('layouts.default')
@section('content')
    <div class="row content-section-header">
        <div class="small-8 large-8 column">
            <h1 style="padding-left: 0" class="page-title">{!! $project->name !!} Project Work Plans</h1>
        </div>
        <div class="small-4 large-4 column">
            <a href="{{url('/project/'.$project->id.'/board')}}" class="button pull-right">Back</a>
            <a href="{{route('phase.create',$project->id)}}" class="button pull-right"><strong>+</strong> Add New Work Plan</a>
        </div>
    </div>
    <div class="row">
        <div class="content-section-text large-12 small-12 column">
            @if($phases->count() > 0)
                <table>
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Start Date</th>
                        <th>Due Date</th>
                        <th style="text-align: center;">Percentage Complete</th>
                        <th>Description</th>
                        <th>Created By</th>
                        <th>Approval Status</th>
                        <th style="text-align: center;" colspan="3">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($phases as $phase)
                        <tr>
                            <td>{!! $phase->name !!}</td>
                            <td>{!! $phase->start_date !!}</td>
                            <td>{!! $phase->end_date !!}</td>
                            <td style="text-align: center;">{!! \PM\Presenters\ProjectPhasePresenter::phasePercentDone($phase->id) !!}%</td>
                            <td>@if(!is_null($phase->decisions))
                                {!! $phase->decisions !!}@endif
                            </td>
                            <td>{!! \PM\Presenters\UserPresenter::presentFullNames($phase->created_by) !!}</td>
                            <td>{!! \PM\Presenters\ProjectPhasePresenter::phaseApprovalStatus($phase->id) !!}</td>
                            <td>
                                    <span><a href="{{route('phase.show',[$project->id,$phase->id])}}"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
                            </td>
                            <td>@if($phase->created_by == Auth::id() || $project->project_lead == Auth::id())
                                <a href="{{route('project_phase.delete',$phase->id)}}"><i class="fa fa-trash" aria-hidden="true"></i></a>@endif</td>
                            <td>@if($phase->created_by == Auth::id() || $project->project_lead == Auth::id())
                                <a href="{{route('phase.edit',[$project->id,$phase->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>@endif</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="text-align: right">
                    {!! $phases->links() !!}
                </div>
            @else
                <p style="padding:1px 1px 1px 7px;">No Project Phases</p>
            @endif
        </div>
    </div>
@endsection