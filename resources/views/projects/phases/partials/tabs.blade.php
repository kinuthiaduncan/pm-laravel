@section('content')
    <ul class="tabs" data-tabs id="p-tabs">
        <li class="tabs-title  is-active"><a href="#new">New</a></li>
        <li class="tabs-title"><a href="#updated" aria-selected="true">Updates</a></li>
    </ul>
    <div class="tabs-content" data-tabs-content="p-tabs">
        <div class="tabs-panel is-active" id="new">
            @yield('new')
        </div>
        <div class="tabs-panel" id="updated">
            @yield('updated')
        </div>
    </div>
@endsection