<div class="export-modal-content">
    <h4>Export Project Work plan Summary</h4>
    {!! Form::open(['route'=>'project_phase.summary']) !!}
    <div class="small-12 large-12 columns">
        {!! Form::label('project_id', 'Select Projects to Export') !!}
        {!! Form::select('project_id[]', $projectSelect,'' ,['required'=>'required', 'v-select'=>'', 'multiple'])!!}
    </div>
    <div class="small-12 large-12 columns">
        <h6>Select the Date Range (optional)</h6>
    </div>
    <div class="small-12 large-6 columns">
        {!! Form::label('start_date', 'Start Date') !!}
        {!! Form::text('start_date', '', ['required'=>'required','class'=>'filterdate']) !!}
    </div>
    <div class="small-12 large-6 columns">
        {!! Form::label('end_date', 'End Date') !!}
        {!! Form::text('end_date', '', ['required'=>'required','class'=>'filterdate']) !!}
    </div>
    <div class="small-12 large-12 submit columns">
        {!! Form::submit('Export Project Work Plans',['class'=>'button']) !!}
    </div>
    {!! Form::close() !!}
</div>
