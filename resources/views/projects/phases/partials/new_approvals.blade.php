<div class="content-section-header">
    <h1 class="page-title" style="padding-left: 0">New Project Work Plans Awaiting Approval
        <a class="button pull-right" href="{{route('projects_index')}}" style="margin-right: 15px;">Back</a>
    </h1>
    <div class="content-section-text small-12 columns">
        <div class="row">
            <new-approvals></new-approvals>
        </div>
    </div>
</div>