@extends('layouts.default')
@section('content')
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>

    <div class="row content-section-text team-tasks">
        <div class="small-11 large-11 columns content-section-header">
            <h1 class="page-title">Edit {{$phase->name}} Project Phase</h1>
        </div>
        <div class="small-1 large-1 columns">
            <a href="{{route('phase.index',$project->id)}}" class="button pull-left">Back</a>
        </div>
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    {!! Form::open(['method'=>'PUT','route'=>['phase.update',$project->id,$phase->id]]) !!}
                    <br>
                    @include('layouts.partials.errors')
                    {!! Form::label('title','Title*') !!}
                    {!! Form::text('title',$phase->name,['placeholder'=>'Work Plan Title','required']) !!}
                    <div class="task-form-controls">
                        <div class="row create-task">
                            {!! Form::label('start_date','Start Date*') !!}
                            {!! Form::text('start_date',$phase->start_date,['required','placeholder'=>'Start Date','class'=>'due-date']) !!}
                        </div>
                        <div class="row create-task">
                            {!! Form::label('end_date','Due Date*') !!}
                            {!! Form::text('end_date',$phase->end_date,['required','class'=>'due-date']) !!}
                        </div>
                        <div class="tinymce-label">
                            {!! Form::label('decisions','Key Decisions Required') !!}
                            {!! Form::textarea('decisions',$phase->decisions,['rows'=> '3',  'class' => 'form-control']) !!}
                            <br/>
                            <br>
                        </div>
                    </div>
                    {!! Form::submit('Request Approval', ['class' => 'submit-task button button-success'])!!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection