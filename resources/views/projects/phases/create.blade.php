@extends('layouts.default')
@section('content')
    <div class="row content-section-text team-tasks">
        <div class="small-11 large-11 columns content-section-header">
            <h1 class="page-title">Add {{$project->name}} Project Phase</h1>
        </div>
        <div class="small-1 large-1 columns">
            <a href="{{route('phase.index',$project->id)}}" class="button pull-left">Back</a>
        </div>
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    {!! Form::open(['route'=>['phase.store',$project->id]]) !!}
                    <br>
                    @include('layouts.partials.errors')
                    {!! Form::label('title','Title*') !!}
                    {!! Form::text('title',null,['placeholder'=>'Project Phase Title','required']) !!}
                    <div class="task-form-controls">
                        <div class="row create-task">
                            {!! Form::label('start_date','Start Date*') !!}
                            {!! Form::text('start_date',\Carbon\Carbon::today()->format('Y-m-d'),['required','placeholder'=>'Start Date','class'=>'due-date']) !!}
                        </div>
                        <div class="row create-task">
                            {!! Form::label('end_date','Due Date*') !!}
                            {!! Form::text('end_date',\Carbon\Carbon::today()->format('Y-m-d'),['required','placeholder'=>'Due Date','class'=>'due-date']) !!}
                        </div>
                        <div class="tinymce-label">
                            {!! Form::label('decisions','Description') !!}
                            <tinymce name="decisions" id="editor" v-model="editor" :options="options" :content
                            ='content'></tinymce>
                            <br>
                        </div>
                    </div>
                    {!! Form::submit('Request Approval', ['class' => 'submit-task button button-success'])!!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection