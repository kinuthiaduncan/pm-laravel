@extends('layouts.default')
@section('content')
    <div class="row content-section-header">
        <div class="small-12 large-7 column">
            <h1 style="padding-left: 0" class="page-title">Project Work Plan <b>{{$phase->name}}</b> Issues</h1>
        </div>
        <div class="small-12 large-5 column">
            <a href="{{route('phase.index',$project->id)}}" class="button  pull-right" style="padding: 11px;">Back</a>
            <ul class="dropdown menu pull-right" data-dropdown-menu>
                <li>
                    <a class="button pull-right"><i class="fa fa-download" aria-hidden="true"></i> Export Work Plan</a>
                    <ul class="menu">
                        <li><a style="font-size: 0.75rem;" href="{{route('project_phase.pdf',$phase->id)}}" class="transitions"> PDF </a></li>
                        <li><a style="font-size: 0.75rem;" href="{{route('project_phase.excel',$phase->id)}}" class="transitions"> Excel </a></li>
                    </ul>
                </li>
            </ul>
            </div>
    </div>
    <div class="row">
        <div class="content-section-text large-12 small-12 column">
            @if($activities->count() > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Issue Title</th>
                            <th>Due Date</th>
                            <th>Days Left</th>
                            <th>Percentage Complete</th>
                            <th>Assigned To</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($activities as $issue)
                        <tr>
                            <td>{!! $issue->title !!}</td>
                            <td>{!! $issue->date_due !!}</td>
                            <td>{!! \PM\Presenters\ProjectPhasePresenter::timeLeft($issue->date_due) !!} days</td>
                            <td>{!! $issue->percentage_done !!}%</td>
                            <td>{!! \PM\Presenters\UserPresenter::presentFullNames($issue->assigned_to) !!}</td>
                            <td>@if(Auth::id()==$issue->created_by || $project->project_lead == Auth::id())
                                <a href="{{url('/'.$project->id.'/issue/'. $issue->id.'/board/edit')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>@endif</td>
                            </tr>
                    @endforeach
                    <tr>

                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right">
                    {!! $activities->links() !!}
                </div>
            @else
                <p style="padding:1px 1px 1px 7px;">No Issues under this project phase.</p>
            @endif
        </div>
    </div>
@stop