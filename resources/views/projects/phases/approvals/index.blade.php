@extends('layouts.default')

@extends('projects.phases.partials.tabs')
@section('new')
    @include('projects.phases.partials.new_approvals')
@endsection
@section('updated')
    @include('projects.phases.partials.updated_approvals')
@endsection