@extends('layouts.default')
@section('content')
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>

    <div class="row content-section-text team-tasks">
        <div class="small-11 large-11 columns content-section-header">
            <h1 class="page-title">Edit {{$activity->name}} Phase Activity</h1>
        </div>
        <div class="small-1 large-1 columns">
            <a href="{{route('phase.show',[$phase->project_id, $phase->id])}}" class="button pull-left">Back</a>
        </div>
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    {!! Form::open(['method'=>'PUT','route'=>['activity.update',$phase->id,$activity->id]]) !!}
                    <br>
                    @include('layouts.partials.errors')
                    {!! Form::label('name','Activity Name*') !!}
                    {!! Form::text('name',$activity->name,['placeholder'=>'Activity Name...','required']) !!}
                    <div class="task-form-controls">
                        <div class="row create-task">
                            {!! Form::label('start_date','Start Date*') !!}
                            {!! Form::text('start_date',$activity->start_date,['required','placeholder'=>'Start Date','class'=>'due-date']) !!}
                        </div>
                        <div class="row create-task">
                            {!! Form::label('end_date','Due Date*') !!}
                            {!! Form::text('end_date',$activity->end_date,['required','placeholder'=>'Due Date','class'=>'due-date']) !!}
                        </div>
                        <div class="row create-task">
                            {!! Form::label('percent_complete','Percentage Done*') !!}
                            {!! Form::number('percent_complete',$activity->percent_complete,['required','max'=>100,'min'=>0,'placeholder'=>'Percentage Done']) !!}
                        </div>
                        <div class="tinymce-label">
                            {!! Form::label('comments','Comments') !!}
                            {!! Form::textarea('comments',$activity->comments,['rows'=> '3',  'class' => 'form-control']) !!}
                            <br/>
                            <br>
                        </div>
                    </div>
                    {!! Form::submit('Update Activity', ['class' => 'submit-task button button-success'])!!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop