@extends('layouts.default')
@section('content')

    <div class="row">
        <div class="content-section-header">
            <div class="small-12 large-6 column">
                <h1 class="page-title">
                    <a href="/project/{{$project->id}}"> {{$project->name}}</a></h1>
            </div>
            <div class="small-12 large-6 column">
                <ul class="dropdown menu pull-right" data-dropdown-menu>
                    <li>
                        <a class="button"><strong>+</strong> Create Issue</a>
                        <ul class="menu">
                            @foreach($project_components as $project_component)
                                <li><a style="font-size: 0.75rem;" href="{!! route('add_issue', [$project->id, $project_component->id]) !!}"
                                       class="transitions"> {!! $project_component->component_name !!} </a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
                <a href="{{route('phase.index',$project->id)}}" style="padding:11px;" class="button pull-right"><i class="fa fa-calendar-o" aria-hidden="true"></i> Project Work Plan</a>
            </div>
        </div>
    </div>
{{--    <board :auth_user = "{{Auth::user()->id}}" id="{{ $project->id }}" :components="{{ json_encode($project_components)}}" :users="{{ json_encode($user_list) }}"></board>--}}
    <board :auth_user = "{{Auth::user()->id}}" id="{{ $project->id }}" :users="{{ json_encode($user_list)}}" :components="{{ json_encode($projects_components_list) }}"></board>
@stop
