@extends('layouts.default')
@section('content')
    <div class="task-progress content-section-text">
        @if (count($errors) > 0)
            <div class="callout alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h5>Progress report for project: {{$project->name}}
            <a href="#" data-open="createProgressModal" class="button pull-right">Add a progress report</a>
            <a href="/project/{{$project->id}}" class="button pull-right">Back</a>
        </h5>
        <!-- Reveal Modals begin -->
        <div id="createProgressModal" class="row entry-modal" data-reveal>
            <div class="title-modal small-12 large-4 column">
                <div class="title-text hide-for-small-only">
                    <h3 style="margin-top: 65%;">Project: {!! $project->name !!} </h3>
                    <p>Project Progress</p>
                </div>
                <div class="title-text-small show-for-small-only">
                    <h4>Project: {!! $project->name !!} </h4>
                    <p>Project Progress</p>
                </div>
            </div>

            <div class="small-12 large-8 column form-modal">
                {!!  Form::open(['route'=>['store_project_progress', $project->id]]) !!}
                <div>
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title', null, ['required'=>'required']) !!}
                </div>
                <div>
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::textarea('description', null, ['required'=>'required', 'rows'=>4]) !!}
                </div>
                <div>
                    {!! Form::submit('Save', ['class'=>'button']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <hr>
        <div class="progress-info">
            <table>
                <tbody>
                @foreach($progress as $report)
                    <tr>
                        @if($report->title =="")
                            <td>
                                Weekly Report
                                <a class="pull-right" data-open="createProgressModal{{ $report->id }}"><i
                                            class="fa fa-pencil"></i></a>
                            </td>
                        @else
                            <td>
                                {{$report->title}}
                                <a class="pull-right" data-open="createProgressModal{{ $report->id }}"><i
                                            class="fa fa-pencil"></i></a>
                            </td>
                        @endif
                        <td>{!! $report->description !!} </td>
                        <td>{{ $report->created_at->diffForHumans() }}</td>
                        <td>By {{ $report->addedBy->preferred_name }}
                            on {{ $report->created_at->toFormattedDateString() }}</td>
                    </tr>

                    <div id="createProgressModal{{ $report->id }}" class="row entry-modal" data-reveal>
                        <div class="title-modal small-12 large-4 column">
                            <div class="title-text hide-for-small-only">
                                <h3 style="margin-top: 65%;">Project: {!! $project->name !!} </h3>
                                <p>Project Progress</p>
                            </div>
                            <div class="title-text-small show-for-small-only">
                                <h4>Project: {!! $project->name !!} </h4>
                                <p>Project Progress</p>
                            </div>
                        </div>
                        <div class="small-12 large-8 column form-modal">
                            {!! Form::open(['method'=>'PUT', 'route'=>['update_project_progress', $project->id, $report->id]]) !!}
                            <div>
                                {!! Form::label('title', 'Title') !!}
                                {!! Form::text('title', $report->title, ['required'=>'required']) !!}
                            </div>
                            <div>
                                {!! Form::label('description', 'Description') !!}
                                {!! Form::textarea('description', $report->description, ['required'=>'required', 'rows'=>4]) !!}
                            </div>
                            <div>
                                {!! Form::submit('Save', ['class'=>'button']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endforeach
                @if($progress->count() == 0)
                    <tr>
                        <td>There are no progress reports recorded for this project</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {!! $progress->links() !!}
        </div>
    </div>
@endsection