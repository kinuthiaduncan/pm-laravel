@extends('layouts.default')
@section('content')
    <div class="project-create">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Create Project</h3>
                    {!! Form::open(['route' => 'create_project']) !!}
                    @include('layouts.partials.errors')

                    <div class="form-group">
                        {!! Form::label('name','Project Name*') !!}
                        {!! Form::text('name',NULL,['class' => 'form-control','required']) !!}

                        <div class="form-group">
                            <div class="small-12 medium-6 columns" style="padding-left: 0">
                                {!! Form::label('project_access','Project Access*') !!}
                                {!! Form::select('project_access', ['public'=>'Public', 'private'=>'Private'] ,'public' ,['placeholder'=>'Please select project access level', 'class' => 'form-control', 'required'])!!}
                            </div>
                            <div class="small-12 medium-6  columns">
                                {!! Form::label('project_type','Project Type*') !!}
                                {!! Form::select('project_type', $project_types , 2 ,['class' => 'form-control','required'])!!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::label('project_description','Project Description') !!}
                        <tinymce name="project_description" id="editor" v-model="editor" :options="options" :content
                        ='content'></tinymce>
                        <br>
                        {!! Form::label('project_url','Project Url*') !!}
                        {!! Form::text('project_url',NULL,['class' => 'form-control','required']) !!}
                        {!! Form::label('project_lead','Project Lead*') !!}
                        {!! Form::select('project_lead', $users , Auth::id() ,['class' => 'form-control','required'])!!}
                        {!! Form::label('category_id','Project Category') !!}
                        {!! Form::select('category_id', $categories , 1 ,['class' => 'form-control'])!!}

                        <div class="form-group">
                            <div class="small-12 medium-6 columns" style="padding-left: 0">
                                {!! Form::label('status','Status*') !!}
                                {!! Form::select('status', ['concept'=>'Concept', 'In Progress'=>'In Progress', 'Deployed'=>'Deployed', 'Complete'=>'Complete'] ,'In Progress' ,['placeholder'=>'Please select current project status', 'class' => 'form-control', 'required'])!!}
                            </div>
                            <div class="small-12 medium-6  columns">
                                {!! Form::label('priority',' Priority*') !!}
                                {!! Form::select('priority', [ 'high'=>'High', 'medium'=>'Medium', 'low'=>'Low'] ,'medium' ,['class' => 'form-control', 'placeholder'=>'Please select project priority','required'])!!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                            {!! Form::label('resources_required','Resources Required*') !!}
                        </div>
                        {!! Form::text('resources_required',NULL,['class' => 'form-control','required']) !!}
                            {!! Form::label('resources_assigned','Resources Assigned*') !!}

                        {!! Form::text('resources_assigned',NULL,['class' => 'form-control','required']) !!}

                        {!! Form::submit('Create', ['class' => 'button button-success'])!!}
                </div>
                </div>
                    {!! Form::close() !!}

                </div>
            </div>

@stop
