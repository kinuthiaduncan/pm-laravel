@extends('layouts.default')

@section('content')

    <div class="project content-section-text">
        <div class="row">
            <div>
                <span class="pull-right">
                    <a href="#" data-open="list_more_projects" class="button"> List More Projects </a>
                    <span > <a href="{!! route('projects_index') !!}" class="button pull-right margin-left-10"> Projects Index </a> </span>
                    <span > <a href="{!! route('get_categories_index') !!}" class="button pull-right margin-left-10"> Categories Index</a> </span>

                    <!-- Reveal List-Projects Modal begin -->
                    <div id="list_more_projects" class="reveal large" data-reveal aria-labelledby="list_more_projects" aria-hidden="true" role="dialog">
                        <div class="small">
                            @include('projects.categories.partials.list_projects')
                        </div>
                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </span>
            </div><br/>
        </div><div class="clearfix"></div>
        <div class="row">
            <div class="row">
                <div class="large-6 columns">

                    <div class="margin-left-15">
                        <table class="table responsive table-scroll">
                            <tr class="green">
                                <th >Category </th>
                                <th ><strong>{!! $category->slug !!} </strong> </th>
                            </tr>
                            <tbody>
                            <tr>
                                <td><strong>Full Name:</strong></td><td>{!! $category->name !!}</td>
                            </tr>
                            <tr>
                                <td><strong> Created By:</strong></td><td>{!! \PM\Presenters\UserPresenter::presentFullNames($category->created_by) !!}</td>
                            </tr>
                            <tr>
                                <td><strong> Category Description: </strong></td><td>{!! $category->description !!}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="large-6 columns">

                    <div class="large-12 pull-right">
                        <table class="table table-scroll">
                            <tr class="green">
                                <th colspan="4" class="text-center" > Projects listed under the <strong> {!! $category->slug !!} </strong> category  </th>
                            </tr>
                            <tr>
                                <th >  <strong> Project Key </strong>  </th>
                                <th >  <strong> Project name </strong>  </th>
                                <th >  <strong>  View </strong>  </th>
                                <th >  <strong>  Delete </strong>  </th>
                            </tr>
                            <tbody>
                            @if($listed->count() == 0)
                                <tr> <td colspan="4"> There are no projects listed under this category.</td></tr>
                            @else
                                @foreach($listed as $project)
                                    <tr>
                                        <td>{!! $project->project_key !!}</td>
                                        <td> {!! $project->name !!} </td>
                                        <td>
                                            <a href="{!! route('project_board', [$project->id]) !!}">View</a>
                                        </td>
                                        <td>
                                            <a href="#" data-reveal-id="remove_project"> Remove </a>

                                            <!-- Reveal Remove-Project Modal begin -->
                                            <div id="remove_project" class="reveal-modal tiny" data-reveal aria-labelledby="remove_project" aria-hidden="true" role="dialog">
                                                <div class="small">
                                                    @include('projects.categories.partials.remove_project')
                                                </div>
                                                <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


@stop
