<div class=" revealHeader ">
    <h4 class="text-center"><strong> Remove Project from Category </strong> </h4>
</div>
<div class="project-members">
    <div class="row">
        <div class="small-12 columns">
            {!! Form::open(['route' => 'download_summaries']) !!}
            @include('layouts.partials.errors')

            <div class="form-group row">
                <p class="margin-left-15"> Are you sure you want to remove project <strong> {!! $project->name !!}</strong> from category <strong> {!! $category->slug !!}? </strong></p>
                {!! Form::hidden('category_id',$category->id) !!}
                {!! Form::hidden('project_id',$project->id) !!}
            </div>
            {!! Form::submit('Remove', ['class' => 'button button-success'])!!} &nbsp;
            {!! Form::close() !!}
        </div>
    </div>
</div>