<div class=" revealHeader ">
    <h4 class="text-center"><strong>Add Projects to Category</strong></h4>
</div>
<div class="project-members">
    <div class="row">
        <div class="small-12 columns">
            {!! Form::open(['route' => 'post_categories_add_projects']) !!}
                @include('layouts.partials.errors')

                <div class="form-group row">
                    {!! Form::hidden('category_id',$category->id) !!}
                    @if($non_listed->count() == 0)
                        <span>All existing projects have been listed</span>
                    @else()
                        @foreach($non_listed as $project)
                            <div class="small-12 medium-6 columns">
                                {!! Form::checkbox('project_id[]', $project->id) !!}
                                {!! Form::label('project_id',$project->name, ['required']) !!}
                            </div>
                        @endforeach

                </div>
                {!! Form::submit('Add', ['class' => 'button button-success'])!!} &nbsp;
            {!! Form::close() !!}
            @endif
        </div>
    </div>
</div>