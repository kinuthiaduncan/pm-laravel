@extends('layouts.default')
@section('content')
    <div class="projects content-section-text">
        <div class="row">
            <div class="large 12 columns">
                <span > <a href="{!! route('projects_index') !!}" class="button pull-right margin-left-10">Projects Index </a> </span>
                @if(Auth::user()->roles->id == 1)
                    <span> <a href="{!! route('get_categories_create') !!}" class="button pull-right margin-left-10">Add a Category</a> </span>
                @endif
            </div>
            <div class="large 12 columns" style="overflow: scroll;">
                <table>
                    <thead>
                    <tr>
                        <th>Slug</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Created By</th>
                        <th colspan="2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{!! $category->slug !!}</td>
                            <td>{!! $category->name !!}</td>
                            <td>{!! $category->description !!}</td>
                            <td>{!! \PM\Presenters\UserPresenter::presentFullNames($category->created_by) !!}</td>
                            <td><a href={{"/categories/show/".$category->id}}><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            <td><a href={{"/categories/edit/".$category->id}}><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div>
                    {!! $categories->links() !!}
                </div>
            </div>
        </div>
    </div>
@stop
