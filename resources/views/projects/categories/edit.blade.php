@extends('layouts.default')
@section('content')
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>
    <div class="project-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Edit Category</h3>
                    {!! Form::open(['method'=>'PUT', 'route' => ['post_categories_edit', $category->id]]) !!}
                    @include('layouts.partials.errors')

                    <div class="form-group">
                        {!! Form::hidden('id', $category->id) !!}

                        {!! Form::label('slug','Slug Name*') !!}
                        {!! Form::text('slug',$category->slug,['class' => 'form-control','required']) !!}

                        {!! Form::label('name','Full Name*') !!}
                        {!! Form::text('name',$category->name,['class' => 'form-control','required']) !!}

                        {!! Form::label('description','Category Description') !!}
                        {!! Form::textarea('description',$category->description,['rows'=> '3',  'class' => 'form-control']) !!}<br/>
                        <br>

                        {!! Form::submit('Edit', ['class' => 'button button-success pull-left'])!!}
                        <span class="button button-danger margin-left-10 pull-left "><a
                                    href="{!! route('get_categories_index') !!}"
                                    class="no-decoration">Cancel</a> </span>

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@stop
