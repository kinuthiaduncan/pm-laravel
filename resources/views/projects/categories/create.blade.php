@extends('layouts.default')
@section('content')
    <div class="project-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Create Category</h3>
                    {!! Form::open(['route' => 'projects_categories.store']) !!}
                    @include('layouts.partials.errors')

                    <div class="form-group">
                        {!! Form::label('slug','Slug Name*') !!}
                        {!! Form::text('slug',NULL,['class' => 'form-control','required']) !!}

                        {!! Form::label('name','Full Name*') !!}
                        {!! Form::text('name',NULL,['class' => 'form-control','required']) !!}

                        {!! Form::label('description','Category Description') !!}
                        <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                        ='content'></tinymce>
                        <br>

                        {!! Form::submit('Create', ['class' => 'button button-success pull-left'])!!}
                        <span class="button button-danger margin-left-10 pull-left "><a
                                    href="{!! route('get_categories_index') !!}"
                                    class="no-decoration">Cancel</a> </span>

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@stop
