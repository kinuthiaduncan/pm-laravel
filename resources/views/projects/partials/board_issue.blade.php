<div class="issue" ng-drag="true" draggable="true" ng-drag-data="[[ issue ]]" ng-drag-success="onDragComplete($data,$event)">
    <p class="issue-key">
        [[ issue.key ]]
        <span class="right">
            <a href="/issue/[[ issue.id ]]"><i class="fa fa-eye"></i></a>
            <a href="/[[ issue.project_id ]]/issue/[[ issue.id ]]/board/edit"><i class="fa fa-pencil-square-o"></i></a>
        </span>
    </p>

    <p class="issue-title">[[ issue.title ]]</p>

    <p class="issue-lead">
        <span>Updated [[ issue.created ]]</span><br/>
        <span class="">Assigned to: [[ issue.assigned ]]</span><br/>
        <span class="">Priority: [[ issue.priority ]]</span><br/>
        <span class="">Component: [[ issue.project_component ]]</span>
        <i class="fa fa-clock-o overdue pull-right" ng-if="overdue(issue)"  data-tooltip aria-haspopup="true" class="has-tip" title="Issue overdue"></i>
        <span class="clearfix"></span>
    </p>
</div>

