<div class="content-section-text">
    <div class="projects">
        <div class="row">
            <div class="small-6 large-4 columns" style="padding: 0 2px 0 2px">
                {{ Form::open() }}
                {{ Form::text('name', null,['placeHolder'=>'Search', 'v-model'=>'name', 'v-on:input'=>'getProjects()']) }}
            </div>
            <div class="small-6 large-4 columns" style="padding: 0 2px 0 2px">
                {{ Form::select('user_id',$users,null, ['placeholder'=>'Select User', 'v-model'=>'user', 'v-on:change'=>'getProjects()' ]) }}
                {{ Form::close() }}
            </div>

            <div class="small-6 large-2 columns">
                    <span> <a href="{!! route('add_project') !!}" class="button pull-right margin-left-10">Create a
                            Project</a> </span>
            </div>
            <div class="small-6 large-2 columns">
                <span> <a data-open="download_summaries" class="button pull-right margin-left-10">Download
                        Summaries</a> </span>
            </div>
                <!-- Reveal Remove-Project Modal begin -->
                <div id="download_summaries" class="reveal download-summaries" data-reveal
                     aria-labelledby="download_summaries" aria-hidden="true" role="dialog">
                    <div class="small">
                        @include('projects.partials.project_summaries')
                    </div>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                    <projects-table :auth_user="{{Auth::user()->id}}"></projects-table>

                </div>
        </div>
    </div>
