<div class="content-section-text">
    <div class="projects">
        <div class="row">
            <div class="small-12 large-4 columns" style="padding: 0 2px 0 2px">
                {{ Form::open() }}
                {{ Form::text('name', null,['placeHolder'=>'Search', 'v-model'=>'name', 'v-on:input'=>'getIndividualProjects()']) }}
                {{Form::close()}}
            </div>
            <div class="small-6 large-2 columns">
                    <span> <a href="{!! route('add_project') !!}" class="button pull-right margin-left-10">Create a
                            Project</a> </span>
            </div>
            <div class="small-6 large-2 columns">
                <a href="{!! route('my_private_projects') !!}" class="button pull-left">My Private Projects</a>
            </div>
            <div class="small-6 large-2 columns">
                <a href="#" data-open="exportPhasesModal" class="button pull-left">Work Plan Export</a>
            </div>
            <div class="small-6 large-2 columns">
                @if(Auth::user()->role_id == 1)
                    <a href="{{route('phase_approval.index')}}" class="button">Work Plan Approvals</a>
                @endif
            </div>
            <div id="exportPhasesModal" class="row export-modal" data-reveal
                 aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
                @include('projects.phases.partials.workplan_export')
            </div>

            <individual-projects :auth_user="{{Auth::user()->id}}"></individual-projects>
        </div>
    </div>
</div>
