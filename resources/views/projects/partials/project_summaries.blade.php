<div class=" revealHeader ">
    <h4 class="text-center"><strong> Download Projects Summaries </strong></h4>
</div>
<div class="project-members" ng-controller="SummariesController">
    <div class="row">
        <div class="small-12 columns">
            {!! Form::open(['route' => 'download_summaries']) !!}
            @include('layouts.partials.errors')

            <p> Please select the lists you require included in the export file:</p>
            <div class="form-group small-12 medium-6 columns">
                @if($categories->count() == 0)
                    <p class="margin-left-15"> <strong> There are no categories. Please add categories to perform this action.</strong> </p>
                    <span > <a href="{!! route('get_categories_create') !!}" class="button pull-right margin-left-10">Add a Category</a> </span>
                @else()
                    <h5><strong> Select Categories </strong></h5>

                    <div class="small-12 medium-6 columns" ng-if="!showCategories">
                        <label><input type="checkbox" ng-model="isCheckedCategory" name="all_categories" value="all_categories"> <span class="margin-left-10"> All</span></label>
                    </div>
                    <div class="small-12 medium-6 columns" ng-if="showCategories">
                        <label><input type="checkbox" name="all_categories" value="all_categories" > <span class="margin-left-10"> All</span></label>
                    </div>
                    <div class="clearfix"></div>

                    <div class="small-12 medium-12 columns" >
                        <label class=""><input type="checkbox" name="" value="" ng-model="showCategories"> <span class="margin-left-10">  Individual</span></label>
                        <div class="small-12 columns" ng-if="showCategories">

                            @foreach($categories as $category)
                                <div class="small-12 columns" ng-if="isCheckedCategory">
                                    {!! Form::checkbox('category_id[]', $category->id, true) !!}
                                    {!! Form::label('category_id',$category->slug) !!}
                                </div>
                                <div class="small-12 columns" ng-if="!isCheckedCategory">
                                    {!! Form::checkbox('category_id[]', $category->id) !!}
                                    {!! Form::label('category_id',$category->slug) !!}
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="clearfix"></div>
            </div>

            <div class="form-group small-12 medium-6 columns">
                @if($project_types->count() == 0)
                    <p class="margin-left-15"> <strong> There are no project types. Please add to perform this action.</strong> </p>
                    <span > <a href="{!! route('get_categories_create') !!}" class="button pull-right margin-left-10">Add a Category</a> </span>
                @else()
                    <h5><strong> Select Project Types </strong></h5>

                    <div class="small-12 medium-6 columns" ng-if="!showTypes">
                        <label><input ng-model="isCheckedType" type="checkbox" name="all_types" value="all_types" > <span class="margin-left-10">  All</span></label>
                    </div>
                    <div class="small-12 medium-6 columns" ng-if="showTypes">
                        <label><input type="checkbox" name="all_types" value="all_types" > <span class="margin-left-10">  All</span></label>
                    </div>
                    <div class="clearfix"></div>

                    <div class="small-12 medium-12 columns">
                        <label class=""><input type="checkbox" name="" value="" ng-model="showTypes"> <span class="margin-left-10">  Individual</span></label>
                        <div class="small-12 columns"  ng-if="showTypes">

                            @foreach($project_types as $type)
                                <div class="small-12 columns" ng-if="isCheckedType">
                                    {!! Form::checkbox('type_id[]', $type->id, true) !!}
                                    {!! Form::label('type_id',$type->name) !!}
                                </div>
                                <div class="small-12 columns"ng-if="!isCheckedType">
                                    {!! Form::checkbox('type_id[]', $type->id) !!}
                                    {!! Form::label('type_id',$type->name) !!}
                                </div>
                            @endforeach
                        </div>

                    </div>
                @endif
                    <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            {!! Form::submit('Download Summaries', ['class' => 'button button-success'])!!} &nbsp;
            {!! Form::close() !!}
            </div>
    </div>
</div>