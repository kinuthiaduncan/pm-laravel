@extends('layouts.default')
@section('content')

    <div class="project content-section-text">
        <div class="row show-for-small-only">
            <div class="small-3 column">
                <a class="button pull-left" href="{{route('projects_index')}}">Back</a>
            </div>
            <div class="small-6 column">
                <ul class="dropdown menu" data-dropdown-menu>
                    <li>
                        <a class="button pull-left">Create Issue</a>
                        <ul class="menu">
                            @foreach($project_components as $project_component)
                                <li><a href="{!! route('add_issue', [$project->id, $project_component->id]) !!}"
                                       class="transitions"> {!! $project_component->component_name !!} </a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="small-3 column">
                <ul class="dropdown menu pull-right more-task-details" data-dropdown-menu>
                    <li>
                        <i class="fa fa-caret-square-o-down" aria-hidden="true"></i> Actions
                        <ul class="menu">
                            @if($current_user == $project->project_lead || Auth::user()->roles->id == 1)
                                <li><a href="{{route('feedback.index',$project->id)}}">View Feedback</a></li>
                            @endif
                            <li> <a href="/project/{{ $project->id }}/progress">Progress</a></li>
                                @if($current_user == $project->project_lead || Auth::user()->roles->id == 1)
                                    <li><a href="/project/{!! $project->id !!}/members/add">Add Members</a></li>
                                    @if($project->active == 1)
                                        <li><a href="/project/{!! $project->id !!}/deactivate"> Deactivate Project</a></li>
                                    @else
                                        <li><a href="/project/{!! $project->id !!}/activate"> Activate Project</a></li>
                                    @endif
                                @elseif($current_user != $project->project_lead)
                                    <li><a href="/project/{!! $project->id !!}/watch">Watch Project</a></li>&nbsp;&nbsp;
                                @endif
                                @if(array_key_exists($current_user, $project_members)|| $current_user == $project->projectLead->id)
                                    <li><a href="/project/{!! $project->id !!}/milestone/create">Add Milestone</a></li>&nbsp;&nbsp;
                                    <li><a href="#" data-open="createComponentModal">Create Component</a></li>
                                @endif
                                <li><a href="/project/{!! $project->id !!}/board"> View Board</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row hide-for-small-only">
            <p>
                <span class="pull-right">
                    <a class="button" href="{{route('projects_index')}}">Back</a>
                    @if($current_user == $project->project_lead || Auth::user()->roles->id == 1)
                        <a class="button" href="{{route('feedback.index',$project->id)}}">View Feedback</a>
                    @endif
                    <a class="button" href="/project/{{ $project->id }}/progress">Progress</a>
                    @if($current_user == $project->project_lead || Auth::user()->roles->id == 1)
                        <a href="/project/{!! $project->id !!}/members/add" class="button">Add Members</a>&nbsp;&nbsp;
                        @if($project->active == 1)
                            <a class="button pull-right" href="/project/{!! $project->id !!}/deactivate"> Deactivate Project</a>
                        @else
                            <a class="button pull-right" href="/project/{!! $project->id !!}/activate"> Activate Project</a>
                        @endif
                    @elseif(!array_key_exists($current_user, $project_members))
                        <a href="/project/{!! $project->id !!}/watch" class="button">Watch Project</a>&nbsp;&nbsp;
                    @endif
                    @if(array_key_exists($current_user, $project_members)|| $current_user == $project->projectLead->id)
                    <a href="/project/{!! $project->id !!}/milestone/create" class="button">Add Milestone</a>&nbsp;&nbsp;
                    <a href="#" data-open="createComponentModal" class="button">Create Component</a>
                    @endif
                    <a class="button" href="/project/{!! $project->id !!}/board"> View Board</a>
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li>
                            <a class="button pull-left">Create Issue</a>
                            <ul class="menu">
                                @foreach($project_components as $project_component)
                                    <li><a href="{!! route('add_issue', [$project->id, $project_component->id]) !!}"
                                           class="transitions"> {!! $project_component->component_name !!} </a></li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>

                    <!-- Reveal Components Modal begin -->
                    <div id="createComponentModal" class="row entry-modal" data-reveal
                         aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
                            @include('projects.components.create_component')
                    </div>
                </span>
            </p>
            <br/>
        </div>
        <div class="row">
            <div class="row">
                <div class="large-12 columns">

                    <div class="margin-left-15">
                        <table class="table  table-scroll">
                            <tr>
                                <th>Project</th>
                                <th><strong>{!! $project->name !!} </strong></th>
                            </tr>
                            <tbody>
                            <tr>
                                <td><strong>Project Lead:</strong></td>
                                <td>{!! $project->projectLead->preferred_name !!}</td>
                            </tr>
                            <tr>
                                <td><strong>Project Category:</strong></td>
                                <td>{!! \PM\Presenters\CategoryPresenter::presentName($project->category_id) !!}</td>
                            </tr>
                            <tr>
                                <td><strong>Project Description:</strong></td>
                                <td>{!! $project->project_description !!}</td>
                            </tr>
                            <tr>
                                <td><strong>Project Members:</strong></td>
                                <td>
                                    @foreach($project_members as $key => $project_member)
                                        <span class="button project-member">{!! $project_member !!}
                                            @if($current_user == $project->project_lead)
                                                <a href="/project/{!! $project->id  !!}/member/{!! $key !!}/remove"><i class="fa fa-times remove-member" aria-hidden="true"></i></a>
                                            @endif
                                        </span>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="margin-left-15 table-scroll">
                        <table style="width:100%" class="table">
                            <tbody>
                            <tr>
                                <td><strong>Project Components</strong></td>
                                <td>
                                <table class="components-tbl responsive">
                                    @foreach($project_components as $project_component)

                                        <!-- Reveal modal for editting components-->
                                            <div id="editComponentModal{{$project_component->id}}" class="reveal-modal tiny"
                                                 data-reveal aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
                                                <div class="small">
                                                    @include('projects.components.edit_component')
                                                </div>
                                                <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                                            </div>
                                <tr>
                                   <td> {!! $project_component->component_name !!} </td>
                                    <td> {!! $project_component->description !!}

                                        @if(!is_null($project_component->component_lead)){!! $project_component->componentLead->preferred_name  !!}@endif

                                    @if(array_key_exists($current_user, $project_members)|| $current_user == $project->projectLead->id)
                                      <a class="edit pull-right" href="#"
                                             data-reveal-id="editComponentModal{{$project_component->id}}">Edit</a>
                                        <a class="pull-right" href="{!! route('project_board', [$project->id, $project_component->id]) !!}">View</a></td>
                            </tr>
                                    @endif
                                @endforeach
                            </table>
                            </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="large-12 small-12 columns table-scroll">
                @if($milestones)
                    <table style="width:100%">
                        <tr>
                            <td><b>{!! $project->name !!} Milestones</b></td>
                        <td>
                        <table class="milestone-tbl responsive">
                        <thead>
                            <tr>
                                <th>Milestone Key</th>
                                <th>Milestone Title</th>
                                <th>Description</th>
                                <th>Due Date</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($milestones as $milestone)
                                <tr>
                                    <td>{!! $milestone->projects->project_key.'-M'.$milestone->id !!}</td>
                                    <td> {!! $milestone->title !!}</td>
                                    <td> {!! $milestone->description !!}</td>
                                    <td> {!! $milestone->due_date !!}</td>
                                    <td>
                                    @if(array_key_exists($current_user, $project_members)|| $current_user == $project->projectLead->id)
                                        <a href="/edit/project/{!! $project->id !!}/milestone/{!! $milestone->id !!}">Edit</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </table>
                    </tr>
                @endif
            </div>

        </div>

    </div>

@stop
