@extends('layouts.default')

@section('content')
    <div class="content-section-header">
        <h1 class="page-title">My Private Projects</h1>
    </div>
    <div class="content-section-text">
        <div class="projects">
            <div class="row">
                <div class="small-12 large-6 columns" style="padding: 0 2px 0 2px">
                    {{ Form::open() }}
                    {{ Form::text('name', null,['placeHolder'=>'Search', 'v-model'=>'name', 'v-on:input'=>'getIndividualProjects()']) }}
                </div>
                <div class="small-4 large-6 columns">
                    <span> <a href="{!! route('add_project') !!}" class="button pull-right margin-left-10">Create a
                            Project</a> </span>

                    <a href="{!! route('my_public_projects') !!}" class="button pull-right">My Public Projects</a>
                </div>
                <private-projects :auth_user="{{Auth::user()->id}}"></private-projects>
            </div>
        </div>
    </div>

@stop

    {{--<div class="content-section-text my_projects">--}}


        {{--<div class="row">--}}

            {{--<div class="row">--}}
                {{--<div class="small-12 columns">--}}
                    {{--<a href="{!! route('add_project') !!}" class="button right">Create a project</a>--}}

                    {{--<a href="{!! route('my_public_projects') !!}" class="button pull-left">My Public Projects</a>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="row" ng-controller="MyProjectsController" >--}}
                {{--<div class="small-12 columns">--}}

                    {{--<table st-pipe="callServer" st-table="displayed" class="table responsive table-scroll">--}}
                        {{--<thead>--}}
                        {{--<tr >--}}
                            {{--<th colspan="11">My Private Projects</th>--}}
                        {{--</tr>--}}
                        {{--<tr colspan="9">--}}
                            {{--<th colspan="6"></th>--}}
                            {{--<th  colspan="3">--}}
                                {{--<input st-search placeholder="global search" class="input-sm form-control right" type="search"/>--}}
                            {{--</th>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<th>Name</th>--}}
                            {{--<th>Project Type</th>--}}
                            {{--<th>Project Key</th>--}}
                            {{--<th>Project url</th>--}}
                            {{--<th>Project Lead</th>--}}
                            {{--<th>Created At</th>--}}
                            {{--<th>Edit</th>--}}
                            {{--<th>View</th>--}}
                            {{--<th>Board</th>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody ng-show="!isLoading">--}}
                        {{--<tr ng-repeat="row in displayed" ng-if="row.access === 'private'">--}}

                            {{--<td>[[ row.name ]]</td>--}}
                            {{--<td>[[ row.type ]]</td>--}}
                            {{--<td>[[ row.key ]]</td>--}}
                            {{--<td>[[ row.url ]]</td>--}}
                            {{--<td>[[ row.lead ]]</td>--}}
                            {{--<td>[[ row.created_at ]]</td>--}}
                            {{--<td><a href="/project/[[ row.id]]/edit">Edit</a></td>--}}
                            {{--<td><a href="/project/[[ row.id ]]">View</a></td>--}}
                            {{--<td><a href="/project/[[ row.id ]]/board">Board</a></td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td class="notification-footer" colspan="13">--}}
                                {{--<div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--<tbody ng-show="isLoading">--}}
                        {{--<tr>--}}
                            {{--<td class="text-center" colspan="13">Loading ...</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                    {{--</table>--}}

                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

{{--@stop--}}
