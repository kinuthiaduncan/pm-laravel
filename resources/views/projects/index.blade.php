{{--/**--}}
{{--* Date: 13/02/17--}}
{{--* Cytonn Technologies--}}
{{--* author: Phillis Kiragu pkiragu.cytonn.com--}}
{{--*/--}}
@extends('layouts.default')

@extends('layouts.partials.tabs')
@section('team-shared')
    @include('projects.partials.all_projects')
@endsection
@section('individual')
    @include('projects.partials.myPublicProjects')
@endsection