
<div ng-controller="componentController">
        <div class="title-modal small-12 large-4 column">
            <div class="title-text hide-for-small-only">
                <h3>Project: {!! $project->name !!} </h3>
                <p>Create a Project Component</p>
            </div>
            <div class="title-text-small show-for-small-only">
                <h4>Project: {!! $project->name !!} </h4>
                <p>Create a Project Component</p>
            </div>
        </div>
        <div class="small-12 large-8 column form-modal">
            {!! Form::open(['route' => ['post_component', $project->id]]) !!}
            @include('layouts.partials.errors')

            <div class="form-group">
                {!! Form::hidden('project_id', $project->id) !!}
                {!! Form::label('component_name','Component Name') !!}
                {!! Form::text('component_name',NULL,['class' => 'form-control','required']) !!}
                {!! Form::label('description','Component Description') !!}
                {!! Form::textarea('description',NULL,['rows'=> '6', 'class' => 'form-control', 'required']) !!}<br/>
                <div style="margin-bottom: 10px">
                    {!! Form::label('lead', 'Component Lead') !!}
                    {!! Form::select('component_lead', $project_members , null ,['class' => 'form-control','required', 'ui-select2', 'init-model'=>'assigned'])!!}
                </div>

                <div>
                    <tags-input ng-model="user" display-property="fullName" key-property="id"  on-tag-added="tagAdded($tag)" on-tag-removed="tagRemoved($tag)" placeholder="Select Component Users" replace-spaces-with-dashes="false"  add-from-autocomplete-only="true">
                        <auto-complete source="loadUsers($query)" min-length="3" max-tags="1" load-on-focus="true" load-on-empty="true"  max-results-to-show="300" ></auto-complete>
                    </tags-input>
                    {!! Form::hidden('component_users', '', ['data-ng-value' => 'componentUsersString']) !!}
                </div>

                {!! Form::submit('Create Component', ['class' => 'button button-success','style'=>'margin-left:0'])!!}
            </div>
            {!! Form::close() !!}
        </div>
</div>

