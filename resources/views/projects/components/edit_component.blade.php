<div ng-controller="componentController">
    <h3 class="text-center">Project: {!! $project->name !!} </h3>
    <p><strong class="sub-title"> Edit Project Component - {!! $project_component->component_name !!}</strong></p>

    {!! Form::open(['route' => ['post_component', $project->id]]) !!}
    @include('layouts.partials.errors')

    <div class="form-group">
        {!! Form::hidden('component_id', $project_component->id) !!}
        {!! Form::hidden('project_id', $project->id) !!}
        {!! Form::label('component_name','Component Name*') !!}
        {!! Form::text('component_name',$project_component->component_name,['class' => 'form-control','required']) !!}
        {!! Form::label('description','Component Description*') !!}
        {!! Form::textarea('description',$project_component->description,['rows'=> '6', 'class' => 'form-control', 'required']) !!}<br/>
        <div style="margin-bottom: 10px">
            {!! Form::label('lead', 'Component Lead*') !!}
            {!! Form::select('component_lead', $project_members , $project_component->component_lead ,['class' => 'form-control','required', 'ui-select2', 'init-model'=>'assigned'])!!}
        </div>

        <div>
            {!! Form::label('lead', 'Component Users') !!}

            @if(count($project_component->componentUser) > 0)
                @foreach($project_component->componentUser as $componentUser)
                    <span class="component-users">{!! $componentUser->user->preferred_name !!}
                        <a class="component-remove" ng-click="removeUser({!! $project_component->id !!}, {!! $componentUser->component_user_id !!})"><i class="fa fa-trash"></i></a>
                    </span>
                @endforeach
            @else
                <span> No users added for this component </span>
            @endif

            <tags-input ng-model="user" display-property="fullName" key-property="id"  on-tag-added="tagAdded($tag)" on-tag-removed="tagRemoved($tag)" placeholder="Add Component Users" replace-spaces-with-dashes="false"  add-from-autocomplete-only="true">
                <auto-complete source="loadUsers($query)" min-length="3" max-tags="1" load-on-focus="true" load-on-empty="true"  max-results-to-show="300" ></auto-complete>
            </tags-input>
            {!! Form::hidden('component_users', '', ['data-ng-value' => 'componentUsersString']) !!}
        </div>

        {!! Form::submit('Update', ['class' => 'button button-success'])!!}
    </div>
    {!! Form::close() !!}
</div>