@extends('layouts.default')
@section('content')


    <div class="content-section-header">
        <h1 class="page-title">Issue</h1>
    </div>

    <div class="row content-section-text team-tasks">
        <div class="large-12 columns">
            <a class="button pull-left" href="/project/{{$issue->project_id}}/board">Back</a>

        @if($issue->created_by == Auth::user()->id)
                <a class="button" href="/{!! $issue->project_id !!}/issue/{!! $issue->id !!}/board/edit">
                    <i class="fa fa-pencil-square-o"></i>&nbsp;Edit
                </a>
            @endif
                <a class="button" href="{{route('issue_track.index',$issue->id)}}">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;Issue Schedule
                </a>
        </div>

        <div class="task-show">
            <h3 class="task-heading">{!! '[ '.$issue->projects->project_key.'-'.$issue->id.' ] ' !!} {!! $issue->title !!}</h3>
            <span>Assigned by <strong>{!! $issue->creator->preferred_name !!}</strong></span>
            <table style="margin-top: 10px">
                <tbody>
                <tr>
                    <td>Assigned To</td>
                    <td>{!! $issue->assignedTo->preferred_name !!}</td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td>{!! $issue->issueTypes->name !!}</td>
                </tr>
                <tr>
                    <td>Priority</td>
                    <td>{!! $issue->priorities->name !!}</td>
                </tr>

                <tr>
                    <td style="width: 180px">Issue description</td>
                    <td class="task-description">{!! $issue->description !!}</td>
                </tr>

                <tr>
                    <td>Due Date</td>
                    <td>{!! $issue->date_due !!}</td>
                </tr>
                <tr>
                    <td>Issue Status</td>
                    <td>{!! $issue->statuses->name !!}</td>
                </tr>
                </tbody>
            </table>
            @if($issue->image)
                <div class="issue-details-right-img" style="width: 300px; height: 300px">
                    <img class="thumbnail" src="{!! $issue->image !!}">
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <issue-comments id="{{ $issue->id }}"></issue-comments>
        </div>
    </div>
    <div class="row new-comment">
        <div class="small-12 large-12 columns">
            {!! Form::open(['route' => 'comment_issue', 'files' => 'true']) !!}
            {!! Form::hidden('issue_id', $issue->id) !!}
            <div class="small-9 large-9 column comment-col">
                {!! Form::textarea('name', null, ['placeholder' => 'Add a comment...', 'rows'=>1, 'class' => 'mycomment-box', 'required']) !!}
            </div>
            <div class="small-1 large-1 column comment-col">
                <input type="file" name="image" id="image" class="inputfile"/>
                <label for="file"><i style="color: #2B3A40" class="fa fa-picture-o myclip" aria-hidden="true"></i></label>
            </div>
            <div class="small-1 large-1 column comment-col">
                <input type="file" name="file" id="file" class="inputfile"/>
                <label for="file"><i style="color: #2B3A40" class="fa fa-paperclip myclip" aria-hidden="true"></i></label>
            </div>
            <div class="small-1 large-1 column mysendbox send-div">
                {!! Form::button('<i class="fa fa-paper-plane send-icon" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'send', 'style'=> "margin-left: -5px;"])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
