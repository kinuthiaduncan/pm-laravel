@extends('layouts.default')
@section('content')

    <div class="issue-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Create an Issue</h3>
                    {!! Form::open(['route' => 'create_issue', 'files' => 'true']) !!}
                    @include('layouts.partials.errors')

                    <div class="form-group">
                        {!! Form::label('project_id','Select Project') !!}
                        {!! Form::select('project_id', $projects ,'' ,['class' => 'form-control','required'])!!}
                        {!! Form::label('component_id','Select Component') !!}
                        {!! Form::select('component_id', $components ,'' ,['class' => 'form-control','required'])!!}
                        {!! Form::label('title','Issue Title') !!}
                        {!! Form::text('title',NULL,['class' => 'form-control','required']) !!}
                        {!! Form::label('description','Issue Description') !!}
                        <tinymce name="description" id="editor" v-model="editor" :options="options" :content
                        ='content'></tinymce>
                        <br>
                        {!! Form::label('issue_type','Issue Type') !!}
                        {!! Form::select('issue_type', $issue_type ,2 ,['class' => 'form-control','required'])!!}
                        {!! Form::label('priority_id','Priority Type') !!}
                        {!! Form::select('priority_id', $priorities ,3 ,['class' => 'form-control','required'])!!}
                        {!! Form::hidden('parent_id', '0') !!}
                        {!! Form::label('assigned_to','Assigned To') !!}
                        {!! Form::select('assigned_to', $users , '' ,['class' => 'form-control','required'])!!}
                        {!! Form::label('date_due','Due Date') !!}
                        {!! Form::text('date_due', Carbon\Carbon::now()->format('Y-m-d'), array('class' => 'form-control date', 'required')) !!}
                        {!! Form::label('image','Upload Image') !!}
                        {!! Form::file('image', NULL) !!}
                        {!! Form::submit('Create', ['class' => 'button button-success'])!!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop
