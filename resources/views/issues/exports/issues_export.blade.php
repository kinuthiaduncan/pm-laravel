<table>
    <tr>
        <th>Project</th>
        <th>Issue</th>
        <th>Description</th>
        <th>Issue type</th>
        <th>Priority</th>
        <th>Status</th>
        <th>Assigned By</th>
        <th>Created By</th>
        <th>status_change_date</th>
        <td>created_at</td>
        <td>updated_at</td>
    </tr>
    @foreach($issues as $issue)
        <tr>
            <td>{!! $issue->projects->name !!}</td>
            <td>{!! $issue->title !!}</td>
            <td>{!! $issue->description !!}</td>
            <td>{!! $issue->issueTypes->name !!}</td>
            <td>{!! $issue->priorities->name !!}</td>
            <td>{!! $issue->statuses->name !!}</td>
            <td>{!! userFullName($issue->assigned_to) !!}</td>
            <td>{!! userFullName($issue->created_by) !!}</td>
            <td>{!! $issue->status_change_date !!}</td>
            <td>{!! $issue->created_at !!}</td>
            <td>{!! $issue->updated_at !!}</td>
        </tr>
    @endforeach
</table>