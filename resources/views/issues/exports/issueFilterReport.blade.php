@extends('layouts.pdf')

@section('content')
<h3 class="title">ISSUES REPORT</h3>

@foreach($data as $issue)
    <h1> {{ $issue->status }} Issues : {{$issue->count}} </h1>
    <table class="table">
        <thead class="summary">
        <tr>
            <th>Project</th>
            <th>Issue Title</th>
            <th>Issue Priority</th>
            <th>Due Date</th>
            <th> Status</th>
            <th>Created By</th>
            <th>Assigned To</th>
        </tr>
        </thead>
        <tbody>
            @forelse($issue->issues['data'] as $d)
                <tr>
                    <td>{!! $d['project'] !!}</td>
                    <td>{!! $d['title'] !!}</td>
                    <td>{!! $d['priority']->name !!}</td>
                    <td>{!! $d['due_date'] !!}</td>
                    <td>{!! $d['status'] !!}</td>
                    <td>{!! $d['assigned'] !!}</td>
                    <td>{!! $d['by'] !!}</td>
                </tr>
            @empty
                <tr>No issues</tr>
            @endforelse
        </tbody>
    </table>
    <br>
@endforeach
@endsection