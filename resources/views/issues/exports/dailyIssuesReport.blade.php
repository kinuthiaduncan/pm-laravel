@extends('layouts.pdf')

@section('content')

    <h3 class="title"> {{$user->preferred_name}} ~ DAILY ISSUES REPORT</h3>
@if(count($data) > 0)
    <h6>Issues Assigned Today</h6>
<table class="table">
    <thead class="summary">
    <tr>
        <th>Project</th><th>Issue Title</th><th>Description</th><th>Status</th><th>Priority</th><th>Percentage Done</th>
        <th>Due Date</th><th>Assigned By</th>
    </tr>
    </thead>
    @foreach ($data as  $myData)
        <tr>
            <td>{{ $myData->projects->name }}</td>
            <td>{{ $myData->title }}</td>
            <td>{!! $myData->description !!}</td>
            <td>{!! $myData->statuses->name !!}</td>
            <td>{!! $myData->priorities->name !!}</td>
            <td>{!! $myData->percentage_done !!}</td>
            <td>{!! $myData->date_due !!}</td>
            <td>{!! $myData->creator->preferred_name !!}</td>
        </tr>
    @endforeach
</table>
    @else
    <p>No Issues Assigned Today</p>
@endif
    @if(count($review) > 0)
        <br />
        <h6>Issues Submitted for Review Today</h6>
        <table class="table">
            <thead class="summary">
            <tr>
                <th>Project</th><th>Issue Title</th><th>Description</th><th>Status</th><th>Priority</th><th>Percentage Done</th>
                <th>Due Date</th><th>Assigned By</th>
            </tr>
            </thead>
            @foreach ($review as  $myData)
                <tr>
                    <td>{{ $myData->projects->name }}</td>
                    <td>{{ $myData->title }}</td>
                    <td>{!! $myData->description !!}</td>
                    <td>{!! $myData->statuses->name !!}</td>
                    <td>{!! $myData->priorities->name !!}</td>
                    <td>{!! $myData->percentage_done !!}</td>
                    <td>{!! $myData->date_due !!}</td>
                    <td>{!! $myData->creator->preferred_name !!}</td>
                </tr>
            @endforeach
        </table>
    @else
        <p>No Issues Sent for Review Today</p>
    @endif
    @if(count($done) > 0)
        <br />
        <h6>Issues Submitted Marked as Done Today</h6>
        <table class="table">
            <thead class="summary">
            <tr>
                <th>Project</th><th>Issue Title</th><th>Description</th><th>Status</th><th>Priority</th><th>Percentage Done</th>
                <th>Due Date</th><th>Assigned By</th>
            </tr>
            </thead>
            @foreach ($done as  $myData)
                <tr>
                    <td>{{ $myData->projects->name }}</td>
                    <td>{{ $myData->title }}</td>
                    <td>{!! $myData->description !!}</td>
                    <td>{!! $myData->statuses->name !!}</td>
                    <td>{!! $myData->priorities->name !!}</td>
                    <td>{!! $myData->percentage_done !!}</td>
                    <td>{!! $myData->date_due !!}</td>
                    <td>{!! $myData->creator->preferred_name !!}</td>
                </tr>
            @endforeach
        </table>
    @else
        <p>No Issues Marked as Done Today</p>
    @endif
@endsection

