@extends('layouts.default')

@section('content')

    <div class="issue-create">
        <div class="row">
            <div class="large-12 large-centered columns">
                <div class="project-create-form">
                    <div class="small-12 columns">
                        <h4>Create an Issue on {{$project->name}}</h4>
                    </div>
                    {!! Form::open(['route' => 'create_issue', 'files' => 'true']) !!}

                        @include('layouts.partials.errors')

                        @include('issues.partials.create_form')

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@stop
