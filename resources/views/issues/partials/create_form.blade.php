
{{ Form::hidden('project_id', $project_id) }}
{{ Form::hidden('component_id', $component_id) }}

<div class="issue-form">
    <div class="small-12 medium-6 columns" style="padding: 10px;">
        <div class="form-group">
            {!! Form::label('title','Issue Title*') !!}
            {!! Form::text('title', null,['class' => 'form-control','required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description','Issue Description') !!}
            <tinymce name="description" id="editor" v-model="editor" :options="options" :content
            ='content'></tinymce>
            <br>
        </div>

        <div class="form-group">
            {!! Form::label('image_file','Upload Image') !!}
            {!! Form::file('image_file', NULL) !!}
        </div>
    </div>
    <div class="small-12 medium-6 columns" style="padding: 10px;">
        <div class="form-group">
            {!! Form::label('assigned_to','Assigned To*') !!}
            {!! Form::select('assigned_to', $users , $lead ,['required', 'v-select'=>''])!!}
        </div>
        <div class="form-group">
            {!! Form::label('project_phase','Project Phase') !!}
            {!! Form::select('project_phase', $phases ,NULL,['placeholder'=>'Select Project Phase'])!!}
        </div>
        <div class="form-group">
            {!! Form::label('issue_type','Issue Type*') !!}
            {!! Form::select('issue_type', $issue_type , 2 ,['class' => 'form-control','required'])!!}
        </div>

        <div class="form-group">
            {!! Form::label('priority_id','Priority Type*') !!}
            {!! Form::select('priority_id', $priorities , 3 ,['class' => 'form-control','required'])!!}
        </div>

        <div class="form-group">
            {!! Form::label('date_due','Due Date*') !!}
            {!! Form::text('date_due', Carbon\Carbon::today()->format('Y-m-d'), array('class' => 'form-control date', 'required')) !!}
        </div>
    </div>
    <div class="small-12 columns">
        <div class="form-group">
            {!! Form::submit('Save', ['class' => 'button'])!!}&nbsp;&nbsp;
            <a class="button pull-right" href="/project/{!! $project_id !!}">Cancel</a>
        </div>
    </div>
</div>
