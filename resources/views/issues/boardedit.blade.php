@extends('layouts.default')

@section('content')

    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>


    <div class="issue-create content-section-text">
        <div class="row">
            <div class="large-12 large-centered columns">
                <div class="project-create-form">
                    <div class="small-12 columns">
                        <h3>Edit Issue</h3>
                    </div>
                    {!! Form::model($issue, ['route' => ['edit_issue', $issue->id], 'files' => 'true']) !!}

                    @include('layouts.partials.errors')

                    {{ Form::hidden('project_id', $project_id) }}
                    {{ Form::hidden('component_id', $component_id) }}
                    {{ Form::hidden('status_id', $issue->status_id) }}


                    <div class="small-12 medium-6 columns" style="padding: 10px;">
                        <div class="form-group">
                            {!! Form::label('title','Issue Title*') !!}
                            {!! Form::text('title', null,['class' => 'form-control','required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('project_phase','Project Phase') !!}
                            {!! Form::select('project_phase', $phases ,$issue->project_phase)!!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description','Issue Description') !!}
                            {!! Form::textarea('description',null,['rows'=> '3',  'class' => 'form-control']) !!}<br/>
                        </div>

                        <div class="form-group">
                            {!! Form::label('image_file','Upload Image') !!}
                            {!! Form::file('image_file', NULL) !!}
                        </div>
                    </div>
                    <div class="small-12 medium-6 columns" style="padding: 10px;">
                        <div class="form-group">
                            {!! Form::label('assigned_to','Assigned To*') !!}
                            {!! Form::select('assigned_to', $users , null ,['class' => 'form-control','required', 'v-select'=>''])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('issue_type','Issue Type*') !!}
                            {!! Form::select('issue_type', $issue_type , null ,['class' => 'form-control','required'])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('priority_id','Priority Type') !!}
                            {!! Form::select('priority_id', $priorities , null ,['class' => 'form-control','required'])!!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('date_due','Due Date') !!}
                            {!! Form::text('date_due', null, array('class' => 'form-control date', 'required')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('percentage_done','Percentage Done') !!}
                            {!! Form::number('percentage_done', null, ['max'=>100, 'min'=>0]) !!}
                        </div>
                    </div>
                    <div class="small-12 columns">
                        <div class="form-group">
                            {!! Form::submit('Save', ['class' => 'button'])!!}&nbsp;&nbsp;
                            <a class="button pull-right" href="/project/{!! $project_id !!}">Cancel</a>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop
