@extends('layouts.default')
@section('content')
    <div class="row content-section-text team-tasks">
        <div class="small-8 large-8 columns content-section-header">
            <h1 class="page-title"><b>{{$issue->title}}</b> Issue Schedule</h1>
        </div>
        <div class="small-2 large-2 columns">
            <a href="{{url('/issue/'.$issue->id)}}" class="button pull-right">Back</a>
        </div>
        <div class="small-2 large-2 columns">
            @if($issue->created_by == Auth::id() || $issue->assigned_to == Auth::id())
                <a href="{{route('issue_track.create', $issue->id)}}" class="button pull-left"><i class="fa fa-clock-o" aria-hidden="true"></i> Add Time</a>
            @endif
        </div>
        <div class="small 12 large-12 columns">
            @if(count($schedule)==null)
                <p>The timesheet for this issue is empty</p>
            @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>Date</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Added By</th>
                            <th style="text-align: center">Number of Hours</th>
                            <th colspan="2">Action</th>
                        </tr>
                     </thead>
                    @foreach($schedule as $track)
                        <tr>
                            <td>{!! $track->description !!}</td>
                            <td>{!! $track->date !!}</td>
                            <td>{!! $track->start_time !!}</td>
                            <td>{!! $track->end_time !!}</td>
                            <td>{!! $track->creator->firstname !!} {!! $track->creator->lastname !!}</td>
                            <td style="text-align: center">{!! number_format((\PM\Presenters\NumberOfHoursPresenter::presentNumberOfHours($track->start_time,$track->end_time)),2) !!}</td>
                            <td><a href="{{route('issue_track.edit',[$issue->id,$track->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                            <td><a href="{{route('delete_issue_schedule',[$track->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                </table>
            <div style="text-align: right">
                {!! $schedule->links() !!}
            </div>
            @endif
        </div>
    </div>
@endsection