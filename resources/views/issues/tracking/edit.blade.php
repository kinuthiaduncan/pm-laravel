@extends('layouts.default')

@section('content')

    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>

    <div class="row content-section-text team-tasks">
        <div class="small-9 large-9 columns content-section-header">
            <h1 class="page-title">Edit Issue Time</h1>
        </div>
        <div class="small-3 large-3 columns">
            <a href="{{route('issue_track.index', $issue)}}" class="button pull-right">Back</a>
        </div>
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    {!! Form::open(['method'=>'PUT','route' => ['issue_track.update',$issue,$schedule->id]]) !!}
                    <br>
                    @include('layouts.partials.errors')
                    <div class="task-form-controls">
                        <div class="row create-task">
                            {!! Form::label('date','Date') !!}
                            {!! Form::text('date',\Carbon\Carbon::today()->format('Y-m-d'),['required','placeholder'=>'Date','class'=>'due-date']) !!}
                        </div>
                        <div class="row create-task">
                            <div class="small-12 medium-6 columns create-task-col-2">
                                {!! Form::label('start_time','Start') !!}
                                {!! Form::text('start_time',NULL,['required','placeholder'=>'Started at','id'=>'issue_start','data-issue-id'=>$schedule->start_time,'class'=>'startTimepicker start']) !!}
                            </div>
                            <div class="small-12 medium-6 columns create-task-col">
                                {!! Form::label('end_time','Stop') !!}
                                {!! Form::text('end_time',NULL,['required','placeholder'=>'Ended at','id'=>'issue_stop','data-issue-id'=>$schedule->end_time,'class'=>'stopTimepicker end']) !!}
                            </div>
                        </div>
                        <div class="tinymce-label">
                            {!! Form::label('description','Schedule Description') !!}
                            {!! Form::textarea('description',$schedule->description,['rows'=> '3',  'class' => 'form-control']) !!}
                            <br/>
                        </div>
                        {!! Form::submit('Update Schedule', ['class' => 'submit-task button button-success'])!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop