@extends('layouts.default')
@section('content')

    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link charmap anchor",
                "searchreplace visualblocks code",
                "insertdatetime  contextmenu paste jbimages"
            ],
            toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | " +
            "bullist numlist outdent indent ",
            relative_urls: false
        });
    </script>
    <div class="issue-create content-section-text">
        <div class="row">
            <div class="large-8 large-centered columns">
                <div class="project-create-form">
                    <h3>Edit Issue</h3>
                    {!! Form::open(['route' => 'edit_issue', 'files' => 'true']) !!}
                    @include('layouts.partials.errors')
                <div class="issue-form">
                    <div class="form-group">
                        {!! Form::hidden('project_id', $project_id) !!}
                        {!! Form::hidden('issue_id', $issue->id) !!}
                        {!! Form::label('title','Issue Title*') !!}
                        {!! Form::text('title', $issue->title,['class' => 'form-control','required']) !!}
                        {!! Form::label('description','Issue Description') !!}
                        {!! Form::textarea('description',$issue->description,['rows'=> '3',  'class' => 'form-control']) !!}<br/>
                        <br>
                        {!! Form::label('issue_type','Issue Type*') !!}
                        {!! Form::select('issue_type', $issue_type , $issue->issue_type ,['class' => 'form-control','required'])!!}
                        {!! Form::label('project_phase','Project Phase') !!}
                        {!! Form::select('project_phase', $phases ,$issue->project_phase)!!}
                        {!! Form::label('priority_id','Priority Type*') !!}
                        {!! Form::select('priority_id', $priorities , $issue->priority_id ,['class' => 'form-control','required'])!!}
                        {!! Form::label('parent_id','Parent Issue*') !!}
                        {!! Form::select('parent_id', $issues , 0 ,['class' => 'form-control','required'])!!}
                        {!! Form::label('assigned_to','Assigned To*') !!}
                        {!! Form::select('assigned_to', $users , $issue->assigned_to ,['class' => 'form-control','required', 'ui-select2', 'ng-model'=>'assigned'])!!}
                        {!! Form::label('status_id','Issue Status*') !!}
                        {!! Form::select('status_id', $status_types , $issue->status_id ,['class' => 'form-control','required'])!!}
                        {!! Form::label('date_due','Due Date*') !!}
                        {!! Form::text('date_due', $issue->date_due, array('class' => 'form-control date', 'required')) !!}
                        {!! Form::label('percentage_done','Percentage Done*') !!}
                        {!! Form::select('percentage_done',
                                array('0' => ' 0%', '10' => '10%','20' => '20%','30' => '30%','40' => '40%','50' => '50%',
                               '60' => '60%', '70' => '70%','80' => '80%', '90' => '90%', '100'=>'100%'),
                                $issue->percentage_done , ['class'=>'form-control','placeholder' => 'Select Percentage Done','required'])
                         !!}
                        {!! Form::label('image','Upload Image') !!}
                        {!! Form::file('image', NULL) !!}
                        {!! Form::submit('Edit', ['class' => 'button'])!!}&nbsp;&nbsp;
                        <a class="button" href="/project/{!! $project_id !!}">Cancel</a>
                    </div>
                </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop
