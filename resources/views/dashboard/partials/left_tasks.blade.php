<div class="dash-assigned">
    <h5>Assigned Tasks</h5>
    @if($assigned->count() > 0)
        <table class="table">
            <tbody>
            @foreach($assigned as $task)
            <tr>
                <td>
                    {!! $task->title !!}</td>
                <td><a href="{{url('/tasks/my-tasks/'.$task->id)}}"><i class="fa fa-eye show-view pull-right" aria-hidden="true"></i></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        <div style="text-align: right;font-weight: 600;"><a style="color:#000000;" href="{{url('/dashboard/assigned')}}">View all
                <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> </a></div>
    @else
        <div class="assigned-null">
            <h3>NO TASK!!</h3>
            <p>Currently you have no task assigned to you</p>
            <a href="{{url('/tasks/my-tasks')}}">Check All Tasks</a>
        </div>
    @endif
</div>
<div class="dash-due">
    <h5>Past Due Tasks</h5>
    @if($pastDue->count() > 0)
        <table class="table">
            <tbody>
            @foreach($pastDue as $task)
                <tr>
                    <td>
                        {!! $task->title !!}
                    <a href="{{url('/tasks/my-tasks/'.$task->id)}}"><i class="fa fa-eye show-view pull-right" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div style="text-align: right;font-weight: 600;"><a style="color:#000000;" href="{{url('/dashboard/past-due')}}">View all
                <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> </a></div>
    @else
        <div class="due-null">
            <i class="fa fa-check-circle-o" aria-hidden="true"></i> Congratulations. You have no overdue tasks.
        </div>
    @endif
</div>