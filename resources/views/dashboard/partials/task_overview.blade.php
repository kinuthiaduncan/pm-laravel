<div class="row">
    <div class="small-12 large-4 column" style="margin-bottom: 2px;">
        <div class="dash-complete">
            <a v-on:click="getCompleteTasks()"><i class="fa fa-refresh pull-right" aria-hidden="true"></i></a>
            <completed-tasks :user="{{$user}}"></completed-tasks>
        </div>
        <div class="dash-all-done"><a href="{{url('/dashboard/completed')}}">View all
                <i class="fa fa-arrow-circle-o-right pull-right" aria-hidden="true"></i></a></div>
    </div>
    <div class="small-12 large-4 column" style="margin-bottom: 2px;">
        <div class="dash-ongoing">
            <a v-on:click="getOngoingTasks()"><i class="fa fa-refresh pull-right" aria-hidden="true"></i></a>
            <ongoing-tasks :user="{{$user}}"></ongoing-tasks>
        </div>
        <div class="dash-all-ongoing"><a href="{{url('/dashboard/ongoing')}}">View all
                <i class="fa fa-arrow-circle-o-right pull-right" aria-hidden="true"></i></a></div>
    </div>
    <div class="small-12 large-4 column" style="margin-bottom: 2px;">
        <div class="dash-pending">
            <a v-on:click="getPendingTasks()"><i class="fa fa-refresh pull-right" aria-hidden="true"></i></a>
            <pending-tasks :user="{{$user}}"></pending-tasks>
        </div>
        <div class="dash-all-pending">
            <a href="{{url('/dashboard/pending')}}">View all <i class="fa fa-arrow-circle-o-right pull-right" aria-hidden="true"></i></a></div>
    </div>
</div>