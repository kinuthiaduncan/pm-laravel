<div class="dash-assigned" style="border: none !important;">
    @if($teamTasks->count() > 0)
        <table style="border: solid #f5f5f5 1px !important;">
            <tbody>
            @foreach($teamTasks as $task)
                <tr>
                    <td>
                        {!! $task->title !!}</td>
                    <td><a href="{{url('/tasks/my-tasks/'.$task->id)}}"><i class="fa fa-eye show-view pull-right" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div style="text-align: right;font-weight: 600;"><a style="color:#000000;" href="{{route('task.index')}}">View all
                <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> </a></div>
    @else
        <div class="null-team">
        <p>Sorry there are no team tasks. Seems your team isn't that busy</p>
        </div>
    @endif

</div>