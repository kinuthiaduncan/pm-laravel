<div class="dash-assigned" style="border: none !important;">
@if($individual->count() > 0)
    <table style="border: solid #f5f5f5 1px !important;">
        <tbody>
        @foreach($individual as $task)
            <tr>
                <td>
                    {!! $task->title !!}</td>
                <td><a href="{{url('/tasks/my-tasks/'.$task->id)}}">
                        <i class="fa fa-eye show-view pull-right" aria-hidden="true"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
        <div style="text-align: right;font-weight: 600;"><a style="color:#000000;" href="{{route('task.index')}}">View all
                <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> </a></div>
@else
    <div class="individual-null">
        Wow!! You currently have no task. If you wish to create one, kindly
        <a href="{{route('task.create')}}">Click here</a>
    </div>
@endif
</div>