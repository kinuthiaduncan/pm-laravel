@extends('layouts.default')

@section('content')
<div class="row">
    <div class=" small-12 large-9 columns content-section-header">
        <h1 class="page-title">{!! $title !!} </h1>
    </div>
    <div class=" small-12 large-3 columns">
        <a class="button pull-left" href="{{route('task.create')}}" style="margin-right: 15px;"><strong>+</strong> Create Task</a>
        <a class="button pull-right" href="{{url('/dashboard')}}" style="margin-right: 15px;"> Dashboard</a>
    </div>
    <div class="content-section-text small-12 large-12 column">
        @if($tasks->count() >0)
        <table>
            <thead>
                <tr>
                    <th>Task Name</th><th>Priority</th><th>Due Date</th><th>Status</th><th>Task Access</th><th>Assigned By</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <td>{!! $task->title !!}
                            <a href="{{url('/tasks/my-tasks/'.$task->id)}}"><i class="fa fa-eye show-view pull-right" aria-hidden="true"></i></td>
                        <td>{!! $task->priority->name !!}</td>
                        <td>{!! $task->due_date !!}</td>
                        <td>{!! $task->status->name !!}</td>
                        <td>{!! $task->task_access !!}</td>
                        <td>{!! $task->creator->preferred_name !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <h6>You have no {!! $title !!}</h6>
        @endif
        <div style="text-align: right">{!! $tasks->links() !!}</div>
    </div>
</div>
@endsection