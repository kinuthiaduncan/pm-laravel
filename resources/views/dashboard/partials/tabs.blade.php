<div class="dash-all">
    <h5>All Tasks</h5>
    <ul class="tabs" data-tabs id="p-tabs">
        <li class="tabs-title  is-active"><a href="#individual">Individual</a></li>
        <li class="tabs-title"><a href="#my-team-shared" aria-selected="true">Team Shared</a></li>
    </ul>
    <div style="padding:10px">
        <div class="tabs-content" data-tabs-content="p-tabs">
            <div class="tabs-panel is-active" id="individual">
                @yield('individual')
            </div>
            <div class="tabs-panel" id="my-team-shared">
                @yield('team-shared')
            </div>
        </div>
    </div>
</div>