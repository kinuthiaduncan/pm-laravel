@extends('layouts.default')

@section('content')
    <div class="content-section-header">
        <div class="row">
            <div class="small-6 large-4 column dash-title">
                <h2>Dashboard </h2>
            </div>
            <div class="small-6 large-4 column">
                <a href="{{url('daily-personal-summary')}}" class="button pull-right">Export My Daily Summary</a>
            </div>
            <div class="small-12 large-4 column">
                <a href="{{url('weekly-personal-summary')}}" class="button pull-left" style="margin-left: 5%">Export My Weekly Summary</a>
                <a @click="taskFormVisible = true" class="button" style="margin-left: 16%" ><strong>+</strong> Create Task</a>
            </div>
            <div class="small-12 large-12 column overview"><h6>Tasks Overview</h6></div>
        </div>
    </div>
    <task-create v-if="taskFormVisible"></task-create>
    @include('dashboard.partials.task_overview')
    <div class="row dash-task-details">
        <div class="small-12 large-6 column">
            @include('dashboard.partials.left_tasks')
        </div>
        <div class="small-12 large-6 column">
            @include('dashboard.partials.right_tasks')
        </div>
    </div>
@endsection