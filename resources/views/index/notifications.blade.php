@extends('layouts.default')
@section('content')
    <div class="task-progress content-section-text">

        <h5> Notifications
            <a href="/dashboard" class="button pull-right">Dashboard</a>
            <a href="/delete/notifications" class="button pull-right">Clear All</a>
        </h5>
        <div class="notifications-info">
            <table>
                <tbody>
                @foreach($notifications  as $notification)
                    <tr>
                        <td>
                            <a href="{{$notification->data['url']}}">
                                <h6>
                                    {{class_basename($notification->type)}}
                                    ~ {{$notification->data['title']}}
                                </h6>
                                <p>{{$notification->created_at->toDayDateTimeString()}}</p>
                            </a>
                        </td>
                    </tr>
                @endforeach
                @if($notifications->count() == 0)
                    <tr>
                        <td>You have no notifications</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {!! $notifications->links() !!}
        </div>
    </div>
@endsection