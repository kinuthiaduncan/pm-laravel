<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Development Company" content="Cytonn Technologies">
    <meta name="Developer" content="Edwin Mukiri">
    <title>Project Management</title>

    {{--CSS FIles Inclusion--}}
    <link rel="shortcut icon" href="/site_images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/assets/bower/foundation-sites/dist/css/foundation.min.css"/>
    <link rel="stylesheet" href="/assets/bower/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{mix('/css/app.css')}}"/>


</head>

<body style="margin: 0;">
<div>
    <div class="signup-left">
        <div class="row">
            <div class="small-11 small-offset-1 small-centered medium-9 medium-centered columns">
                <div class="sl-container">
                    <h1 style="text-align:center;">Make your work coordination easier</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="login">
        <div class="row">
            <div class="login-container-img hide-for-small-only">
                <img src="/site_images/ct_logo.png"/>
            </div>
            <div class="small-11 small-offset-1 small-centered medium-9 medium-centered columns">

                <div class="login-container" style="text-align: center;">
                    <div class="login-container-text">
                        <h2>Welcome to Cytonn Investments Task Management</h2>

                        <p>Login with your corporate email</p>
                        <div class="small-9" style="margin-left:10%;">
                            <p><a class="button expanded" id="sign-up-button" href="/auth/google">Sign In with
                                    google</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-container-disclaimer">
                <p>&copy;Cytonn <?php echo date("Y"); ?> All Right Reserved</p>
            </div>
        </div>
    </div>
</div>

{{-- Javascript Files--}}
<script src="/assets/bower/jquery/dist/jquery.min.js"></script>
<script src="/assets/bower/foundation-sites/dist/js/foundation.min.js"></script>

<script>
    $(document).foundation();
</script>

</body>
</html>
