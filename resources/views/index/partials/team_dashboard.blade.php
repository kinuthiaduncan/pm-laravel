<div class="content-section-header" style="padding: 10px">
    <h1 class="page-title">Summary
        <a href="{{route('task.create')}}" class="button pull-right">
            Create Task
        </a></h1>
</div>

<div class="content-section-text">
    <div class="dashboard">
        <div class="row">
            <div class="small-12 medium-6 columns activity-stream" ng-controller="ActivityStreamController">
                <h5>Activity Stream</h5>
                <activity-stream></activity-stream>
            </div>
            <div class="small-12 medium-6 columns">
                <div class="statistics">
                    <h5>System Statistics</h5>
                    <table width="100%" class="table table-bordered table-striped table-responsive">
                        <tr>
                            <td>Number of Projects</td>
                            <td>{!! $projects !!}</td>
                        </tr>
                        <tr>
                            <td>Number of Users</td>
                            <td>{!! $users !!}</td>
                        </tr>
                        <tr>
                            <td>Number of Issues</td>
                            <td>{!! $issues !!}</td>
                        </tr>
                        <tr>
                            <td>Issues in To Do Level</td>
                            <td>{!! $issues_todo !!}</td>
                        </tr>
                        <tr>
                            <td>Issues in Progress</td>
                            <td>{!! $issues_progress !!}</td>
                        </tr>
                        <tr>
                            <td>Issues in Review</td>
                            <td>{!! $issues_review !!}</td>
                        </tr>
                        <tr>
                            <td>Resolved Issues</td>
                            <td>{!! $issues_done !!}</td>
                        </tr>
                        <tr>
                            <td>Released Issues</td>
                            <td>{!! $issues_released !!}</td>
                        </tr>
                        <tr>
                            <td>Number of Tasks</td>
                            <td>{!! $tasks !!}</td>
                        </tr>
                        <tr>
                            <td>Tasks in To Do Level</td>
                            <td>{!! $tasks_todo !!}</td>
                        </tr>
                        <tr>
                            <td>Tasks in Progress</td>
                            <td>{!! $tasks_progress !!}</td>
                        </tr>
                        <tr>
                            <td>Tasks in Review</td>
                            <td>{!! $tasks_review !!}</td>
                        </tr>
                        <tr>
                            <td>Resolved Tasks</td>
                            <td>{!! $tasks_done !!}</td>
                        </tr>
                        <tr>
                            <td>Released Tasks</td>
                            <td>{!! $tasks_released !!}</td>
                        </tr>
                    </table>
                </div>
                {{--<div class="my-issues" ng-controller="MyIssuesController">--}}
                    {{--<h5>My Issues</h5>--}}
                    {{--<table style="width: 100%;" st-pipe="callServer" st-table="displayed"--}}
                           {{--class="table table-bordered table-responsive table-hover">--}}
                        {{--<tbody ng-show="!isLoading">--}}
                        {{--<tr ng-repeat="row in displayed">--}}
                            {{--<td>--}}
                                {{--<a href="/issue/[[ row.id ]]"> [[ row.key ]] </a>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<a href="/issue/[[ row.id ]]"> [[ row.title ]] </a>--}}
                            {{--</td>--}}
                            {{--<td><a href="/issue/[[ row.id ]]"> [[ row.status ]] </a></td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td class="notification-footer" colspan="3">--}}
                                {{--<div class="pull-right" st-pagination="" st-items-by-page="4"></div>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--<tbody ng-show="isLoading">--}}
                        {{--<tr colspan="3">--}}
                            {{--<td colspan="3" class="text-center">Loading ...</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>