<div>
    <div class="row">
        <div class="medium-3 large-3 column">
            <div class="prof-pic">
                @if (Auth::user()->avatar == null)
                    <i class="fa fa-user-circle profile-avatar" aria-hidden="true"></i>
                @else
                    <img src="{{Auth::user()->avatar}}"
                         class="profile-avatar">
                @endif
            </div>
        </div>
        <div class=" profile-details">
            <p class="prof-name">{{Auth::user()->preferred_name}}</p>
            <p class="gen-info">General Information</p>
            <table class="profile-info-table">
                <tr><th>Email:</th><td>{{$user->email}}</td></tr>
                <tr><th>Phone:</th><td>123456789</td></tr>
                <tr><th>Department:</th><td>{{$user->department->name}}</td></tr>
                <tr><th>Job Title:</th><td>{{$user->job_level}}</td></tr>
            </table>
        </div>
        <div class="small-6 large-3 column">
            <a href="{{url('dailyPersonalSummary')}}" class="button pull-right margin-left-10" style="float:right;">
                Export My Daily Summary</a>
        </div>
        <div class="small-6 large-3 column">
            <a href="{{url('weeklyPersonalSummary')}}" class="button pull-right margin-left-10" style="float:left;">
                Export My Weekly Summary</a>
        </div>
    </div>
    <div class="row hide-for-small-only">
            @if(($user->department_id)==2)
                    <div class="profile-issues" ng-controller="MyIssuesController">
                        <p class="gen-info">My Recent Issues</p>
                        <table st-pipe="callServer" st-table="displayed"
                               class=" table-responsive issues-table">
                            <tbody ng-show="!isLoading">
                            <tr ng-repeat="row in displayed" class="issues-row">
                                <td class="issue-data">
                                    <a href="/issue/[[ row.id ]]" class="issue-title"> [[ row.title ]] </a>
                                    <p>[[ row.due_date ]]</p>
                                </td>
                                <td class="fi-comment-quotes issue-data"><a href="/issue/[[ row.id ]]"> Comment</a></td>
                                <td class="fi-plus issue-data"><a href="/12/issue/[[ row.id ]]/board/edit"> Update</a></td>
                                <td class="fi-eye issue-data"><a href="/issue/[[ row.id ]]"> View Issue</a></td>
                            </tr>
                            <tr>
                                <td class="notification-footer" colspan="3">
                                    <div class="pull-right" st-pagination="" st-items-by-page="4"></div>
                                </td>
                            </tr>
                            </tbody>
                            <tbody ng-show="isLoading">
                            <tr colspan="3">
                                <td colspan="3" class="text-center">Loading ...</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                @else
                <div class="recent-tasks">
                    <profile-tasks-table></profile-tasks-table>
                </div>
                @endif
    </div>
</div>