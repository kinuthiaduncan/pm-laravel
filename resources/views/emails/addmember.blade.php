
@extends('layouts.email')

@section('content')

    You have been added to project <span class="heading">{!! $data->name !!}</span> by <span class="heading">  {!! $added_by !!} </span>

   <p> Below are the project details: </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project</td><td>{!!$data->name !!}</td></tr>
        </thead>
        <tbody>

        <tr><td class="bold"> Created By </td><td>  {!! \PM\Presenters\UserPresenter::presentFullNames($data->created_by) !!}  </td></tr>

        <tr><td class="bold"> Project Lead </td><td>  {!! \PM\Presenters\UserPresenter::presentFullNames($data->project_lead) !!}  </td></tr>

        <tr><td class="bold"> Project Url </td><td>{!! $data->project_url !!} </td></tr>

        <tr><td class="bold"> Project Description </td><td> {!! $data->project_description !!} </td></tr>


        </tbody>
    </table>

    <br/>
    To view the project visit <a href="http://pm.cytonn.com/project/{!! $data->id !!}">Project Management System</a>


@endsection