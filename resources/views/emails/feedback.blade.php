@extends('layouts.email')

@section('content')
    <h3> New Feedback Created</h3>
    <p> Below are the details: </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project</td><td>{!! $data->project->name !!}</td></tr>
        </thead>
        <tbody>
        <tr>
            <td><b>Created By</b></td><td>{!! $data->client_email !!}</td>
        </tr>
        <tr>
            <td><b>Subject</b></td><td>{!! $data->subject !!}</td>
        </tr>
        <tr>
            <td><b>Description</b></td><td>{!! $data->description !!}</td>
        </tr>
        </tbody>
    </table>
    <br/>
    To view the task click <a href="{!! URL::to('/project/'.$data->project->id.'/feedback/'.$data->id) !!}">HERE</a>
@endsection