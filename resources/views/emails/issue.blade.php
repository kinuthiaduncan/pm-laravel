
@extends('layouts.email')

@section('content')

    <h3> New Issue Assigned to {!! $data->assignedTo->preferred_name !!} </h3>
    <p> The following are the details </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project</td><td>{!! $data->projects->name !!}</td></tr>
        </thead>
        <tbody>

        <tr><td class="bold">Issue Title </td><td> {!! $data->title !!} </td></tr>

        <tr><td class="bold">Created By </td><td> {!! $data->creator->preferred_name !!} </td></tr>

        <tr><td class="bold">Priority</td><td>{!! $data->priorities->name !!} </td></tr>

        <tr><td class="bold">Due Date</td><td>{!! $data->date_due !!} </td></tr>

        <tr><td class="bold">Issue Type</td><td>{!! $data->issueTypes->name !!} </td></tr>

        <tr><td class="bold">Issue Description </td><td>{!! $data->description !!}</td></tr>

        </tbody>
    </table>

    <br/>
    To view the issue visit <a href="http://pm.cytonn.com/issue/{!! $data->id !!}">Project Management System</a>


@endsection

