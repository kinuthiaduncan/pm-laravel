
@extends('layouts.email')

@section('content')

    <h3>New Project Component Created by {!! $data->component_creator !!}</h3>

    <p>Further details on the component include: </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project Title </td><td>{!! $data->project->name !!}</td></tr>
        </thead>
        <tbody>

        <tr><td class="bold"> Component Name </td><td>   {!! $component_name !!}  </td></tr>

        <tr><td class="bold"> Created By </td><td>  {!! $data->component_creator !!}  </td></tr>

        <tr><td class="bold"> Component Lead </td><td> {!! $componentLead->preferred_name !!} </td></tr>

        <tr><td class="bold"> Component Description </td><td> {!! $description !!} </td></tr>


        </tbody>
    </table>

    <br/>
    To view the project component visit   <a href="http://pm.cytonn.com/project/{!! $project_id !!}">Project Management System</a>


@endsection