@extends('layouts.email')

@section('content')
    <h1> Due Issues Reminder </h1>

        <h4>Hi {!! $user->preferred_name !!},</h4>

    @if($overdue->count() > 0)
        <p class="alert">&#9888; The following issues have already exceeded their due date </p>

        <table class="table table-responsive table-striped">
            <thead>
            <tr><td>Project</td><td>Issue Title</td><td>Due Date</td><td> Status  </td><td>Created By</td><td>Action</td></tr>
            </thead>
            <tbody>
            @foreach($overdue as $issue)
                <tr>
                    <td class="project"> {!! \PM\Presenters\ProjectPresenter::presentTitle($issue['project_id']) !!} </td>
                    <td> {!! $issue['title'] !!} </td>
                    <td> {!! $issue['date_due'] !!} </td>
                    <td> {!! \PM\Presenters\StatusPresenter::presentTitle($issue['status_id']) !!} </td>
                    <td> {!! \PM\Presenters\UserPresenter::presentFullNames($issue['created_by']) !!} </td>
                    <td><a href="http://pm.cytonn.com/issue/{!! $issue['id'] !!}" class="no-underline">View On PM </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif


    @if($today->count() > 0)
        <p >The following issues are due today  </p>

        <table class="table table-responsive table-striped">
            <thead>
            <tr><td>Project</td><td>Issue Title</td><td>Due Date</td><td> Status  </td><td>Created By</td><td>Action</td></tr>
            </thead>
            <tbody>
            @foreach($today as $issue)
                <tr>
                    <td> {!! \PM\Presenters\ProjectPresenter::presentTitle($issue['project_id']) !!} </td>
                    <td> {!! $issue['title'] !!} </td>
                    <td> {!! $issue['date_due'] !!} </td>
                    <td> {!! \PM\Presenters\StatusPresenter::presentTitle($issue['status_id']) !!} </td>
                    <td> {!! \PM\Presenters\UserPresenter::presentFullNames($issue['created_by']) !!} </td>
                    <td><a href="http://pm.cytonn.com/issue/{!! $issue['id'] !!}" class="no-underline">View On PM </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif


    @if($next2days->count() > 0)
        <p>The following issues will be due within the next two days </p>

        <table class="table table-responsive table-striped">
            <thead>
            <tr><td>Project</td><td>Issue Title</td><td>Due Date</td><td> Status  </td><td>Created By</td><td>Action</td></tr>
            </thead>
            <tbody>
            @foreach($next2days as $issue)
                <tr>
                    <td> {!! \PM\Presenters\ProjectPresenter::presentTitle($issue['project_id']) !!} </td>
                    <td> {!! $issue['title'] !!} </td>
                    <td> {!! $issue['date_due'] !!} </td>
                    <td> {!! \PM\Presenters\StatusPresenter::presentTitle($issue['status_id']) !!} </td>
                    <td> {!! \PM\Presenters\UserPresenter::presentFullNames($issue['created_by']) !!} </td>
                    <td><a href="http://pm.cytonn.com/issue/{!! $issue['id'] !!}" class="no-underline">View On PM </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

@endsection

