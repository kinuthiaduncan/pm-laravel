
@extends('layouts.email')

@section('content')

    <h3>A new comment has been added</h3>
    <p> Here are the details</p>

    <table class="table table-responsive ">
        <thead>
        <tr><td class="bold"> Parent Issue  </td><td>  {!! $data->issues->title !!}  </td></tr>
        </thead>
        <tbody>
        <tr>
            <td class="bold"> Created By</td><td> {!! $data->creator->preferred_name !!} </td>
        </tr>
        <tr class="stripe">
            <td> Comment Details</td><td> {!! $data->name !!} </td>
        </tr>
        </tbody>
    </table>

    <br/>
    To view the comment visit <a href="{{ URL::to('/issue/'.$data->issues->id)}}">Project Management System</a>

@endsection