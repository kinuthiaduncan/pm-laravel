
@extends('layouts.email')

@section('content')

    The following issue has been assigned to <span class="heading">{!! \PM\Presenters\UserPresenter::presentFullNames($data->assigned_to) !!}</span>  <br/>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project</td><td>{!! $data->projects->name !!}</td></tr>
        </thead>
        <tbody>

        <tr><td class="bold">Issue Title </td><td> {!! $data->title !!} </td></tr>

        <tr><td class="bold">Edited By </td><td> {!! $data->creator->preferred_name !!} </td></tr>

        <tr><td class="bold">Priority</td><td>{!! $data->priorities->name !!} </td></tr>

        <tr><td class="bold">Due Date</td><td>{!! $data->date_due !!} </td></tr>

        <tr><td class="bold">Issue Type</td><td>{!! $data->issueTypes->name !!} </td></tr>

        <tr><td class="bold">Percentage Done</td><td> {!! $data->percentage_done !!}% </td></tr>

        <tr><td class="bold">Issue Description </td><td>{!! $data->description !!}</td></tr>

        </tbody>
    </table>

    <br/>
    To view the issue visit <a href="http://pm.cytonn.com/issue/{!! $data->id !!}">Project Management System</a>


@endsection