@extends('layouts.email')

@section('content')
    <h3> Today's Issues Reminder </h3>

    @if($today->count() > 0)
        <p>The following issues are due today </p>

        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Project</td>
                <td>Issue Title</td>
                <td>Due Date</td>
                <td> Status</td>
                <td>Created By</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($today as $issue)
                <tr>
                    <td> {!! \PM\Presenters\ProjectPresenter::presentTitle($issue['project_id']) !!} </td>
                    <td> {!! $issue['title'] !!} </td>
                    <td> {!! $issue['date_due'] !!} </td>
                    <td> {!! \PM\Presenters\StatusPresenter::presentTitle($issue['status_id']) !!} </td>
                    <td> {!! \PM\Presenters\UserPresenter::presentFullNames($issue['created_by']) !!} </td>
                    <td><a href="http://pm.cytonn.com/issue/{!! $issue['id'] !!}" class="no-underline">View On PM </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
        <p>No issues</p>
    @endif
@endsection

