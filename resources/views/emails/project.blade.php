
@extends('layouts.email')

@section('content')

    <h3>New @if($data->access == 'private') Private @else Public @endif Project Created and has been Assigned to You</h3>

    <p>Further details on the project include: </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project Title </td><td>{!!$data->name !!}</td></tr>
        </thead>
        <tbody>

        <tr><td class="bold"> Created By </td><td>  {!! \PM\Presenters\UserPresenter::presentFullNames($data->created_by) !!}  </td></tr>

        <tr><td class="bold"> Project Lead </td><td>  {!! \PM\Presenters\UserPresenter::presentFullNames($data->project_lead) !!}  </td></tr>

        <tr><td class="bold"> Project Url </td><td>{!! $data->project_url !!} </td></tr>

        <tr><td class="bold"> Project Description </td><td> {!! $data->project_description !!} </td></tr>


        </tbody>
    </table>

    <br/>
    To view the project visit   <a href="http://pm.cytonn.com/project/{!! $data->id !!}">Project Management System</a>


@endsection