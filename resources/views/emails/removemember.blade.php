
@extends('layouts.email')

@section('content')


    You have been removed from project <span class="heading"> {!! $data->name !!} </span>

    <p>Further details include: </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project</td><td>{!!$data->name !!}</td></tr>
        </thead>
        <tbody>

        <tr><td class="bold"> Removed By </td><td>  {!! $added_by !!}  </td></tr>

        <tr><td class="bold"> Project Lead </td><td>  {!! \PM\Presenters\UserPresenter::presentFullNames($data->project_lead) !!}  </td></tr>

        <tr><td class="bold"> Project Url </td><td>{!! $data->project_url !!} </td></tr>

        <tr><td class="bold"> Project Description </td><td> {!! $data->project_description !!} </td></tr>


        </tbody>
    </table>

    <br/>
    To view the project visit <a href="http://pm.cytonn.com/project/{!! $data->id !!}">Project Management System</a>



@endsection