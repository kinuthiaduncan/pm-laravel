@extends('layouts.email')

@section('content')
    <h3> This Week's Project Progress Reminder </h3>

    @if($this_week->count() > 0)
        <p>You have not updated weekly project progress for:</p>

        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Project Name</td>
                <td>Project Key</td>
                <td>Project Description</td>
                <td> Status</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($this_week as $project)
                <tr>
                    <td> {!! $project->name !!} </td>
                    <td> {!! $project->project_key !!} </td>
                    <td> {!! $project->project_description !!} </td>
                    <td> {!! $project->status !!}</td>
                    <td><a href="http://pm.cytonn.com/project/{!! $project->id !!}" class="no-underline">View On PM </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>No Projects</p>
    @endif
@endsection

