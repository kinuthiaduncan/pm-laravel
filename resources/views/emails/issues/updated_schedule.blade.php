@extends('layouts.email')

@section('content')

    <h3> New Issue Schedule Updated</h3>
    <p> Below are the details: </p>
    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Issue</td><td>{!! $data->title !!}</td></tr>
        </thead>
        <tbody>
        <tr>
            <td><b>Date</b></td><td>{!! $schedule->date !!}</td>
        </tr>
        <tr>
            <td><b>Start</b></td><td>{!! $schedule->start_time !!}</td>
        </tr>
        <tr>
            <td><b>Ended</b></td><td>{!! $schedule->end_time !!}</td>
        </tr>
        <tr>
            <td><b>Added By</b></td><td> {{$schedule->creator->firstname}} {{$schedule->creator->lastname}}</td>
        </tr>
        <tr>
            <td><b>Description</b></td><td>{!! $schedule->description !!}</td>
        </tr>
        </tbody>
    </table>
    <br/>
    To view the issue schedule click <a href="{{URL::to('/issue/'.$data->id.'/track')}}">HERE</a>
@stop