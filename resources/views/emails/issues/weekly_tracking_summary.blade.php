@extends('layouts.email')

@section('content')
    <h3> Weekly Issue Time Tracking Summary </h3>
    @if($tracked->count() > 0)
        <p><b>The following users recorded time spent on issues as:</b> </p>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Total Hours Worked</th>
                <th>Total % of Time Worked</th>
                <th>Deficiency</th>
                <th>Overtime</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tracked as $user)
                <tr>
                    <td>{!! $user->firstname !!} {!! $user->lastname !!}</td>
                    <td>{!! number_format((\PM\Presenters\NumberOfHoursPresenter::presentIssuesTotalHours($user->id,$startDate,$endDate)),2) !!}</td>
                    <td>{!! number_format((\PM\Presenters\NumberOfHoursPresenter::presentIssuesTotalHours($user->id,$startDate,$endDate))*(100/44),2) !!}%</td>
                    <td>{!! max(100-(number_format((\PM\Presenters\NumberOfHoursPresenter::presentIssuesTotalHours($user->id,$startDate,$endDate))*(100/44),2)),0) !!}%</td>
                    <td>{!! max(((number_format((\PM\Presenters\NumberOfHoursPresenter::presentIssuesTotalHours($user->id,$startDate,$endDate))*(100/44),2) )-100),0) !!}%</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    <br/>

    @if($tasksTracked->count() > 0)
        <p><b>The following users recorded time spent on tasks as:</b> </p>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Total Hours Worked</th>
                <th>Total % of Time Worked</th>
                <th>Deficiency</th>
                <th>Overtime</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tasksTracked as $user)
                <tr>
                    <td>{!! $user->firstname !!} {!! $user->lastname !!}</td>
                    <td>{!! number_format((\PM\Presenters\NumberOfHoursPresenter::presentTotalHours($user->id,$startDate,$endDate)),2) !!}</td>
                    <td>{!! number_format((\PM\Presenters\NumberOfHoursPresenter::presentTotalHours($user->id,$startDate,$endDate))*(100/44),2) !!}%</td>
                    <td>{!! max(100-(number_format((\PM\Presenters\NumberOfHoursPresenter::presentTotalHours($user->id,$startDate,$endDate))*(100/44),2)),0) !!}%</td>
                    <td>{!! max(((number_format((\PM\Presenters\NumberOfHoursPresenter::presentTotalHours($user->id,$startDate,$endDate))*(100/44),2) )-100),0) !!}%</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

    @if($notTracked->count() > 0)
        <p><b>The following members did not record time spent on issues or tasks:</b> </p>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            @foreach($notTracked as $user)
                <tr>
                    <td>{!! $user->firstname !!} {!! $user->lastname !!}</td>
                    <td>{!! $user->email !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@stop