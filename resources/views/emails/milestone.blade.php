
@extends('layouts.email')

@section('content')

    The action above has been performed by <span class="heading">{!! $data->projects->creator->preferred_name !!}</span>.<br/>

    <p>The milestone details are as follows:</p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project</td><td>{!!$data->projects->name !!}</td></tr>
        </thead>
        <tbody>

        <tr><td class="bold"> Milestone Title </td><td> {!! $data->title !!} </td></tr>

        <tr><td class="bold"> Due Date </td><td>{!! $data->due_date !!} </td></tr>

        <tr><td class="bold"> Milestone Description </td><td> {!! $data->description !!} </td></tr>


        </tbody>
    </table>

    <br/>
    To view the milestone visit <a href="http://pm.cytonn.com/project/{!! $data->id !!}">Project Management System</a>

@endsection