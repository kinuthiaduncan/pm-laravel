@extends('layouts.email')

@section('content')
    <h3>Progress Report</h3>

    <p>The following project progress reports have been filed within the last two weeks</p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project</td><td>Progress Reports</td></tr>
        </thead>
        <tbody>
        @foreach($progress as $project)
            <tr>
                <td>{{ $project->name }}</td>
                <td>
                    <ol type="i">
                        @foreach($project->progress()->filedAfter($cut_off)->get() as $report)
                            <li>{{ $report->description }} ~ <small>{{ $report->created_at->diffForHumans() }}</small></li>
                        @endforeach
                        <li>
                            {{ $created = $project->issues()->createdAfter($cut_off)->count() }} {{ str_plural('issue', $created) }} created,

                            {{ $resolved = $project->issues()->completedAfter($cut_off)->count() }} {{ str_plural('issue', $resolved) }} resolved.
                        </li>
                    </ol>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <p>There has been no progress report filed on the following projects</p>

    <table class="table table-responsive table-striped">
        <thead>
            <tr><td>Project</td><td>Issues Completed</td></tr>
        </thead>
        <tbody>
        @foreach($no_progress as $project)
            <tr>
                <td>{{ $project->name }}</td>
                <td>
                   {{ $created = $project->issues()->createdAfter($cut_off)->count() }} {{ str_plural('issue', $created) }} created,

                    {{ $resolved = $project->issues()->completedAfter($cut_off)->count() }} {{ str_plural('issue', $resolved) }} resolved.
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <p>This report covers progress between {{ $cut_off->toFormattedDateString() }} and {{ $end->toFormattedDateString() }}</p>
@endsection

