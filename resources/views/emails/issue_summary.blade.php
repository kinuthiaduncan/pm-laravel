@extends('layouts.email')

@section('content')
    <h3> This Week's Issues Summary </h3>

    @if($issuesCreated->count() > 0)
        <h3> Issues Created this Week </h3>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Project</td>
                <td>Issue Title</td>
                <td>Due Date</td>
                <td> Status</td>
                <td>Created By</td>
                <td>Assigned To</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($issuesCreated as $project)
                <tr>
                    <td rowspan="{{ ($project->issues()->IssuesCreatedAfter($startDate,$project->id)->get()->count()) + 1 }}"><b>{!! $project->name !!}</b></td>
                @foreach($project->issues()->IssuesCreatedAfter($startDate,$project->id)->get() as $issue)
                    <tr>
                        <td> {!! $issue->title !!} </td>
                        <td> {!! $issue->date_due !!} </td>
                        <td> {!! \PM\Presenters\StatusPresenter::presentTitle($issue->status_id) !!} </td>
                        <td> {!! \PM\Presenters\UserPresenter::presentFullNames($issue->created_by) !!} </td>
                        <td> {!! \PM\Presenters\UserPresenter::presentFullNames($issue->assigned_to) !!} </td>
                        <td><a href="http://pm.cytonn.com/issue/{!! $issue->id !!}" class="no-underline">View On PM </a>
                        </td>
                    </tr>
                    @endforeach
                    </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>No issues created this week</p>
    @endif
    @if($issuesUpdatedReview->count() > 0)
        <h3> Issues Submitted for Review this Week </h3>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Project</td>
                <td>Issue Title</td>
                <td>Due Date</td>
                <td> Status</td>
                <td> Status Change Date</td>
                <td>Assigned To</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($issuesUpdatedReview as $project)
                <tr>
                    <td rowspan="{{ ($project->issues()->StatusChangedAfter($startDate,$project->id,3)->get()->count()) + 1 }}"><b>{!! $project->name !!}</b></td>
                        @foreach($project->issues()->StatusChangedAfter($startDate,$project->id,3)->get() as $issue)
                        <tr>
                            <td> {!! $issue->title !!} </td>
                            <td> {!! $issue->date_due !!} </td>
                            <td> {!! \PM\Presenters\StatusPresenter::presentTitle($issue->status_id) !!} </td>
                            <td>{!! $issue->status_change_date !!}</td>
                            <td> {!! \PM\Presenters\UserPresenter::presentFullNames($issue->assigned_to) !!} </td>
                            <td><a href="http://pm.cytonn.com/issue/{!! $issue->id !!}" class="no-underline">View On PM </a>
                            </td>
                        </tr>
                        @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p><b>No issues Submitted for review this week</b></p>
    @endif

    @if($issuesUpdatedDone->count() > 0)
        <h3> Issues Resolved this Week </h3>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Project</td>
                <td>Issue Title</td>
                <td>Due Date</td>
                <td> Status</td>
                <td>Status Change Date</td>
                <td>Assigned To</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($issuesUpdatedDone as $project)
                <tr>
                    <td rowspan="{{ ($project->issues()->StatusChangedAfter($startDate,$project->id,4)->get()->count()) + 1 }}"><b>{!! $project->name !!}</b></td>
                @foreach($project->issues()->StatusChangedAfter($startDate,$project->id,4)->get() as $issue)
                    <tr>
                        <td> {!! $issue->title !!} </td>
                        <td> {!! $issue->date_due !!} </td>
                        <td> {!! \PM\Presenters\StatusPresenter::presentTitle($issue->status_id) !!} </td>
                        <td>{!! $issue->status_change_date !!}</td>
                        <td> {!! \PM\Presenters\UserPresenter::presentFullNames($issue->assigned_to) !!} </td>
                        <td><a href="http://pm.cytonn.com/issue/{!! $issue->id !!}" class="no-underline">View On PM </a>
                        </td>
                    </tr>
                    @endforeach
                    </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p><b>No issues resolved this week</b></p>
    @endif
@stop