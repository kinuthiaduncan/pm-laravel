@extends('layouts.email')

@section('content')
    <h3> New Project Phase Activity Created</h3>
    <p> Below are the details: </p>
    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Project</td><td>{!! $phase->project->name !!}</td></tr>
        </thead>
        <tbody>
        <tr>
            <td><b>Project Phase</b></td><td>{!! $phase->name !!}</td>
        </tr>
        <tr>
            <td><b>Title</b></td><td>{!! $data->name !!}</td>
        </tr>
        <tr>
            <td><b>Start Date</b></td><td>{!! $data->start_date !!}</td>
        </tr>
        <tr>
            <td><b>Due Date</b></td><td>{!! $data->end_date !!}</td>
        </tr>
        <tr>
            <td><b>Percent Complete</b></td><td>{!! $data->percent_complete !!}%</td>
        </tr>
        <tr>
            <td><b>Created By</b></td><td>{!! \PM\Presenters\UserPresenter::presentFullNames($data->created_by) !!}</td>
        </tr>
        <tr>
            <td><b>Comments</b></td>
            <td>@if(!is_null($data->comments))
                    {!! $data->comments !!}
                @endif</td>
        </tr>
        </tbody>
    </table>
    <br/>
    To view the project Phase Activity visit <a href="{{URL::to('/project/'.$phase->project->id.'/phases/'.$phase->id)}}">Project Management System</a>

@stop