@extends('layouts.email')

@section('content')
    <h3> New Project Phase Added</h3>
    <p> Below are the details: </p>
    <table class="table table-responsive table-striped">
        <thead>
            <tr><td>Project</td><td>{!! $data->project->name !!}</td></tr>
        </thead>
        <tbody>
            <tr>
                <td><b>Title</b></td><td>{!! $data->name !!}</td>
            </tr>
            <tr>
                <td><b>Start Date</b></td><td>{!! $data->start_date !!}</td>
            </tr>
            <tr>
                <td><b>Due Date</b></td><td>{!! $data->end_date !!}</td>
            </tr>
            <tr>
                <td><b>Created By</b></td><td>{!! \PM\Presenters\UserPresenter::presentFullNames($data->created_by) !!}</td>
            </tr>
            <tr>
                <td><b>Description</b></td>
                <td>@if(!is_null($data->decisions))
                {!! $data->decisions !!}
                @endif</td>
            </tr>
        </tbody>
    </table>
    <br/>
    To view the project Phase visit <a href="{{URL::to('/project/'.$data->project->id.'/phases')}}">Project Management System</a>

@stop