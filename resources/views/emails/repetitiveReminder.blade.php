@extends('layouts.email')
@section('content')
    <h3>Reminder for the Repetitive Task Assigned to you</h3>
    <p> Below are the task details: </p>
    <table class="table table-responsive table-striped">
        <thead>
        <th>Task Title</th><th>Assigned By</th><th>Due Date</th><th>Description</th>
        </thead>
        <tbody>
        @foreach($tasksToRemind as $task)
          <tr>
              <td>{!! $task['title'] !!} </td>
              <td> {!! $task['creator'] !!}</td>
              <td>{!! $task['due_date'] !!} </td>
              <td>{!! $task['description'] !!}</td>
          </tr>

        {{--<tr></tr>--}}
        @endforeach
        </tbody>
            </table>



    <br/>
    {{--To view the task click <a href="http://pm.cytonn.com/task/{!! $tasksToRemind->id !!}">HERE</a>--}}
@endsection