@extends('layouts.email')

@section('content')
    <h3> Today's Tasks Reminder </h3>

    @if($today->count() > 0)
        <p>The following tasks are due today </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr>
            <td>Task Title</td>
            <td>Due Date</td>
            <td> Status</td>
            <td>Created By</td>
            <td>Action</td>
        </tr>
        </thead>
        <tbody>
        @foreach($today as $task)
            <tr>
                <td> {!! $task['title'] !!} </td>
                <td> {!! $task['due_date'] !!} </td>
                <td> {!! \PM\Presenters\StatusPresenter::presentTitle($task['status_id']) !!} </td>
                <td> {!! \PM\Presenters\UserPresenter::presentFullNames($task['created_by']) !!} </td>
                <td><a href="http://pm.cytonn.com/tasks/my-tasks/{!! $task['id'] !!}" class="no-underline">View On PM </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
        <p>No tasks</p>
    @endif
@stop