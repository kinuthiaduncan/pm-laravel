@extends('layouts.email')

@section('content')

    <h3> New Comment Created</h3>
    <p> Below are the details: </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Task</td><td>{!! $data->title !!}</td></tr>
        </thead>
        <tbody>


        <tr><td class="bold">Assigned To </td>
            <td> <ul>
                    @foreach($assigned_users as $assignee)
                        <li>
                            {{$assignee->user->preferred_name }}
                        </li>
                    @endforeach
                </ul>
            </td></tr>

        <tr><td class="bold">Assigned By </td><td> {!! $data->creator->preferred_name !!} </td></tr>

        <tr><td class="bold">Priority</td><td>{!! $data->priority->name !!} </td></tr>

        <tr><td class="bold">Due Date</td><td>{!! $data->due_date !!} </td></tr>

        <tr><td class="bold">Comment </td><td>{!! $comment->comment !!}</td></tr>

        <tr><td class="bold">Comment By </td><td>{!! $comment->user->preferred_name !!}</td></tr>

        </tbody>
    </table>

    <br/>
    To view the task click <a href="{!! URL::to('/tasks/my-tasks/'.$data->id) !!} ">HERE</a>
@endsection

