@extends('layouts.email')

@section('content')
    <h3> New Progress Report made for Task {{$data->task->title}} by
        {{$data->addedBy->preferred_name }} </h3>
    <p> Below are the progress report details: </p>
    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Title</td><td>{!! $data->title !!}</td></tr>
        </thead>
        <tbody>
        <tr><td class="bold">Description </td><td> {!! $data->description !!} </td></tr>
        </tbody>
    </table>

    <br/>
    To view the progress report click <a href="{!! URL::to('/tasks/my-tasks/'.$data->task->id) !!} ">HERE</a>

@stop