@extends('layouts.email')

@section('content')
    @if($users->count() > 0)
        <p><b>The following users did not track time on their tasks this week:</b> </p>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Name</td><td>Email</td>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{!! $user->preferred_name !!}</td>
                    <td>{!! $user->email !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection