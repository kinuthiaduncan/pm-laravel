@extends('layouts.email')

@section('content')

    <h3> New Task Schedule Added</h3>
    <p> Below are the details: </p>
    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Task</td><td>{!! $task->title !!}</td></tr>
        </thead>
        <tbody>
        <tr>
            <td><b>Date</b></td><td>{!! $data->date !!}</td>
        </tr>
        <tr>
            <td><b>Start</b></td><td>{!! $data->start_time !!}</td>
        </tr>
        <tr>
            <td><b>Ended</b></td><td>{!! $data->end_time !!}</td>
        </tr>
        <tr>
            <td><b>Added By</b></td><td> {{$data->creator->preferred_name}}</td>
        </tr>
        <tr>
            <td><b>Description</b></td><td>{!! $data->description !!}</td>
        </tr>
        </tbody>
    </table>
    <br/>
    To view the task time schedule click <a href="http://pm.cytonn.com/tasks/task/{!! $task->id !!}/track">HERE</a>
@stop