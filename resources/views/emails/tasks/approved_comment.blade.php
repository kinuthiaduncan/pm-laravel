@extends('layouts.email')

@section('content')
    <h3> Task Comment Approved</h3>
    <p> Below are the details: </p>
    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Task</td><td>{!! $data['task']->title !!}</td></tr>
        </thead>
        <tbody>
        <tr><td class="bold">Comment</td><td> {!! $data['comment'] !!} </td></tr>
        </tbody>
    </table>

    <br/>
    To view the comment click <a href="{!! URL::to('/tasks/my-tasks/'.$data->task->id) !!} ">HERE</a>
@endsection