@extends('layouts.email')

@section('content')
    <h3> Weekly Task Tracking Summary </h3>
    @if($schedules->count() > 0)
        <p><b>The following staff members tracked time on their tasks as:</b> </p>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Total Hours Worked</th>
                <th>Total % of Time Worked</th>
                <th>Deficiency</th>
                <th>Overtime</th>
            </tr>
            </thead>
            <tbody>
            @foreach($schedules as $task)
                <tr>
                    <td>{!! $task->creator->preferred_name !!}</td>
                    <td>{!! number_format((\PM\Presenters\NumberOfHoursPresenter::presentTotalHours($task->created_by,$startDate,$endDate)),2) !!}</td>
                    <td>{!! number_format((\PM\Presenters\NumberOfHoursPresenter::presentTotalHours($task->created_by,$startDate,$endDate))*(100/44),2) !!}%</td>
                    <td>{!! max(100-(number_format((\PM\Presenters\NumberOfHoursPresenter::presentTotalHours($task->created_by,$startDate,$endDate))*(100/44),2)),0) !!}%</td>
                    <td>{!! max(((number_format((\PM\Presenters\NumberOfHoursPresenter::presentTotalHours($task->created_by,$startDate,$endDate))*(100/44),2) )-100),0) !!}%</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@stop