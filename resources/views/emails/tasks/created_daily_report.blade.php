
@extends('layouts.email')

@section('content')

    <h3> New Daily Report Created</h3>
    <p> Below are the report details: </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr>
            <th>Item Name</th>
            <th>Sub Item</th>
            <th>Department</th>
            <th>Date</th>
            <th>Description</th>
            <th>Created By</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $emailData)
        <tr>
            <td>{!! $emailData->item->item !!}</td>
            <td>
                @if($emailData->subitem != null)
                     {!! $emailData->subitem->name !!}
                @endif
            </td>
            <td>{!! $emailData->department->name !!}</td>
            <td>{!! $emailData->date !!}</td>
            <td>{!! $emailData->description !!}</td>
            <td>{!! $emailData->creator->preferred_name !!}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <br/>
@endsection

