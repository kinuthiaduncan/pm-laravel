@extends('layouts.email')

@section('content')
    <h3> Due Tasks Report </h3>
    @if($overdue->count() > 0)
        <p class="alert">The following tasks have already exceeded their due date </p>

        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Task Title</td>
                <td>Due Date</td>
                <td> Status</td>
                <td>Assigned Users</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($overdue as $task)
                <tr>
                    <td> {!! $task['title'] !!} </td>
                    <td> {!! $task['due_date'] !!} </td>
                    <td> {!! \PM\Presenters\StatusPresenter::presentTitle($task['status_id']) !!} </td>
                    <td>
                        @foreach($task->assignedUsers as $assigned)
                            <p style="font-size: 11px;line-height: 2px;">{!! $assigned->user->preferred_name !!}</p>
                        @endforeach
                    </td>
                    <td><a href="{!! URL::to('/tasks/my-tasks/'.$task['id']) !!}" class="no-underline">View On
                            PM </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

    @if($today->count() > 0)
        <p>The following tasks are due today </p>

        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Task Title</td>
                <td>Due Date</td>
                <td> Status</td>
                <td>Assigned Users</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($overdue as $task)
                <tr>
                    <td> {!! $task['title'] !!} </td>
                    <td> {!! $task['due_date'] !!} </td>
                    <td> {!! \PM\Presenters\StatusPresenter::presentTitle($task['status_id']) !!} </td>
                    <td>
                        @foreach($task->assignedUsers as $assigned)
                            <p style="font-size: 11px;line-height: 2px;">{!! $assigned->user->preferred_name !!}</p>
                        @endforeach
                    </td>
                    <td><a href="{!! URL::to('/tasks/my-tasks/'.$task['id']) !!}" class="no-underline">View On
                            PM </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

    @if($next2days->count() > 0)
        <p>The following tasks will be due within the next two days </p>

        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Task Title</td>
                <td>Due Date</td>
                <td> Status</td>
                <td>Assigned Users</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($overdue as $task)
                <tr>
                    <td> {!! $task['title'] !!} </td>
                    <td> {!! $task['due_date'] !!} </td>
                    <td> {!! \PM\Presenters\StatusPresenter::presentTitle($task['status_id']) !!} </td>
                    <td>
                        @foreach($task->assignedUsers as $assigned)
                            <p style="font-size: 11px;line-height: 2px;">{!! $assigned->user->preferred_name !!}</p>
                        @endforeach
                    </td>
                    <td><a href="{!! URL::to('/tasks/my-tasks/'.$task['id']) !!}" class="no-underline">View On
                            PM </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection