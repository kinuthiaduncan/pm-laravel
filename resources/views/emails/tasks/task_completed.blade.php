
@extends('layouts.email')

@section('content')

    <h3>The following task has been marked as complete by {{Auth::user()->preferred_name}}.</h3>

    <table class="table table-responsive table-striped">
        <thead>
        <tr><td>Title</td><td>{!! $data->title !!}</td></tr>
        </thead>
        <tbody>

        <tr><td class="bold">Assigned To </td>
            <td> <ul>
                    @foreach($assigned_users as $assignee)
                        <li>
                            {{$assignee->user->preferred_name }}
                        </li>
                    @endforeach
                </ul>
            </td></tr>

        <tr><td class="bold">Assigned By </td><td> {!! $data->creator->preferred_name !!} </td></tr>

        <tr><td class="bold">Priority</td><td>{!! $data->priority->name !!} </td></tr>

        <tr><td class="bold">Due Date</td><td>{!! $data->due_date !!} </td></tr>

        @if(!is_null($data->category_id))
            <tr><td class="bold">Category</td><td>{!! $data->taskCategory->name !!} </td></tr>
        @endif

        <tr><td class="bold">Description </td><td>{!! $data->description !!}</td></tr>

        </tbody>
    </table>

    <br/>
    To view the task click <a href="{!! URL::to('/tasks/my-tasks/'.$data->id) !!} ">HERE</a>
@endsection

