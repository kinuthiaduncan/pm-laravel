@extends('layouts.email')

@section('content')
    <h3> Below are the Tasks Created Today</h3>
    @if($createdTasks->count() > 0)
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Task Name</td>
                <td>Priority</td>
                <td>Due Date</td>
                <td> Status</td>
                <td>Created By</td>
                <td>Assigned To</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($createdTasks as $task)
                <tr>
                    <td>{!! $task->title !!}</td>
                    <td>{!! $task->priority->name !!}</td>
                    <td>{!! $task->due_date !!}</td>
                    <td>{!! $task->status->name !!}</td>
                    <td>{!! $task->creator->preferred_name !!}</td>
                    <td>
                        @foreach($task->assignedUsers as $assigned)
                            <p class="assigned">{!! $assigned->user->preferred_name !!}</p>
                        @endforeach
                    </td>
                    <td>View</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@stop