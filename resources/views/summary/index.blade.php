@extends('layouts.default')

@section('content')
    <div class="content-section-header">
        <div class="small-12 column">
            <h1 class="page-title">Dashboard
                <a class="button pull-right" href="{{route('task.create')}}"
                   style="margin-right: 15px;"><strong>+</strong>
                    Add Task</a>
            </h1>
        </div>
        <div class="small-12 columns">

            <a href="{{url('daily-personal-summary')}}" class="button pull-left" style="margin-left: 24px">
                Export My Daily Summary</a>

            <a href="{{url('weekly-personal-summary')}}" class="button pull-left margin-left-10">
                Export My Weekly Summary</a>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <user-summary :users="{{json_encode($users)}}"></user-summary>
        </div>
        {{--<div class="small-2 columns">--}}
            {{--<a href="{{url('exportSummary')}}" class="button pull-left margin-left-10" style="margin-top: 20px;">--}}
                {{--Export User Summary</a>--}}
        {{--</div>--}}
    </div>
@endsection