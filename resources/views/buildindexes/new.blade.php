@extends('layouts.default')
@section('content')
    @if($response == 1)
        <p>
            A new Elasticsearch index has been created.
        </p>
    @else
        <p>
            The Elasticsearch index was not created successfully.
        </p>
        <p>
            Please check if another index exists and delete it before recreating another.
        </p>
    @endif
@stop