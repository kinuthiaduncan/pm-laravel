$(function () {
    today = new Date();
    today.setHours(0,0,0);

    todayNow = new Date();

    //Date when a project starts
    $('#projectStartDate').fdatepicker({
        format: 'yyyy-mm-dd',
        disableDblClickSelection: true,
        onRender: function (date) {
            return date.valueOf() < today.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        today = new Date(ev.date);
        today.setHours(0,0,0);
    }).keydown(function(event) {event.preventDefault();});

    //Date when the project is to end
    $('#projectEndDate').fdatepicker({
        format: 'yyyy-mm-dd',
        disableDblClickSelection: true,
        onRender: function (date) {
            return date.valueOf() < today.valueOf() ? 'disabled' : '';
        }
    }).keydown(function(event) {event.preventDefault();});
});