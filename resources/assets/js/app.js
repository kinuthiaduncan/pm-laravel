/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('board', require('./components/board/board.vue'));
Vue.component('drop-down', require('./components/packages/VueMultiSelect.vue'));
Vue.component('vue-single-select', require('./components/packages/VueSingleSelect.vue'));
Vue.component('projects-table', require('./components/projects/ProjectsTable.vue'));
Vue.component('tasks-table', require('./components/tasks/TasksTable.vue'));
Vue.component('public-tasks-table', require('./components/tasks/PublicTasksTable.vue'));
Vue.component('vue-date-picker', require('./components/packages/VueDatePicker.vue'));
Vue.component('comments', require('./components/tasks/TaskComments.vue'));
Vue.component('profile-tasks-table', require('./components/tasks/ProfileTasksTable.vue'));
Vue.component('users', require('./components/users/Users.vue'));
Vue.component('followed-tasks', require('./components/tasks/FollowedTasks.vue'));
Vue.component('user-summary', require('./components/summary/Summary.vue'));
Vue.component('user-profile', require('./components/summary/UserProfile.vue'));
Vue.component('activity-stream', require('./components/projects/ActivityStream.vue'));
Vue.component('reports-table',require('./components/reports/ReportsTable.vue'));
Vue.component('full-calendar', require('./components/packages/Timeline.vue'));
Vue.component('task-category-table',require('./components/tasks/TaskCategories.vue'));
Vue.component('completed-tasks', require('./components/dashboard/TasksComplete.vue'));
Vue.component('ongoing-tasks',require('./components/dashboard/TasksOngoing.vue'));
Vue.component('pending-tasks',require('./components/dashboard/TasksPending.vue'));
Vue.component('date-picker', require('./components/packages/Datepicker.vue'));
Vue.component('modal',require('./components/board/modal.vue'));
Vue.component('start-time',require('./components/packages/StartTimepicker.vue'));
Vue.component('end-time',require('./components/packages/EndTimepicker.vue'));
Vue.component('new-approvals',require('./components/projects/NewApprovals.vue'));
Vue.component('updated-approvals',require('./components/projects/UpdatedApprovals.vue'));
Vue.component('individual-projects', require('./components/projects/IndividualProjects.vue'));
Vue.component('private-projects', require('./components/projects/PrivateProjects.vue'));
Vue.component('issue-comments', require('./components/issues/IssueComments.vue'));
Vue.component('tasks-modal', require('./components/tasks/Modal.vue'));
Vue.component('assigned-to', require('./components/packages/AssignedSelect.vue'));
Vue.component('task-edit', require('./components/tasks/EditForm.vue'));
Vue.component('task-create', require('./components/tasks/CreateForm.vue'));


Vue.directive('select', {
    twoWay: true,
    inserted: function (el) {
        $(el).select2();
    },
    updated: function (el) {
        $(el).select2();
    }
});

const app = new Vue({
    el: '#app',
    data: {
        taskFormVisible: false,
        seen: true,
        tasks: null,
        name: '',
        item:'',
        user: '',
        title: '',
        status: '',
        task_access: '',
        options: {},
        content: '',
        value: '',
        department: '',
        date: '',
        editor: '',
        selected: '',
        showSelect: false,
        isDisabled: false,
        search_input: null,
        global_search_results: {},
        isRepetitive: false,
        repetitive: 'small-8 medium-3 columns create-task-col-2 repetitive',
        notRepetitive: 'small-8 medium-3 columns create-task-col-2 not-repetitive',
        medium3: 'small-8 medium-3 columns create-task-col-2',
        medium6: 'small-8 medium-6 columns create-task-col-2',
        medium12:'small-12 medium-12 columns create-task-col-2',
        showApproversField: false,
        subItems: '',
        department_id:'',
        department_items:'',
        loading: false,
        daily_reports: [],
        task_category_id:'',
        subcategories:[],
        hasSubCategory: false,
        showModal: false,
        tasksModal: false,
        more_fields: false,
        status_id:'',
        assign_date:'',
        due_date:'',
        assigned_to:[],
        interval:'',
        user_group:[],
        description:'',
    },

    methods: {
        showTasksModal(){
            console.log('tasksModal');
            this.tasksModal = !this.tasksModal;
            console.log(this.tasksModal);
        },
        getProjects() {
            Event.$emit('filtering', {
                name: this.name,
                user: this.user
            });
        },
        getIndividualProjects() {
            Event.$emit('filtering', {
                name: this.name,
            });
        },
        getTasks() {
            Event.$emit('filtering_tasks', {
                status: this.status,
                task_access: this.task_access,
                title: this.title
            });
        },
        getPublicTasks() {
            Event.$emit('filtering_public_tasks', {
                status: this.status,
                title: this.title,
                task_access: this.task_access,
                department: this.department,
                user: this.user
            });
        },

        fetch() {
            Event.$emit('filtering_issues', {
                assigned: this.assigned,
                search: this.search,
                start_date: this.start_date,
                end_date: this.end_date,
            });
        },

        getReports() {
            Event.$emit('filtering_reports', {
                date: this.date,
                department: this.department,
            });
        },

        getTaskCategories(){
            Event.$emit('filtering_categories', {
                department: this.department,
            });
        },
        getCompleteTasks(){
            Event.$emit('get_completed_tasks');
        },
        getOngoingTasks(){
            Event.$emit('get_ongoing_tasks');
        },
        getPendingTasks(){
            Event.$emit('get_pending_tasks');
        },
        globalSearch() {
            var vm = this;
            Vue.http.get('/api/global/search', {
                params: {
                    search_input: this.search_input
                }
            })
                .then(function (response) {
                    vm.global_search_results = response.data;
                });
        },
        isRepetitiveChecker () {
            this.isRepetitive = !this.isRepetitive;
        },
        subItemsPicker(){
            let vm = this;
            Vue.http.get( "/api/get-sub-items", {
                params: {
                    item: this.item
                }
            })
                .then(function (response) {
                    console.log(response.data);
                    vm.subItems = response.data;
                });

            // vm.subItems = vm.subitems;
            console.log( vm.subItems );
        },

        subCategoriesPicker(){
            let vm = this;
            Vue.http.get("/api/get/task-subcategories",{
                params:{
                    task_category_id: this.task_category_id
                }
            }).then(function(response){
                vm.subcategories = response.data;
                if((Object.keys(vm.subcategories).length) > 0)
                {
                    vm.hasSubCategory = true;
                }
                else{
                    vm.hasSubCategory = false;
                }
                console.log(vm.hasSubCategory);
            });
        },

        itemsPicker(){
           let vm = this;
            vm.loading = true;

            Vue.http.get( "/api/get-items", {
                params: {
                    department_id: this.department_id
                }
            }).then(function (response) {
                    vm.department_items = response.data;
                });

            Vue.http.get( "/api/get-reports", {
                params: {
                    department_id: this.department_id
                }
            }).then(function (response) {
                console.log(response.data);
                vm.daily_reports = response.data;
            });
            vm.loading = false;
        },
    }
});
