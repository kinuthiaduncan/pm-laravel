<?php
namespace Tests;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use PM\Models\Department;
use PM\Models\Issue;
use PM\Models\IssueType;
use PM\Models\Priority;
use PM\Models\Project;
use PM\Models\ProjectCategory;
use PM\Models\ProjectComponent;
use PM\Models\ProjectType;
use PM\Models\Roles;
use PM\Models\Status;
use PM\Models\Task;
use PM\Models\TaskCategory;
use PM\Models\TaskTracking;
use PM\Models\User;

trait Base
{

    /**
     * Set up system user
     * @return mixed
     */
    public function setUpUser()
    {
        $user = [
        'id' => '108',
        'firstname' => 'Duncan  ',
        'lastname' => 'Kinuthia',
        'preferred_name'=>'Duncan Kinuthia',
        'email' => 'dkinuthia@cytonn.com',
        'phone_number' => '07000104993',
        'active' => 1
            ];

        return User::create($user);
    }

    /**
     * User setup
     */
    public function setUpUserRequirements()
    {
        $this->setUpDepartments();
        $this->setUpTaskCategories();
        $this->setUpTaskStatus();
        $this->setUpTaskPriorities();
        $this->setUpRoles();
    }

    /**
     * Set up system roles
     */
    public function setUpRoles()
    {
        $roles = [
            ['role' => 'super Administrator'],
            ['role' => 'Administrator'],
            ['role' => 'Staff'],
            ['role' => 'Restricted']
        ];
        foreach($roles as $role)
        {
            return Roles::create($role);
        }
    }

    /**
     * Set up task categories
     * @return mixed
     */
    public function setUpTaskCategories()
    {
        $department = Department::where('name','=','Cytonn Distribution')->pluck('id');
        $task_categories = [
            ['name' => 'Category 1','department_id'=>1],
            ['name' => 'Category 2','department_id'=>2],
            ['name' => 'Category 3','department_id'=>3],
            ['name' => 'Category 4','department_id'=>4]
        ];
        foreach ($task_categories as $task_category)
        {
            return TaskCategory::create($task_category);
        }
    }

    /**
     * Set up task statuses
     * @return mixed
     */
    public function setUpTaskStatus()
    {
        $task_status = [
            ['name' => 'Todo'],
            ['name' => 'In Progress'],
            ['name'=>'Complete']
        ];
        foreach ($task_status as $status)
        {
            return Status::create($status);
        }
    }

    /**
     * set up task priorities
     * @return mixed
     */
    public function setUpTaskPriorities()
    {
        $priorities = [
            ['name' => 'High'],
            ['name' => 'Low']
        ];
        foreach ($priorities as $priority)
        {
            return Priority::create($priority);
        }
    }

    /**
     *Department Setup
     */
    public function setUpDepartments()
    {
        $departments = [
            ['name' => 'Cytonn Distribution','department_email'=>'distribution@cytonn.com','hr_department_id'=>'1'],
            ['name' => 'Cytonn Technologies','department_email'=>'ct@cytonn.com','hr_department_id'=>'2'],
            ['name' => 'Cytonn Real Estate','department_email'=>'cre@cytonn.com','hr_department_id'=>'3'],
            ['name' => 'Cytonn Brand','department_email'=>'brand@cytonn.com','hr_department_id'=>'4'],
            ['name' => 'Cytonn Administration','department_email'=>'admin@cytonn.com','hr_department_id'=>'5'],
            ['name' => 'Cytonn Diaspora','department_email'=>'diaspora@cytonn.com','hr_department_id'=>'6'],
            ['name' => 'Cytonn Human Resource','department_email'=>'hr@cytonn.com','hr_department_id'=>'7'],
            ['name' => 'Cytonn Investments','department_email'=>'investment@cytonn.com','hr_department_id'=>'8'],
            ['name' => 'Cytonn Finance','department_email'=>'finance@cytonn.com','hr_department_id'=>'9'],
            [ 'name' => 'Cytonn Operations','department_email'=>'operation@cytonn.com','hr_department_id'=>'10'],
            [ 'department_name' => 'Cytonn Legal','department_email'=>'legal@cytonn.com','hr_department_id'=>'11']
        ];

        foreach($departments as $department)
        {
            return Department::create($department);
        }
    }

    /**
     * Create new task
     * @return mixed
     */
    public function createTask()
    {
        $this->setUpUserRequirements();

        $category = TaskCategory::where('name',"Category 1")->first();
        $user = User::where('email',"dkinuthia@cytonn.com")->first();
        $department =  Department::where('name',"Cytonn Distribution")->first();
        $status = Status::where('name',"Todo")->first();
        $priority = Priority::where('name',"high")->first();


        $task = Task::create([
            'title'=>'TDD test task',
            'description'=>'TDD task description',
            'task_access'=>'public',
            'priority_id'=>$priority->id,
            'due_date'=>'2017-06-22',
            'repetitive'=>0,
            'task_category_id'=>$category->id,
            'status_id'=>$status->id,
            'created_by'=>Auth::id(),
            'department_id'=>$department->id,
        ]);
        return $task;
    }

    /**
     * Create task schedule
     * @param $task
     * @return mixed
     */
    public function createTaskSchedule($task)
    {
       $data = ([
            'date'=>'2017-05-06',
            'start_time'=>'03:00:00',
            'end_time'=>'04:30:00',
            'description'=>'TDD Test Task Schedule Description',
           'task_id'=>$task
        ]);

        return $schedule = $this->post("/tasks/task/$task/track",$data);
    }

    /**
     * Add project categories
     * @return mixed
     */
    public function addProjectCategories()
    {
        $data = ([
            'slug'=>'TDD Project Category Slug',
            'name'=>'TDD Project Category',
            'description'=>'A TDD Test project category',
            'created_by'=>Auth::id()
        ]);
        return $projectCategory = ProjectCategory::create($data);
    }

    /**
     * Add project types
     * @return mixed
     */
    public function addProjectTypes()
    {
        $data = ([
           'name'=>'software'
        ]);
        return $projectType = ProjectType::create($data);
    }

    /**
     * Create new project
     * @return mixed
     */
    public function createProject()
    {
        $this->addProjectCategories();
        $this->addProjectTypes();
        $category = ProjectCategory::where('slug','TDD Project Category Slug')->first();
        $type = ProjectType::where('name','software')->first();
        $data = ([
           'name'=>'TDD Test Project',
            'project_type'=>$type->id,
            'project_key'=>'TDD',
            'project_description'=>'TDD Test project description',
            'project_url'=>'tdd.com',
            'project_lead'=>Auth::id(),
            'project_access'=>'public',
            'category_id'=>$category->id,
            'status'=>'Ongoing',
            'active'=>1,
            'priority'=>'high',
            'resources_required'=>'n/a',
            'resources_assigned'=>'n/a'
        ]);
        return $project = $this->post('/project/store',$data);
    }

    /**
     * create project components
     * @return mixed
     */
    public function createProjectComponents()
    {
        $this->createProject();
        $project = Project::where('project_key','TDD')->first();
        $data = ([
            'project_id'=>$project->id,
            'component_name'=>'development',
            'description'=>'A test component',
            'component_lead'=>Auth::id()
        ]);
        return $component = $this->post('/project/'.$project->id,$data);
    }

    /**
     * Create issue types
     * @return mixed
     */
    public function createIssueTypes()
    {
        $data = ([
            'name'=>'improvement'
        ]);
        return $issueType = IssueType::create($data);
    }

    /**
     * Create issue
     * @return mixed
     */
    public function createIssue()
    {
        $this->createProjectComponents();
        $this->createIssueTypes();
        $this->setUpTaskStatus();
        $this->setUpTaskPriorities();
        $project = Project::where('project_key','TDD')->first();
        $type = IssueType::where('name','improvement')->first();
        $status = Status::where('name','Todo')->first();
        $priority = Priority::where('name','High')->first();
        $component = ProjectComponent::where('project_id',$project->id)->first();
        $data = ([
            'project_id'=>$project->id,
            'title'=>'TDD Test Issue',
            'description'=>'TDD Test issue description',
            'status_id'=>$status->id,
            'assigned_to'=>Auth::id(),
            'issue_type'=>$type->id,
            'priority_id'=>$priority->id,
            'percentage_done'=>10,
            'date_due'=>Carbon::today()->addDays(5)->toDateString(),
            'created_by'=>Auth::id(),
            'component_id'=>$component->id
        ]);
        return $issue = $this->post('/issue/store/',$data);
    }

    /**
     * Create issue schedule
     * @return mixed
     */
    public function createIssueSchedule()
    {
        $issue = Issue::where('title','TDD Test Issue')->first();
        $data = ([
            'issue_id'=>$issue->id,
            'date'=>Carbon::today()->toDateString(),
            'start_time'=>'10:00',
            'end_time'=>'11:00',
            'description'=>'TDD Issue schedule',
        ]);
         $schedule = $this->post('/issue/'.$issue->id.'/track',$data);
    }

    /**
     * Create project types
     */
    public function setupProjectTypes()
    {
        $projectTypes = [
            ['name' => 'software'],
            ['name' => 'design']
        ];
        foreach ($projectTypes as $projectType) {
            return ProjectType::create($projectType);
        }
    }

    public function setupProjectCategories()
    {
        $user = User::where('email', "dkinuthia@cytonn.com")->first();

        $projectCategories = [
            ['slug' => 'AUTOMATION OF INTERNAL PROCESSES TDD',
                'name' => 'Automation of Internal Processes TDD',
                'description' => 'A TDD project category',
                'created_by' => $user->id],
        ];
        foreach ($projectCategories as $projectCategory) {
            return $category = $this->post('/categories/save', $projectCategory);
        }
    }

    /**
     *Create project
     */
    public function createProject()
    {
        $this->setupProjectTypes();
        $this->setUpUserRequirements();
        $user = User::where('email', "dkinuthia@cytonn.com")->first();
        $user = $this->setUpUser();
        $projectType = ProjectType::where('name', 'software')->first();

        $this->setupProjectCategories();
        $category = ProjectCategory::where('name', 'Automation of Internal Processes TDD')->first();

        $data = ([
            'name' => 'TDD Tests Project',
            'project_type' => $projectType->id,
            'project_key' => 'TDD_Test',
            'project_description' => 'TDD Tests Project description',
            'project_url' => 'tdd.dev',
            'project_lead' => $user->id,
            'created_by' => $user->id,
            'project_access' => 'public',
            'category_id' => $category->id,
            'status' => 'In Progress',
            'priority' => 'high',
            'resources_required' => '1 Developer',
            'resources_assigned' => '1 Developer'
        ]);
        return $project = Project::create($data);
    }

    public function addProjectPhase()
    {
        $project = $this->createProject();
        $user = User::where('email', "dkinuthia@cytonn.com")->first();
        $data = ([
            'title' => 'TDD project phase',
            'start_date' => Carbon::now()->toDateString(),
            'end_date' => Carbon::now()->addDays(2)->toDateString(),
            'decisions' => 'TDD test project phase decisions'
        ]);

        return $phase = $this->post('/project/' . $project->id . '/phases', $data);

    }

    /**
     *Add a project phase activity
     */
    public function addPhaseActivity()
    {
        $this->addProjectPhase();
        $projectPhase = ProjectPhase::where('name','TDD project phase')->first();

        $user = User::where('email', "dkinuthia@cytonn.com")->first();
        $data = ([
            'project_phase'=>$projectPhase->id,
            'name'=>'TDD Test project phase activity',
            'start_date'=>Carbon::today()->toDateString(),
            'end_date'=>Carbon::today()->addDays(1)->toDateString(),
            'percent_complete'=>10,
            'comments'=>'TDD Test phase activity comment',
        ]);
        return $phaseActivity = $this->post('/phases/'.$projectPhase->id.'/activity',$data);
    }

    /**
     * USer Login
     */
    public function login()
    {
        Session::start();

//        $this->setUpUserRequirements();

        $user = $this->setUpUser();
        $this->actingAs($user);

        return $user;
    }
}