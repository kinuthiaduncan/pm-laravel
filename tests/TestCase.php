<?php

namespace Tests;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithoutMiddleware;

    public $baseUrl = 'http://pm.dev';
}
