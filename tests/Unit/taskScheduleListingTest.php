<?php

namespace Tests\Unit;


use PM\Models\Task;
use PM\Models\TaskTracking;
use Tests\TestCase;
use Tests\Base;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class taskScheduleListingTest extends TestCase
{
    use Base;
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTaskScheduleListing()
    {
        $this->login();

        $this->createTask();

        $task = Task::where('title',"TDD test task")->first();
        $this->createTaskSchedule($task->id);

        $schedule = $this->get("/tasks/task/$task->id/track");

        $this->see('03:00:00')->see('04:30:00');
    }
}
