<?php

namespace Tests\Unit;

use Carbon\Carbon;
use PM\Models\Task;
use PM\Models\TaskTracking;
use PM\Presenters\NumberOfHoursPresenter;
use Tests\Base;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class timeSpentOnTaskTest extends TestCase
{
    use Base;
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTimeSpentOnTask()
    {
        $this->login();
        $this->createTask();

        $task = Task::where('title',"TDD test task")->first();
        $this->createTaskSchedule($task->id);

        $schedule = TaskTracking::where('task_id',$task->id)->first();

        $timeSpent = NumberOfHoursPresenter::presentNumberOfHours($schedule->start_time,$schedule->end_time);
        $this->assertEquals(1.5, $timeSpent);
    }
}
