<?php

namespace Tests\Unit;

use Tests\Base;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class taskListingTest extends TestCase
{
    use DatabaseTransactions;
    use Base;

    /**
     * Listing a Task after creation
     *
     * @return void
     */
    public function testTaskListingTest()
    {
        $this->login();
       $task = $this->createTask();

        $response = $this->get('/tasks/my-tasks/'.$task->id);
        $response->assertResponseStatus(200);
        $response ->see('TDD test task');
        $response ->see('TDD task description');
        $response ->see('2017-06-22');
    }
}
