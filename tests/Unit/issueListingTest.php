<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use PM\Models\Issue;
use PM\Models\IssueTracking;
use PM\Presenters\NumberOfHoursPresenter;
use Tests\Base;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class issueListingTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;
    use Base;

    /**
     *Test creation and listing of issues
     */
    public function testIssueListing()
    {
        $this->login();
        $this->createProject();
        $issue = $this->createIssue();
        $this->seeInDatabase('issues',[ 'title'=>'TDD Test Issue','description'=>'TDD Test issue description']);
    }

    /**
     *Creation and listing of an issue schedule
     */
    public function testIssueScheduleListing()
    {
        $this->login();
        $this->createProject();
        $this->createIssue();
        $this->createIssueSchedule();
        $this->seeInDatabase('issue_trackings',['start_time'=>'10:00','description'=>'TDD Issue schedule']);

        $issue = Issue::where('title','TDD Test Issue')->first();
        $schedule = $this->get('/issue/'.$issue->id.'/track')
            ->see('TDD Issue schedule');
    }

    /**
     *Time spent on an issue test
     */
    public function testTimeSpentOnIssue()
    {
        $this->login();
        $this->createProject();
        $this->createIssue();
        $this->createIssueSchedule();
        $schedule = IssueTracking::first();
        $timeSpent = NumberOfHoursPresenter::presentNumberOfHours($schedule->start_time,$schedule->end_time);
        $this->assertEquals(1, $timeSpent);
    }
}
