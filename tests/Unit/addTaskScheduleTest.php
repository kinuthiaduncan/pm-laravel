<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use PM\Models\Task;
use PM\Models\TaskTracking;
use Tests\Base;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class addTaskScheduleTest extends TestCase
{
    use DatabaseTransactions;
    use Base;
    use WithoutMiddleware;

    /**
     * Add schedule to task.
     *
     * @return void
     */
    public function testAddTaskScheduleTest()
    {
        $this->login();
        $this->createTask();

        $task = Task::where('title',"TDD test task")->first();

        $this->createTaskSchedule($task->id);

        $this->visit("/tasks/task/{$task->id}/track")->see('03:00:00');
    }

}
