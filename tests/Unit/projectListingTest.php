<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use PM\Models\ProjectCategory;
use Tests\Base;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class projectListingTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;
    use Base;

    /**
     *Test creation and listing of a project categories
     */
    public function testProjectListingCategoriesTest()
    {
        $this->login();
        $this->addProjectCategories();
        $category = $this->get('/api/categories');
        $category->assertResponseStatus(200);
        $category->see('TDD Project Category')
                ->see('A TDD Test project category');
    }

    /**
     *Project creation and listing test
    use Base;

    /**
     * Creation and listing of projects test
     *
     * @return void
     */
    public function testProjectListingTest()
    {
        $this->login();

        $this->createProject();
        $this->seeInDatabase('projects',['project_key'=>'TDD']);

        $project = $this->get('/api/projects');
        $project->assertResponseStatus(200);
        $project->see('TDD Test Project')->see('tdd.com');
    }
}
