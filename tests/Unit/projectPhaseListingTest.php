<?php

namespace Tests\Unit;

use PM\Models\Project;
use PM\Models\ProjectPhase;
use Tests\Base;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class projectPhaseListingTest extends TestCase
{
    use Base;
    use DatabaseTransactions;

    /**
     *Creation and listing of project phases test
     */
    public function testListProjectPhase()
    {

        $this->login();
        $this->addProjectPhase();
        $this->seeInDatabase('project_phases',['name' => 'TDD project phase']);
        $project= Project::first();
       $phase = $this->get('/project/'.$project->id.'/phases')
           ->see('TDD project phase');
    }

}
