<?php

namespace Tests\Unit;

use PM\Models\Project;
use PM\Models\ProjectPhase;
use PM\Models\ProjectPhaseActivity;
use PM\Presenters\ProjectPhasePresenter;
use Tests\Base;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class phaseActivitiesListingTest extends TestCase
{
    use Base;
    use DatabaseTransactions;

    /**
     *Creation and listing of Project phase activities test
     */
    public function testPhaseActivitiesListing()
    {
        $this->login();
        $this->addPhaseActivity();
        $this->seeInDatabase('project_phase_activities',[ 'name'=>'TDD Test project phase activity']);
        $phase = ProjectPhase::first();
        $project = Project::first();
        $phaseActivity = $this->get('project/'.$project->id.'/phases/'.$phase->id)
           ->see('TDD Test project phase activity')
            ->see('TDD Test phase activity comment');
    }

    /**
     *Calculate days left until the project phase activity is due
     */
    public function testDaysLeftToDueTest()
    {
        $this->login();
        $this->addPhaseActivity();
        $activity = ProjectPhaseActivity::first();
        $daysLeft = ProjectPhasePresenter::timeLeft($activity->end_date);
        $this->assertEquals($daysLeft, 1);
    }

    /**
     *Overall project phase percentage completed
     */
    public function testPhasePercentDoneTest()
    {
        $this->login();
        $this->addPhaseActivity();
        $phase = ProjectPhase::first();
        $percent_done = ProjectPhasePresenter::phasePercentDone($phase->id);
        $this->assertEquals($percent_done, 10);
    }
}
